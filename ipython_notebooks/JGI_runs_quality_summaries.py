# -*- coding: utf-8 -*-
# <nbformat>3.0</nbformat>

# <codecell>

import pymysql
conn = pymysql.connect(host='redwood.jgi-psf.org',user='jazz',passwd='jazz234',db='rqc')

# <codecell>

cur = conn.cursor()

# <codecell>

cur.execute("SELECT * from seq_units limit 10")

# <codecell>

cur.execute("select run_id,seq_unit_name,run_date from seq_units where run_date >= '2012-09-25' AND run_date < '2012-11-26'")

for r in cur.fetchall():
    print r

# <codecell>


#cur.execute(""" select su.seq_unit_name, su.run_barcode, su.run_id, su.run_date,su.run_section,m2m.analysis_id,
#stats.stats_name,stats.stats_value
#from seq_units su, m2m_analysis_seq_unit m2m, rqc_analysis_stats stats
#where su.run_date >= '2012-01-01' and su.run_date < '2012-03-10' and su.seq_unit_name REGEXP '^.+[0-9]\.srf$'
#and m2m.seq_unit_name = su.seq_unit_name
#and m2m.analysis_id = stats.analysis_id """)

# <codecell>

import MySQLdb
import datetime
conn = MySQLdb.connect(host='redwood.jgi-psf.org',user='rqc_ro',passwd='rqcro',db='rqc')
cur = conn.cursor()

# <codecell>

def summarize_sequencing_by_runtype(results, instrument_type):
    
    if results.get(instrument_type,None) is None:
        print 'Instrument type : %s not present in the results' % instrument_type
        return (None,None)
    else:
        dates = []
        readcounts = []
        
        if instrument_type == 'HiSeq':
            #separate the control lane
            hiseq_control_lane_dates = []
            hiseq_control_lane_read_counts = []
        
        
        for (count,fastq) in enumerate(results[instrument_type].keys()):
            
            run_date = results[instrument_type][fastq]['run_date']
            if type(run_date) is not datetime.date:
                print ('arg must be a datetime.date, not a %s' % type(run_date))
                print instrument_type,fastq,run_date,results[instrument_type][fastq]['read count'],results[instrument_type][fastq]['library_name']
                print 'Skipping this row of data'
                continue

            try:
                lane_read_count = int(results[instrument_type][fastq]['read count'])
            except:
                print 'error %s ' % results[instrument_type][fastq]['read count']
                print instrument_type,fastq,run_date,results[instrument_type][fastq]['read count'],results[instrument_type][fastq]['library_name']
                print 'Skipping this row of data'
                continue
            
            if instrument_type == 'HiSeq' and (results[instrument_type][fastq]['library_name'] == 'PhiX' or results[instrument_type][fastq]['library_name'] == 'GAZP' ):
                #print instrument_type,fastq,run_date,results[instrument_type][fastq]['read count'],results[instrument_type][fastq]['library_name']
                hiseq_control_lane_dates.append(run_date)
                hiseq_control_lane_read_counts.append(lane_read_count)
                continue
                
            
            
            
            dates.append(run_date)
            readcounts.append(lane_read_count)
    print 'Processed %d fastq for instrument %s' % (count+1,instrument_type)
    
    if instrument_type == 'HiSeq':
        return (dates,readcounts,hiseq_control_lane_dates,hiseq_control_lane_read_counts)
    else:
        return (dates,readcounts)





user_start_date = "2012-07-01"
user_end_date = "2012-09-30"
fmt_start_date = datetime.datetime.strptime(user_start_date.replace("'",''),'%Y-%m-%d').date()
fmt_end_date = datetime.datetime.strptime(user_end_date.replace("'",''),'%Y-%m-%d').date()

print 'formatted_start_date : %s ' % fmt_start_date
print 'formatted_end_date : %s ' % fmt_end_date


sql_query = """
select DISTINCT 
sps.physical_run_unit_id,spr.physical_run_unit_id,spr.physical_run_id
,sps.dt_begin
,srun.physical_run_type,srun.platform_name,srun.instrument_type,srun.instrument_name,srun.read1_length,srun.read2_length
,ssu.library_name
,ssf.file_name
,spr.run_section
,ras.stats_name,ras.stats_value
from 
sdm.physical_run_unit_status_history sps
,sdm.physical_run_unit spr
,sdm.physical_run srun
,sdm.sdm_seq_unit ssu
,sdm.sdm_seq_unit_file ssf
,rqc.m2m_analysis_seq_unit rm2m
,rqc.rqc_analysis_stats ras
where sps.dt_begin >= '%s' and sps.dt_begin < '%s'
and sps.physical_run_unit_status_id = 3
and sps.physical_run_unit_id = spr.physical_run_unit_id
and spr.physical_run_id = srun.physical_run_id
and spr.physical_run_unit_id = ssu.physical_run_unit_id
and ssu.sdm_seq_unit_id = ssf.sdm_seq_unit_id
and ssf.file_type = 'FASTQ'
and ssf.file_name REGEXP '^.+[0-9]\.fastq.gz'
and REPLACE (ssf.file_name, '.fastq.gz','.srf') = rm2m.seq_unit_name
and rm2m.analysis_id = ras.analysis_id
and ras.stats_name = 'read count'
""" % (fmt_start_date,fmt_end_date+datetime.timedelta(days=1))

#debug
#print sql_query

cur.execute(sql_query)
results = {}

for (count_rows,row) in enumerate(cur.fetchall()):
    
    #first 3 cols returned by sql are for debugging purposes only
    debug_part = row[:3]
    data = row[3:]
    
    #the order of the following is based on sql query
    run_date = data[0]
    physical_run_type = data[1]
    platform = data[2]
    instrument_type = data[3]
    instrument_name = data[4]
    read1_length = data[5]
    read2_length = data[6]
    library_name = data[7]
    seq_unit = data[8]
    seq_unit_lane = data[9]
    fastq_rqc_stats_name = data[10]
    fastq_rqc_stats_value =  data[11]
    
    
    #print run_date,physical_run_type,platform,instrument_type,instrument_name,library_name,seq_unit
    
    #register new instrument type
    if results.get(instrument_type, None) is None:
        results[instrument_type] = {}
    
    #register new fastq
    if results[instrument_type].get(seq_unit, None) is None:
        results[instrument_type][seq_unit] = {}
        results[instrument_type][seq_unit]['run_date'] = run_date.date()
        results[instrument_type][seq_unit]['physical_run_type'] = physical_run_type
        results[instrument_type][seq_unit]['platform'] = platform
        results[instrument_type][seq_unit]['instrument_type'] = instrument_type
        results[instrument_type][seq_unit]['instrument_name'] = instrument_name
        results[instrument_type][seq_unit]['library_name'] = library_name
        results[instrument_type][seq_unit]['lane'] = seq_unit_lane
        results[instrument_type][seq_unit][fastq_rqc_stats_name] = fastq_rqc_stats_value
    else:
        results[instrument_type][seq_unit][fastq_rqc_stats_name] = fastq_rqc_stats_value
    

count_rows += 1
print 'processed %d rows from mysql results' % count_rows
(hiseq_dates,hiseq_lanes_read_counts,hiseq_control_lane_dates,hiseq_control_lane_read_counts)=summarize_sequencing_by_runtype(results,'HiSeq')
(miseq_dates,miseq_lanes_read_counts)=summarize_sequencing_by_runtype(results,'MiSeq')
(ga2seq_dates,ga2seq_lanes_read_counts)=summarize_sequencing_by_runtype(results,'GA2')
(ga2xseq_dates,ga2xseq_lanes_read_counts)=summarize_sequencing_by_runtype(results,'GAIIx')








    
    




    

# <codecell>

import matplotlib.pyplot as plt
from matplotlib.font_manager import FontProperties
fig = plt.figure(figsize=(14,12))
fig.suptitle('JGI - Sequencing Summary (read counts) \n Q: %s-%s -- %s-%s' % (fmt_start_date.strftime("%B"),fmt_start_date.year,fmt_end_date.strftime("%B"),fmt_end_date.year), fontsize=20)

#TBD
#determine the number of rows in plot, based on the number of plots



#hiseq
ax1 = plt.subplot2grid((3,4),(0,0),colspan=4)
plt.plot_date(hiseq_dates,hiseq_lanes_read_counts,color='#33A02C')
plt.title("HiSeq's Output")
plt.ylabel('#reads per lane(in 100 millions)')
plt.plot_date(hiseq_control_lane_dates,hiseq_control_lane_read_counts,color='red',label="PhIX-lane")
plt.plot
fontP = FontProperties()
fontP.set_size('small')
plt.legend(loc='upper left',prop=fontP)
ax2 = plt.subplot2grid((3,4),(2,0),colspan=2)
plt.hist(hiseq_lanes_read_counts,bins=50,color='#33A02C')
plt.hist(hiseq_control_lane_read_counts,bins=50,color='red',label="PhiX")
plt.xlabel('Hi-Seq read count histogram',fontsize=14)
plt.legend(loc='upper left')


#MiSeq
ax7 = plt.subplot2grid((3,4),(1,0),colspan=4,sharex=ax1)
plt.plot_date(x=miseq_dates,y=miseq_lanes_read_counts,color='#A6CEE3')
plt.title("MiSeq's Output")
plt.ylabel('#reads per lane(in 10 millions)')
plt.xlabel('Time Line',fontsize=12)
ax8 = plt.subplot2grid((3,4),(2,2),colspan=2)
plt.hist(miseq_lanes_read_counts,bins=50,color='#A6CEE3')
plt.xlabel('Mi-Seq read count histogram',fontsize=14)



#GAIIx
#ax3 = plt.subplot2grid((4,4),(1,0),colspan=3,sharex=ax1)
#plt.plot_date(x=ga2xseq_dates,y=ga2xseq_lanes_read_counts,color='#B2DF8A')
#plt.title("GAIIx's Output")
#ax4 = plt.subplot2grid((4,4),(1,3),colspan=1)
#plt.hist(ga2xseq_lanes_read_counts,bins=50,color='#B2DF8A')

'''
#GA2
ax5 = plt.subplot2grid((3,4),(1,0),colspan=4,sharex=ax1)
plt.plot_date(x=ga2seq_dates,y=ga2seq_lanes_read_counts,color='#1F78B4')
plt.title("GAII Output")
ax6 = plt.subplot2grid((3,4),(2,2),colspan=2)
plt.hist(ga2seq_lanes_read_counts,bins=50,color='#1F78B4')
'''


out_file_name = 'JGI_sequencing_summary_%s-%s__%s-%s.png' % (fmt_start_date.strftime("%B"),fmt_start_date.year,fmt_end_date.strftime("%B"),fmt_end_date.year)
plt.savefig(out_file_name)

plt.show()

# <codecell>

sql_query = """select ssu.sdm_seq_unit_id, ssf.sdm_seq_unit_id
#ssu.physical_run_unit_id,ssu.sdm_seq_unit_status_id
,ssf.file_name,ssu.parent_sdm_seq_unit_id
,ssu1.library_name as parent_library_name, ssf1.file_name as parent_lib_file_name
,ssu.library_name as lib_name, ssu.seq_barcode,ssu.is_multiplexed
#,ssu.physical_run_unit_id,spu.physical_run_unit_id
,srun.physical_run_type,srun.platform_name,srun.instrument_type,srun.instrument_name,srun.read1_length,srun.read2_length
,rm2m.analysis_id
,rstats.stats_name,rstats.stats_value

from 
sdm.sdm_seq_unit ssu

INNER JOIN sdm.sdm_seq_unit_file ssf
ON ssf.sdm_seq_unit_id = ssu.sdm_seq_unit_id

INNER JOIN sdm.physical_run_unit spu
ON ssu.physical_run_unit_id = spu.physical_run_unit_id

INNER JOIN sdm.physical_run srun
on spu.physical_run_id = srun.physical_run_id

LEFT JOIN rqc.m2m_analysis_seq_unit rm2m
on REPLACE (ssf.file_name, '.fastq.gz','.srf') = rm2m.seq_unit_name

LEFT JOIN rqc.rqc_analysis_stats rstats
on rm2m.analysis_id = rstats.analysis_id

LEFT JOIN sdm.sdm_seq_unit ssu1
on ssu.parent_sdm_seq_unit_id = ssu1.sdm_seq_unit_id

LEFT JOIN sdm.sdm_seq_unit_file ssf1
on ssu1.sdm_seq_unit_id = ssf1.sdm_seq_unit_id

and ssf.file_type = 'FASTQ'
and ssf1.file_type = 'FASTQ'

where ssu.parent_sdm_seq_unit_id in ( 
					select sdm_seq_unit_id from sdm.sdm_seq_unit 
					where  library_name  IN ("HCOX")
				    )

or ssu.library_name in ("HCOX")


"""

cur.execute(sql_query)

data = {}

for count,row in enumerate(cur.fetchall()):
    
    
    if count == 200:
        pass
    
    
    sdm_seq_unit_id = row[0]
    #ssf_sdm_seq_unit_id = row[1].strip() #skipping this, mainly for debugging purpose
    seq_unit_name = row[2].strip()
    parent_sdm_seq_unit_id = row[3]
    parent_lib_name = row[4]
    parent_lib_seq_unit_name = row[5]
    lib_name = row[6].strip()
    barcode = row[7]
    is_multiplexed = row[8]
    physical_run_type = row[9].strip()
    platform_name = row[10].strip()
    instrument_type = row[11].strip()
    instrument_name = row[12].strip()
    read_1_length = row[13]
    read_2_length = row[14]
    rqc_analysis_id = row[15]
    rqc_stats_name = row[16]
    rqc_stats_value = row[17]
    
    
    if seq_unit_name.find('FAIL') != -1:
        continue
    
    insert_record_at = None
    child_lib_name = None
    child_seq_unit_name = None
    
    #not a multiplexed library
    if parent_lib_name is None and lib_name is not None:
        parent_lib_name = lib_name
        parent_lib_seq_unit_name = seq_unit_name
    
    #multiplexed library
    elif parent_lib_name is not None and lib_name is not None:
        child_lib_name = lib_name
        child_seq_unit_name = seq_unit_name
        
    #by now we have the relation, parent and child lib and fastq name
    
    
    
    if parent_lib_name is None:  #sanity check
        #most likely probem
        print 'Parent lib None'
        
    else:
        
        #new parent library
        if data.get(parent_lib_name,None) is None:
            data[parent_lib_name] = {}
            data[parent_lib_name]['libs'] = {}
            data[parent_lib_name]['seq_units'] = {}
            data[parent_lib_name]['seq_units'][parent_lib_seq_unit_name] = {}
            insert_record_at =  data[parent_lib_name]['seq_units'][parent_lib_seq_unit_name]
        elif data[parent_lib_name]['seq_units'].get(parent_lib_seq_unit_name,None) is None:
            data[parent_lib_name]['seq_units'][parent_lib_seq_unit_name] = {}
            insert_record_at =  data[parent_lib_name]['seq_units'][parent_lib_seq_unit_name]
        else:
            insert_record_at =  data[parent_lib_name]['seq_units'][parent_lib_seq_unit_name]
        
        #insert the child under appropriate parent
        if child_lib_name is not None:
            if data[parent_lib_name]['libs'].get(child_lib_name,None) is None:
                data[parent_lib_name]['libs'][child_lib_name] = {}
                data[parent_lib_name]['libs'][child_lib_name]['seq_units'] = {}
                data[parent_lib_name]['libs'][child_lib_name]['seq_units'][child_seq_unit_name] = {}
                insert_record_at =  data[parent_lib_name]['libs'][child_lib_name]['seq_units'][child_seq_unit_name]
            elif data[parent_lib_name]['libs'][child_lib_name]['seq_units'].get(child_seq_unit_name,None) is None:
                data[parent_lib_name]['libs'][child_lib_name]['seq_units'][child_seq_unit_name] = {}
                insert_record_at =  data[parent_lib_name]['libs'][child_lib_name]['seq_units'][child_seq_unit_name]
            else:
                insert_record_at =  data[parent_lib_name]['libs'][child_lib_name]['seq_units'][child_seq_unit_name]
        
        
        if insert_record_at is None:
            print 'Insert Record Error for row'
            print row
        
        else:
            
            if insert_record_at.get('rqc_stats',None) is None:
                insert_record_at['rqc_stats'] = {}
            
            if rqc_stats_name is None:
                continue
            
            insert_record_at['file_name'] = seq_unit_name 
            insert_record_at['lib_name'] = lib_name
            insert_record_at['barcode'] = barcode
            insert_record_at['seq_unit_name'] = seq_unit_name
            insert_record_at['is_multiplexed'] = is_multiplexed
            insert_record_at['physical_run_type'] = physical_run_type
            insert_record_at['platform_name'] = platform_name
            insert_record_at['instrument_type'] = instrument_type
            insert_record_at['read_1_length'] = read_1_length
            insert_record_at['read_2_length'] = read_2_length
            insert_record_at['parent_lib_seq_unit_name'] = parent_lib_seq_unit_name
            insert_record_at['rqc_analysis_id'] = rqc_analysis_id
            insert_record_at['rqc_stats'][rqc_stats_name]=rqc_stats_value
        
        
        
    
    
    
      

# <codecell>

import pprint



def summarize(data):
    
    
    print '%s \t %s \t %s \t %s \t %s \t %s \t %s \t %s' % ('parent_lib','child_lib','#read','#cleaned_read','#rawGB','#cleaned_GB','%artifacts','%rRNA')
                                                             
    #parent library summary
    for parent_lib in data:
        parent_lib_total_reads = 0
        for seq_unit_name in data[parent_lib]['seq_units']:
            
            parent_seq_unit_stats = data[parent_lib]['seq_units'][seq_unit_name]
            #pprint.pprint(parent_seq_unit_stats)
            parent_lib_read_count = int(parent_seq_unit_stats['rqc_stats']['read count'])
            parent_lib_read_count = parent_seq_unit_stats['rqc_stats']['read count']
            parent_lib_read_1_length = int(parent_seq_unit_stats['read_1_length'])
            parent_lib_read_2_length = int(parent_seq_unit_stats['read_2_length'])
            parent_lib_read1_q20_len = parent_seq_unit_stats['rqc_stats']['read q20 read1']
            parent_lib_read2_q20_len = parent_seq_unit_stats['rqc_stats']['read q20 read2']
            parent_lib_percent_rRNA = parent_seq_unit_stats['rqc_stats']['illumina read percent contamination rrna']
            parent_lib_percent_contamination = float(parent_seq_unit_stats['rqc_stats']['illumina read percent contamination artifact']) + float(parent_seq_unit_stats['rqc_stats']['illumina read percent contamination contaminants'])
            parent_lib_read_count_artifacts_filtered =  float(round(float(parent_lib_read_count)*( (int(100)-parent_lib_percent_contamination)/ 100)) )
                
            
            #parent_lib_raw_based_in_gb =   
            parent_lib_raw_based_in_gb =  round ( ( ( float(parent_lib_read_count) * float(parent_lib_read_1_length) ) / 1000000000),2)
            parent_lib_cleaned_bases_in_gb = round( ( parent_lib_read_count_artifacts_filtered * float(parent_lib_read_1_length) / 1000000000),2)
                
                
            print '%s \t %s \t %s \t %s \t %s \t %s \t %s' % (parent_lib,'None',
                                                               round(float(int(parent_lib_read_count)/1000000.0),2),
                                                               round(float(int(parent_lib_read_count_artifacts_filtered)/1000000.0),2),
                                                               parent_lib_raw_based_in_gb,
                                                               parent_lib_cleaned_bases_in_gb,
                                                               parent_lib_percent_contamination)
                
            
            #print parent_lib,'None',seq_unit_name,parent_lib_read_count,parent_lib_read1_q20_len,parent_lib_read2_q20_len
            #break
        
        for child_lib in data[parent_lib]['libs']:
            total_per_child_lib = 0
            
            child_lib_read_count = 0
            child_lib_read_count_artifact_filtered = 0
            child_lib_raw_bases_in_gb = 0
            child_lib_cleaned_bases_in_gb = 0
            child_lib_percent_contamination = []
            child_lib_percent_rRNA = []
            
            for child_seq_unit in data[parent_lib]['libs'][child_lib]['seq_units']:
                child_seq_unit_stats = data[parent_lib]['libs'][child_lib]['seq_units'][child_seq_unit]
                
                child_lib_su_read_count = int(child_seq_unit_stats['rqc_stats']['read count'])
                child_lib_su_read1_q20_len = child_seq_unit_stats['rqc_stats']['read q20 read1']
                child_lib_su_read2_q20_len = child_seq_unit_stats['rqc_stats']['read q20 read2']
                child_lib_su_read_1_length = int(child_seq_unit_stats['read_1_length'])
                child_lib_su_read_2_length = int(child_seq_unit_stats['read_2_length'])
                child_lib_su_percent_rRNA = float(child_seq_unit_stats['rqc_stats']['illumina read percent contamination rrna'])
                child_lib_su_percent_contamination = round( float(child_seq_unit_stats['rqc_stats']['illumina read percent contamination artifact']) + float(child_seq_unit_stats['rqc_stats']['illumina read percent contamination contaminants']),2)
                child_lib_su_read_count_artifacts_filtered =  int(round(float(child_lib_su_read_count)*( (int(100)-child_lib_su_percent_contamination)/ 100)) )
                
                child_lib_su_raw_based_in_gb = round( ( child_lib_su_read_count * float(child_lib_su_read_1_length) / 1000000000),2)
                child_lib_su_cleaned_bases_in_gb = round( ( child_lib_su_read_count_artifacts_filtered * float(child_lib_su_read_1_length) / 1000000000),2)
                
                
                child_lib_read_count += child_lib_su_read_count
                child_lib_read_count_artifact_filtered += child_lib_su_read_count_artifacts_filtered
                child_lib_raw_bases_in_gb += child_lib_su_raw_based_in_gb
                child_lib_cleaned_bases_in_gb += child_lib_su_cleaned_bases_in_gb
                child_lib_percent_contamination.append(child_lib_su_percent_contamination)
                child_lib_percent_rRNA.append(child_lib_su_percent_rRNA)
                
                '''
                print '%s \t %s \t %s \t %s \t %s \t %s \t %s \t %s \t %s' % (parent_lib,child_lib,child_seq_unit,
                                                                  round((child_lib_su_read_count/1000000.0),2),round((child_lib_su_read_count_artifacts_filtered/1000000.0),2),
                                                                 child_lib_su_raw_based_in_gb,child_lib_su_cleaned_bases_in_gb,
                                                                  child_lib_su_percent_contamination,child_lib_su_percent_rRNA)
                '''
    
            print '%s \t %s \t %s \t %s \t %s \t %s \t %s \t %s' % (parent_lib,child_lib,
                                                                        round((child_lib_read_count/1000000.0),2),
                                                                        round((child_lib_read_count_artifact_filtered/1000000.0),2),
                                                                        child_lib_raw_bases_in_gb,child_lib_cleaned_bases_in_gb,
                                                                        round(sum(child_lib_percent_contamination)/float(len(child_lib_percent_contamination)),2),
                                                                        round(sum(child_lib_percent_rRNA)/len(child_lib_percent_rRNA),2)
                                                                ) 
        
summarize(data)
#pprint.pprint(data)

# <codecell>

import math
print (round(3.349384934,2))

# <codecell>

'''
6104.3.36427.fastq.gz {'platform_name': 'Illumina', 'instrument_type': 'HiSeq', 'file_name': '6104.3.36427.fastq.gz', 
'barcode': None, 'is_multiplexed': 1, 'physical_run_type': 'Paired-End Indexed', 'read_1_length': 150L, 
'seq_unit_name': '6104.3.36427.fastq.gz', 

        'rqc_stats': 
                    {'read bwa aligned duplicate percent': '23.3', 'demultiplex stats plot': 
                     '6104.3.36427.index_sequence_detection.png', 'base Q5': '91', 'read Q25': '89.65', 
                     'read tax species of refseq.microbial': '137', 'read Q20': '94.70', 'base Q20': '88', 'base Q25': '85', 
                       'read GC std': '9.40', 'illumina read percent contamination phix': '0', 'read N frequence': '0.24', 
                    'read qual pos plot merged': '6104.3.36427.s0.01.qrpt.png', 
                    'read tophit file of nt': 'megablast.reads.fa.v.nt.FmLD2a10p90E30JFfTITW45.parsed.tophit', 
                    'read screend': '2382583', 'read artifact percent': '0.0', 'illumina read percent contamination artifact': '3.00606', 
                    'read Q10': '98.62', 'read q20 read2': '137', 'read level': 'completed', 'read artifact': '0', 
                    'read base quality stats plot': '6104.3.36427.s0.01.fastq.base_qual.stats.png', 
                    'read 21mer window sliding plot': '6104.3.36427.s0.01.21mer.qual.png', 'read base count': '35743708800', 
                    'base C15': '317828752', 'base Q30': '78', 'base C10': '319645201', 'read bwa aligned duplicate': '555945', 
                    'read qhist plot': '6104.3.36427.s0.01.fastq.qhist.png', 'base Q15': '90', 'read count': '238291392', 
                    'read 20mer uniqueness text': '6104.3.36427.merSampler.m20.e25000', 'read matching hits of nt': '728799', 
                    'illumina read percent contamination mitochondrion': '1.66504', 'read qual pos qrpt 1': '6104.3.36427.s0.01.r1.qrpt', 
                    'read qual pos qrpt 2': '6104.3.36427.s0.01.r2.qrpt', 'base C25': '302227827', 
                    'read base percentage plot 1': '6104.3.36427.s0.01.r1.fastq.base.stats.Npercent.png', 
                    'read base percentage plot 2': '6104.3.36427.s0.01.r2.fastq.base.stats.Npercent.png', 
                    'illumina read percent contamination contaminants': '1.27311', 'read GC text hist': '6104.3.36427.s0.01.gc.hist',
                    'fake read qual pos plot': '6104.3.36427.s0.01.index.qrpt.png', 
                    'read q20 read1': '134', 
                    'read taxlist file of nt': 'megablast.reads.fa.v.nt.FmLD2a10p90E30JFfTITW45.parsed.taxlist', 
                    'read length 2': '150', 'Q30 bases Q score mean': '36.2917', 'read top hits of nt': '7604', 
                    'read level MEGAN: Taxa Distribution of contigs vs. NT': '6104.3.36427.taxa.jpg', 
                    'read 20mer uniqueness plot': '6104.3.36427.merSampler.m20.e25000.png', 
                    'demultiplex stats': '6104.3.36427.demultiplex_stats', 'read GC mean': '47.99', 
                    'read matching hits of refseq.microbial': '12590', 'base C30': '278972589', 
                    'read 21mer window sliding text data': '6104.3.36427.s0.01.21mer.qual', 
                    'demultiplex filtered stats': '6104.3.36427.filtered_demultiplex_stats', 
                    'read parsed file of nt': 'megablast.reads.fa.v.nt.FmLD2a10p90E30JFfTITW45.parsed', 
                    'read taxlist file of refseq.microbial': 'megablast.reads.fa.v.refseq.microbial.FmLD2a10p90E30JFfTITW45.parsed.taxlist', 
                    'base Q10': '90', 'read length 1': '150', 'read base count plot 2': '6104.3.36427.s0.01.r2.fastq.base.stats.png', 
                    'read tax species of nt': '308', 'read base quality stats': '6104.3.36427.s0.01.fastq.base_qual.stats', 
                    'read tophit file of refseq.microbial': 'megablast.reads.fa.v.refseq.microbial.FmLD2a10p90E30JFfTITW45.parsed.tophit', 
                    'illumina read percent contamination fosmid': '0', 'illumina read percent contamination rrna': '19.3655', 
                    'read level MEGAN: Phylogeny of contigs vs NT filtered by class': '6104.3.36427.phylo2.jpg', 
                    'read 20mer sample size': '238275000', 'overall bases Q score mean': '31.3062', 
                    'read level MEGAN: Phylogeny of contigs vs NT': '6104.3.36427.phylo.jpg', 
                    'read base count plot 1': '6104.3.36427.s0.01.r1.fastq.base.stats.png', 
                    'illumina read sciclone DNA count file': '6104.3.36427.s0.01.fastq.trim37.GC.standards.well.fa.aln.sam.count.txt', 
                    'read tagdust coverage cutoff': '100.0', 'Q30 bases Q score std': '3.01621', 
                    'illumina read percent contamination plastid': '1.64137', 
                    'read qhist text': '6104.3.36427.s0.01.fastq.qhist', 
                    'read parsed file of refseq.microbial': 'megablast.reads.fa.v.refseq.microbial.FmLD2a10p90E30JFfTITW45.parsed', 
                    'base C20': '310938645', 'read base count text 1': '6104.3.36427.s0.01.r1.fastq.base.stats',
                     'read base count text 2': '6104.3.36427.s0.01.r2.fastq.base.stats', 'read Q5': '99.29', 'base C5': '323083905', 
                    'read 20mer percentage random mers': '44.46', 'Overall bases Q score std': '11.1185', 
                    'read Q30': '75.78', 'read Q15': '97.32', 'read 20mer percentage starting mers': '32.89', 
                    'read top hits of refseq.microbial': '1882', 'reads number': '49868', 'read GC plot': '6104.3.36427.s0.01.gc.hist.png', 
                    'read bwa aligned': '2382583', 'illumina read percent contamination ecoli combined': '0.0771012'}, 
                    'lib_name': 'BTYS', 'rqc_analysis_id': 67175L, 'read_2_length': 150L}

# <codecell>


# <codecell>


