#!/bin/bash

echo "All command line in the script"
echo "$*"
echo "$@"

echo "Number of positional characters in the script"
echo "$#"

echo "Exit status $?"

echo "hyphen expansion $-"

echo "Process ID $$"

echo "Process ID of most recent execution $!"

echo "Name of the shell script $0"

echo "Underscore variable : $_"