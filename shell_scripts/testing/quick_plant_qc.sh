#!/bin/bash


fastq=$1
prefix=`echo $fastq | sed 's|.fastq.gz||'`
fastq_subsample=$prefix'_10percent.fastq'


# subsample fastq and trim if read quality is poor (uses less mem)
echo "zcat $fastq | fastqSmpl -r 0.10 > $fastq_subsample";
zcat $fastq | fastqSmpl -r 0.10 > $fastq_subsample

# detect mito
echo " Now Running duk -k 25 /home/jhan/QAQC/databases/refseq.mitochondrion $fastq_subsample > $fastq_subsample.duk.out ";

duk -k 25 /home/jhan/QAQC/databases/refseq.mitochondrion $fastq_subsample > $fastq_subsample.duk.out.mitchondrion;

# detect chloro
echo "Now Running duk -k 25 /home/jhan/QAQC/databases/refseq.plastid $fastq_subsample > $fastq_subsample.duk.out ;"
duk -k 25 /home/jhan/QAQC/databases/refseq.plastid $fastq_subsample > $fastq_subsample.duk.out.plastid ;



# do std assembly qc
echo " Running velvetqc.pl -k 63 $fastq_subsample;"
velvetqc.pl -k 63 $fastq_subsample;

# delete fastq
#rm IAIX.1770.7.1609.100kreads.fastq ;