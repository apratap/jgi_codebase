#!/bin/sh

fastq_file=$1;
mosaik_reference=$2;

date
echo "===================================";
###echo "Running Mosaik Mapper to estimate the uniq pairs";
#cat $subsampled_fastq | ~/bin/perl_scripts/edit_fastq.pl -sfq 1 

###building a binary set of reads
mosaikBuild_reads=$fastq_file'_mosaikReads.dat'

command1='MosaikBuild -q '$fastq_file' -out '$mosaikBuild_reads' -st illumina'
echo "Running 1. $command1";
echo "";
`$command1`



###Aligning reads
aligned_file=$fastq_file'_aligned.dat'
command2='MosaikAligner -in '$mosaikBuild_reads' -out '$aligned_file' -ia '$mosaik_reference' -hs 15 -mm 3 -mhp 100 -act 20 -m all -p 5'
echo "Running 2. $command2 ";
echo "";
`$command2`;

####
sorted_file=$fastq_file'_sorted.dat'
command3='MosaikSort -in '$aligned_file' -out '$sorted_file
echo "Running 3. $command3 ";
echo "";
`$command3`;


###
sam_file=$fastq_file'_aligned.sam'
command4='MosaikText -in '$sorted_file' -sam '$sam_file
echo "Running 4. $command4";
echo "";
`$command4`;


bam_file=$fastq_file'_aligned.bam'
command5='MosaikText -in '$sorted_file' -bam '$bam_file
echo "Running 5. $command5";
echo""
`$command5`;