#!/bin/bash



fastq_file=$1;
mosaik_reference=$2
linker=$3;
min_length=$4

#fastq_file_linker_removed=`echo $fastq_file| sed 's|.fastq.gz||'`_"$linker"removed.fastq;

#zcat $fastq_file | clippe -j $linker -m $min_length > $fastq_file_linker_removed;


###submit the job for velvet assembly
#date
cp ~/dev/velvet_wrapper.sh .
~/dev/eclipse_workspace/perl_scripts/replaceall.pl fastq_file $fastq_file velvet_wrapper.sh
source /opt/sge/theia/common/settings.sh
#qsub -cwd -b yes -v PATH -now no -j yes -m as -M apratap@lbl.gov -w e -l long.c,ram.c=45G './velvet_wrapper.sh'



########
#Mapping
########


##sub sampling the reads using 5% for mapping
subsampled_fastq=$fastq_file'_5percent.fastq';
cat $fastq_file | fastqSmpl > $subsampled_fastq


date
echo "===================================";
echo "Running Mosaik Mapper to estimate the uniq pairs";
cat $subsampled_fastq | ~/bin/perl_scripts/edit_fastq.pl -sfq 1 

###building a binary set of reads
mosaikBuild_reads=$fastq_file'_mosaikReads.dat'
echo "Running ~/bin/MosaikBuild -q read_1.fastq -q2 read_2.fastq -out $mosaikBuild_reads -st illumina";
~/bin/MosaikBuild -q read_1.fastq -q2 read_2.fastq -out $mosaikBuild_reads -st illumina;


###Aligning reads
aligned_file=$subsampled_fastq'_aligned.dat'
echo "Running ~/bin/MosaikAligner -in $mosaikBuild_reads -out $aligned_file -ia $mosaik_reference -hs 15 -mm 4 -mhp 100 -act 15 -m unique -p 5";
~/bin/MosaikAligner -in $mosaikBuild_reads -out $aligned_file -ia $mosaik_reference -hs 15 -mm 4 -mhp 100 -act 15 -m all -p 3


####
sorted_file=$subsampled_fastq'_sorted.dat'
echo "Running ~/bin/MosaikSort -in $aligned_file -out $sorted_file ";
~/bin/MosaikSort -in $aligned_file -out $sorted_file


###
sam_file=$subsampled_fastq'_aligned.sam'
echo "Running ~/bin/MosaikText -in $sorted_file -sam $sam_file"
~/bin/MosaikText -in $sorted_file -sam $sam_file


bam_file=$subsampled_fastq'_aligned.bam'
echo "Running ~/bin/MosaikText -in $sorted_file -bam $bam_file"
~/bin/MosaikText -in $sorted_file -bam $bam_file










