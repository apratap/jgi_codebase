#!/bin/bash

CWD='/house/homedirs/a/apratap/playground/single_Cell/Ecoli_HiSeq_Data_My_playarea'
reference='/house/homedirs/a/apratap/playground/single_Cell/Ecoli_HiSeq_Data_My_playarea/reference/Ecoli_DH10B.fasta'

for file in `find . -name "*.1.fq" `
do

dir=`dirname $file`


index=`basename $file| sed s'|.1.fq||'`
echo "Processing $dir / $index"


read_1=$index'.1.fq'
read_2=$index'.2.fq'
out_sam=$index'.sam'

echo $read_1
echo $read_2


~/dev/eclipse_workspace/perl_scripts/runBWA.pl -r1 $read_1 -r2 $read_2 -nt 16 -os $out_sam -ref_dbase $reference

done
