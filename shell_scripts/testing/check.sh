#!/bin/bash
source ~apratap/dev/shell_scripts/bash_auto_called_scripts/qc_functions.sh
source ~apratap/dev/shell_scripts/bash_auto_called_scripts/run_softwares.sh
source ~apratap/dev/shell_scripts/bash_auto_called_scripts/bam_utils.sh
source ~apratap/dev/shell_scripts/bash_auto_called_scripts/uge_functions.sh
source ~apratap/dev/shell_scripts/bash_auto_called_scripts/shell_utils.sh

printf "Running check with arguments $*"
printf ""

check  $*;
