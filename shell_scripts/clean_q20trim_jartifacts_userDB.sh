#!/usr/bin/env bash


##script to clean fastq of JGI artifacts
input_fastq=$1

##bowtie index
bowtie2_index=$2

###path to databases
artifacts_fastq="/house/groupdirs/QAQC/databases/Artifacts.fa"

n_slots=4

fastq_prefix=$(echo $input_fastq | sed -e 's|.fastq.gz||')'_qtrim_jartifacts'
duk_log_file=${fastq_prefix}.duk.log

##quality trim fastq / remove the Illumina artifacts/JGI contaminants and split the read in to read 1 and read 2
command1="zcat $input_fastq | /house/groupdirs/QAQC/scripts/fastqTrimmer -paired | duk -k 18 /house/groupdirs/QAQC/databases/Artifacts.fa -n - -o ${duk_log_file} | ~apratap/bin/perl_scripts/edit_fastq.pl -sfq 1 -p ${fastq_prefix}"

##formulating the files names
fai_file=${bowtie2_index}'.fai'
read_1_fastq=$fastq_prefix'_read_1.fastq'
read_2_fastq=$fastq_prefix'_read_2.fastq'
final_bam_file=$fastq_prefix'.bam'
bam_stats=${final_bam_file}'.alignstats'
final_clean_unmapped_reads=$fastq_prefix'_unmapped.fastq'


#mapping
command2="memtime bowtie2 -x ${bowtie2_index} --local -p ${n_slots} --local -1 $read_1_fastq -2 $read_2_fastq | samtools view -bS -t $fai_file - -o ${final_bam_file}"


#final cleaned unmapped reads
command3="samtools view -f 4  ${final_bam_file} | ~apratap/bin/perl_scripts/edit_sam.pl -fq 1 > $final_clean_unmapped_reads"


#bamstats
command4="samtools flagstat ${final_bam_file} > ${bam_stats}"

#data cleanup
command5="rm -rf ${sam_file} ${read_1_fastq} ${read_2_fastq}"



printf "1. running $command1 \n\n"
eval $command1

printf "2. running $command2 \n\n "
eval $command2

printf "3. running $command3 \n\n "
eval $command3

printf "4 .running $command4 \n\n "
eval $command4

printf "5 running $command5 \n\n "
eval $command5
