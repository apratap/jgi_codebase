#!/bin/bash

##script to clean standard small RNA adaptors

#input fastq
input_fastq=${1:?'Need a fastq file'}

#fastq prefix
fastq_prefix=$(echo $input_fastq | sed -e 's|.fastq.gz||')_jcutadapt


base_script_exec1='/global/homes/a/apratap/softwares/cutadapt/cutadapt-1.2.1/bin/cutadapt'
options='-q 20 -e 0.20 -n 4 -O 14 --match-read-wildcards -m 8 --format fastq'
#first -a is 3' adapter, second -g rev complement of first -a for read 2
adaptors='-a TGGAATTCTCGGGTGCCAAGG -g GTTCAGAGTTCTACAGTCCGACGATC -a GATCGTCGGACTGTAGAACTCTGAAC -g CCTTGGCACCCGAGAATTCCA'

command1="${base_script_exec1} ${options} ${adaptors} -o ${fastq_prefix}.fastq.gz  ${input_fastq} 2>&1 | tee ${fastq_prefix}.cutadapt.log "
echo "Executing $command1"
eval $command1



#step 2
base_script_exec2='/global/u1/a/apratap/dev/eclipse_workspace/perl_scripts/edit_fastq.pl'
command2=" zcat ${fastq_prefix}.fastq.gz | ${base_script_exec2} -cfq 1 -prefix ${fastq_prefix}"
echo "Executing $command2"
eval $command2

echo "----Done------"
