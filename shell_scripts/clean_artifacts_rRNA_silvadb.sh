#!/bin/bash


##script to clean fastq of JGI artifacts, rRNA, Illumina Artifacts


input_fastq=$1

###path to databases
rna_fai_file="/house/groupdirs/QAQC/databases/silva/2012.05.silva/bwa_index/combined_SSU_LSU_108_Ref.fasta.fai"
artifacts_fastq="/house/groupdirs/QAQC/databases/Artifacts.fa"

n_slots=6

fastq_prefix=$(echo $input_fastq | sed -e 's|.fastq.gz||')'_qtrim_jartifacts'
duk_log_file=${fastq_prefix}.duk.log
trim_fastq=$fastq_prefix'_qtrim.fastq'
trim_fastq_jartifacts=${fastq_prefix}.fastq
trim_fastq_jartifacts_read1=${fastq_prefix}'_read_1.fastq'
trim_fastq_jartifacts_read2=${fastq_prefix}'_read_2.fastq'
sam_file=${fastq_prefix}'.sam'
final_bam_file=${fastq_prefix}'_map_rRNA.bam'
final_clean_reads=$fastq_prefix'.rRNA_cleaned.fastq'

##quality trim fastq / remove the Illumina artifacts/JGI contaminants and split the read in to read 1 and read 2
#command1="zcat $input_fastq | /house/groupdirs/QAQC/scripts/fastqTrimmer -paired > $trim_fastq"

command1="zcat $input_fastq | /house/groupdirs/QAQC/scripts/fastqTrimmer -paired | duk -k 20 /house/groupdirs/QAQC/databases/Artifacts.fa -n - -o ${duk_log_file} | /house/homedirs/a/apratap/dev/eclipse_workspace/perl_scripts/edit_fastq.pl -sfq 1 -p ${fastq_prefix}"


#split the reads into read1 and read2
#command2="cat $trim_fastq | /house/homedirs/a/apratap/dev/eclipse_workspace/perl_scripts/edit_fastq.pl -sfq 1 -p ${prefix}"


##mapping
command3="memtime bowtie2 -x /house/groupdirs/QAQC/databases/silva/2012.05.silva/bowtie2_index/combined_SSU_LSU_108_Ref.fasta --phred64 -S $sam_file --local -p ${n_slots} -L 30 -1 $trim_fastq_jartifacts_read1  -2 $trim_fastq_jartifacts_read2"

##sam to bam
command4="samtools view -bS -t $rna_fai_file $sam_file -o $final_bam_file"


#final cleaned reads
command5="samtools view -f 4  $final_bam_file | /house/homedirs/a/apratap/dev/eclipse_workspace/perl_scripts/edit_sam.pl -fq 1 > $final_clean_reads"


#data cleanup
command6="rm -rf $sam_file $trim_fastq_jartifacts_read1 $trim_fastq_jartifacts_read2 "


printf "1. running $command1 \n\n"
eval $command1

#printf "2. running $command2 \n\n "
#eval $command2

printf "3. running $command3 \n\n "
eval $command3

printf "4 .running $command4 \n\n " 
eval $command4

printf "5 running $command5 \n\n "
eval $command5

printf "6 running $command6 \n\n "
eval $command6

