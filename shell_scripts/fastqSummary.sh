#!/bin/bash

fastq_file=$1

if [ -e $fastq_file ]
then
    
    num_read1=`egrep -c -e '^@.+?\/1.*$' $fastq_file`;
    num_read2=`egrep -c -e '^@.+?\/2.*$' $fastq_file`;
    
fi

echo "#read 1 $num_read1";
echo "#read 2 $num_read2";
