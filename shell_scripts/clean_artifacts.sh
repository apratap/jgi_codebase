#!/bin/bash


##script to clean fastq of JGI artifacts
input_fastq=${1:?'Need a fastq file'}


###path to databases
screen_dbase=${2:-/global/u1/a/apratap/databases/Ill_Artifacts_JGIContam_ERCC.fa};

#kmer size
kmer=${3:-10}

fastq_prefix=$(echo $input_fastq | sed -e 's|.fastq.gz||')_jA
duk_log_file=${fastq_prefix}.duk.log


command1="zcat $input_fastq | duk -k ${kmer} ${screen_dbase} -n - -o ${duk_log_file} | ~apratap/dev/eclipse_workspace/perl_scripts/edit_fastq.pl -cfq 1 -p ${fastq_prefix}"

printf "1. running $command1 \n\n"
eval $command1


