zcat *gz | /home/jhan/scripts/fq2fa.pl | 
/home/jhan/scripts/merSampler.pl -f - -m 29 -e 25000 > all_lanes_merSampler.out

echo "set terminal png; set output 'all_lanes_merSampler.png'; plot 'all_lanes_merSampler.out' using 1:3 w lp t 'start' , 
'all_lanes_merSampler.out' using 1:5 w lp t 'random' ; "|gnuplot;
