#!/bin/bash


##script to clean fastq of JGI artifacts
input_fastq=${1:?'Need a fastq file'}


###path to databases
screen_dbase=${2:-/global/u1/a/apratap/databases/Ill_Artifacts_JGIContam_ERCC.fa};

#fastq type(new old)
#fastq_type=${2:?'Specify Fastq type(new|old)'}


##qtrim score
avg_Q_score=${3:-20};




if [[ ${input_fastq}  == *fastq.gz ]]
then
    fastq_prefix=`echo $input_fastq | sed -e 's|.fastq.gz||'`
    fastq_prefix=${fastq_prefix}_q${avg_Q_score}trim_jartifacts
    display_command="zcat"
    

elif [[ ${input_fastq}  == *fastq ]]
then
    fastq_prefix=`echo $input_fastq | sed -e 's|.fastq||'`
    fastq_prefix=${fastq_prefix}_q${avg_Q_score}trim_jartifacts
    display_command="cat"

fi



duk_log_file=${fastq_prefix}.duk.log


command1="$display_command $input_fastq | /house/groupdirs/QAQC/scripts/fastqTrimmer -avgQ ${avg_Q_score} | duk -k 20 ${screen_dbase} -n - -o ${duk_log_file} | ~apratap/dev/eclipse_workspace/perl_scripts/edit_fastq.pl -cfq 1 -p ${fastq_prefix}"

printf "1. running $command1 \n\n"
eval $command1


