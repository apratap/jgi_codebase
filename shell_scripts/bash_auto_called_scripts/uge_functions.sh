#!/bin/bash


#for normal queue
function q()
{
    if [ -z $@ ]
    then
	echo "$0 Usage: <max_time_in_hours>  <max_ram_for_all_slots>   <total_slots> <job_name>  <command_line>";
    fi
    

    local time=${1:?Max time for job};
    shift ;
    
    local total_ram=${1:?Max memory needed for all slots}
    shift ;

    local slots=${1:?Total number of slots needed}
    shift ;
    
    local job_name=${1:=Job_Name}
    job_name=$(echo "$job_name" | sed -e 's|\.\.\/||g' -e 's|\/|_|g' )
    shift ;
   

    local COMMAND_LINE=$@
    
   
    local ram_c=$(( ( $total_ram * 1024 )/$slots )) 
    
    #generating qsub options
    EMAIL_OPTS='-M apratap@lbl.gov -m ae'
    #QUEUE_OPTS=
    TIME_OPTS='-l h_rt='${time}':00:00';
    RAM_OPTS='-l ram.c='${ram_c}'M'   ;
    SLOTS_OPTS="-pe pe_slots ${slots}";

    PROJECT='gentech-rqc.p'

    QSUB_COMMAND="qsub -w e -cwd -b yes -j yes -N ${job_name} -P ${PROJECT} ${EMAIL_OPTS} ${TIME_OPTS} ${RAM_OPTS} ${SLOTS_OPTS} ${COMMAND_LINE}";

    echo $QSUB_COMMAND
    $QSUB_COMMAND
}


#high priority queue
function qh()
{
    if [ -z $@ ]
    then
	echo "Usage: <max_time_in_hours> <max_ram_for_all_slots> <total_slots> <command_line>";
    fi
    

    local time=${1:?Max time for job};
    shift ;
    
    local total_ram=${1:?Max memory needed for all slots}
    shift ;

    local slots=${1:?Total number of slots needed}
    shift ;
    
    local job_name=${1:=Job_Name}
    job_name=$(echo "$job_name" | sed -e 's|/|_|g' -e 's|^\.*||g' -e 's|^_*||' )
    shift ;
   

    local COMMAND_LINE=$@
    
   
    local ram_c=$(( ( $total_ram * 1024 )/$slots )) 
    
    #generating qsub options
    EMAIL_OPTS='-M apratap@lbl.gov -m ae'
    #QUEUE_OPTS=
    TIME_OPTS='-l h_rt='${time}':00:00';
    RAM_OPTS='-l ram.c='${ram_c}'M'   ;
    SLOTS_OPTS="-pe pe_slots ${slots}";
    PROJECT='gentech-qaqc.p'

    QSUB_COMMAND="qsub -w e -cwd -b yes -j yes -l high.c -N ${job_name} -P ${PROJECT} ${EMAIL_OPTS} ${TIME_OPTS} ${RAM_OPTS} ${SLOTS_OPTS} ${COMMAND_LINE}";
    

    echo $QSUB_COMMAND
    $QSUB_COMMAND
}


function killjobs()
{
    for job in `qstat | awk '{print $1}' | xargs`; do qdel $job; done
}
