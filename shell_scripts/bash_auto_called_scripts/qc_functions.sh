#!/bin/bash







####copied from Andrew's bashrc

function cdproj () { 
	local ID=${1:?"Need a project or lib name"};
	ID=$(echo $ID | tr '[a-z]' '[A-Z]');
	local ROOT="/psf/bermuda/draft/projects/*";    
	local PID="";
	local DIR="";
	echo "ID = $ID"
	case $ID in
		[A-Z][A-Z][A-Z][A-Z])
			local dir=`/house/homedirs/a/ajtritt/bin/libloc $ID 2>&1`

            if [[ $dir =~ ^/ ]]; then
				echo "Found project directory: $dir"
				cd $dir
			else 
				echo "Could not find directory using libloc : $dir"
				return
			fi
        ;;
        [3-9][0-9][0-9][0-9]*)
     		local dir=`projloc $ID`
			if [[ $dir =~ ^/ ]]; then
				echo "Found project directory: $dir"
				cd $dir
			else 
				echo "Could not find directory using projloc : $dir"
				return
			fi
        ;;
        [A-z][A-z]*)
            eval cd $ROOT/$(org2proj $ID)
        ;;
        *)
            echo "(cdproj) Didn't understand $ID";
            return
        ;;
    esac
}

function mkproj () {
	local ID=${1:?"Need a project or lib name"};
	ID=$(echo $ID | tr '[a-z]' '[A-Z]');
	local LNDIR="/psf/bermuda/draft/projects/microbe";    
	local PROJDIR="/house/groupdirs/pi/project" 
	local proj=`lib2projid $ID 2>&1`
    if [[ $proj =~ [3-9][0-9][0-9][0-9]* ]]; then
		local TGT="$PROJDIR/$proj/ill_dir/$ID"
		echo "proj = $proj"
		echo "making directory $TGT"
		mkdir -v -p $TGT
		chmod -R 775 $PROJDIR/$proj
		echo "Linking files"
		ln -s -v -t $LNDIR $PROJDIR/$proj
	else 
		echo "Couldn't find project for $ID: $proj"
		return
	fi
}
