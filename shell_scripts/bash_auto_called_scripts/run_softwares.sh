#!/usr/bin sh


##DEFAULT PATHS
export BOWTIE2=/jgi/tools/bin/bowtie2
PICARD_TOOLS='/jgi/tools/misc_bio/picard/versions/picard-tools-1.77/picard'


##picard MarkDuplicates
function rdup()
{
   SUB='rdup'

   PICARD_VERSION=${PICARD_TOOLS}
   local bamFile=${1:? Need a sorted bam file ${PICARD_TOOLS} };
   local out_bamFile=$(echo $bamFile| sed 's|.bam|_rmdup.bam|');
   local out_metrics_file=$(echo $bamFile| sed 's|.bam|_rmdup_metircs.txt|');
   echo "Using Picard Tools : ${PICARD_TOOLS}"
   #echo "input : $bamFile output : $out_bamFile \n";

   DEFAULT_ARGS="REMOVE_DUPLICATES=False ASSUME_SORTED=True TMP_DIR=$SCRATCH VALIDATION_STRINGENCY=LENIENT"
   #OPTICAL_DUPLICATE_PIXEL_DISTANCE=10
   VAR_ARGS="INPUT=${bamFile} OUTPUT=${out_bamFile} METRICS_FILE=${out_metrics_file}";
   COMMAND="${PICARD_TOOLS} MarkDuplicates ${DEFAULT_ARGS} ${VAR_ARGS}";
   $COMMAND
   
   echo "Duplicates Marked Bam ${out_bamFile} ";
}


function sampleBam()
{
    SUB='sampleBam'
    RANDOM_SEED='100' #needed by DownsampleSam

    local bamFile=${1:? Need a sorted bam file ${PICARD_TOOLS} };
    local probability=${2:? Need a probability for keeping any read ${PICARD_TOOLS} };
    
    local out_bamFile=$(echo $bamFile| sed 's|.bam|_subsampled.bam|');

    echo "Using Picard Tools : ${PICARD_TOOLS}"
    COMMAND="${PICARD_TOOLS} DownsampleSam INPUT=${bamFile} OUTPUT=${out_bamFile} RANDOM_SEED=${RANDOM_SEED} PROBABILITY=${probability}"
    $COMMAND
}
    
    










function run_tophat2()
{
    if [ -z $@ ]
    then
	error "Tophat v2_0_4 Usage: <read1_fastq> <read2_fastq> <bowtie2_index> <num_threads> <pair_inner_distance> <out_dir>"
    fi
   
# expecting paired end reads
    local read1=${1:?Need a read 1};
    shift;
    local read2=${1:?Need a read 2};
    shift;
    local bowtie2_index=${1:?Need a bowtie2 index};
    shift;
    local num_threads=${1:?Need number of threads};
    shift;
    local pair_inner_dist=${1:?Need a pair inner distance};
    shift;
    local out_dir=${1:?Need a output dir name};
    shift;
    local transcriptome_index=${1:?Need a transcriptome index};
    shift;

    #Tophat out Dir
    local base_dir=$(dirname $read1)
    
    local tophat_out_dir=${base_dir}/${out_dir}_tophat_v2_0_4_out

    #echo "Base dir is $base_dir"
    #echo "Tophat out dir is $tophat_out_dir"

    #local TOPHAT_EXEC='/jgi/tools/aligners/tophat/versions/tophat-2.0.0/tophat2'
    #local TOPHAT2_BIN='/jgi/tools/aligners/tophat/versions/tophat-2.0.0/'

    #Tophat v_2_0_3
    #local TOPHAT_EXEC='/jgi/tools/aligners/tophat/versions/tophat-2.0.3/tophat2'
    #local  TOPHAT2_BIN='/jgi/tools/aligners/tophat/versions/tophat-2.0.3/'

    #Tophat v_2_0_4
    local TOPHAT_EXEC='/global/homes/a/apratap/softwares/topat/tophat-2.0.4.Linux_x86_64/tophat'
    local  TOPHAT2_BIN='/global/homes/a/apratap/softwares/topat/tophat-2.0.4.Linux_x86_64/'

    # Nov 5, 2012 
    #removed the --solexa1.3-quals from TOPHAT_DEFAULTS : to map the S- Chlamy data, most of the fastq are now in ASCII-33 format

    local TOPHAT_DEFAULTS='--library-type fr-firststrand --mate-std-dev 40 --segment-mismatches 2 --min-intron-length 20 --max-intron-length 25000 --segment-length 25 --min-coverage-intron 20 --min-coverage-intron 25000 --min-segment-intron 20 --max-segment-intron 25000 --b2-sensitive --read-mismatches 3'
    
    
    local TOPHAT_FUSION_SETTINGS='--fusion-search --fusion-min-dist 1000000'
    
    ORIG_PATH=$(echo $PATH)
    export PATH=$TOPHAT2_BIN:$ORIG_PATH
  
    #run tophat
    command=$(echo "$TOPHAT_EXEC $TOPHAT_DEFAULTS $TOPHAT_FUSION_SETTINGS --output-dir ${tophat_out_dir} --num-threads $num_threads --mate-inner-dist $pair_inner_dist --transcriptome-index=${transcriptome_index} $bowtie2_index $read1 $read2 ");

    echo "Running command $command";    
    $command

    if [ $? == 0 ]
    then
	touch ${tophat_out_dir}/tophat_v2_0_4.success
    else
	touch ${tophat_out_dir}/tophat_v2_0_4.failed

    fi
    
    #set the path back to original
    export PATH=$ORIG_PATH
}


##########################
### run bowtie2 on a gzipped paired fastq file
### returns bam file
########################    

function run_bowtie2(){

    if [[ -z $@ ]]
    then
	echo "$0 :   Usage: <paired_fastq> <bowtie2_index> <num_threads> [optional: additional prefix]"
    fi

    # expecting paired end reads

    local input_fastq=${1:?Need a paired fastq};
    shift;
    local fastq_type=${1:?fastq_type : < new | old >};
    shift;
    local bowtie2_index=${1:?Need a bowtie2 index};
    shift;
    local num_threads=${1:?Need number of threads};
    shift;
    local additional_prefix=${1:-bowtie2};


    
    if [[ ! -e ${bowtie2_index} ]]
	then
	echo "[Error]: bowtie2_index path not found"
	exit
    fi
    


    
        
    #split the reads into read1 and read2
    if [[ ${input_fastq}  == *fastq.gz ]]
    then
	fastq_prefix=`echo $input_fastq | sed -e 's|.fastq.gz||'`
	fastq_prefix=${fastq_prefix}_${additional_prefix}
	command1="zcat ${input_fastq} | ~apratap/bin/perl_scripts/edit_fastq.pl -sfq 1 -fq_type ${fastq_type} -p ${fastq_prefix}"
    
    elif [[ ${input_fastq}  == *fastq ]]
    then
	fastq_prefix=`echo $input_fastq | sed -e 's|.fastq||'`
	fastq_prefix=${fastq_prefix}_${additional_prefix}
	command1="cat ${input_fastq} | ~apratap/bin/perl_scripts/edit_fastq.pl -sfq 1 -fq_type ${fastq_type} -p ${fastq_prefix}"
    

    else
	printf "file ${input_fastq} can't be recognized \n will terminate!!! \n"
	kill_command="kill 0"
	echo $kill_command
#	kill $$
	$kill_command
    fi 

      
    ##formulating the files names
    fai_file=${bowtie2_index}'.fai'
    read_1_fastq=$fastq_prefix'_read_1.fastq'
    read_2_fastq=$fastq_prefix'_read_2.fastq'
    sam_file=$fastq_prefix'.sam'
    final_bam_file=$fastq_prefix'.bam'
    bam_stats=${final_bam_file}'.alignstats'
    final_clean_mapped_reads=$fastq_prefix'_mapped.fastq'
    final_clean_unmapped_reads=$fastq_prefix'_unmapped.fastq'

    #check if index needs to be created
    #if fai file doesnt exists, we assume bowtie2 index is not available
    
    if [[ ! -e ${fai_file} ]]
	then
	base_analysis_dir=$(dirname $input_fastq)
	index_dir=${base_analysis_dir}/bowtie2_index/
	echo "base analysis dir : ${base_analysis_dir}"
	echo "index_dir : ${index_dir}"
	mkdir -p ${index_dir}
	ln -s $(realpath ${bowtie2_index}) ${index_dir}
	ref_fasta_name=$(basename ${bowtie2_index})
	
	#building the index
	commandA="bowtie2-build ${index_dir}/${ref_fasta_name} ${index_dir}/${ref_fasta_name}"
	eval $commandA
	
	#creating the fai file
	commandB="samtools faidx ${index_dir}/${ref_fasta_name}"
	eval $commandB

	
	#updating to the correct newly created bowtie_index and fai file
	index_name=$(basename ${bowtie2_index})
	bowtie2_index=${index_dir}/${index_name}
	fai_file=${bowtie2_index}'.fai'

	echo "new bowtie index : ${bowtie2_index}"
	echo "new fai file :     ${fai_file}"

	
    fi
	


   ##mapping
    command2="memtime bowtie2 -x ${bowtie2_index} -p ${num_threads} --local -1 $read_1_fastq -2 $read_2_fastq | samtools view -bS -t $fai_file - -o $final_bam_file"

    #final cleaned unmapped reads
    command3="samtools view -f 4  $final_bam_file | ~apratap/dev/eclipse_workspace/perl_scripts/edit_sam.pl -fq 1 > $final_clean_unmapped_reads"

    #final cleaned mapped reads
    command4="samtools view -F 4  $final_bam_file | ~apratap/dev/eclipse_workspace/perl_scripts/edit_sam.pl -fq 1 > $final_clean_mapped_reads"

    #bamstats
    command5="samtools flagstat ${final_bam_file} > ${bam_stats}"

    #data cleanup
    command6="rm -rf ${read_1_fastq} ${read_2_fastq}"


    printf "1. running $command1 \n\n"
    eval $command1

    printf "2. running $command2 \n\n "
    eval $command2

    printf "3. running $command3 \n\n "
    eval $command3

    printf "4 .running $command4 \n\n " 
    eval $command4

    printf "5 running $command5 \n\n "
    eval $command5

    printf "6 running $command6 \n\n "
    eval $command6
    
}



function check(){
    ls -al
}