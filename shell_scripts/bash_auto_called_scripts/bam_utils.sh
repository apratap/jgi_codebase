#!/bin/sh


PICARD_TOOLS='/jgi/tools/misc_bio/picard/versions/picard-tools-1.64/picard'


function rdup()
{
   SUB='rdup'

   local bamFile=${1:? Need a sorted bam file};
   local out_bamFile=$(echo $bamFile| sed 's|.bam|_rmdup.bam|');
   local out_metrics_file=$(echo $bamFile| sed 's|.bam|_rmdup_metircs.txt|');
   echo "input : $bamFile output : $out_bamFile \n";

   DEFAULT_ARGS="REMOVE_DUPLICATES=True ASSUME_SORTED=True TMP_DIR=$SCRATCH"
   VAR_ARGS="INPUT=${bamFile} OUTPUT=${out_bamFile} METRICS_FILE=${out_metrics_file}";
   COMMAND="${PICARD_TOOLS} MarkDuplicates ${DEFAULT_ARGS} ${VAR_ARGS}";
   $COMMAND
   
   echo "Duplicates Removed Bam ${out_bamFile} ";
   
}
