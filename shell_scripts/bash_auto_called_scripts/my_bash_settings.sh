#!/usr/bin/env bash

#echo "[debug]: Executing my_bash_settingns.sh"
# .bashrc

LANGUAGE="en_US"
LANG="en_US"
LC_ALL=C


###call the functions file
source ~apratap/dev/shell_scripts/bash_auto_called_scripts/qc_functions.sh
source ~apratap/dev/shell_scripts/bash_auto_called_scripts/run_softwares.sh
source ~apratap/dev/shell_scripts/bash_auto_called_scripts/bam_utils.sh
source ~apratap/dev/shell_scripts/bash_auto_called_scripts/uge_functions.sh
source ~apratap/dev/shell_scripts/bash_auto_called_scripts/shell_utils.sh



export MODULEBUILDRC="/house/homedirs/a/apratap/perl5/.modulebuildrc"
export PERL_MM_OPT="INSTALL_BASE=/house/homedirs/a/apratap/perl5"
export PERL5LIB="/house/homedirs/a/apratap/perl5/lib/perl5:/house/homedirs/a/apratap/perl5/lib/perl5/x86_64-linux-thread-multia"
#export PATH="/house/homedirs/a/apratap/perl5/bin:$PATH"
#export TMPDIR="/scratch"
#export MOSAIK_TMP="/scratch"


# Bash prompt formatting
PS1='<\u@\h:\w>'


#my PATH variable
#PATH="/global/homes/a/apratap/bin/:/jgi/tools/bin:/scratch/bin:/house/groupdirs/QAQC/scripts/:/bin:/sbin:/usr/bin:/usr/sbin:/usr/X11R6/bin:/usr/local/bin:/jgi/tools/groups/randd/bin"
#export PATH

#based on Doug's suggestion
export PATH="$HOME/bin/:/house/groupdirs/QAQC/scripts/:/jgi/tools/groups/randd/bin:$PATH" 



##R 
export R_LIBS_USER="/global/homes/a/apratap/lib/R"


#loading default modules
module load python
module load hdf5 lzo2
module load zeromq
module load oracle_client
module load R
module load firefox
#module load jgi-python-packages


##Python specific
#PYTHONPATH="${PYTHONPATH}:/global/homes/a/apratap/lib/python2.7/site-packages/:/jgi/tools/misc_software/python/2.7/lib/python2.7/site-packages/"
PYTHONPATH="/global/homes/a/apratap/lib/python2.7/site-packages/:${PYTHONPATH}"
export PYTHONPATH

##PYTHON DRMAA binding
DRMAA_LIBRARY_PATH='/opt/uge/uge-8.0.1/lib/lx-amd64/libdrmaa.so.1.0'
export DRMAA_LIBRARY_PATH


#NERSC related
alias nx='./bin/nxclient'


#ORACLE data wareshouse settings
#export ORACLE_HOME=/jgi/tools/oracle_client/DEFAULT
#export PATH=$ORACLE_HOME/bin:$PATH



#DISPLAY related
#export DISPLAY=localhost:0



#OPEN MP PATHS
#export OMP_NUM_THREADS=32
#export OMP_THREAD_LIMIT=32



##SGE related
#alias qs='qsub -cwd -b yes -v PATH -now no -j yes -m as -M apratap@lbl.gov -w e';
#alias qgp='source /opt/uge//genepool/common/settings.sh'
#alias qgenepool='source /opt/uge/uge/genepool/common/settings.sh'
#alias qphoebe='/opt/sge/phoebe/common/settings.sh';

#defaulting to genepool
#`source /opt/uge/crius/uge/genepool/common/settings.sh`



##color grep results
export GREP_OPTIONS='--color=auto'


### my commands
alias ll='ls -al'
alias bashexe='source ~apratap/dev/shell_scripts/bash_auto_called_scripts/my_bash_settings.sh'
alias play='cd /house/homedirs/a/apratap/playground'
alias p='cd /house/homedirs/a/apratap/playground'
alias badlinks='find -L . -ilname "*"'
alias bl='find -L . -ilname "*"'
alias npc='cat /proc/cpuinfo | grep "physical id" | uniq | wc -l'
alias pcores='cat /proc/cpuinfo | grep "physical id" | uniq | wc -l'
alias tslots='cat /proc/cpuinfo | grep "processor" | uniq | wc -l'
alias cores='cat /proc/cpuinfo | grep "processor" | uniq | wc -l'
alias dev='cd ~apratap/dev/'
alias d='cd ~apratap/dev/'
#alias stimpy='ssh -X stimpy'
#alias s='ssh -X -Y gpint15'
alias w='ssh -X -Y wesley'
alias gp15='ssh -X -Y gpint15'
alias gp16='ssh -X -Y gpint16'
alias gp17='ssh -X -Y gpint17'
alias gp18='ssh -X -Y gpint18'
alias gp19='ssh -X -Y gpint19'
alias gp20='ssh -X -Y gpint20'
alias gp21='ssh -X -Y gpint21'
alias gp22='ssh -X -Y gpint22'
alias gp='ssh -X -Y genepool'
alias genepool='ssh -X -Y genepool'
alias RNA='cd /house/groupdirs/genetic_analysis/rna/projects'
alias rna='cd /house/groupdirs/genetic_analysis/rna/projects'
#alias igvd='/house/homedirs/a/apratap/playground/software/IGV_2.0/igv.sh'
alias plant='cd /house/groupdirs/QAQC/seq_qc/plants/'
alias fstats='/house/groupdirs/QAQC/scripts/fasta_stats2.linux -n 1'
alias qcr='~/bin/perl_scripts/gen_MG_base_QC_report.pl -lib'
alias qcreport='~/bin/perl_scripts/gen_MG_base_QC_report.pl -lib'
alias R-215='/jgi/tools/math/R/versions/2.15.0/bin/R'
#alias R='/jgi/tools/math/R/versions/2.14.1/bin/R'
alias apxreads='/jgi/tools/groups/randd/bin/EstimatedReadNums'
#alias py27='/jgi/tools/misc_software/python/2.7/bin/python'
#alias lpy='/house/homedirs/a/apratap/playground/software/epd-7.2-2-rh5-x86_64/bin/python'
#alias lipy='/house/homedirs/a/apratap/playground/software/epd-7.2-2-rh5-x86_64/bin/ipython'
alias chlamy='cd /global/projectb/projectdirs/PI/Chlamy_Chia_Lin'
alias ebash='emacs ~apratap/dev/shell_scripts/bash_auto_called_scripts/my_bash_settings.sh &'
alias editbash='emacs ~apratap/dev/shell_scripts/bash_auto_called_scripts/my_bash_settings.sh &'
alias batchqc='/house/homedirs/q/qc_user/rqc_software/utility/auto_segment_qc.py'


## adding the color to dir / executable files  and symlinks
alias ls='ls -F --color=always'




#functions


function top_specie () {
grep $1 unique_annotated_tophits_across_all_dbases.txt | cut -f 4 | sort  | uniq -c | sort -nr -k1,1 | less
}



function dotplot() {
    local REF=${1:?"Need a reference fasta file"};
    local QUERY=${2:?"Need a Query fasta file as second argument"};
    
    REF_FILENAME=`basename $REF`
    QUERY_FILENAME=`basename $QUERY`
    PREFIX=$QUERY_FILENAME'_vs_'$REF_FILENAME
   

    command1="nucmer -p $PREFIX $REF $QUERY"
    $command1
    
    DELTA_FILE=$PREFIX'.delta'
    command2="mummerplot -layout -title $PREFIX -color -png -p $PREFIX $DELTA_FILE"
    $command2
    
}
