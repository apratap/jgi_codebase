#!/bin/bash


# script to process metatranscriptome assembly
# 1. clean fastq of JGI artifacts, rRNA, Illumina Artifact

#############
###parameters
############
avg_phred_quality=20
kmer_size=18


############
##USER INPUT
############
n_slots=$1
shift;
meta_genome_reference=$1
shift;
fastqs=$*

echo "######################################"
echo "[slots]:  ${n_slots}"
echo "[mg_ref]: ${meta_genome_reference}"
echo "[fastqs]: ${fastqs}"


###check if multiple fastq
echo "[#fastqs]:" $(echo ${fastqs} | wc -w)

if [[ $(echo ${fastqs} | wc -w)  > 1 ]]
then 
    echo "Multiple fastq detected"
    #get the basedir for the first fastq ..so that the output is created under that dir
    
    first_fastq=$(echo ${fastqs} | awk '{print $1}')
    #echo "first fastq : ${first_fastq}"
    dir=$(dirname ${first_fastq})
    #echo "[dir] : $dir"
    basedir=${dir}/
    fastq_prefix='all_combined_qtrim_jartifacts'
    
else
    echo "single fastq found"
    input_fastq=${fastqs}
    #full_path_fastq=$(realpath ${input_fastq})
    basedir=$(dirname ${input_fastq})/
    fastq_prefix=$(echo $input_fastq | sed -e 's|.fastq.gz||')'_qtrim_jartifacts'


fi

echo "[basedir]: ${basedir}"
echo "[fastq_prefix]: ${fastq_prefix}"


#meta_genome_reference=$(realpath ${meta_genome_reference}



#########
#path to databases
rna_fai_file="/house/groupdirs/QAQC/databases/silva/2012.05.silva/bwa_index/combined_SSU_LSU_108_Ref.fasta.fai"
artifacts_fastq="/house/groupdirs/QAQC/databases/Artifacts.fa"
##########


CWD=`pwd`





########################
#creating the file names
########################
duk_log_file=${fastq_prefix}.duk.log
trim_fastq=$fastq_prefix'_qtrim.fastq'
trim_fastq_jartifacts=${fastq_prefix}.fastq
trim_fastq_jartifacts_read1=${fastq_prefix}'_read_1.fastq'
trim_fastq_jartifacts_read2=${fastq_prefix}'_read_2.fastq'
sam_file=${fastq_prefix}'.sam'
final_bam_file=${fastq_prefix}'_map_rRNA.bam'
final_clean_reads=$fastq_prefix'.rRNA_cleaned.fastq'




################################################################################


if [ ! -e ${final_clean_reads} ]
then 

     ##quality trim fastq / remove the Illumina artifacts/JGI contaminants and split the read in to read 1 and read 2
     #command1="zcat $input_fastq | /house/groupdirs/QAQC/scripts/fastqTrimmer -paired > $trim_fastq"

    command1="zcat ${fastqs} | /house/groupdirs/QAQC/scripts/fastqTrimmer -avgQ ${avg_phred_quality} -paired | duk -k ${kmer_size} /house/groupdirs/QAQC/databases/Artifacts.fa -n - -o ${duk_log_file} | ~apratap/bin/perl_scripts/edit_fastq.pl -sfq 1 -p ${fastq_prefix}"
 
    ##mapping
    command2="memtime bowtie2 -x /house/groupdirs/QAQC/databases/silva/2012.05.silva/bowtie2_index/combined_SSU_LSU_108_Ref.fasta --phred64 --local -p ${n_slots} -L 25 -1 $trim_fastq_jartifacts_read1  -2 $trim_fastq_jartifacts_read2 | samtools view -bS -t $rna_fai_file - -o $final_bam_file"


    #final cleaned reads
    #this step doesnt keep singeltons so the resultant fastq is all paired
    command3="samtools view -f 4  $final_bam_file | ~apratap/bin/perl_scripts/edit_sam.pl -fq 1 > $final_clean_reads"
    
    
    #clean reads stats
    command4="samtools flagstat ${final_bam_file} > ${final_bam_file}.alignstats"


    #data cleanup
    #command5="rm -rf $trim_fastq_jartifacts_read1 $trim_fastq_jartifacts_read2 "


    printf "1. running $command1 \n\n"
    eval $command1

    printf "2. running $command2 \n\n "
    eval $command2

    printf "3. running $command3 \n\n "
    eval $command3

    printf "4 .running $command4 \n\n " 
    eval $command4
    
    #printf "5 .running $command5 \n\n " 
    #eval $command5
    

else
    echo "Expected output ${final_clean_reads} already present"
fi

#####################################################################################




############################################
##Step 2  : If Metagenome reference available
############################################

if [[ -n ${meta_genome_reference} ]]
then

    #################################################################
    ### Mapping to Reference Metagenome
    #################################################################

    echo ""
    echo "######################################"
    echo "MG reference genome mapping"
    
    #checking if expected outfiles exists
    mg_ref_map=${basedir}'/mapping_to_mg_ref/'
    prefix=${mg_ref_map}$(echo ${basedir}${final_clean_reads} | sed 's|.fastq||')
    mg_ref_map_stats=${prefix}_map_to_mg_ref.bam.alignstats
    mg_unmapped_reads=${prefix}_map_to_mg_ref_unmapped.fastq
    
    #echo ${prefix}
    #echo ${mg_ref_map_stats}
    
    if [[ -e ${mg_ref_map_stats} && -e ${mg_unmapped_reads} ]]
    then
	echo "mapping to MG ref complete....."

    else
	mkdir -p ${mg_ref_map}
	ln -s  $(realpath ${basedir}${final_clean_reads}) ${mg_ref_map}
        command6="~apratap/dev/shell_scripts/run_bowtie2.sh ${mg_ref_map}${final_clean_reads} ${meta_genome_reference} ${n_slots} map_to_mg_ref"
        eval $command6
    fi
  

    #################################################################
    ### Creating the assembly dir for assembling unmapped reads
    #################################################################
    


    echo ""
    echo "#################################################"
    echo "Rnnotator Assembly"
    

    asm_dir=${basedir}'/rnnotator_assembly_post_mg_ref_mapped/'
    #echo "Assembly dir : ${asm_dir}"
    mkdir -p ${asm_dir}
    
    if [[ -e ${asm_dir}/final_contigs.fa ]]
	then
	echo "Rnnotator assembly is complete...."

	#################################################################
        ### mapping the reads that were assembled back to assembly
        #################################################################
	echo ""
	echo "#################################################"
	echo "Mapping the reads back to assembly"
	#geting the name of the fastq that were assembled
	fastq_for_assembly=$(basename ${mg_unmapped_reads})
	
	command7="~apratap/dev/shell_scripts/run_bowtie2.sh ${asm_dir}/${fastq_for_assembly} ${asm_dir}/final_contigs.fa  ${n_slots} map_to_assembly"
        eval $command7

    else
          #Now run the rnnotator   #sample command : non strand specific
	ln -s  $(realpath ${mg_unmapped_reads}) ${asm_dir}
	echo "Rnnotator needs to be run manually now"
	echo "Sample command "
	echo "rnnotator.pl -nonP 300 <fastq-name> -n 8 -rRNA off -low_qual off -adapter off -trim off"
    fi


    


  
    
#########################################
### No MG Reference available
#########################################
else
    echo "Sample has NO Metagenome Reference Available"
    asm_dir=${basedir}'/rnnotator_assembly/'
    #mkdir -p ${asm_dir}
    

fi







