#!/bin/bash


#echo "Enter the reference fast";
#read reference_fasta

#echo "Enter the path to convey contigs "
#read convey_contigs

#echo "Enter the path to in house velvet contigs"
#read velvet_contigs

#echo "Enter the prefix "
#read prefix


reference_fasta=$1;
convey_contigs=$2;
velvet_contigs=$3;
prefix=$4;



echo "Following things were entered";

echo " Reference Fasta : $reference_fasta";
echo " Convey Contigs : $convey_contigs";
echo " Velvet Contigs :  $velvet_contigs";
echo " Prefix         : $prefix";




### running nucmer convey v/s in_house_velvet

p1=$prefix'_convey_vs_velvet'


if [ ! -e $p1'.delta' ]
then
command1="nucmer -p $p1  $convey_contigs $velvet_contigs"
echo "Running $command1";
$command1 
mummerplot -layout -title $p1 -png  -p $p1 $p1'.delta' 
fi





### running nucmer on convey v/s reference


p2=$prefix'_reference_vs_convey'

if [ ! -e $p2'.delta' ]
then
command2="nucmer -p $p2 $convey_contigs $reference_fasta"
echo "Running $command2";
$command2 
mummerplot -layout -title $p2 -png -p $p2 $p2'.delta'
fi 







## running nucmer on in-house velvet v/s reference

p3=$prefix'_reference_vs_velvet'


if [ ! -e $p3'.delta' ]
then
command3="nucmer -p $p3  $velvet_contigs $reference_fasta "
echo "Running $command3";
$command3 
mummerplot -layout -title "$p3" -png -p $p3 $p3'.delta'
fi




