#!/jgi/tools/bin/perl

use strict;
use FindBin;
use lib "$FindBin::Bin/lib";
use JGI::QAQC::RQCdbAPI;
use JGI::QAQC::Utilities;
use File::Path qw(mkpath rmtree);
use Log::Log4perl qw(:easy);
use Data::Dumper;
use Getopt::Long;


## global variables
our ($LOGGER, $BASE_UPLOAD_PATH, $VERBOSE, $DELIVER_FILTERED_FASTQ);
our (@library_names,@seq_units_by_user);

main();



sub main {
	
	### Print the standard RQC header 
	get_standard_usage_header();
	
	###setup the logger
	config_logger();
	$LOGGER = get_logger();
	
	## get the user arguments
	get_user_arguments();

	## Instantiate the DB access
	my $obj = JGI::QAQC::RQCdbAPI->new();
	
	
# 	Get the final set of seq_unit_names for which data needs to be delivered
# 	RESULTS available in SEQ_UNITS array
	my $SEQ_UNITS_REF = $obj->get_final_seq_units_set(\@library_names,\@seq_units_by_user);
	
	#hashes to store all the uniq values for the following
	my $global_data_info = {};
	
		
	foreach my $seq_unit_name ( @{$SEQ_UNITS_REF} ) {
			next if ($seq_unit_name=~/^$/);
			
			#following two lines not needed the script shud support non-indexed data
			#skipping if the seq unit is not for a indexed library or UNKNOWN
			#next if ($seq_unit_name !~/[AGCT]/ );
			
			#skip the UNKOWN file
			next if ($seq_unit_name=~/UNKNOWN/ );
	
			## set the library name
			$obj->seq_unit_name($seq_unit_name);
			
			
			## get the library detail using library name 
			$obj->get_library_info_from_seq_unit_name();	
			
			
			## get the lane level info for this seq_unit
			$obj->get_lane_level_info();
			
			## also get the library detail from venonat
			$obj->get_library_info_from_venonat();
			
			##get the seq_unit_info from ITS DW
			$obj->get_seq_unit_info_from_ITS_DW();
			
			##get the info from SDM webservice
			$obj->get_seq_unit_info_SDM_webservice();
			
			
			
			
			# sample name
			my $SAMPLE_NAME = $obj->seq_unit_info_from_ITS_DW()->{$seq_unit_name}->{'SAMPLE_NAME'} || 'NA';
			
			my $seq_unit_organism_name 				= $obj->seq_unit_info_from_ITS_DW()->{$seq_unit_name}->{'NCBI_ORGANISM_NAME'} || 'NA';
			my $seq_unit_sequencing_project_name 	= $obj->seq_unit_info_from_ITS_DW()->{$seq_unit_name}->{'SEQUENCING_PROJECT_NAME'} || 'NA';
			my $seq_unit_PI_name 					= $obj->seq_unit_info_from_ITS_DW()->{$seq_unit_name}->{'PRINCIPAL_INVESTIGATOR_NAME'} || 'NA';
			my $seq_unit_proposal_ID 				= $obj->seq_unit_info_from_ITS_DW()->{$seq_unit_name}->{'PROPOSAL_ID'} || 'NA';
			my $seq_unit_proposal_name 				= $obj->seq_unit_info_from_ITS_DW()->{$seq_unit_name}->{'TITLE'} || 'NA';
			my $scientific_program	 				= $obj->seq_unit_info_from_ITS_DW()->{$seq_unit_name}->{'SCIENTIFIC_PROGRAM'} || 'NA';
			
			#get the seq unit path from SDM web service
			# this part can be slow
			my $seq_unit_location 					= $obj->get_sdm_seq_unit_path();
			
			
			#storing the global vars in a hash so that only uniq entries are stored
			$global_data_info->{'organism_name'}->{$seq_unit_organism_name}=1;
			$global_data_info->{'scientific_program'}->{$scientific_program}=1;
			$global_data_info->{'sequencing_project_name'}->{$seq_unit_sequencing_project_name} = 1;
			$global_data_info->{'PI_name'}->{$seq_unit_PI_name} =1;
			$global_data_info->{'proposal_ID'}->{$seq_unit_proposal_ID}=1;
			$global_data_info->{'proposal_name'}->{$seq_unit_proposal_name} =1;
			
			
		#	Get the total number of reads for this seq_unit
			my $read_count = $obj->get_total_read_count();
			
			
		#	Get the libarary name for this seq_unit_name
			my $library_name = $obj->get_library_name();
				
		#	get the source DNA number
			my $source_dna_number = $obj->get_source_dna_number();
	
		
		#	Get the full fastq location for this seq_unit_name
			my $qualified_individual_seq_unit_name = $obj->get_qualified_seq_unit_name();
			my $full_path_seq_unit = $seq_unit_location.'/'.$qualified_individual_seq_unit_name;
							
	
		#	Delivery file name
			my $delivery_file_name = "$library_name".'.'.$qualified_individual_seq_unit_name;
		
		
		#	##get the bio_class_name
		#	my $bioclass_name = $obj->get_bio_classification_name();
		#	##substituting all the spaces with underscrores as the names will
		#	##used to create dirs
		#	$bioclass_name =~s/\s/_/g;

		my $status = deliver_fastq_data($full_path_seq_unit, $delivery_file_name, $BASE_UPLOAD_PATH);
		my $library_info_line = "$library_name \t\t $source_dna_number \t\t $read_count \t\t $SAMPLE_NAME \n";
		
		$global_data_info->{'seq_units'}->{$seq_unit_name}->{'status'}=$status;
		$global_data_info->{'seq_units'}->{$seq_unit_name}->{'lib_info'}=$library_info_line;
	
		#if user requests to deliver RQC filtered fastq
		if ($DELIVER_FILTERED_FASTQ){
			#formulate the filtered fastq name
			#this could be buggy  if the naming format change
			my $filtered_seq_unit_file_name = $seq_unit_name;
			$filtered_seq_unit_file_name=~s/\.srf/\.anqr\.fastq\.gz/;
			my $delivery_filtered_seq_unit_file_name = "$library_name".'.'.$filtered_seq_unit_file_name;
			#fastq basepath
			my $filtered_fastq_base_path = '/house/groupdirs/QAQC/rqc_archive/filter/rqc/';
			#full filtered fastq path
			my $full_filtered_fastq_path = $filtered_fastq_base_path . $filtered_seq_unit_file_name;
			
			if (! -e $full_filtered_fastq_path ){
				print "\nError: Cant deliver Filtered fastq for $seq_unit_name ";
				print "\nSearched for $filtered_seq_unit_file_name in $filtered_fastq_base_path : Not Found \n\n";
				next;	
			}
			deliver_fastq_data($full_filtered_fastq_path, $delivery_filtered_seq_unit_file_name, $BASE_UPLOAD_PATH);
		}
	}
	write_LIBRARIES_file($global_data_info,$BASE_UPLOAD_PATH);
}

sub deliver_fastq_data {
		my $SUB = 'deliver_data';
	
		my $full_path_seq_unit  = shift @_;
		my $delivery_file_name  = shift @_;
		my $path_where_to_upload  = shift @_;
		
		unless ( -e $full_path_seq_unit ){
			print "Error[$SUB] Path $full_path_seq_unit  doesn't exists !!! Will exit \n";
			die "$! \n\n";
		}
		unless ( -d $path_where_to_upload ) { 
			print "Error[$SUB] Path $path_where_to_upload doesn't exists !!! Will exit \n";
			die "$! \n\n";
		}
		if( -e $path_where_to_upload."/".$delivery_file_name ) { 
			print "Warn[$SUB] File $delivery_file_name already symlinked in $path_where_to_upload \n";
			return 0;
		}
		else{
			my $symlink_del_status = eval { symlink("$full_path_seq_unit","$path_where_to_upload/$delivery_file_name") , 1 };
			print "Symlink delivery status \t $symlink_del_status\n";
			if($@) {
					print "Error[$SUB] $@  \n\n";
				}
			if( $symlink_del_status ) {
				print "\n\nDelivered $delivery_file_name to $path_where_to_upload \n";
				return 1;
			}
		}
}


sub write_LIBRARIES_file {
	my $SUB = 'write_LIBRARIES_file';
	
	my $global_data_hash = shift @_;
	my $path_to_write_file = shift @_;
	
	#var to hold seq unit info 
	my $seq_unit_info;
	
	#create the lib info blob for all the seq_units for which information needs to be put in the LIB file
	foreach my $seq_unit (keys %{$global_data_hash->{'seq_units'}}){
		if ($global_data_hash->{'seq_units'}->{$seq_unit}->{'status'} == 1){
			$seq_unit_info .= $global_data_hash->{'seq_units'}->{$seq_unit}->{'lib_info'}
		}
	}


	#determine if there is content to write to file
	if ($seq_unit_info){	
		#out file handle 
		my $OUT_FH;
		my $out_file = "$path_to_write_file".'/LIBRARIES.txt';
		if( -e $out_file ){
			open($OUT_FH, ">> $out_file ") || die "Error[$SUB] File $out_file can't be opened \n $! \n\n";
			print $OUT_FH ,"\n\n";
		}
		else {
			open($OUT_FH, "> $out_file ") || die "Error[$SUB] File $out_file can't be opened \n $! \n\n";
		}		
		
		#get the header section data
		my $header;
		$header .= '=========================='."\n";
		$header .= 'Scientific Program: '. join(',', keys %{$global_data_hash->{'scientific_program'}}). "\n";
		$header .= 'Organism Name: '. join(',', keys %{$global_data_hash->{'organism_name'}}). "\n";
		$header .= 'Principal Investigator: '. join(',', keys %{$global_data_hash->{'PI_name'}}). "\n";
		$header .= 'Proposal ID:'. join(',', keys %{$global_data_hash->{'proposal_ID'}}). "\n";
		$header .= 'Proposal Name:  '. join(',', keys %{$global_data_hash->{'proposal_name'}}). "\n";
		$header .= 'Sequencing Project Name: '. join(',', keys %{$global_data_hash->{'sequencing_project_name'}})."\n";
		$header .= '==========================';
		$header .=  "\n\n\n";
		
		
		print $OUT_FH $header;
		print $OUT_FH 'LIBRARY_NAME '."\t\t".' SampleID '."\t\t".' RawReads'."\t\t".'SAMPLE_NAME'."\n";
		print $OUT_FH $seq_unit_info;
		close (OUT);
		print "Finished creating/appending LIBRARIES file $out_file \n";		
	}
	
}



sub get_user_arguments {
	my $SUB = 'get_user_arguments';
	my %temp;
	if(scalar @ARGV ==  0 ) {
		usage();
		exit 2;
	}
	GetOptions(\%temp,
					"lib_name|lib:s@",
					"seq_units|su:s@",
                    "upload_path|up:s",
                    "deliver_filtered_fastq|dff!",
                    "verbose|v!",
                    "email|e:i",
              );

    if ( defined $temp{'lib_name'} ){
    	@library_names = @ {$temp{'lib_name'} };
    	#print 'You entered  ',scalar @library_names,' libs to deliver',"\n";
    }
    if ( defined $temp{'seq_units'} ){
    	@seq_units_by_user = @{ $temp{'seq_units'} };
    	print 'You entered ',scalar @seq_units_by_user,' seq_units to deliver',"\n";
    }
    if( defined $temp{'upload_path'} ){
    	$BASE_UPLOAD_PATH = $temp{'upload_path'};
	}
	else{
		print "Error";
		usage();
		exit 2;
	}
	if ( defined $temp{'verbose'}){
		$VERBOSE = $temp{'verbose'};
	}
	else {
		$VERBOSE = 0;
	}
	if ( defined $temp{'deliver_filtered_fastq'}){
		$DELIVER_FILTERED_FASTQ = 1;
	}
	else{
		$DELIVER_FILTERED_FASTQ = 0;
	} 
}



sub usage {
	my $SUB  ='usage';
	my $script_name = `basename $0`;

print<<MSG;
USAGE:
./$script_name 
			-lib <lib_name> 
			-su <seq_unint_name> 
			-up <upload_path> 
			-dff <flag: deliver_filtered_fastq> [optional]

Notes:
-lib can accept both singleplex or multiplexed
-multiplex lib names: this can be broken for multiplex libs due to a change in RQC database)
-su can accept the exact seq_unit_name (2042.7.1728.TAGCTT.fastq.srf) OR 
	(2042.7.*) to include all the files from lane 7 from run 2042
MSG

}



	
	