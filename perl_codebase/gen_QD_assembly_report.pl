#!/jgi/tools/bin/perl

use strict;
use FindBin;
use lib "$FindBin::Bin/lib";
use JGI::QAQC::RQCdbAPI;
use JGI::QAQC::Utilities;
use Statistics::Descriptive::Discrete;
use Statistics::Descriptive::Weighted;
use Data::Dumper;
use Getopt::Long;


use vars qw($verbose @library_names @seq_units_by_user $VERBOSE $EMAIL @SEQ_UNITS $IS_RUN_PE $FINAL_ASSEMBLY_FILE $XML_BLOB);
use vars qw($base_scripts_dir $BIO_CLASS_NAME);


our ($bioclassication_summary_line_per_seq_unit);
our ($read_quality_per_seq_unit);
our ($GC_content_per_seq_unit);
our ($read_Count_per_seq_unit);
our ($generic_info);
our ($sample_id_per_library);


main();


sub main {
	
	
	my $SUB = 'main';
	
	### Print the standard RQC header 
	get_standard_usage_header();
	
	
	## get the user arguments
	my $seq_unit_name = _get_user_arguments();
	
	##get the base name of the dir in which production perl scripts are present
	$base_scripts_dir = '/house/homedirs/a/apratap/bin/perl_scripts';
	
	
	## Instantiate the DB access
	my $obj = JGI::QAQC::RQCdbAPI->new();
	$obj->logger()->info('####Program Stated######');

	
	if ($BIO_CLASS_NAME){
		@library_names = $obj->get_library_names_from_bio_classification_name($BIO_CLASS_NAME);
		print "Libraries associated with $BIO_CLASS_NAME \t @library_names \n";
	}
	
	
	
# 	Get the final set of seq_unit_names to be included in QC report
# 	RESULTS available in SEQ_UNITS array
	my $SEQ_UNITS_REF = $obj->get_final_seq_units_set(\@library_names,\@seq_units_by_user);
	@SEQ_UNITS = @{$SEQ_UNITS_REF};

	
	#getting the details for each $seq_unit for the individual library that 
	foreach my $seq_unit_name ( @SEQ_UNITS ) {
			
			## Setting the right seq_unit for down_stream called to RQCdbAPI
			$obj->seq_unit_name($seq_unit_name);
		
			###Now fetching the results for each seq_unit_name from RQC db
			$obj->get_lane_level_info();
			$obj->get_library_info_from_seq_unit_name();
			
			## also get the library detail from venonat
			$obj->get_library_info_from_venonat();
			
			$sample_id_per_library						 .= $obj->get_library_name()."\t\t".$obj->get_library_type()."\t\t".$obj->get_source_dna_number()."\n";
			$bioclassication_summary_line_per_seq_unit   .= $obj->get_bio_classification_name().', '.$obj->get_library_name().', '.$obj->get_library_type().', '.$obj->get_lane_yield().' Mbp'.', '.$obj->get_run_config().', '.$obj->get_qualified_seq_unit_name()."\n";
			$read_Count_per_seq_unit   	.=$obj->get_library_name()."\t\t".$obj->get_qualified_seq_unit_name()."\t\t".commify($obj->get_total_read_count())."\n";
			$read_quality_per_seq_unit 	.=  $obj->get_library_name()."\t\t".$obj->get_qualified_seq_unit_name()."\t\t". $obj->get_Q30_percent().'  : Q20 rl = '.$obj->get_min_Q20_readlen_of_read_1_2()."\n";
			$GC_content_per_seq_unit   	.= $obj->get_library_name()."\t\t".$obj->get_qualified_seq_unit_name()."\t\t".$obj->get_GC_content()."\n";
			$generic_info			   	.=$obj->get_library_name()."\t\t". $obj->get_library_type()."\t\t".$obj->get_qualified_seq_unit_name()."\t\t\n";
			$XML_BLOB				.=   generate_XML_block_per_library($obj);
		
	}

	## Create the QD text base REPORT
	create_QD_text_BaseReport($obj);

#	## Create the XML  base report
	create_QD_XML_BaseReport($obj);
	
	
	print "\n\n";
	
	$obj->logger()->info("####Program Finished######\n\n")
}


sub _get_user_arguments {
	
	# need to improve this
	my $SUB = '_get_user_arguments';
	
	my %temp;
	
	if(scalar @ARGV ==  0 ) {
		usage();
		exit 2;
	}
	
	GetOptions(\%temp,
					"lib_name|lib:s@",
					"seq_units|su:s@",
					"bio_class_name|bcn:s",
					"final_assembly|fa:s",
                    "prefix|p:s",
                    "verbose|v!",
                    "email|e:i",
              );
    
    
    if ( defined $temp{'lib_name'} ){
    	@library_names = @ {$temp{'lib_name'} };
 
    }
    
    if ( defined $temp{'seq_units'} ){
    	@seq_units_by_user = @{ $temp{'seq_units'} };
    	
    }
	
	if ( defined $temp{'bio_class_name'}){
		$BIO_CLASS_NAME = $temp{'bio_class_name'};
	}
	
	if ( defined $temp{'verbose'}){
		$VERBOSE = $temp{'verbose'};
	}
	else {
		$VERBOSE = 0;
	}
	
	if ( defined $temp{'email'} ){
		$EMAIL = $temp{'email'};
	}
	else {
		$EMAIL = 0;
	}
	
	if ( defined $temp{'final_assembly'} ){
		$FINAL_ASSEMBLY_FILE = $temp{'final_assembly'};
	}
	else{
		die "Please specify a final assembly fasta file";
	}
	
	if ( (scalar @library_names == 0) && ( scalar @seq_units_by_user == 0 ) && ( ! $BIO_CLASS_NAME )  ){
		die " You have not provided either the library name or seq_unit_name/s to generate a QC template on.";
	}
	
	
	
}


sub usage {
	
print<<USAGE;

-------------------------------------------------
Metagenomic Library QC template generator script 
Standard options for script
-------------------------------------------------


1.	For single library
$0 -lib <lib_name> -email <0|1 : default 1> -final_assembly <final_assembly_fasta_file>

2. 	For multiple libraries
$0 -lib <lib_name1> -lib <lib_name2> -lib <lib_name3>  -final_assembly <final_assembly_fasta_file>

3.	If you want to specify seq_units_name
$0 -su <seq_unit_name1> -su <seq_unit_name2> -final_assembly <final_assembly_fasta_file>


4.	You can also club library names and seq_unit_names if needed 
$0 	
-lib <lib_name1> -lib <lib_name2> -lib <lib_name3>  
-su <seq_unit_name1> -su <seq_unit_name2>



What is a SEQ_UNIT_NAME ? 
Full seq unit name is the full lane name as per RQC. Also shown on the RQC page post library name based search.
Example: For library HZON  with one lane of data 1690.5.1538.fastq.gz : seq_unit_name will be 1690.5.1538.srf
	
USAGE

}



 sub get_GC_percent_fasta {
 	
 	my $SUB = 'get_GC_percent_fasta';
 	
 	my @GC_percent = `cat $FINAL_ASSEMBLY_FILE | $base_scripts_dir/edit_fasta.pl -cr | cut -f 9`;
 	
 	if (scalar @GC_percent == 1 ) {
 		print "GC percentage could not be calculated in $FINAL_ASSEMBLY_FILE\n\n Will Return 0";
 		return '0';
 	}
 	
 	#removing the header$sample_id_per_library
 	@GC_percent = splice(@GC_percent,1);
 	
 	my $stats = new Statistics::Descriptive::Discrete;
 	$stats->add_data(@GC_percent);
 	
 	my $mean_GC = sprintf('%.2f', $stats->mean()); 
 	my $sd_GC	= sprintf('%.2f', $stats->standard_deviation() );
 	
 	return ("$mean_GC +/- $sd_GC");
 	
 	
 }
 
 
 sub get_length_weighted_GC_percent_fasta {
 	
 	my $SUB = 'get_length_weighted_GC_percent_fasta';
 	
 	my @GC_percent 			= `cat $FINAL_ASSEMBLY_FILE | $base_scripts_dir/edit_fasta.pl -cr | cut -f 9`;
 	my @length_per_sequence = `cat $FINAL_ASSEMBLY_FILE | $base_scripts_dir/edit_fasta.pl -cr | cut -f 2`;
 	
 	if (scalar @GC_percent == 1 ) {
 		print "GC percentage could not be calculated in $FINAL_ASSEMBLY_FILE\n\n Will Return 0";
 		return '0';
 	}
 	
 	#removing the header
 	@GC_percent = splice(@GC_percent,1);
 	
 	#removing the header
 	@length_per_sequence = splice(@length_per_sequence,1);
 	
 	my $weighted_stats =  Statistics::Descriptive::Weighted::Full->new();;
 	$weighted_stats->add_data(\@GC_percent,\@length_per_sequence);
 	
 	my $mean_GC = sprintf('%.2f', $weighted_stats->mean()); 
 	my $sd_GC	= sprintf('%.2f', $weighted_stats->standard_deviation() );
 	
 	return ("$mean_GC +/- $sd_GC");
 	
 	
 }
 
 
  sub get_max_contig_size_fasta {
 	
 	my $SUB = 'get_max_contig_size_fasta';
 	
 	my @max_contig_size = `cat $FINAL_ASSEMBLY_FILE | $base_scripts_dir/edit_fasta.pl -summary 1 | cut -f 2`;
 	
 	if (scalar @max_contig_size == 1 ) {
 		print "Contig length could not be calculated in $FINAL_ASSEMBLY_FILE \n\n Will Return 0";
 		return '0';
 	}
 	
 	@max_contig_size = splice(@max_contig_size,1);
 	
 	if (scalar @max_contig_size != 1) {
 		print "max_contig_size array contains more than one value ...please check $SUB \n Will Return 0";
 		return '0';
 	}
 	else{
 		my $max_size = $max_contig_size[0];
 			$max_size =~s/[\s\n\r\t]//g;
 		  
 		return ($max_size);
 	}
 	
 	
 	
 	
 }


sub generate_XML_block_per_library {
	
	my $SUB = 'generate_XML_block_per_library';
	
	my $obj = shift @_;
	 
	 my $xml_block_per_lib;
	 
	 $xml_block_per_lib .= '<Illumina_Library>'."\n";
		$xml_block_per_lib .= "\t".'<name>'.$obj->get_library_name().'</name>'."\n";
	 	$xml_block_per_lib .= "\t".'<type>'.$obj->get_library_type().'</type>'."\n";
	 	$xml_block_per_lib .= "\t".'<read_length>'.$obj->get_run_config().'</read_length>'."\n";
	 	$xml_block_per_lib .= "\t".'<sample_id>'.$obj->get_source_dna_number().'</sample_id>'."\n";
	 	$xml_block_per_lib .= "\t".'<insert_size_bp>TO BE FILLED</insert_size_bp>'."\n";
	 	$xml_block_per_lib .= "\t".'<file_names>'.$obj->get_qualified_seq_unit_name().'</file_names>'."\n";
	 	$xml_block_per_lib .= "\t".'<data_quality>'.$obj->get_min_Q20_readlen_of_read_1_2().'</data_quality>'."\n";
	 	$xml_block_per_lib .= "\t".'<data_quality_comments>None</data_quality_comments>'."\n";
	 	if ( $obj->get_library_type() =~/CLIP/){
	 		$xml_block_per_lib .= "\t".'<data_screen_other>'."\n";
			$xml_block_per_lib .= "\t\t".	'1. 	Artifacts Cleaned using in-house tool duk'."\n\n";
			$xml_block_per_lib .= "\t\t".	 '2.  Linker Cleaning:'."\n";
			$xml_block_per_lib .= "\t\t".	'Long insert library(CLIP-PE) data was trimmed using in-house clippe removal script'."\n";
			$xml_block_per_lib .= "\t\t".	'Min length of the read pair : 45 bp'."\n";
			$xml_block_per_lib .= "\t\t".	'Site : CATG'."\n";
			$xml_block_per_lib .= "\t\t".	'Linker and Artifact removed data available : ***INSERT CLEANED FASTQ NAME HERE***'."\n";
			$xml_block_per_lib .= "\t\t".'</data_screen_other>'."\n";
	 	}
	 	else{
	 		$xml_block_per_lib .= "\t".'<data_screen_other>artifacts-removed</data_screen_other>'."\n";
	 	}
	 	
	 $xml_block_per_lib .= '</Illumina_Library>'."\n";
	 
	return $xml_block_per_lib;	 
}


sub create_QD_XML_BaseReport {
	
	my $SUB = 'create_QD_XML_BaseReport';
	
	my $obj = shift @_;
	
	my $XML_REPORT;
	
	$XML_REPORT .= '<?xml version="1.0" encoding="utf-8"?>'."\n";
	
	$XML_REPORT .= '<QDReport>'."\n";
	
#	$XML_REPORT .= "\t".'<Project_ID>'.$obj->get_source_dna_number().'</Project_ID>'."\n";
	$XML_REPORT .= "\t".'<PMO_Project_ID>'.$obj->get_pmo_project_id().'</PMO_Project_ID>'."\n";
	$XML_REPORT .= "\t".'<QD_dir>'.get_cwd().'</QD_dir>'."\n";
	$XML_REPORT .= "\t".'<Project_name>'.$obj->get_bio_classification_name().'</Project_name>'."\n";
	$XML_REPORT .= "\t".'<Sequencing_program>'.$obj->get_pmo_program_name().'</Sequencing_program>'."\n";
	$XML_REPORT .= "\t".'<Sequencing_program_year>'.$obj->get_pmo_program_year().'</Sequencing_program_year>'."\n";
	$XML_REPORT .= "\t".'<Scope_of_work>[****IMP OR FIN ****]</Scope_of_work>'."\n";
	
	
	$XML_REPORT .= $XML_BLOB;
	
	
	my $parsed_assembly_stats =  parse_assembly_stats($obj);
	
	
	$XML_REPORT .= '<Assembly_Stats>'."\n";
	$XML_REPORT .= "\t".'<scaffold_total>'.$parsed_assembly_stats->{'scaffold_total'}.'</scaffold_total>'."\n";
	$XML_REPORT .= "\t".'<contig_total>'.$parsed_assembly_stats->{'contig_total'}.'</contig_total>'."\n";
	$XML_REPORT .= "\t".'<genome_size_scaffold_mbp>'.$parsed_assembly_stats->{'genome_size_scaffold_mbp'}.'</genome_size_scaffold_mbp>'."\n";
	$XML_REPORT .= "\t".'<genome_size_contig_mbp>'.$parsed_assembly_stats->{'genome_size_contig_mbp'}.'</genome_size_contig_mbp>'."\n";
	$XML_REPORT .= "\t".'<scaffoldL50_mb>'.$parsed_assembly_stats->{'scaffoldL50_mb'}.'</scaffoldL50_mb>'."\n";
	$XML_REPORT .= "\t".'<contigL50_kb>'.$parsed_assembly_stats->{'contigL50_kb'}.'</contigL50_kb>'."\n";
	$XML_REPORT .= "\t".'<scaffold_over_50kb>'.$parsed_assembly_stats->{'scaffold_over_50kb'}.'</scaffold_over_50kb>'."\n";
	$XML_REPORT .= "\t".'<percent_genome_in_scaffolds>'.$parsed_assembly_stats->{'percent_genome_in_scaffolds'}.'</percent_genome_in_scaffolds>'."\n";
	$XML_REPORT .= "\t".'<gc_content>'.get_length_weighted_GC_percent_fasta().'</gc_content>'."\n";
	$XML_REPORT .= "\t".'<max_contig_bp>'.get_max_contig_size_fasta().'</max_contig_bp>'."\n";
	$XML_REPORT	.= "\t".'<Assembler_version>RunAllPathsLG r38445</Assembler_version>'."\n";
	
	$XML_REPORT .= '</Assembly_Stats>'."\n";
	
	$XML_REPORT .= '</QDReport>'."\n";
	
	my $QD_REPOR_XML_FILE_NAME = $obj->get_source_dna_number().'.QD.xml';
	open(OUT,">$QD_REPOR_XML_FILE_NAME") || die "Cant open the the file \n";
	print OUT $XML_REPORT ;
	
	$obj->logger()->info("Created XML report $QD_REPOR_XML_FILE_NAME");
}



sub parse_assembly_stats {
	
	my $SUB = 'parse_assembly_stats';
	
	my @text_from_script = `/house/groupdirs/QAQC/scripts/fasta_stats2.linux -n 1 $FINAL_ASSEMBLY_FILE`;
	
	my $parsed_assembly_stats;
	
	foreach my $line(@text_from_script){
		
		chomp $line;
		
		if($line=~/^Main genome scaffold total:\s+(\d+?)$/){
			$parsed_assembly_stats->{'scaffold_total'} = $1;
			print "Found Scaffold total : $1\n" if $VERBOSE;
		}
		
		if($line=~/^Main genome contig total:\s+(\d+?)$/){
			$parsed_assembly_stats->{'contig_total'} = $1;
			print "Found contig totoal : $1 \n" if $VERBOSE;
		}
		if($line=~/^Main genome scaffold sequence total:\s+(\d{1,3}\.\d{1,2}).*$/){
			$parsed_assembly_stats->{'genome_size_scaffold_mbp'} = $1;
			print "Found genome scaffold seq total : $1 \n" if $VERBOSE;
		}
		if($line=~/^Main genome contig sequence total:\s+(\d{1,3}\.\d{1,2}).*$/){
			$parsed_assembly_stats->{'genome_size_contig_mbp'} = $1;
			print "Found genome_size_contig_mbp total : $1 \n" if $VERBOSE;
		}
		if($line=~/^Main genome scaffold N\/L50:\s+.*\/(\d{1,3}\.\d{1,2}?).*$/){
			$parsed_assembly_stats->{'scaffoldL50_mb'} = $1;
			print "Found scaffoldL50_mb total : $1 \n" if $VERBOSE;
		}
		if($line=~/^Main genome contig N\/L50:\s+.*\/(\d{1,3}\.\d{1,2}?).*$/){
			$parsed_assembly_stats->{'contigL50_kb'} = $1;
			print "Found contigL50_kb total : $1 \n" if $VERBOSE;
		}
		if($line=~/^Number of scaffolds > 50 KB:\s+(\d+?)$/){
			$parsed_assembly_stats->{'scaffold_over_50kb'} = $1;
			print "Found scaffold_over_50kb : $1 \n" if $VERBOSE;
		}
		if($line=~/^\% main genome in scaffolds > 50 KB:\s+(\d{1,3}\.\d{1,2}?).*$/){
			$parsed_assembly_stats->{'percent_genome_in_scaffolds'} = $1;
			print "Found percent_genome_in_scaffolds : $1 \n" if $VERBOSE;
		}
		
		
		
	}
	
	return $parsed_assembly_stats;
}



sub create_QD_text_BaseReport {
	
	my $SUB = 'create_QD_text_BaseReport';
	
	my $obj = shift @_;
	
	my $REPORT;
	
	$REPORT .= 'QD/[IMP/FIN] HIGH QUALITY DRAFT ASSEMBLY REPORT -   '.$obj->get_source_dna_number().'  '.$obj->get_bio_classification_name()."\n\n\n";
	
	$REPORT .= 'Program Name and Year :  '.$obj->get_pmo_program_name()."\t".$obj->get_pmo_program_year()."\n";
	$REPORT .= 'PMO Project ID : '.$obj->get_pmo_project_id()."\n\n";
	
	
	$REPORT .= 'Sample ID'."\n";
	$REPORT .= $sample_id_per_library."\n\n";

	
	
	$REPORT .= '1) File Locations'."\n";
	$REPORT .= 'Assembly dir : <<FILL Assembly Dir>>'."\n";
	$REPORT .= 'QD Report : '.get_cwd()."\n";
	$REPORT .= 'Fastq Files : '.get_cwd()."\n";
	$REPORT .= 'Scaffolds : '.get_cwd()."\n";
	$REPORT .= 'XML Report  : '.get_cwd()."\n";
	$REPORT .= "\n\n";
	
	$REPORT .= '2) Raw Data Statistics : '."\n";
	$REPORT .= 'Fastq Files used '.	scalar @SEQ_UNITS."\n";
	$REPORT .= 'bioclassification, library, library type, Mbp, run configuration, file name'."\n";
	$REPORT .= $bioclassication_summary_line_per_seq_unit."\n";
	$REPORT .= "\n\n";
	
	
	$REPORT .= '3) Raw Reads'."\n";
	$REPORT .= 'Library 		Fastq			Raw_Reads'."\n";
	$REPORT .=  $read_Count_per_seq_unit."\n";
	$REPORT .= "\n\n";

	
	$REPORT .= '4) Quality of Data'."\n";
	$REPORT .= 'Run.lane.basecall    :   percent of scores >= Q30%   :  Q20 rl min(read 1/2)'."\n";
	$REPORT .= $read_quality_per_seq_unit."\n";
	$REPORT .= "\n\n";
		
	$REPORT .= '5) Read GC Content'."\n";
	$REPORT .= $GC_content_per_seq_unit."\n";
	$REPORT 	.= "\n\n";
	
	$REPORT .= '6) Assembly Statistics'."\n\n";
	$REPORT .= 'Max Scaffold Size '.get_max_contig_size_fasta().' bp'."\n";
	$REPORT .= `/house/groupdirs/QAQC/scripts/fasta_stats2.linux -n 1 $FINAL_ASSEMBLY_FILE`;
	$REPORT .= "\n\n";	
	
	$REPORT .= '7) Read Filtering and Trimming'."\n";
	$REPORT .= 
	qq( 
		1. 	Artifacts cleaned using in-house tool duk
		
		2.  Linker Cleaning:
			Long insert library(CLIP-PE) data was trimmed using in-house clippe removal script
			Min length of the read pair : 45 bp
			Site : CATG
		
			Linker & Artifact removed data available : <<<INSERT CLEANED FASTQ NAME HERE>>>>
	   );
	   
	   
	$REPORT .= "\n\n";
	
	$REPORT	.= '8) Insert Size : '."\n";
	$REPORT	.= $generic_info."\n";
	$REPORT .= "\n\n";
	
	$REPORT .= '9) Assembly GC %'."\n";
	$REPORT .= get_length_weighted_GC_percent_fasta(). ' mean % weighted by length of scaffolds'."\n";
	$REPORT .= "\n\n";
	
	$REPORT .= '10) Assembly Commands '."\n";
	$REPORT .= '<<<PASTE ALLPATHS CMDS>>>>'."\n\n\n";
	$REPORT .= "\n\n\n";
	
	
	$REPORT .=  '11) Release Details'."\n";
	$REPORT .=  'High Quality Draft done by '.get_username().' released at time '.get_timeStamp()."\n";
	$REPORT .= "\n\n";
	
	
	
	my $QD_REPOR_FILE_NAME = $obj->get_source_dna_number().'.QD.txt';
	open(OUT, ">$QD_REPOR_FILE_NAME") || die "Cant open the the file \n";
	print OUT $REPORT ;
	
	$obj->logger()->info("Created text based report $QD_REPOR_FILE_NAME");
	

}











