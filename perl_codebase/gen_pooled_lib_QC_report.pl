#!/jgi/tools/bin/perl

use strict;
use FindBin;
use lib "$FindBin::Bin/lib";
use JGI::QAQC::RQCdbAPI;
use JGI::QAQC::Utilities;
use JGI::QAQC::Plot;
use File::Path qw(mkpath rmtree);
use Log::Log4perl qw(:easy);
use Data::Dumper;
use Getopt::Long;
use PDF::API2;





our ( @library_names, @seq_units_by_user, $VERBOSE, $EMAIL, @SEQ_UNITS, $RNA_SEQ, $IS_RUN_PE);


## global variables
our ($LOGGER, $CWD);
our ($bioclassication_summary_line_per_lib);
our ($read_quality_per_lib);
our ($GC_content_per_lib);
our ($read_Count_per_lib);
our ($parent_library_read_count);
our ($parent_library_name);
our ($num_individual_libraries);
our ($bwa_dup_rate_per_libraries);
our ($contamination_per_seq_unit);
our ($num_reads_in_all_individual_libraries);
our ($pooled_libraries_read_count);


our ($read_Count_per_seq_unit);
our ($artifcats_mapping_reads_per_seq_unit);
our ($rRNA_mapping_reads_per_seq_unit);
our ($num_reads_mapped_transcriptome_per_seq_unit);
our ($num_reads_mapped_genome_per_seq_unit);
our ($insert_size_per_seq_unit);
our ($read_1_Q20_len_per_seq_unit);
our ($read_2_Q20_len_per_seq_unit);
our ($transcriptome_SE_percent_per_seq_unit);
our ($transcriptome_PE_percent_per_seq_unit);
our ($transcriptome_PE_agree_percent_per_seq_unit);
our ($transcriptome_PE_conflict_percent_per_seq_unit);



### array for plotting
our (@seq_unit_prefix);
our (@read_counts);
our (@total_artifacts_percentage);
our (@illumina_artifacts);
our (@jgi_contaminants);
our (@rRNA_contaminants);
our (@chart_keys);
our (@ecoli_contaminants);
our (@read_1_Q20);
our (@read_2_Q20);
our (@reads_map_genome);
our (@reads_map_transcriptome);
our	(@insert_sizes);
our (@insert_sizes_sd);




main();


sub main {
	
	my $SUB = 'main';
	
	$RNA_SEQ = 0;
	### Print the standard RQC header 
	get_standard_usage_header();
	
	## get the user arguments
	_get_user_arguments();
	
	## Instantiate the DB access
	my $obj = JGI::QAQC::RQCdbAPI->new();
	$obj->logger()->info('####Program Stated######');
	
# 	Get the final set of seq_unit_names to be included in QC report
# 	RESULTS available in SEQ_UNITS array
	my $SEQ_UNITS_REF = $obj->get_final_seq_units_set(\@library_names,\@seq_units_by_user);
	@SEQ_UNITS = @{$SEQ_UNITS_REF};
	
	$CWD = get_cwd();
	
# process the pooled library names from SEQ_UNIT array
	process_parent_libs($obj);
	
#  Setting the number of individual seq units
	$num_individual_libraries = scalar @SEQ_UNITS -1;  ##removing the one for UNKNOWN seq unit, there are UNKNOWN then adjust mannually
	

	my $count_num_seq_units = 0;

	##getting the details for each $seq_unit for the individual library that 
	## are part of pooled library
	foreach my $individual_seq_unit_name ( @SEQ_UNITS ) {
		
		
		
			my $guessed_pooled_library_sequnit_name = guess_pooled_library_sequnit_name($individual_seq_unit_name);
			my $pooled_library_read_count = $pooled_libraries_read_count->{$guessed_pooled_library_sequnit_name};
#			print "$guessed_pooled_library_sequnit_name \t $pooled_library_read_count";
		
		#	# Setting the right seq_unit for down_stream called to RQCdbAPI
			$obj->seq_unit_name($individual_seq_unit_name);
		
			###Now fetching the results for each seq_unit_name from RQC db
			$obj->get_lane_level_info();
			$obj->get_library_info_from_seq_unit_name();
			
			## also get the library detail from venonat
			$obj->get_library_info_from_venonat();
			
			print "Generating stats for individual library ", $obj->get_library_name(),"\t",$individual_seq_unit_name,"\n";
			
			##if the DNA is RNA type then activate the RNA_SEQ flag
			my $dna_type 			= $obj->get_source_dna_type() 	|| $obj->logger()->info("$SUB: DNA type can't be determined for $individual_seq_unit_name");
			chomp $dna_type;
			if ($dna_type=~/RNA/){
				$RNA_SEQ = 1;
			}
			
			$bioclassication_summary_line_per_lib   .= $obj->get_bio_classification_name().', '.$obj->get_library_name().', '.$obj->get_library_type().', '.$obj->get_lane_yield().' Mbp'.', '.$obj->get_run_config().', '.$obj->get_qualified_seq_unit_name()."\n";
			$GC_content_per_lib   .= $obj->get_library_name().' : '.$obj->get_qualified_seq_unit_name().' : '.$obj->get_GC_content()."\n";

			## calculating the percent of reads that fall in each individual lib's bin
			my $percent_reads_per_individual_lib = sprintf("%.02f", ($obj->get_total_read_count()/$pooled_library_read_count)*100) if $pooled_library_read_count;
			$read_Count_per_lib   .= $obj->get_library_name()."\t\t".$obj->get_qualified_seq_unit_name()."\t\t".commify($obj->get_total_read_count())."\t\t".$percent_reads_per_individual_lib.' %'."\n";
			$num_reads_in_all_individual_libraries = $num_reads_in_all_individual_libraries + $obj->get_total_read_count();
			$bwa_dup_rate_per_libraries .= $obj->get_library_name()."\t\t".$obj->get_qualified_seq_unit_name()."\t\t".$obj->get_bwa_algn_dup_percent().' % '."\n";
			
			my $total_percent_contamination = $obj->get_percent_illumina_artifacts()+$obj->get_percent_jgi_contaminants()+$obj->get_percent_rRNA_contaminants()+$obj->get_percent_reads_map_Ecoli()+$obj->get_percent_reads_map_fosmid();

	
			$contamination_per_seq_unit  .= sprintf("%s \t %s \t\t %3.2f %% \t %3.2f %% \t %3.2f %% \t %3.2f %% \t %3.2f %% \t %3.2f %% \n" , 
											    $obj->get_library_name(),
											    $obj->get_qualified_seq_unit_name(),
											    $total_percent_contamination,
											    $obj->get_percent_illumina_artifacts(),
											    $obj->get_percent_jgi_contaminants(),
											    $obj->get_percent_rRNA_contaminants(),
											    $obj->get_percent_reads_map_Ecoli(),
											    $obj->get_percent_reads_map_fosmid()
											    );
            
            $read_quality_per_lib  		.= sprintf("%s \t %s \t\t %d \t %d \n",
            									$obj->get_library_name(),
            									$obj->get_qualified_seq_unit_name(),
            									$obj->get_Q20_readlen_read_1(),
												$obj->get_Q20_readlen_read_2()
            									); 
			
			
			##pushing the stuff to arrays to create charts
			next if $individual_seq_unit_name=~/UNKNOWN/;
			push (@total_artifacts_percentage,$total_percent_contamination);
			push (@illumina_artifacts, $obj->get_percent_illumina_artifacts() );
			push (@jgi_contaminants, $obj->get_percent_jgi_contaminants() );
			push (@rRNA_contaminants, $obj->get_percent_rRNA_contaminants() );
			push (@ecoli_contaminants, $obj->get_percent_reads_map_Ecoli() );
			push (@read_counts, int($obj->get_total_read_count() ) ) ;
			push (@read_1_Q20, $obj->get_Q20_readlen_read_1());
			push (@read_2_Q20, $obj->get_Q20_readlen_read_2());
			
			
			my $prefix = $individual_seq_unit_name;
			$prefix =~/^([0-9]{4})\.([\d]{1})\.([0-9]{4})\.([AGCT]{6}).*/;
			$prefix = $obj->get_library_name().'.'.$2.'.'.$4;
			push (@seq_unit_prefix, $prefix);

			
		if($RNA_SEQ){
			RNA_SEQ_REPORT($obj);
		}
		
		}
		

		
		###generate pdf report
		#create_PDF_report();
	
		# Generate the QC report and email it , locally store it
		generateReport($obj);
		
		
		#generate the graphs
		render_graphs();
		
		$obj->logger()->info("############END###############");
}
	
	
	
sub render_graphs(){
	
	my $SUB  = 'render_graphs';
	
	
		my $plot = JGI::QAQC::Plot->new();
		
		
		#contamination plot
		my $data 				= [ \@total_artifacts_percentage, \@illumina_artifacts,  \@jgi_contaminants, \@rRNA_contaminants  ];
		my $series_name 		= [ '1. Total', '2.IL_artifacts', '3. jgi_contam', '4. rRNA' ];
		my $chart_label 		= 'Contamination Report';
		my $x_axis_label		= 'Fastq';
		my $y_axis_label		= 'Percent-Contamination';
		my $x_axis_tick_labels	= \@seq_unit_prefix;
		my $out_file			= 'contamination_report.png';
		$plot->plot_series_data_as_lines($data,$series_name,$chart_label,$x_axis_label,$y_axis_label,$x_axis_tick_labels,$out_file);
		
		
	
		#read counts
		my $data 				= [ \@read_counts  ];
		my $series_name 		= [ 'Read-Count' ];
		my $chart_label 		= 'Raw-Read-Counts';
		my $x_axis_label		= 'Fastq';
		my $y_axis_label		= '#-of-reads';
		my $x_axis_tick_labels	= \@seq_unit_prefix;
		my $out_file			= 'read_counts.png';
		$plot->plot_series_data_as_lines($data,$series_name,$chart_label,$x_axis_label,$y_axis_label,$x_axis_tick_labels,$out_file);
		
		
		
		
	
		#Q20 read lengths
		my $data 				= [ \@read_1_Q20 , \@read_2_Q20  ];
		my $series_name 		= [ '1. Read1', '2. Read2' ];
		my $chart_label 		= 'Phred Score(Q20) Read Length';
		my $x_axis_label		= 'Fastq';
		my $y_axis_label		= 'Q20-read-length';
		my $x_axis_tick_labels	= \@seq_unit_prefix;
		my $out_file			= 'read_q20_length.png';
		$plot->plot_series_data_as_lines($data,$series_name,$chart_label,$x_axis_label,$y_axis_label,$x_axis_tick_labels,$out_file);
		
	
	
		##RNA-Seq level
		
#		#genome/transcriptome mappable reads
#		my $data 				= [ \@reads_map_genome , \@reads_map_transcriptome  ];
#		my $series_name 		= [ '1. genome', '2. Transcriptome' ];
#		my $chart_label 		= 'Percentage reads map to genome and transcriptome';
#		my $x_axis_label		= 'Fastq';
#		my $y_axis_label		= 'reads-percent';
#		my $x_axis_tick_labels	= \@seq_unit_prefix;
#		my $out_file			= 'mapped_reads.png';
#		$plot->plot_series_data_as_lines($data,$series_name,$chart_label,$x_axis_label,$y_axis_label,$x_axis_tick_labels,$out_file);
#		
#		
#		
#		
#		#insert size
#		my $data 				= [ \@insert_sizes,  \@insert_sizes_sd  ];
#		my $series_name 		= [ '1. Insert_Size', '2. standard_deviation' ];
#		my $chart_label 		= 'Paired end reads : insert size';
#		my $x_axis_label		= 'Fastq';
#		my $y_axis_label		= 'insert-size';
#		my $x_axis_tick_labels	= \@seq_unit_prefix;
#		my $out_file			= 'insert-size.png';
#		$plot->plot_series_data_as_lines($data,$series_name,$chart_label,$x_axis_label,$y_axis_label,$x_axis_tick_labels,$out_file)
	
}


sub guess_pooled_library_sequnit_name(){
	my $individual_seq_unit_name  = shift @_;
	$individual_seq_unit_name =~/(^.*\.)(\D+?)(srf$)/g;
	my $guessed_pooled_library_sequnit_name = $1.$3;
	return $guessed_pooled_library_sequnit_name;
}





sub process_parent_libs {
	my $SUB = 'process_parent_libs';
	
	my $obj = shift @_;
	my @temp_SEQ_units;
	
	foreach my $individual_seq_unit_name ( @SEQ_UNITS ) {
		##every seq unit passing this if test will be a parent pooled library
		if ( $individual_seq_unit_name !~/[AGCT]|UNKNOWN/ ){
			my $pooled_library_seq_unit_name = $individual_seq_unit_name;
			set_parent_lib_report($obj, $pooled_library_seq_unit_name)
		}
		else {
			push(@temp_SEQ_units, $individual_seq_unit_name);
		}
	}
	### assigning the leftover sequnits to main arrary for library level stats
	@SEQ_UNITS = @temp_SEQ_units;
}


sub set_parent_lib_report {
	
	my $SUB = 'set_parent_lib_report';
	
	my $obj             = shift @_ || die "[$SUB] : Class object not passed \n";
	my $pooled_library_seq_unit_name = shift @_ || die "[$SUB] : parent lib name not passed \n";

		$obj->seq_unit_name($pooled_library_seq_unit_name);
		
		###Now fetching the results for PARENT library from RQC db
		$obj->get_lane_level_info();
		$obj->get_library_info_from_seq_unit_name();
		
		## also get the library detail from venonat
		$obj->get_library_info_from_venonat();
		
		
		##if the DNA is RNA type then activate the RNA_SEQ flag
		my $dna_type = $obj->get_source_dna_type() 	|| $obj->logger()->info("$SUB: DNA type can't be determined for $pooled_library_seq_unit_name");
		chomp $dna_type;
		if ($dna_type=~/RNA/){
			$RNA_SEQ = 1;
		}
		
		
		$bioclassication_summary_line_per_lib   .= $obj->get_bio_classification_name().', '.$obj->get_library_name().', '.$obj->get_library_type().', '.$obj->get_lane_yield().' Mbp'.', '.$obj->get_run_config().', '.$obj->get_qualified_seq_unit_name().' [Pooled Lib] '."\n";
		$GC_content_per_lib   .= $obj->get_library_name().' : '.$obj->get_qualified_seq_unit_name().' : '.$obj->get_GC_content().' [Pooled Lib] '."\n";
		$read_Count_per_lib   .= $obj->get_library_name()."\t\t".$obj->get_qualified_seq_unit_name()."\t\t".commify($obj->get_total_read_count()).' [Pooled Lib] '."\n";
		$bwa_dup_rate_per_libraries .= $obj->get_library_name()."\t\t".$obj->get_qualified_seq_unit_name()."\t\t".$obj->get_bwa_algn_dup_percent().' % [Pooled Lib]'."\n";
		my $total_percent_contamination = $obj->get_percent_illumina_artifacts()+$obj->get_percent_jgi_contaminants()+$obj->get_percent_rRNA_contaminants()+$obj->get_percent_reads_map_Ecoli();
		
		$contamination_per_seq_unit  .= sprintf("%s \t %s \t\t\t %3.2f %% \t %3.2f %% \t %3.2f %% \t %3.2f %% \t %3.2f %% \t %3.2f %% \n" , 
											    $obj->get_library_name(),
											    $obj->get_qualified_seq_unit_name(),
											    $total_percent_contamination,
											    $obj->get_percent_illumina_artifacts(),
											    $obj->get_percent_jgi_contaminants(),
											    $obj->get_percent_rRNA_contaminants(),
											    $obj->get_percent_reads_map_Ecoli(),
											    $obj->get_percent_reads_map_fosmid()
											    );
											    
		$read_quality_per_lib  		.= sprintf("%s \t %s \t\t\t %d \t %d  %s\n",
            									$obj->get_library_name(),
            									$obj->get_qualified_seq_unit_name(),
            									$obj->get_Q20_readlen_read_1(),
												$obj->get_Q20_readlen_read_2(),
												'[Pooled Lib]'
            									); 
												    
		##also set the parent library read count
		
		
		$pooled_libraries_read_count->{$pooled_library_seq_unit_name}= $obj->get_total_read_count();
		
		
		
	
		##pushing the stuff to arrays to create charts
			next if $pooled_library_seq_unit_name=~/UNKNOWN/;
			push (@total_artifacts_percentage,$total_percent_contamination);
			push (@illumina_artifacts, int($obj->get_percent_illumina_artifacts() ) );
			push (@jgi_contaminants, int($obj->get_percent_jgi_contaminants() ) );
			push (@rRNA_contaminants, int( $obj->get_percent_rRNA_contaminants() ) );
			push (@ecoli_contaminants, int ($obj->get_percent_reads_map_Ecoli() ) );
			push (@read_counts, int($obj->get_total_read_count() ) ) ;
			push (@read_1_Q20, $obj->get_Q20_readlen_read_1());
			push (@read_2_Q20, $obj->get_Q20_readlen_read_2());
			
			
			my $prefix = $pooled_library_seq_unit_name;
			$prefix =~/^([0-9]{4})\.([\d]{1})\.([0-9]{4})\.([AGCT]{6}).*/;
			$prefix = $obj->get_library_name().'.'.$2.'.'.$4;
			push (@seq_unit_prefix, $prefix);
		
		
		if ($RNA_SEQ){
			RNA_SEQ_REPORT($obj);
		}
		
		
		
		
		
		
			
}





	
sub _get_user_arguments {
	
	# need to improve this
	my $SUB = '_get_user_arguments';

	my %temp;
	
	if(scalar @ARGV ==  0 ) {
		usage();
		exit 2;
	}
	
	GetOptions(\%temp,
					"lib_name|lib:s@",
					"seq_units|su:s@",
                    "prefix|p:s",
                    "verbose|v!",
                    "email|e:i",
              );
    
    
    if ( defined $temp{'lib_name'} ){
    	@library_names = @ {$temp{'lib_name'} };
 
    }
    
    if ( defined $temp{'seq_units'} ){
    	@seq_units_by_user = @{ $temp{'seq_units'} };
    	
    }
	
	if ( defined $temp{'verbose'}){
		$VERBOSE = $temp{'verbose'};
	}
	else {
		$VERBOSE = 0;
	}
	
	if ( defined $temp{'email'} ){
		$EMAIL = $temp{'email'};
	}
	else {
		$EMAIL = 0;
	}
	
	if ( (scalar @library_names == 0) && ( scalar @seq_units_by_user == 0 )  ){
		die " You have not provided either the library name or seq_unit_name/s to generate a QC template on.";
	}
	
}



sub usage {
	
	my $SUB  ='usage';

print<<USAGE;
TBD
generate the Main pooled library QC report
USAGE
	
}




sub RNA_SEQ_REPORT{
	
	my $obj = shift @_;
	
	
	# check if run is PE or SE
   			 my $read_1_len = $obj->get_read_1_len();
    		 my $read_2_len = $obj->get_read_2_len();
    
    		if($read_1_len && ($read_2_len == 'NA')  ){
    			$IS_RUN_PE = 0
    		}
    		elsif($read_1_len == $read_2_len ){
    			$IS_RUN_PE = 1;
    		}
			
			
			$read_Count_per_seq_unit   	.=$obj->get_library_name()."\t\t".$obj->get_qualified_seq_unit_name()."\t\t".commify($obj->get_total_read_count())."\n";
			
			my $percent_num_reads_matched_adapter = sprintf('%0.2f', (  $obj->get_num_reads_matched_adapter()/$obj->get_total_read_count() )*100 ) if ($obj->get_total_read_count());
			$artifcats_mapping_reads_per_seq_unit		.=$obj->get_library_name()."\t\t".$obj->get_qualified_seq_unit_name()."\t\t".commify($obj->get_num_reads_matched_adapter() ) .' ('.$percent_num_reads_matched_adapter.' %)'."\n";
			
			
			my $percent_reads_matched_rRNA= sprintf('%0.2f', (  $obj->get_num_reads_matched_rRNA()/$obj->get_total_read_count() )*100 ) if ($obj->get_total_read_count());
			$rRNA_mapping_reads_per_seq_unit			.=$obj->get_library_name()."\t\t".$obj->get_qualified_seq_unit_name()."\t\t".commify($obj->get_num_reads_matched_rRNA() ).' ('.$percent_reads_matched_rRNA.' %)'."\n";
			
			
			my $percent_reads_matched_transcriptome = sprintf('%0.2f', (  $obj->get_num_reads_matched_transcriptome()/$obj->get_total_read_count() )*100 ) if ($obj->get_total_read_count());
			$num_reads_mapped_transcriptome_per_seq_unit .= $obj->get_library_name()."\t\t".$obj->get_qualified_seq_unit_name()."\t\t".commify($obj->get_num_reads_matched_transcriptome()). ' ('.$percent_reads_matched_transcriptome.' %)'."\n";	
			
			
			my $percent_reads_matched_genome = sprintf('%0.2f', (  $obj->get_num_reads_matched_genome()/$obj->get_total_read_count() )*100 ) if ($obj->get_total_read_count());
			$num_reads_mapped_genome_per_seq_unit .= $obj->get_library_name()."\t\t".$obj->get_qualified_seq_unit_name()."\t\t".commify($obj->get_num_reads_matched_genome()) .' ('.$percent_reads_matched_genome.' %)'."\n";
			
			
			$insert_size_per_seq_unit  .= $obj->get_library_name()."\t\t".$obj->get_qualified_seq_unit_name()."\t\t".commify($obj->get_insert_size_RNA_lib() )."\n" if $IS_RUN_PE;
			
			$read_1_Q20_len_per_seq_unit .= $obj->get_library_name()."\t\t".$obj->get_qualified_seq_unit_name()."\t\t".commify($obj->get_Q20_readlen_read_1() ).' bp'."\n";
			$read_2_Q20_len_per_seq_unit .= $obj->get_library_name()."\t\t".$obj->get_qualified_seq_unit_name()."\t\t".commify($obj->get_Q20_readlen_read_2() ).' bp'."\n" if $IS_RUN_PE;
			
			$transcriptome_SE_percent_per_seq_unit .= $obj->get_library_name()."\t\t".$obj->get_qualified_seq_unit_name()."\t\t".commify($obj->get_transcriptome_SE_percent() ) ."\n" ;
			
			$transcriptome_PE_percent_per_seq_unit .= $obj->get_library_name()."\t\t".$obj->get_qualified_seq_unit_name()."\t\t".commify($obj->get_transcriptome_PE_percent() ) ."\n" if $IS_RUN_PE ;
			
			$transcriptome_PE_agree_percent_per_seq_unit .= $obj->get_library_name()."\t\t".$obj->get_qualified_seq_unit_name()."\t\t".commify($obj->get_transcriptome_PE_agree_percent() ) ."\n" if $IS_RUN_PE;
			
			$transcriptome_PE_conflict_percent_per_seq_unit .= $obj->get_library_name()."\t\t".$obj->get_qualified_seq_unit_name()."\t\t".commify($obj->get_transcriptome_PE_conflict_percent() ) ."\n" if $IS_RUN_PE;
	
	
			push(@reads_map_genome, $percent_reads_matched_genome);
			push(@reads_map_transcriptome, $percent_reads_matched_transcriptome);
			
			my $insert_size = $obj->get_insert_size_RNA_lib();
			chomp $insert_size;
			$insert_size  =~/(\d+\.\d+).*?(\d+\.\d+).*/;
			$insert_size = sprintf('%d',$1);
			my $insert_size_sd = sprintf('%d',$2);
#			print " $insert_size \t $insert_size_sd \n";
			push(@insert_sizes,$insert_size );
			push(@insert_sizes_sd,$insert_size_sd );
			
}









sub generateReport {
	
	my $SUB = 'generateReport';
	
	my $obj = shift @_ || die " Class obj not passed \n\n";
	
	my $REPORT;
	

	
	$REPORT .= 'ACTION:'."\n\n\n";
	$REPORT .= '1) Comments: <PASS|FAIL>'."\n";
	$REPORT .= 'Pooled barcode library : '.$num_individual_libraries.' individual libraries found'."\n";
	$REPORT .= 'Individual Library Quality: '."\n";
	$REPORT .= 'Individual Library representation: '."\n";
	
	$REPORT .= "\n\n";
	
	
	$REPORT .= 'QC dir: ' .get_cwd()."\n";
	
	$REPORT .= '2) Lanes sequenced : 1'."\n";
	$REPORT .= 'bioclassification, library, library type, Mbp, run configuration, file name'."\n";
	$REPORT .= $bioclassication_summary_line_per_lib."\n\n\n";
	
	
	$REPORT .= '3) Read Level QC Metrics'."\n\n";
	$REPORT .= "\n";
	$REPORT .= '3.1) Read quality'; 
	$REPORT .= "\n";
	$REPORT .= '3.1.1) Quality score are';
	$REPORT .= "\n";
	$REPORT .= 'Run.lane.basecall : percent of scores >= Q30%';
	$REPORT .= "\n\n";
	$REPORT .= sprintf("%s \t %s \t\t\t %s \t %s \n",
					   'Library',
					   'Fastq',
					   'Q20_read1',
					   'Q20_read2',	
            		  ); 
	#$REPORT .= 'Library :             FastQ            :   %reads_Q30    :     Q20_read2     : Q20_read2'."\n";
	$REPORT .= $read_quality_per_lib."\n\n\n";
	
	
	$REPORT .= '3.2) Read GC Content'."\n\n";
	$REPORT .= 'Library'."\t\t".'FastQ'."\t\t".'GC_content'."\n";
	$REPORT .=  $GC_content_per_lib."\n\n\n";
	
	$REPORT .= '3.3) Uniqueness - 5% sampled 50bp read vs. read alignment using BWA'."\n";
	$REPORT .= $bwa_dup_rate_per_libraries."\n\n";
	
	
	$REPORT .= '3.5) Percent Contamination :'."\n";
	$REPORT .= 'Note: Total %reads contaminated can be > 100% as databases can have some overlap'."\n\n\n";
	
	$REPORT .= sprintf("%s \t %s \t\t\t\t %s \t %s \t %s \t %s \t\t %s \t\t %s \n" ,
						'library',
						'fastq',
						'total',
						'ILLM_artifacts',
						'jgi_contm',
						'rRNA',
						'ecoli',
						'fosmid' 
						);
	$REPORT .= $contamination_per_seq_unit."\n\n";
	
	
	$REPORT  .= '4) Demultiplex Numbers '."\n\n";
	$REPORT  .=  'Library'."\t\t".'FastQ'."\t\t\t".'Read Count'."\t\t\t".'Percent Reads'."\n\n";
	$REPORT  .= $read_Count_per_lib."\n\n";

	if ($RNA_SEQ){
			
		$REPORT .='5) RNA-Seq Basic Summary'."\n\n";
		$REPORT .= '5.1) 	Total Reads 	              	'."\n";
		$REPORT .= $read_Count_per_seq_unit."\n";
		$REPORT .= '5.2) 	#reads matching Adapter 	'."\n";
		$REPORT .= $artifcats_mapping_reads_per_seq_unit."\n";
		$REPORT .= '5.3) 	#reads matching rRNA	 					'."\n";
		$REPORT .= $rRNA_mapping_reads_per_seq_unit."\n";
		$REPORT .= '5.4) 	Transcriptome Mappable Reads	'."\n";
		$REPORT .= $num_reads_mapped_transcriptome_per_seq_unit."\n";
		$REPORT .= '5.5) 	Genome Mappable Reads			'."\n";
		$REPORT .= $num_reads_mapped_genome_per_seq_unit."\n";
		$REPORT .= '5.6) 	Insert Size  					'."\n" if $IS_RUN_PE;
		$REPORT .= $insert_size_per_seq_unit."\n" if $IS_RUN_PE;
		$REPORT .= '5.7) 	Q20 Read 1 length				'."\n";
		$REPORT .= $read_1_Q20_len_per_seq_unit."\n";
		$REPORT .= '5.8) 	Q20 Read 2 length				'."\n" if $IS_RUN_PE;
		$REPORT .= $read_2_Q20_len_per_seq_unit."\n" if $IS_RUN_PE;
		$REPORT .= '5.9) 	Single Reads %					'."\n";
		$REPORT .= $transcriptome_SE_percent_per_seq_unit."\n";
		$REPORT .= '5.10)	Paired End Reads % 				'."\n" if $IS_RUN_PE ; 
		$REPORT .= $transcriptome_PE_percent_per_seq_unit."\n" if $IS_RUN_PE ; 
		$REPORT .= '5.11) 	PE Reads agree % 				'."\n" if $IS_RUN_PE ; 
		$REPORT .= $transcriptome_PE_agree_percent_per_seq_unit."\n" if $IS_RUN_PE ; 
		$REPORT .= '5.12) 	PE Reads conflict % 			'."\n" if $IS_RUN_PE ; 
		$REPORT .= $transcriptome_PE_conflict_percent_per_seq_unit."\n" if $IS_RUN_PE ; 
	}
	
	

	my $filename = 'README.pooled_library_report';
	open(OUT,">$filename") || die "Error[$SUB] : Could not open the file $filename \n\n";
	print (OUT $REPORT);
	close(OUT);
	my $SUBJECT = 'Pooled Library '."$parent_library_name".' Base QC Template generated';
	send_text_email (guess_user_email(),guess_user_email(),$SUBJECT,$REPORT) if $EMAIL;
	$obj->logger()->info("\n\n============================================================");
	$obj->logger()->info("QC Base Template email sent to user email :", guess_user_email() ) if $EMAIL;
	$obj->logger()->info("Also generated the base QC template $filename in the current dir\n");
	
	
	
}


sub create_PDF_report(){
	
	my $SUB = 'create_PDF_report';
	
	my $pdf_report = PDF::API2->new();
	
	

	
	
	
	
	my $page = $pdf_report->page();
	$page->mediabox('A4'); 
	
		##preferences
	$pdf_report->preferences(
        -fullscreen => 1,
        -onecolumn => 1,
        -afterfullscreenoutlines => 1,
        -firstpage => [$page, -fit => 1],
    );
    
     $pdf_report->pageLabel(0, {
        -style => 'roman',
    });
	
	
	my $font = $pdf_report->corefont('Helvetica');
	
	#add text
	
	my $text = $page->text();
	$text->font($font,20);
	$text->translate(200,700);
	$text->text($bioclassication_summary_line_per_lib);
	
	$pdf_report->saveas('check.pdf');
	
		
	
	
	
	
}

	