#!/jgi/tools/bin/perl


use strict;
use POSIX 'isatty';
use Getopt::Long;
use Data::Dumper;

use Chart::Clicker;
use Chart::Clicker::Context;
use Chart::Clicker::Data::DataSet;
use Chart::Clicker::Data::Marker;
use Chart::Clicker::Data::Series;
use Chart::Clicker::Renderer::Bar;
use Geometry::Primitive::Rectangle;
use Graphics::Color::RGB;



use vars qw($OVERLAPPING_BASES $OVERLAP_READS_SUMMARY);

main();


sub main {
	
	check_input();
    read_fastq_and_compare_overlapping_bases();
}



sub check_input() {
	
	if ( isatty(*STDIN) ) {
		 usage();
	}
	
	my %input;
	
	GetOptions(\%input,
					"overlap_bases|ob=i",
					"percent_mismatches|pm:i",
					"verbose|v!",
					);
					
	if( defined $input{'overlap_bases'}  && ( $input{'overlap_bases'} >= 30  ) ) {
		$OVERLAPPING_BASES =  $input{'overlap_bases'};
	}
	else {
		usage();
	}
	
	
#	
#	if(defined $input{'percent_mismatches'}){
#		$PERCENT_MISMATCHES = $input{'percent_mismatches'};
#	}
#	else{
#		$PERCENT_MISMATCHES = 2;
#	}
	
	
	print "\n\n\n==============================================================\n";
	print "Options Selected \n";
	print "Num of overlapping bases to be tested \t $OVERLAPPING_BASES \n";
#	print "Percent Mismatch \t $PERCENT_MISMATCHES \n";
	print "==============================================================\n";
}

sub usage {
	
	
	my $SUB = 'usage';
	
	print qq (
	
	Options:

	-ob		:	number of bases to overlap from last part of read 1/2 [must be >=30 ]
	
	);
	print "\n";
	exit 2;
	
}


sub read_fastq_and_compare_overlapping_bases {
	
	my $SUB = 'read_fastq_and_compare_overlapping_bases';
	
	my $total_reads_read = 0;
	
	my $read_1_N	=	0;
	my $read_2_N	=	0;

		
	my $overlap_max_bp = 0;
	my $overlap_10_bp = 0;
	my $overlap_20_bp = 0;
	my $overlap_15_bp  =0;
	my $overlap_25_bp  =0;
	my $full_match = 0;
	my $no_match = 0;
	

	
	
	while(<STDIN>){
		chop $_;
		
		$total_reads_read++;
	
#		print "$total_reads_read \n";
		if( $total_reads_read % 10000 == 0) { print "Processed $total_reads_read Pairs \n";}
		
		my ($read_1_header, $read_1, $read_2_header, $read_2);
		
		if(/^$/) { next;}
		
		#get read 1
		if(/^@.*\/1/){
	
		
		$read_1_header = $_;
		$read_1 =<STDIN>;
		
		my $junk_line = <STDIN>;  #quality header
		$junk_line = <STDIN>  ; #quality line
		
		$read_2_header = <STDIN>;
			if( $read_2_header =~/^@.*\/2/ ){
				$read_2 =<STDIN>;
			
				$junk_line = <STDIN>;  #quality header read 2
				$junk_line = <STDIN>  ; #quality line read 2
		}
		
		
		
		
		##comparing we have the right pair of read 1 and 2
		# header should be same
		$read_1_header =~s/\/1//;
		$read_2_header =~s/\/2//;
		
		if ( $read_1_header == $read_2_header ){
			#we have a perfect read pair and now can compare their sequences for overlap
			
			my $read_1_last_part = substr($read_1,length($read_1)-$OVERLAPPING_BASES,$OVERLAPPING_BASES);
			chomp $read_1_last_part;
						
			my $read_2_last_part = substr($read_2,length($read_2)-$OVERLAPPING_BASES, $OVERLAPPING_BASES);
			chomp $read_2_last_part;
			
#			print "Read 1 header \t $read_1_header \n $read_1 \n $read_1_last_part \n\n";
#			print "Read 2 header \t $read_2_header \n $read_2 \n $read_2_last_part \n ";
			
			##reverse complementing the second read sub part for matching with the read 1 last 10-30 bp
			$read_2_last_part = reverse $read_2_last_part;
			$read_2_last_part =~tr/AGCTagct/TCGAtcga/;
#		
#			 print "Rev Compliment $read_2_last_part \n\n ";
			
			
			if( $read_1 =~/^.*[N]{10,}.*$/g   ) {
				$read_1_N++;
				next; 
			}
		
			if( $read_2 =~/^.*[N]{10,}.*$/g) {
				$read_2_N++;
				next;	
			}
			
			
			if($read_1_last_part eq $read_2_last_part) {
				$full_match++;
			}
			else{
				$no_match++;
			}
				 
			my @read_1_last_part_array = split('',$read_1_last_part);
			my @read_2_last_part_array = split('',$read_2_last_part);
				
#			print "Read 1 array @read_1_last_part_array \n";
#			print "Read 2 array @read_2_last_part_array \n";
				
			my $count_mismatches_last_30_bp =0;
			my $count_mismatches_last_25_bp = 0;
			my $count_mismatches_last_20_bp = 0;
			my $count_mismatches_last_15_bp = 0;
			my $count_mismatches_last_10_bp=0;
			
		
	
			
			for( my $i = 0 ; $i< scalar @read_1_last_part_array; $i++){
					
						##If the mismatch occurs
						if( $read_1_last_part_array[$i]  ne $read_2_last_part_array[$i]){
							
							$count_mismatches_last_30_bp++  if (($OVERLAPPING_BASES >= 30) && ( ($OVERLAPPING_BASES-($i) ) <= 30) );
							
							$count_mismatches_last_25_bp++  if (($OVERLAPPING_BASES >= 25) && ( ($OVERLAPPING_BASES-($i) ) <= 25) );
												
							$count_mismatches_last_20_bp++  if (($OVERLAPPING_BASES >= 20) && ( ($OVERLAPPING_BASES- ($i) ) <= 20) );
							
							$count_mismatches_last_15_bp++  if (($OVERLAPPING_BASES >= 15) && ( ($OVERLAPPING_BASES- ($i) ) <= 15) );					
				
							$count_mismatches_last_10_bp++ if ( ($OVERLAPPING_BASES >= 10)  && (   ($OVERLAPPING_BASES-($i) ) <= 10 ) ) ;		
						}
			} ## END FOR for looping over last part of read 1 and read 2 checking for overlap
			
			create_overlap_reads_summary_hash(50, $count_mismatches_last_30_bp, $count_mismatches_last_25_bp, $count_mismatches_last_20_bp, $count_mismatches_last_15_bp, $count_mismatches_last_10_bp , \@read_1_last_part_array,\@read_2_last_part_array );
			create_overlap_reads_summary_hash(40, $count_mismatches_last_30_bp, $count_mismatches_last_25_bp, $count_mismatches_last_20_bp, $count_mismatches_last_15_bp, $count_mismatches_last_10_bp ,\@read_1_last_part_array,\@read_2_last_part_array );						
			create_overlap_reads_summary_hash(30, $count_mismatches_last_30_bp, $count_mismatches_last_25_bp, $count_mismatches_last_20_bp, $count_mismatches_last_15_bp, $count_mismatches_last_10_bp , \@read_1_last_part_array,\@read_2_last_part_array );
			create_overlap_reads_summary_hash(20, $count_mismatches_last_30_bp, $count_mismatches_last_25_bp, $count_mismatches_last_20_bp, $count_mismatches_last_15_bp, $count_mismatches_last_10_bp  ,\@read_1_last_part_array,\@read_2_last_part_array);
			create_overlap_reads_summary_hash(15, $count_mismatches_last_30_bp, $count_mismatches_last_25_bp, $count_mismatches_last_20_bp, $count_mismatches_last_15_bp, $count_mismatches_last_10_bp, \@read_1_last_part_array,\@read_2_last_part_array );
			create_overlap_reads_summary_hash(10, $count_mismatches_last_30_bp, $count_mismatches_last_25_bp, $count_mismatches_last_20_bp, $count_mismatches_last_15_bp, $count_mismatches_last_10_bp, \@read_1_last_part_array,\@read_2_last_part_array );
			create_overlap_reads_summary_hash(5, $count_mismatches_last_30_bp, $count_mismatches_last_25_bp, $count_mismatches_last_20_bp, $count_mismatches_last_15_bp, $count_mismatches_last_10_bp,\@read_1_last_part_array,\@read_2_last_part_array  );
		
		} ## END IF where we check if right pair of read 1/2 is picked for comparison
	
		}
		
		else{
			die "First line of fastq not a read 1 as expected \n Line is $_\n\n";
		}

	} ## END While loop of reading the file from the STDIN
	
	print_results();
	
	print "\n\n\n";
	print "Read that matched in full \t $full_match \n";
	print "Reads that dint match in full \t $no_match \n\n";
	
}



sub print_results {
	
	
	my $SUB = 'print_results';
	
	my $out_file = "overlapping_reads.txt";
	
	open(OUT, ">$out_file" ) || die "Error[$SUB] File $out_file can't be opened \n";
	
	my @percent_mismatch = (5,10,15,20,30,40,50 );
	
	 my $cc = Chart::Clicker->new;	
	 
	 my $series = [];
	
	foreach my $percent_mismatch (@percent_mismatch) {
		
		print "With $percent_mismatch percent mismatch \n";
#		print OUT "With $percent_mismatch percent mismatch \n";
		
		if (defined $OVERLAP_READS_SUMMARY->{$percent_mismatch}){
			
			my @overlapping_bases;
			my @count;
			
			foreach my $overlapping_bp ( reverse sort keys %{$OVERLAP_READS_SUMMARY->{$percent_mismatch}} ){
				print "$overlapping_bp \t $OVERLAP_READS_SUMMARY->{$percent_mismatch}->{$overlapping_bp} \n";
				print OUT "$percent_mismatch \t $overlapping_bp \t $OVERLAP_READS_SUMMARY->{$percent_mismatch}->{$overlapping_bp} \n";
				
				}
		}
		
		else {
			print "Error[$SUB] Percent ";
			next ;
		}
	}
}
















sub create_overlap_reads_summary_hash {
	
	my $SUB = 'create_overlap_reads_summary_hash';
	
	my $PERCENT_MISMATCH = shift @_;
	
	my $count_mismatches_last_30_bp = shift @_;
	
	my $count_mismatches_last_25_bp = shift @_;
	
	my $count_mismatches_last_20_bp = shift @_;
	
	my $count_mismatches_last_15_bp = shift @_;
	
	my $count_mismatches_last_10_bp  = shift @_;
	
	my @read_1_last_part_array = @{shift @_} ;
	my @read_2_last_part_array = @{shift @_} ;
		
		#	Maintaing total count
			if (defined $OVERLAP_READS_SUMMARY->{$PERCENT_MISMATCH}->{'#pairs'}) {
				
				$OVERLAP_READS_SUMMARY->{$PERCENT_MISMATCH}->{'#pairs'}++
			}			
			
			else {
				$OVERLAP_READS_SUMMARY->{$PERCENT_MISMATCH}->{'#pairs'}=1;
			}
		
	

#		For last 30 bp
		if(    ( ($count_mismatches_last_30_bp/30 )* 100 ) <= $PERCENT_MISMATCH ){
							
			if (defined $OVERLAP_READS_SUMMARY->{$PERCENT_MISMATCH}->{'30bp'}) {
				
				$OVERLAP_READS_SUMMARY->{$PERCENT_MISMATCH}->{'30bp'}++
			}			
			
			else {
				$OVERLAP_READS_SUMMARY->{$PERCENT_MISMATCH}->{'30bp'}=1;
			}
		}
		
		

#		For last 25 bp
		if(    ( ($count_mismatches_last_25_bp/25 )* 100 ) <= $PERCENT_MISMATCH ){
							
			if (defined $OVERLAP_READS_SUMMARY->{$PERCENT_MISMATCH}->{'25bp'}) {
				
				$OVERLAP_READS_SUMMARY->{$PERCENT_MISMATCH}->{'25bp'}++
			}			
			
			else {
				$OVERLAP_READS_SUMMARY->{$PERCENT_MISMATCH}->{'25bp'}=1;
			}
		}
		
	
#		For last 20 bp
		if(    ( ($count_mismatches_last_20_bp/20 )* 100 ) <= $PERCENT_MISMATCH ){
			
						
			if (defined $OVERLAP_READS_SUMMARY->{$PERCENT_MISMATCH}->{'20bp'}) {
				$OVERLAP_READS_SUMMARY->{$PERCENT_MISMATCH}->{'20bp'}++
			}			
			else {
				$OVERLAP_READS_SUMMARY->{$PERCENT_MISMATCH}->{'20bp'}=1;
			}
		}
		

#		For last 15 bp
		if(    ( ($count_mismatches_last_15_bp/15 )* 100 ) <= $PERCENT_MISMATCH ){
							
			
			if (defined $OVERLAP_READS_SUMMARY->{$PERCENT_MISMATCH}->{'15bp'}) {
				$OVERLAP_READS_SUMMARY->{$PERCENT_MISMATCH}->{'15bp'}++
			}			
			else {
				$OVERLAP_READS_SUMMARY->{$PERCENT_MISMATCH}->{'15bp'}=1;
			}
		}
		


#		For last 10 bp
		if(    ( ($count_mismatches_last_10_bp/10 )* 100 ) <= $PERCENT_MISMATCH ){
			
			if (defined $OVERLAP_READS_SUMMARY->{$PERCENT_MISMATCH}->{'10bp'}) {
				$OVERLAP_READS_SUMMARY->{$PERCENT_MISMATCH}->{'10bp'}++
			}			
			else {
				$OVERLAP_READS_SUMMARY->{$PERCENT_MISMATCH}->{'10bp'}=1;
			}
		}
	
}







