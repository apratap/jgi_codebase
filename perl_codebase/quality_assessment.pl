#!/jgi/tools/bin/perl


use strict;
use Data::Dumper;
use Statistics::Descriptive::Discrete;




main();


sub main {

    read_parse_input();
    
#	getAVGquality();
}



sub read_parse_input {

    my $SUB='read_parse_input';
    
   
    my $count_proper_pair = 0;

	my $reads;
	
    while(<STDIN>) {
       
	my $header= $_;
	chomp $header;
	my $seq=<STDIN>;
	chomp $seq;
	my $qual_header=<STDIN>;
	chomp $qual_header;
	my $quality = <STDIN>;
	chomp $quality;
	
	 my @temp = split(':',$header);
	 my $X_position = $temp[3];
	 my $Y_position = $temp[4];
	 $Y_position =~s/\/.*$//;
	
		
	my ($percent_GC, $percent_AT, $percent_N) =0;	
	

	
	 ($percent_GC, $percent_AT, $percent_N)=getGCpercent($seq);

	
	
	my ($mean_phred_quality, $sd_phred_quality, $median_phred_quality) =0;
	($mean_phred_quality, $sd_phred_quality, $median_phred_quality) = getAVGquality($quality);
	
	print "$X_position \t $Y_position \t $percent_GC \t $percent_AT \t $percent_N \t $mean_phred_quality \n";
	
	} ## END while loop



}





sub getGCpercent {
	
	my $SUB='getGCpercent';
	
	my $seq = shift @_ || die 'Error[$SUB] Read sequence not passed \n';
	
	my $length = length($seq);
	
	my $count_G = 0;
    my $count_C = 0;
    my $count_A = 0;
    my $count_T = 0;
    my $count_N = 0;
	

	$count_G++ while( $seq=~/G/g);
	$count_C++ while( $seq=~/C/g);
	
	$count_A++ while( $seq=~/A/g);
	$count_T++ while( $seq=~/T/g);
	
	
	$count_N++ while( $seq=~/N/g);
	
	
	my $count_GC = $count_C + $count_G;
	my $count_AT = $count_A + $count_T;
	
	my $percent_GC =  sprintf('%.2f', $count_GC/$length);
	my $percent_AT =  sprintf('%.2f', $count_AT/$length);
	my $percent_N  =  sprintf('%.4f', $count_N/$length);
	
	return ($percent_GC, $percent_AT, $percent_N);
}



sub getAVGquality {
	
	my $SUB = 'getAVGquality';

	my $read_quality_illumina = shift @_;
	
	my @read_quality_ASCII = unpack("C*", $read_quality_illumina); 
	
#	print "@read_quality_ASCII\n";
	
	my $stat = Statistics::Descriptive::Discrete->new();
    $stat->add_data(@read_quality_ASCII);
    my $mean_phred_quality = ( sprintf("%.2f",$stat->mean()) - 64);
    my $sd_phred_quality  = sprintf("%.2f",$stat->standard_deviation());
    my $median_phred_quality  = ( sprintf("%.2f",$stat->median()) - 64);
	
	return($mean_phred_quality, $sd_phred_quality, $median_phred_quality);
	
}



#  Inefficient way of counting the AGCT in a nucleotide sequence
#sub getGCpercent {
#	
#	
#	my $SUB='getGCpercent';
#	
#	my $read = shift @_ || die 'Error[$SUB] Read sequence not passed \n';
#	
#	my $length = length($read);
#	
#	
#	my $count_G = 0;
#    my $count_C = 0;
#    my $count_A = 0;
#    my $count_T = 0;
#    my $count_N = 0;
#	
#	my @nucleotides = split('',$read);
#	
#	foreach my $nucleotide ( @nucleotides ) {
#
#		
#		$count_G++ if ($nucleotide=~/G/);
#		$count_C++ if ($nucleotide=~/C/);
#	
#		$count_A++ if ($nucleotide=~/A/);
#		$count_T++ if ($nucleotide=~/T/);
#	
#	
#		$count_N++ if ($nucleotide=~/N/);
#	
#	}
#	
#	
#	my $count_GC = $count_C + $count_G;
#	my $count_AT = $count_A + $count_T;
#	
#	my $percent_GC =  sprintf('%.2f', $count_GC/$length);
#	my $percent_AT =  sprintf('%.2f', $count_AT/$length);
#	my $percent_N  =  sprintf('%.4f', $count_N/$length);
#	
#	return ($percent_GC, $percent_AT, $percent_N);
#}
