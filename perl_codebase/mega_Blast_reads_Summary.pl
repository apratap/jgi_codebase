#!/jgi/tools/bin/perl

use strict;

use Statistics::Descriptive::Discrete;
use FindBin;
use lib "$FindBin::Bin/lib";
use JGI::QAQC::Utilities;
use Bio::DB::GenBank;
use Getopt::Long;
use lib "/house/homedirs/a/apratap/lib/perl5/site_perl/5.10.1";
use Bio::LITE::Taxonomy::NCBI::Gi2taxid qw/new_dict/;
use Bio::LITE::Taxonomy::NCBI;
use Data::Dumper;
use Log::Log4perl qw(:easy);

use GraphViz::Data::Grapher;

use vars qw($LOGGER $VERBOSE $TAXA_LEVEL $TOTAL_READS $UNIQ_READS $DUP_READS $TOTAL_NUM_READS_BLASTED);

#use Text::Table;

main();

sub main {

	config_logger();
	$LOGGER = get_logger();

	#	get/set user args
	_parse_user_arguments();

	print "\n\n\n";

	#	##get the names of uniq tophits reads across all databases
	my $uniq_tophits_file = unify_read_tophits();
	print "\n\n\n";

##	create a new file for unique contigs with taxa information
##	The header of the file will have the column names
	my ($annotated_reads_file, $read_hash) = properly_annotate_reads($uniq_tophits_file);


	print  "No of items in read_hash \t\t",  scalar(keys %{$read_hash}),"\n\n";


##	create a tree for taxonomy with each taxa having the contigs which belong to it and
##  	and count of total contigs that hit each taxa
 	my $TAX_TREE = build_read_tree_based_on_taxonaomy($annotated_reads_file);
	
	
	analyse_read_tree($TAX_TREE, $read_hash);

}

sub _parse_user_arguments {

	my $SUB = '_parse_user_arguments';
	my %temp;

	if ( scalar @ARGV == 0 ) {
		usage();
		exit 3;
	}

	GetOptions( \%temp, 
					 "total_reads_blasted|trb=i",
					 "taxa_level|tl=i", 
					 "verbose|v!", 
			   );


	unless( $temp{'total_reads_blasted'}  ){
		
		$LOGGER->fatal("Must specify total num of reads that were blasted\n");
		die "Will die\n";
		
	}

	$VERBOSE    = $temp{'verbose'} || 0;
	$TAXA_LEVEL = $temp{'taxa_level'} || 0;
	$TOTAL_NUM_READS_BLASTED = $temp{'total_reads_blasted'};

}    ### End

sub usage {

	my $SUB = 'usage';

	print <<USAGE

$0 still to write

USAGE

}

###For each contig will return a hash with lineage and specie name
### Input is read from the $uniq_tophits_file
### output is a hash with key contig name and references to lineage

sub properly_annotate_reads {

	my $SUB = 'properly_annotate_reads';

	my $uniq_tophits_file = shift @_;
	my $force_run = shift @_ || 0;

	### Read the file created by the SUB : unify_tophits
	open( IN, "< $uniq_tophits_file" )
	  || die "Error[$SUB} file $uniq_tophits_file can't be opened \n";

	### Name of the output file
	my $out_file = 'unique_annotated_reads_tophits_across_all_dbases.txt';

#	my %annotated_contigs will store all contig names who have been previously annotated in $out_file
#	they will be skipped during the re-run
	my %annotated_reads;

	# 	Main contig hash storing all the info per contig
	my $read_hash;

	#	Checking if the expected output already exists in full in the file
	if ( -e $out_file ) {

		print"Expected outfile $out_file of this method $SUB already exists .. \nWill skip running $SUB for contigs already annotated and present in the file \n\n";
		open( READIN, "< $out_file " )
		  || die "Error[$SUB] out_file $out_file can't be opened \n ";

		while (<READIN>) {
			chomp $_;
			next if (/^contigs.*$/);
			my @temp = split( ' ', $_ );
			my $read_name = $temp[0];
			my $organism_name = $temp[1];

			$read_name =~ s/\s+?//g;
			$organism_name =~s/\s+?//g;

			$annotated_reads{$read_name} = $organism_name;

		}

		open( OUT, ">> $out_file" )
		  || die "Could not open file $out_file $! \n\n";

	}
	else {

		open( OUT, "> $out_file" )
		  || die "Could not open file $out_file $! \n\n";

	  #		Print the header of outfile if this is the first time creating the file
		my @HEADER_WORDS = (
			"reads",   "organism", "lineage",   "NCBI_ACCESSION",
			"NCBI_GI", "Escore",   "match_len", "percent_identity"
		);
		my $HEADER = join( "\t", @HEADER_WORDS );
		print OUT $HEADER;
	}

	print
"Creating the first method handle to query genbank and access lineage by accession number...";
	##First Method : Slow but needed when NCBI Accession is only given
	###db handle to extract the taxanomy when the NCBI_Accession is present
	my $gbh = Bio::DB::GenBank->new();
	print "Done\n";

	print
"Creating the second method to give lineage based on Bio::Lite::Taxonomy ....";
	##Second Method :: WAY FASTER
	## To get taxonomy when the NCBI_GI  is given
	my $taxNCBI = Bio::LITE::Taxonomy::NCBI->new(
		db    => "NCBI",
		nodes => '/house/homedirs/a/apratap/playground/NCBI_taxanomy/nodes.dmp',
		names => '/house/homedirs/a/apratap/playground/NCBI_taxanomy/names.dmp',
		dict =>
		  "/house/homedirs/a/apratap/playground/NCBI_taxanomy/gi_taxid_nucl.bin"
	);
	print "Done\n";

	#  	Initializing few counters
	my $reads_allready_annotated            = 0;
	my $reads_annotated_from_NCBI_GI        = 0;
	my $reads_annotated_from_NCBI_Accession = 0;

	my $line = 0;
	while (<IN>) {

		$line++;
		print "line $line read\n";

		chomp $_;

		my ( $read_name, $blast_hit, $Escore, $match_len, $percent_identity,
			$contig_length )
		  = split( ' ', $_ );

		my ( $NCBI_ACCESSION, $NCBI_GI, @LINEAGE );

		$read_name =~ s/\s+?//g;

		#		##		Prepare a single contig hash with all the info for that contig
		if ( defined $read_hash->{$read_name} ) {
##			Possible problem as we expect to see each contig only once
			print "Error[$SUB] Read name : $read_name seen already once\n\n";
			die " ";
		}
		else {

			#			print "Storing contig $read_name in hash  \n";
##			store the contig info
			
			$read_hash->{$read_name}->{'Escore'}       = $Escore;
			$read_hash->{$read_name}->{'match_length'} = $match_len;
			$read_hash->{$read_name}->{'percent_identity'} =$percent_identity;

		}

		###extracting the GI and NCBI Accession from blast hit
		if ( defined $annotated_reads{$read_name} ) {

			print "Contig $read_name already annotated ..Will skip\n";
			$reads_allready_annotated++;

#			since the contig is already annotated the organism name will be pushed to contig_hash
#			as the routine will jump and not do calculate any lineage
			$read_hash->{$read_name}->{'organism'} =
			  $annotated_reads{$read_name};

			next;
		}

		#		If blast hit had NCBI GI
		if ( $blast_hit =~ /^gi\|(.+?)\|(.+?)\|(.+?)\|.*$/ ) {

			$NCBI_GI        = $1;
			$NCBI_ACCESSION = $3;

			print
"Fetching the lineage for Read $read_name with NCBI GI : $NCBI_GI\n";
			##Using second method which is faster
			my @classification = $taxNCBI->get_taxonomy_from_gi($NCBI_GI);

	  #			# no need to reverse the classification to get lineage for this method
			@LINEAGE = @classification;

			$reads_annotated_from_NCBI_GI++;
		}

		#		If blast hit had NCBI accession
		elsif ( $blast_hit =~ /^(.+?)\.(.+?)\.(.+?)__.*$/ ) {

			$NCBI_ACCESSION = $1;
			$NCBI_GI        = 'NA';

			print"Fetching the lineage for Read $read_name with NCBI Accession : $NCBI_ACCESSION \n";
			## Using the first method to get taxanomy when GI is not present
			my $seq            = $gbh->get_Seq_by_acc($NCBI_ACCESSION);
			my $org            = $seq->species;
			my @classification = $org->classification;

	 #		reverse the classification to get the lineage begining from superkingdom
			@LINEAGE = reverse(@classification);

			$reads_annotated_from_NCBI_Accession++;
		}
		else {
			print "Warn [$SUB] Could not extract GI or Accession \n $blast_hit \n\n";
			next;
		}

		#		fix the spaces in lineage
		@LINEAGE = fix_spaces_in_lineage_terms( \@LINEAGE );

		#		Taking out the organism name from the lineage.
		my $organism_name = pop(@LINEAGE);

  	#	setting up the organism name for contigs which were not previously annotated
		$read_hash->{$read_name}->{'organism'} = $organism_name;

##printing each annotated contig
		print OUT "$read_name\t$organism_name\t@LINEAGE\t$NCBI_ACCESSION\t$NCBI_GI\t$Escore\t$match_len\t$percent_identity\n";

	}

	print "\n\n\n";
	print "Annotation Results \n";
	print "Total contigs pre-annotated \t\t $reads_allready_annotated \n";
	print "Total Contigs annotated using NCBI GI \t\t $reads_annotated_from_NCBI_GI \n";
	print "Total Contigs annotated using NCBI Accession \t\t $reads_annotated_from_NCBI_Accession \n\n\n";

	close(OUT);
	close(IN);

	return ( $out_file, $read_hash );

}

### To fix the spaces in each level of lineage
### The return array will have _ for spaces in the each value of lineage array
sub fix_spaces_in_lineage_terms {

	my $SUB = 'fix_spaces_in_lineage_terms';

	my @array = @{ shift @_ };

	#	print "IN SUB @array\n";

	my @return_array;

	foreach my $value (@array) {
		$value =~ s/\s/_/g;
		push( @return_array, $value );
	}

	#	print "Return from SUB @return_array \n";
	return @return_array;

}

##Build a tree that will have all the details about each level of lineage and which contig falls into it
sub build_read_tree_based_on_taxonaomy {

	my $SUB = 'build_contig_tree_based_on_taxonaomy';

	my $annotated_reads_file = shift @_;

	my $TAXA_TREE;

	open( READIN, "< $annotated_reads_file " ) || die "Error[$SUB] Annotated Contigs file $annotated_reads_file can't be opened \n";

	while (<READIN>) {

		chomp $_;

		##Skip the header line
		next if (/^contig.*$/);

		my @each_read_hit = split( "\t", $_ );

		my $read_name = $each_read_hit[0];
		$read_name =~ s/\s+?//;
		
		my @lineage = split( ' ', $each_read_hit[2] );

		$TAXA_TREE = build_lineage_tree( \@lineage, \$read_name, $TAXA_TREE );

	}

	return $TAXA_TREE;

}

sub build_lineage_tree {

		
	my @lineage 		=	@{shift @_} ;
	my $contig_name		=	${shift @_} ;		
	my $phylo_tree		= 	shift @_;
	
	my $depth=0;
	
	next if(scalar @lineage == '0');
	
	
   #depth=0
	if (defined $phylo_tree->{$lineage[$depth]} ) {
		$phylo_tree->{$lineage[$depth]}->{'count'}++;
	}
	else {
		$phylo_tree->{$lineage[$depth]}->{'count'}=1;
	}
	
	##depth=1
	$depth++;
	if (defined $phylo_tree->{$lineage[$depth-1]}->{$lineage[$depth]} ) {
		
		$phylo_tree->{$lineage[$depth-1]}->{$lineage[$depth]}->{'count'}++;
	}
	else{
		
		$phylo_tree->{$lineage[$depth-1]}->{$lineage[$depth]}->{'count'}=1;
	
	}
		
		
	#depth=2
	$depth++;
	if (defined $phylo_tree->{$lineage[$depth-2]}->{$lineage[$depth-1]}->{$lineage[$depth]} ) {
		
		$phylo_tree->{$lineage[$depth-2]}->{$lineage[$depth-1]}->{$lineage[$depth]} ->{'count'}++;
		
	}
	else{
		
		$phylo_tree->{$lineage[$depth-2]}->{$lineage[$depth-1]}->{$lineage[$depth]} ->{'count'}=1;
	}
	
	#depth=3
	$depth++;
	if (defined $phylo_tree->{$lineage[$depth-3]}->{$lineage[$depth-2]}->{$lineage[$depth-1]}->{$lineage[$depth]}) {
		
		$phylo_tree->{$lineage[$depth-3]}->{$lineage[$depth-2]}->{$lineage[$depth-1]}->{$lineage[$depth]}->{'count'}++;
	
	}
	else{
		
		$phylo_tree->{$lineage[$depth-3]}->{$lineage[$depth-2]}->{$lineage[$depth-1]}->{$lineage[$depth]}->{'count'}=1;
	
	}
	
	
	#depth=4
	$depth++;
	if (defined $phylo_tree->{$lineage[$depth-4]}->{$lineage[$depth-3]}->{$lineage[$depth-2]}->{$lineage[$depth-1]}->{$lineage[$depth-1]} ) {
		
		 $phylo_tree->{$lineage[$depth-4]}->{$lineage[$depth-3]}->{$lineage[$depth-2]}->{$lineage[$depth-1]}->{$lineage[$depth-1]}->{'count'}++; 
	}
	else{
		
		$phylo_tree->{$lineage[$depth-4]}->{$lineage[$depth-3]}->{$lineage[$depth-2]}->{$lineage[$depth-1]}->{$lineage[$depth-1]}->{'count'}=1;
		
	}
	
	return $phylo_tree;
}

sub analyse_read_tree {
	
	 
	 my $SUB = 'analyse_read_tree';
	 
	 
	 my $TAXA_TREE			= 	shift @_;
	 my $contig_hash		=	shift @_;
	 my $level				=	$TAXA_LEVEL;
	 
	 
	 foreach my $taxa_level0 ( keys %{$TAXA_TREE} ){
	 
	 	
#	 	for level 0
		next if ( $TAXA_TREE->{$taxa_level0}->{'count'} == 1);
		my $read_percent = sprintf('%.2f', ($TAXA_TREE->{$taxa_level0}->{'count'}/$TOTAL_NUM_READS_BLASTED)*100 );
		print "\n\n $taxa_level0 \t $read_percent \n"  if ($level >= 0 );




# 		for level 1
			foreach my $taxa_level1 ( keys %{ $TAXA_TREE->{$taxa_level0} }  ) {
				
				next if ( $taxa_level1 =~/(reads)|(count)|(\s+?)|(' ')|('')|(undef)/);
				next if ($TAXA_TREE->{$taxa_level0}->{$taxa_level1}->{'count'} == 1) ;
				my $read_percent = sprintf('%.2f', ($TAXA_TREE->{$taxa_level0}->{$taxa_level1}->{'count'}/$TOTAL_NUM_READS_BLASTED)*100 );
				print "\t\t\t $taxa_level1 \t\t\t $read_percent \n " if ($level >=1 );
				
			 	


# 		for level 2
				foreach my $taxa_level2 ( keys %{ $TAXA_TREE->{$taxa_level0}->{$taxa_level1} }  ) {
				
					next if ( $taxa_level2 =~/(reads)|(count)|(\s+?)|(' ')|('')|(undef)/);
					next if ($TAXA_TREE->{$taxa_level0}->{$taxa_level1}->{$taxa_level2}->{'count'} == 1) ;
					my $read_percent = sprintf('%.2f', ($TAXA_TREE->{$taxa_level0}->{$taxa_level1}->{$taxa_level2}->{'count'}/$TOTAL_NUM_READS_BLASTED)*100 );
					print "\t\t\t\t\t\t$taxa_level2 \t\t\t $read_percent \n " if ($level >=2  );
				
				}	##END level 2
	 
			} ## END level 1
	 
	 } ## END level 0
	

}

sub unify_read_tophits {

	my $SUB = 'unify_read_tophits';

### open file for writing out contigs both uniq and non unique

	my $uniq_reads_file = 'unique_reads_tophits_across_all_dbases.txt';
	open( OUT_UNIQ, ">$uniq_reads_file" )
	  || die "Cant open the file $uniq_reads_file\n";

	my $non_uniq_reads_file = 'non_unique_reads_tophits_across_all_dbases.txt';
	open( OUT_NON_UNIQ, ">$non_uniq_reads_file" )
	  || die "Cant open the file $non_uniq_reads_file\n";

	my $uniq_reads;
	my @duplicate_reads;

	my @files = `ls *tophit`;

	foreach my $file (@files) {

		print "Reading reads from file \t $file\n";
		### reading each file and unifying read level info
		open( IN, "<$file" ) || die "Can't open file $file\n";

		#				print "Reading reads from file $file \n;

		while (<IN>) {
			chomp $_;
			my $read_line = $_;
			my @read_line_split = split( ' ', $read_line );

			my $read_name = $read_line_split[0];

			if ( defined $uniq_reads->{$read_name} ) {

				print OUT_NON_UNIQ "$read_line\n";
				push( @duplicate_reads, $read_name );
			}
			else {
				print OUT_UNIQ "$read_line\n";
				$uniq_reads->{$read_name} = 0;
			}
		}

	}

	my $total_uniq_reads = scalar keys %{$uniq_reads};
	my $dup_reads        = scalar @duplicate_reads;

	print "Total Uniq Reads \t $total_uniq_reads \n";
	print "Duplicate Contigs \t $dup_reads \n";

	$UNIQ_READS 	=	$total_uniq_reads;
	$DUP_READS		=	$dup_reads;
	
	$TOTAL_READS = $total_uniq_reads + $dup_reads;
	
	return $uniq_reads_file;

}


