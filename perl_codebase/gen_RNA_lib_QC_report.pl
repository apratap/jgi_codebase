#!/jgi/tools/bin/perl

use strict;
use FindBin;
use lib "$FindBin::Bin/lib";
use JGI::QAQC::RQCdbAPI;
use JGI::QAQC::Utilities;
use Data::Dumper;
use Getopt::Long;


our ($IS_RUN_PE, @SEQ_UNITS , $VERBOSE, @seq_units_by_user, $EMAIL, @library_names, $STDOUT );

our ($bioclassication_summary_line_per_seq_unit);
our ($read_quality_per_seq_unit);
our ($GC_content_per_seq_unit);
our ($read_Count_per_seq_unit);
our ($bwa_dup_rate_per_seq_unit);
our ($artifcats_mapping_reads_per_seq_unit);
our ($nucleotide_content_first_12_cycles);
our ($nucleotide_content_post_12_cycles);
our($rRNA_mapping_reads_per_seq_unit);
our ($num_reads_mapped_transcriptome_per_seq_unit);
our ($num_reads_mapped_genome_per_seq_unit);
our($insert_size_per_seq_unit);
our($read_1_Q20_len_per_seq_unit);
our($read_2_Q20_len_per_seq_unit);
our($transcriptome_SE_percent_per_seq_unit);
our($transcriptome_PE_percent_per_seq_unit);
our($transcriptome_PE_agree_percent_per_seq_unit);
our($transcriptome_PE_conflict_percent_per_seq_unit);
our ($tagdust_found_artifcats_per_seq_unit);




main();



sub main {
	
	
	my $SUB = 'main';
	
	### Print the standard RQC header 
	get_standard_usage_header();
	
	
	## get the user arguments
	_get_user_arguments();
	
	
	## Instantiate the DB access
	my $obj = JGI::QAQC::RQCdbAPI->new();
	
	$obj->logger()->info('####Program Stated######');

# 	Get the final set of seq_unit_names to be included in QC report
# 	RESULTS available in SEQ_UNITS array
# 	Get the final set of seq_unit_names to be included in QC report
# 	RESULTS available in SEQ_UNITS array
	my $SEQ_UNITS_REF = $obj->get_final_seq_units_set(\@library_names,\@seq_units_by_user);
	@SEQ_UNITS = @{$SEQ_UNITS_REF};

	
	
	#getting the details for each $seq_unit for the individual library that 
		foreach my $seq_unit_name ( @SEQ_UNITS ) {
			
			## Setting the right seq_unit for down_stream called to RQCdbAPI
			$obj->seq_unit_name($seq_unit_name);
			
			
			###Now fetching the results for each seq_unit_name from RQC db
			$obj->get_lane_level_info();
			$obj->get_library_info_from_seq_unit_name();
			
			## also get the library detail from venonat
			$obj->get_library_info_from_venonat();
			
	
#			# check if run is PE or SE
   			 my $read_1_len = $obj->get_read_1_len();
    		 my $read_2_len = $obj->get_read_2_len();
    
    		if($read_1_len && ($read_2_len == 'NA')  ){
    			$IS_RUN_PE = 0
    		}
    		elsif($read_1_len == $read_2_len ){
    			$IS_RUN_PE = 1;
    		}
			
			my $lib_name  = $obj->get_library_name();
			
			$obj->logger()->info("Generating QC report for $lib_name : $seq_unit_name");
		
		
			$bioclassication_summary_line_per_seq_unit   .= $obj->get_bio_classification_name().', '.$obj->get_library_name().', '.$obj->get_library_type().', '.$obj->get_lane_yield().' Mbp'.', '.$obj->get_run_config().', '.$obj->get_qualified_seq_unit_name()."\n";
			
			
			$read_Count_per_seq_unit   	.=$obj->get_library_name()."\t\t".$obj->get_qualified_seq_unit_name()."\t\t".commify($obj->get_total_read_count())."\n";
			
			
			my $percent_num_reads_matched_adapter = sprintf('%0.2f', (  $obj->get_num_reads_matched_adapter()/$obj->get_total_read_count() )*100 ) if ($obj->get_total_read_count());
			$artifcats_mapping_reads_per_seq_unit		.=$obj->get_library_name()."\t\t".$obj->get_qualified_seq_unit_name()."\t\t".commify($obj->get_num_reads_matched_adapter() ) .' ('.$percent_num_reads_matched_adapter.' %)'."\n";
			
			
			my $percent_reads_matched_rRNA= sprintf('%0.2f', (  $obj->get_num_reads_matched_rRNA()/$obj->get_total_read_count() )*100 ) if ($obj->get_total_read_count());
			$rRNA_mapping_reads_per_seq_unit			.=$obj->get_library_name()."\t\t".$obj->get_qualified_seq_unit_name()."\t\t".commify($obj->get_num_reads_matched_rRNA() ).' ('.$percent_reads_matched_rRNA.' %)'."\n";
			
			
			my $percent_reads_matched_transcriptome = sprintf('%0.2f', (  $obj->get_num_reads_matched_transcriptome()/$obj->get_total_read_count() )*100 ) if ($obj->get_total_read_count());
			$num_reads_mapped_transcriptome_per_seq_unit .= $obj->get_library_name()."\t\t".$obj->get_qualified_seq_unit_name()."\t\t".commify($obj->get_num_reads_matched_transcriptome()). ' ('.$percent_reads_matched_transcriptome.' %)'."\n";	
			
			
			my $percent_reads_matched_genome = sprintf('%0.2f', (  $obj->get_num_reads_matched_genome()/$obj->get_total_read_count() )*100 ) if ($obj->get_total_read_count());
			$num_reads_mapped_genome_per_seq_unit .= $obj->get_library_name()."\t\t".$obj->get_qualified_seq_unit_name()."\t\t".commify($obj->get_num_reads_matched_genome()) .' ('.$percent_reads_matched_genome.' %)'."\n";
			
			
			$insert_size_per_seq_unit  .= $obj->get_library_name()."\t\t".$obj->get_qualified_seq_unit_name()."\t\t".commify($obj->get_insert_size_RNA_lib() )."\n" if $IS_RUN_PE;
			
			$read_1_Q20_len_per_seq_unit .= $obj->get_library_name()."\t\t".$obj->get_qualified_seq_unit_name()."\t\t".commify($obj->get_Q20_readlen_read_1() ).' bp'."\n";
			$read_2_Q20_len_per_seq_unit .= $obj->get_library_name()."\t\t".$obj->get_qualified_seq_unit_name()."\t\t".commify($obj->get_Q20_readlen_read_2() ).' bp'."\n" if $IS_RUN_PE;
			
			$transcriptome_SE_percent_per_seq_unit .= $obj->get_library_name()."\t\t".$obj->get_qualified_seq_unit_name()."\t\t".commify($obj->get_transcriptome_SE_percent() ) ."\n" ;
			
			$transcriptome_PE_percent_per_seq_unit .= $obj->get_library_name()."\t\t".$obj->get_qualified_seq_unit_name()."\t\t".commify($obj->get_transcriptome_PE_percent() ) ."\n" if $IS_RUN_PE ;
			
			$transcriptome_PE_agree_percent_per_seq_unit .= $obj->get_library_name()."\t\t".$obj->get_qualified_seq_unit_name()."\t\t".commify($obj->get_transcriptome_PE_agree_percent() ) ."\n" if $IS_RUN_PE;
			
			$transcriptome_PE_conflict_percent_per_seq_unit .= $obj->get_library_name()."\t\t".$obj->get_qualified_seq_unit_name()."\t\t".commify($obj->get_transcriptome_PE_conflict_percent() ) ."\n" if $IS_RUN_PE;
	
			
			
			$read_quality_per_seq_unit .=  $obj->get_library_name().' : '.$obj->get_qualified_seq_unit_name().' : '. $obj->get_Q30_percent().'  : Q20 rl = '.$obj->get_min_Q20_readlen_of_read_1_2()."\n";
			
			$GC_content_per_seq_unit   .= $obj->get_library_name().' : '.$obj->get_qualified_seq_unit_name().' : '.$obj->get_GC_content()."\n";
			
			
			$tagdust_found_artifcats_per_seq_unit  .= $obj->get_library_name()."\t\t".$obj->get_qualified_seq_unit_name()."\t\t".$obj->get_percent_illumina_artifacts().' % '."\n";
			 
			
			my $read_1_basecount_filename = $obj->get_read1_basecount_filename();
			my $read_2_basecount_filename = $obj->get_read2_basecount_filename() if $IS_RUN_PE;
			
			$nucleotide_content_first_12_cycles  .= $obj->get_qualified_seq_unit_name()."\n";
			$nucleotide_content_first_12_cycles  .= 'A Percent :		'.$obj->get_A_percent($read_1_basecount_filename,1, 12)."\t\t";
    		$nucleotide_content_first_12_cycles .=  $obj->get_A_percent($read_2_basecount_filename,1, 12) if ($IS_RUN_PE);
    		$nucleotide_content_first_12_cycles .= "\n";
    		$nucleotide_content_first_12_cycles .= 'T Percent :		'.$obj->get_T_percent($read_1_basecount_filename,1, 12)."\t\t";
    		$nucleotide_content_first_12_cycles .=  $obj->get_T_percent($read_2_basecount_filename,1, 12) if $IS_RUN_PE ;
    		$nucleotide_content_first_12_cycles .= "\n";
    		$nucleotide_content_first_12_cycles .= 'G Percent :		'.$obj->get_G_percent($read_1_basecount_filename,1, 12)."\t\t";
   			$nucleotide_content_first_12_cycles .=  $obj->get_G_percent($read_2_basecount_filename,1, 12) if($IS_RUN_PE);
    		$nucleotide_content_first_12_cycles .= "\n";
    		$nucleotide_content_first_12_cycles .= 'C Percent :		'.$obj->get_C_percent($read_1_basecount_filename,1, 12)."\t\t";
    		$nucleotide_content_first_12_cycles .=  $obj->get_C_percent($read_2_basecount_filename,1, 12) if $IS_RUN_PE;
    		$nucleotide_content_first_12_cycles .= "\n";
    		$nucleotide_content_first_12_cycles .= 'N Percent :		'.$obj->get_N_percent($read_1_basecount_filename,1, 12)."\t\t"; 
    		$nucleotide_content_first_12_cycles .= $obj->get_N_percent($read_2_basecount_filename,1, 12) if $IS_RUN_PE;
    		$nucleotide_content_first_12_cycles	.= "\n\n";
  
	
	
			$nucleotide_content_post_12_cycles .= "\n";
			$nucleotide_content_post_12_cycles  .= $obj->get_qualified_seq_unit_name()."\n";
    		$nucleotide_content_post_12_cycles .= 'A Percent :			'.$obj->get_A_percent($read_1_basecount_filename,13,$obj->get_read_1_len())."\t\t";
   		 	$nucleotide_content_post_12_cycles .=  $obj->get_A_percent($read_2_basecount_filename,13,$obj->get_read_2_len() ) if ($IS_RUN_PE);
    		$nucleotide_content_post_12_cycles .= "\n";
    		$nucleotide_content_post_12_cycles .= 'T Percent :		'.$obj->get_T_percent($read_1_basecount_filename,13,$obj->get_read_1_len())."\t\t";
    		$nucleotide_content_post_12_cycles .=  $obj->get_T_percent($read_2_basecount_filename,13,$obj->get_read_2_len() ) if $IS_RUN_PE ;
    		$nucleotide_content_post_12_cycles .= "\n";
    		$nucleotide_content_post_12_cycles .= 'G Percent :		'.$obj->get_G_percent($read_1_basecount_filename,13,$obj->get_read_1_len())."\t\t";
    		$nucleotide_content_post_12_cycles .=  $obj->get_G_percent($read_2_basecount_filename,13,$obj->get_read_2_len() ) if($IS_RUN_PE);
    		$nucleotide_content_post_12_cycles .= "\n";
    		$nucleotide_content_post_12_cycles .= 'C Percent :		'.$obj->get_C_percent($read_1_basecount_filename,13,$obj->get_read_1_len())."\t\t";
    		$nucleotide_content_post_12_cycles .=  $obj->get_C_percent($read_2_basecount_filename,13,$obj->get_read_2_len() ) if $IS_RUN_PE;
    		$nucleotide_content_post_12_cycles .= "\n";
    		$nucleotide_content_post_12_cycles .= 'N Percent :		'.$obj->get_N_percent($read_1_basecount_filename,13,$obj->get_read_1_len())."\t\t"; 
    		$nucleotide_content_post_12_cycles .= $obj->get_N_percent($read_2_basecount_filename,13,$obj->get_read_2_len() ) if $IS_RUN_PE;
    		$nucleotide_content_post_12_cycles .= "\n\n";
		}
	generateReport($obj);
	$obj->logger()->info("####Program Finished######\n\n")
}


sub _get_user_arguments {
	# need to improve this
	my $SUB = '_get_user_arguments';
	my %temp;
	if(scalar @ARGV ==  0 ) {
		usage();
		exit 2;
	}
	GetOptions(\%temp,
					"lib_name|lib:s@",
					"seq_units|su:s@",
                    "prefix|p:s",
                    "verbose|v!",
                    "email|e!",
                    "stdout|so!",               
              );
    
    
    if ( defined $temp{'lib_name'} ){
    	@library_names = @ {$temp{'lib_name'} };
    }
    if ( defined $temp{'seq_units'} ){
    	@seq_units_by_user = @{ $temp{'seq_units'} };
    }
	if ( defined $temp{'verbose'}){
		$VERBOSE = $temp{'verbose'};
	}
	else {
		$VERBOSE = 0;
	}
	
	if ( defined $temp{'email'} ){
		$EMAIL = 1;
	}
	else {
		$EMAIL = 0;
	}
	if ( defined $temp{'stdout'} ){
		$STDOUT = 1;
	}
	else {
		$STDOUT = 0;
	}
	if ( (scalar @library_names == 0) && ( scalar @seq_units_by_user == 0 )  ){
		die " You have not provided either the library name or seq_unit_name/s to generate a QC template on.";
	}
}


sub usage {
	
print<<USAGE;

-------------------------------------------------
RNA-Seq Library QC template generator script 
Standard options for script
-------------------------------------------------


1.	For single library
$0 -lib <lib_name> -email <0|1 : default 1>

2. 	For multiple libraries
$0 -lib <lib_name1> -lib <lib_name2> -lib <lib_name3> -email <0|1 : default 1>

3.	If you want to specify seq_units_name
$0 -su <seq_unit_name1> -su <seq_unit_name2> -email <0|1 : default 1>


4.	You can also club library names and seq_unit_names if needed 
$0 	
-lib <lib_name1> -lib <lib_name2> -lib <lib_name3>  
-su <seq_unit_name1> -su <seq_unit_name2>
-email <0|1 : default 1>


What is a SEQ_UNIT_NAME ? 
Full seq unit name is the full lane name as per RQC. Also shown on the RQC page post library name based search.
Example: For library HZON  with one lane of data 1690.5.1538.fastq.gz : seq_unit_name will be 1690.5.1538.srf
	
USAGE
	
}




sub generateReport {
	
	my $SUB = 'generateReport';
	my $obj = shift @_ || die " Class obj not passed \n\n";
	my $REPORT;
	
	
	$REPORT .= 'ACTION:'."\n\n\n";
	$REPORT .= '1) Comments: <PASS|FAIL>'."\n";
	$REPORT .= 'Run base output <OK|low|high>.'."\n";
	$REPORT .= 'Read quality <OK|low|high>.'."\n";
	$REPORT .= '<More | No More> sequencing needed'."\n";
	$REPORT .= '<Y|N> evidence of process contamination '."\n\n\n";
	
	
	
	$REPORT .= 'QC dir: ' .get_cwd()."\n\n\n";
	
	$REPORT .= '2) Lanes sequenced : '.scalar @SEQ_UNITS . "\n";
	$REPORT .= 'bioclassification, library, library type, Mbp, run configuration, file name'."\n";
	$REPORT .= $bioclassication_summary_line_per_seq_unit."\n\n\n";
	
	
	$REPORT .='3) RNA-Seq Basic Summary'."\n\n";
	
	$REPORT .= '3.1) 	Total Reads 	              	'."\n";
	$REPORT .= $read_Count_per_seq_unit."\n";
	
	
	$REPORT .= '3.2) 	#reads matching Adapter 	'."\n";
	$REPORT .= $artifcats_mapping_reads_per_seq_unit."\n";
	

	$REPORT .= '3.3) 	#reads matching rRNA	 					'."\n";
	$REPORT .= $rRNA_mapping_reads_per_seq_unit."\n";
	
	
	
	$REPORT .= '3.4) 	Transcriptome Mappable Reads	'."\n";
	$REPORT .= $num_reads_mapped_transcriptome_per_seq_unit."\n";
	
	

	$REPORT .= '3.4) 	Genome Mappable Reads			'."\n";
	$REPORT .= $num_reads_mapped_genome_per_seq_unit."\n";


	$REPORT .= '3.5) 	Insert Size  					'."\n" if $IS_RUN_PE;
	$REPORT .= $insert_size_per_seq_unit."\n" if $IS_RUN_PE;
	
	
	$REPORT .= '3.7) 	Q20 Read 1 length				'."\n";
	$REPORT .= $read_1_Q20_len_per_seq_unit."\n";
	
	$REPORT .= '3.8) 	Q20 Read 2 length				'."\n" if $IS_RUN_PE;
	$REPORT .= $read_2_Q20_len_per_seq_unit."\n" if $IS_RUN_PE;
	
	
	$REPORT .= '3.9) 	Single Reads %					'."\n";
	$REPORT .= $transcriptome_SE_percent_per_seq_unit."\n";
	
	$REPORT .= '3.10)	Paired End Reads % 				'."\n" if $IS_RUN_PE ; 
	$REPORT .= $transcriptome_PE_percent_per_seq_unit."\n" if $IS_RUN_PE ; 
	
	$REPORT .= '3.11) 	PE Reads agree % 				'."\n" if $IS_RUN_PE ; 
	$REPORT .= $transcriptome_PE_agree_percent_per_seq_unit."\n" if $IS_RUN_PE ; 
	
	$REPORT .= '3.12) 	PE Reads conflict % 			'."\n" if $IS_RUN_PE ; 
	$REPORT .= $transcriptome_PE_conflict_percent_per_seq_unit."\n" if $IS_RUN_PE ; 
	
	

	$REPORT .= '4) Read Level QC Metrics'."\n";
	$REPORT .= "\n";
	$REPORT .= '4.1) Read quality'; 
	$REPORT .= "\n";
	$REPORT .= '4.1.1) Quality score are : <<<LOW|| ACCEPTABLE>>';
	$REPORT .= "\n";
	$REPORT .= 'Library :             FastQ            :   %reads_Q30    :     Q20_read_length min of read1/2'."\n";
	$REPORT .= $read_quality_per_seq_unit."\n";
	
	$REPORT .= "\n\n";
	
	$REPORT .= '4.2) Illumina Artifacts % as per tagdust calculcation  '."\n";
	$REPORT .= $tagdust_found_artifcats_per_seq_unit."\n";	
	
	
	
	
	$REPORT .= "\n\n";
	
	
	$REPORT .= '4.3) Read GC Content'."\n\n";
	$REPORT .= 'Library'."\t\t".'FastQ'."\t\t".'GC_content'."\n";
	$REPORT .=  $GC_content_per_seq_unit."\n\n\n";
	
	
	$REPORT .= '4.4) Nucleotide Composition per lane '."\n\n";
	$REPORT .= '4.4.1)				Read 1(first 12 cycles)';
	$REPORT .= "\t\t\t".' Read 2(first 12 cycles) ' if ( $IS_RUN_PE);
	$REPORT .= "\n\n";
	$REPORT .= $nucleotide_content_first_12_cycles;
	
	
	
	$REPORT .= "\n\n";
	
	
	$REPORT .= '4.4.2)			Read 1(post 12 cycles)';
	$REPORT .= "\t\t\t".' Read 2(post 12 cycles) ' if ( $IS_RUN_PE);
	$REPORT .= "\n\n";
	$REPORT .= $nucleotide_content_post_12_cycles;
	
	
	
	$REPORT .= '4.5) Uniqueness - 20mer sampling     :  <<< COMMENT ON PLOT >>>'."\n";
#	Only write the uniqueness stats from RQC db if only one seq_unit is analysed else separate analysis needs to be done combining all the lanes
	if ( scalar @SEQ_UNITS == 1 ){
		$REPORT .= '4.5.1) '. 	$obj->get_num_reads_sampled().' Million 20mers sampled'."\n";
		$REPORT .= '4.5.2) '.	$obj->get_20mer_uniq_start_mers_percent().'% unique starting 20mers'."\n";
		$REPORT .= '4.5.3) '.   $obj->get_20mer_uniq_random_mers_percent().'% random 20mers'."\n\n\n";
	}
	else{
		$REPORT .= 'Separate Combined analysis for all seq_units clubbed together needs to done to get the right numbers '."\n\n"
	}
	
	
	$REPORT .= "\n";
	
	$REPORT .= '5) Sample validation and contamination'."\n";
	$REPORT .= '5.1) Megablast of '.commify($obj->get_num_blastable_contigs()). ' contigs produced '.commify($obj->get_num_NT_tophits()).' top hits to nt.'."\n\n";
	$REPORT .= '<<<<INSERT TOP HITS >>>>'."\n\n\n";
	
	
	$REPORT .= '5.2) Top hits <<are|not>> consistent wqith genome of interest'."\n";
	$REPORT .= '5.3) <Y|N> evidence of major contamination.'."\n";
	
	$REPORT .= "\n\n";
	
	$REPORT .= 'Report generated by '.get_username().' at time '.get_timeStamp()."\n";
	
	my $suffix = get_suffix_for_report_generation();
	
	my $filename = 'README.'.$suffix;
	open(OUT,">$filename") || die "Error[$SUB] : Could not open the file $filename \n\n";
	
	print (OUT $REPORT);
	close(OUT);
	
	$obj->logger->info("QC template for librry $suffix generated in file $filename \n\n ");
	
	if ( $EMAIL ){
		my $SUBJECT = 'RNA Library '."$suffix".' Base QC Template generated';
		send_text_email (guess_user_email(),guess_user_email(),$SUBJECT,$REPORT);
		$obj->logger()->info("QC Base Template email sent to user email :", guess_user_email() );
	}
	
	if ($STDOUT){
		print STDOUT $REPORT;
	}
}


sub get_suffix_for_report_generation {
	my $SUB = 'get_suffix_for_report_generation';
	
	if (  ( scalar @library_names == 1 ) && (scalar @seq_units_by_user == 0) ) {
		return pop(@library_names);
	}
	else{
		return 'mulitpleLibs.QC.report';
	}
}








