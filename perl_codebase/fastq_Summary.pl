#!/jgi/tools/bin/perl

use strict;




main();


sub main {
	

if ( scalar @ARGV == 0 ) {
		usage();
}
	
	
my $count_read1;
my $count_read2;
my $total_reads;
	
	while(my $file = shift @ARGV) {
		
#		print "$file \n";
		
			print "=========================\n";
		
		if ($file =~/^.+?\.bz2$/ ) {
		print "$file\n";
		
		$count_read1 = `bzcat $file | egrep -c -e \'^@.+?\/1.*?$\'`;
		$count_read2 = `bzcat $file | egrep -c -e \'^@.+?\/2.*?$\'`;
		
		print "#read1 : $count_read1\n";
		chomp $count_read1;
		print "#read2 : $count_read2\n";
		chomp $count_read2;
		}
			
			
		elsif ($file =~/^.+?\.(fq|fastq)$/ ) {
		
		print "$file\n";
		$count_read1 = `cat $file | egrep -c -e \'^@.+?\/1.*?$\'`;
		chomp $count_read1;
		$count_read2 = `cat $file | egrep -c -e \'^@.+?\/2.*?$\'`;
		chomp $count_read2;
		
		print "#read1 : $count_read1\n";
		print "#read2 : $count_read2\n";
				
		}
		
		else {
			print "\n File format $file could not recognized ..Skipping!!!!!\n\n";
			next ;
		}
				
		
	}	
	
	
	
}


sub usage(){
	
	print " Give one or more fastq files \n";
}