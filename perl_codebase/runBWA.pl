#!/jgi/tools/bin/perl


use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/lib";
use FindBin;
use Getopt::Long;
use Log::Log4perl qw(:easy);
use JGI::QAQC::Utilities;
use Data::Dumper;
use JGI::QAQC::BWA;



my ($fastq_read1, $fastq_read2,$ref_dbase,$num_threads,$out_sam,$email,$force_run,$grid, $logger,$final_bam_file,$bam_location);
my ($cluster, $combined_fastq, $verbose, $create_unmapped_reads_fastq_from_bam);
my ($seed_edit_distance, $max_edit_distance, $quality_ascii_64);

main();



sub main {

	my $SUB = 'main';
	
	#set up the logger
	config_logger();
	$logger = get_logger();
	
	
	## get the start time
	my $main_prog_start_time = time();
	
	## get the user arguments
	_parse_user_arguments();
	
	
	##instantiate the class
	my $BWA = JGI::QAQC::BWA->new();
	
	## set the flag for force run
	$BWA->force_run($force_run);
	
	
	## set the verbosity level for class
	$BWA->verbose($verbose);
	
	##set the edit distance
	$BWA->seed_edit_distance($seed_edit_distance);
	$BWA->max_edit_distance($max_edit_distance);
	
	
	## checking if the db reference provided else index the reference
	$BWA->check_if_reference_is_indexed($ref_dbase);
	
	
	$BWA->combined_fastq($combined_fastq);
	$BWA->fastq_read1($fastq_read1);
	$BWA->fastq_read2($fastq_read2);
	$BWA->ref_dbase($ref_dbase);
	$BWA->num_threads($num_threads);
	$BWA->grid($grid);
	$BWA->email($email);
	$BWA->force_run($force_run);
	$BWA->quality_ascii_64($quality_ascii_64);
	

	#set the out file names
	#mainly to check if the expected file is present
	$BWA->set_out_fileNames();
	
		
	### if a combined fastq is provided split it into read 1 and read 2 for mapping and set the fastq read 1/2 names
	if ($combined_fastq ){
		$BWA->combined_fastq($combined_fastq);
		
		my $start = time();
		$BWA->split_combined_fastq_gz();  ## will set up $fastq_read1 and fastq_read2
		my $end = time();
		my $time_spent = $end-$start;
		$logger->info("Splitting the $combined_fastq took $time_spent seconds \n ");	
	}
	
	
	
	#################
	### For PE data
	#################
	
	if( $BWA->fastq_read1() && $BWA->fastq_read2() )  {
		$logger->info("Running BWA for PE Data") if $verbose;
		#
		#Align the reads against reference
		my $start = time();
		$BWA->aln();
		my $end = time();
		my $time_spent = $end-$start;
		$logger->info("Aligning the $combined_fastq took $time_spent seconds \n ");
		
		### Creating the SAM file
		my $start = time();
		$BWA->sampe();
		my $end = time();
		my $time_spent = $end-$start;
		$logger->info("Producing the sam file  took $time_spent seconds \n ");
	}
	
	###############################
	## For Single Read(SR) data
	################################
	elsif( $BWA->fastq_read1() && !($BWA->fastq_read2()) ) {
		$logger->info("Running BWA for Single Read Data\n") if $verbose;
		
		
		##Align the reads against reference
		my $start = time();
		$BWA->aln();
		my $end = time();
		my $time_spent = $end-$start;
		$logger->info("Producing the alignment took $time_spent seconds \n ");
		
		
		my $start = time();
		$BWA->samse();
		my $end = time();
		my $time_spent = $end-$start;
		$logger->info("Producing the sam file  took $time_spent seconds \n ");
	}
	
##Post Alignment steps
	## Creating the indexed/sorted bam file
	my $start = time();
	($bam_location,$final_bam_file)=$BWA->sam_to_bam();
	my $end = time();
	my $time_spent = $end-$start;
	$logger->info("Producing the sorted bam file  took $time_spent seconds \n ");
	
	
	###Clean UP
	##deleting the sam sai and read1/2 files
	$BWA->cleanUP();

	
	###create a file *readCounts file
	## num of reads per reference contig/scaffold/chromosome
	my $start = time();
	$BWA->count_reads_per_reference();
	my $end = time();
	my $time_spent = $end-$start;
	$logger->info("Producing the readCounts file  took $time_spent seconds \n ");
	
	
	
	####creating the fastq for unmapped reads
	$BWA->create_unmapped_reads_fastq_from_bam()  if $create_unmapped_reads_fastq_from_bam;
	
	
	my $main_prog_end_time=time();
	my $total_time_taken= $main_prog_end_time - $main_prog_start_time;
	my $final_bam_file = $BWA->out_sorted_bam();
	

	### will only send the email if the final bam file exists
	if( -e $bam_location.'/'.$final_bam_file ) {
	
		## getting saved message for user from BWA.pm
		my $email_message = $BWA->email_body_message();
		$email_message    = $email_message."\n\nFinal Bam file $bam_location/$final_bam_file\n";
		$email_message    = $email_message."Total Time Taken $total_time_taken seconds\n\n";
		
		send_text_email('BWA',$email,"BWA Job completed $bam_location/$final_bam_file", $email_message);
		
		$logger->info("Final Bam file $bam_location/$final_bam_file");
		$logger->info("Total Time Taken $total_time_taken seconds");
	}
	
	else {
		send_text_email('BWA',$email,"Error running BWA Job ", "Please check the out.log file");
		
	}

} ### END MAIN

sub _parse_user_arguments {
	
	my $SUB='parse_user_arguments';
	my %temp;
	
	if(scalar @ARGV ==  0 ) {
		usage();
		exit 2;
	}
	
	GetOptions(\%temp,
					"combined_fastq|cf=s",
                    "fastq_read1|r1=s",
                    "fastq_read2|r2=s",
                    "seed_edit_distance|sed:s",
                    "max_edit_distance|med:s",
                    "ref_dbase|db=s",
                    "num_threads|nt=i",
                    "out_sam|os=s",
                    "unmapped_fastq|ufq!",
                    "quality_ascii_64|q64!",
                    "email|e=s",
                    "force_run|fr!",
                    "grid|gd!",
                    "cluster|cl:s",
                    "prefix|p:s",
                    "verbose|v!",
              );
    
   
   ### if a combined fastq is provided split it into read 1 and read 2 for mapping 
  	if (  $temp{'combined_fastq'} ) {
    	$combined_fastq = $temp{'combined_fastq'};
    }
    else {
    	$combined_fastq = 0;
    }
  
    unless (  $temp{'fastq_read1'} || $temp{'combined_fastq'}   ) { 
    	print "Read 1 fastq not provided \n";
   		usage();
   		exit 3;
    }
    
    unless ( $temp{'ref_dbase'} ) { 
    	print "Reference dbase not provided \n";
   		usage();
   		exit 3;
    }
    
    unless ( $temp{'num_threads'} ) { 
    	print "num_threads not provided \n";
   		usage();
   		exit 3;
    }
     
    unless ( $temp{'num_threads'} >= 1 && $temp{'num_threads'} <= 16 ) {
    	print "number of threads should be between 1 and 16..please restart with appropriate values\n";
    	exit 3;
    }          

  	$fastq_read1	=  $temp{'fastq_read1'}|| 0;
  	$fastq_read2    =  $temp{'fastq_read2'} || 0;
  	$ref_dbase 		=  $temp{'ref_dbase'};
  	$num_threads	=  $temp{'num_threads'};
  	$out_sam		=  $temp{'out_sam'};
  	$email			=  $temp{'email'} || guess_user_email();
  	$force_run		=  $temp{'force_run'} || 0;
  	$grid			=  $temp{'grid'} || 0;
  	$verbose 		=  $temp{'verbose'} || 0;
  	$seed_edit_distance 	=  $temp{'seed_edit_distance'} || 2;
  	$max_edit_distance 		=  $temp{'max_edit_distance'}  || .03;
  	
  	if (defined $temp{'quality_ascii_64'}){
  		$quality_ascii_64		=  1;	
  	}
  	else{
  		$quality_ascii_64		=  0;
  	}
  	
  	
  	if($temp{'unmapped_fastq'}){
  		$create_unmapped_reads_fastq_from_bam =1;
  	}
  	else {
  		$create_unmapped_reads_fastq_from_bam = 0;
  	}
  	
  	$logger->info("SUB[$SUB] Parsing user arguemtns") if $verbose;  
  	  
  	
  	if ( $grid && (defined  $temp{'cluster'} )   ) {
  		$cluster = $temp{'cluster'}
  	}
  
	
} ### End 


sub usage {

### printing the standard usage header from IGS::Utilities
get_standard_usage_header($0);

print<<USAGE;
[Available Options]
--combined_fastq|cf			[required]  combined fastq file unless read 1/2 are specifed separately
--fastq_read1|r1			[required]	fastq file for read 1 [optional is combined_fastq is specified
--fastq_read2|r2			[optional]	when aligning paired end data , second fastq should be provided
--ref_dbase|db				[required]	location of fasta file against which alignment is to be done by BWA(1)
--num_threads|nt			[required]	number between 1-16 : total number of threads/slots to use to run BWA
--out_sam|os				[optional]  name of the output sam/bam file. default : out_combined.sam/bam
--edit_distance|ed			[optional]  #mismatches allowed in the seed
--email|e					[optional]  by default job completion email is sent to user
--force_run|fr				[optional]	for reprocessing the data


Examples:

### 1.
## Running BWA on PE GA data on grid  
***Note : only BWA alignment steps runs on grid.. All other steps are executed on local machine**** 
runBWA.pl -r1 s_1_1_sequence.txt -r2 s_1_2_sequence.txt \
          -ref_dbase /local/sequence/reference/BWA_ref/mm9/mm9.fa 
          -nt 12 \
          -os sample_2323.sam \
          -grid


### 2. Run BWA on your local machine
runBWA.pl -r1 s_1_1_sequence.txt -r2 s_1_2_sequence.txt \
          -ref_dbase /local/sequence/reference/BWA_ref/mm9/mm9.fa 
          -nt 12 \
          -os sample_2323.sam \
         
### 3. Run BWA on grid and force to redo the analysis
****In a directory where BWA has been run before using this script, the script will guess the output 
skip those commands. You can FORCERUN if you want ****
runBWA.pl -r1 s_1_1_sequence.txt -r2 s_1_2_sequence.txt \
          -ref_dbase /local/sequence/reference/BWA_ref/mm9/mm9.fa 
          -nt 12 \
          -os sample_2323.sam \
          -grid
          -force_run 
USAGE


#--grid|gd					[optional]	run BWA on grid
#--cluster|cl     			[optional]  required when using the option --gird
#									specify the cluster to run the BWA job on (rhea, theia etc)
}
	
    


