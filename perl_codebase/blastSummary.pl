#!/jgi/tools/bin/perl

use strict;

use Statistics::Descriptive::Discrete;
#use Text::Table;


my $uniq_tophits_file= unify_tophits();
my @uniq_species  = unify_taxanomy();



###opening a new file to store output
my $out_file = 'blasty_summary.txt';
open (OUT, ">$out_file") || die"Error : could not open the file $out_file\n";

## header for output
print OUT "Specie \t #contigs \t #bases_contigs \t #base_match_ref \t median_contig \t max_contig \t mean_contig \t SD_mean_contig \t  mean_kmer_cov \t SD_mean_kmer_cov  \t normalized_kmer_cov \n"; 



foreach my $specie (@uniq_species) {

#    print "Specie : $specie\n";

    $specie =~s/\n//;
    my @contigs_tophits_per_specie=`cat $uniq_tophits_file | grep $specie `;

	if (scalar @contigs_tophits_per_specie == '0') {
		print "\n No contigs found for specie \t $specie\n";
		next ;
	}

	else {

	    ##temp variable
	    my ($total_contigs,@contigs_kmer_len,@contigs_kmer_cov,@contigs_matching_len,@contigs_length)=0;
	    my ($total_length_all_contigs,$total_length_that_matches_reference)=0;
	    my ($temp_total)=0;


	    foreach my $contig_line  (@contigs_tophits_per_specie) {

		$total_contigs++;

		my($contig_info,$gi,$Escore,$match_len,$percent_identity,$contig_length)=split(' ',$contig_line);
		my @contig_info_split=split('_',$contig_info);

		my $contig_kmer_length     = $contig_info_split[3];
		my $contig_kmer_coverage   = $contig_info_split[5];
		$total_length_all_contigs += $contig_length;
		$total_length_that_matches_reference += $match_len;


		next if ( number_sanity_check($contig_kmer_length,$contig_line) == '0');
		next if ( number_sanity_check($contig_kmer_coverage, $contig_line) == '0');
		next if  (  number_sanity_check($contig_length,$contig_line)  == '0');
		next if (  number_sanity_check($match_len,$contig_line) == '0' );


			   $temp_total+=$contig_kmer_length;
			   push(@contigs_kmer_len,$contig_kmer_length);
			   push(@contigs_kmer_cov,$contig_kmer_coverage);
			   push(@contigs_matching_len,$match_len);
			   push(@contigs_length,$contig_length);


	     }


    my $stat = Statistics::Descriptive::Discrete->new();
    $stat->add_data(@contigs_kmer_len);
    my $mean_contig_kmer_len = sprintf("%.2f",$stat->mean());
    my $sd_contig_kmer_len = sprintf("%.2f",$stat->standard_deviation());
    my $median_contig_len = sprintf("%.2f",$stat->median());
    
  
    my $stat = Statistics::Descriptive::Discrete->new();
    $stat->add_data(@contigs_kmer_cov);
    my $mean_contig_kmer_cov = sprintf("%.2f",$stat->mean());
    my $sd_contig_kmer_cov = sprintf("%.2f",$stat->standard_deviation());


    my $stat = Statistics::Descriptive::Discrete->new();
    $stat->add_data(@contigs_matching_len);
    my $mean_contig_matching_length = sprintf("%.2f",$stat->mean());
    my $sd_contig_matching_length = sprintf("%.2f",$stat->standard_deviation());


    my $stat = Statistics::Descriptive::Discrete->new();
    $stat->add_data(@contigs_length);
    my $mean_contigs_length       = sprintf("%.2f",$stat->mean());
    my $sd_contigs_length       = sprintf("%.2f",$stat->standard_deviation());
    my $max_contig_length       = sprintf("%.2f",$stat->max());
    my $min_contig_length       = sprintf("%.2f",$stat->min());
    
  
    my $normalized_kmer_coverage = sprintf ("%.2f", ($mean_contig_kmer_cov/$total_length_all_contigs) );

    print OUT  "$specie \t $total_contigs \t";
    print OUT  "$total_length_all_contigs \t";
    print OUT  "$total_length_that_matches_reference \t";
    print OUT  "$median_contig_len\t";    
    print OUT  "$max_contig_length \t";                 
    print OUT  "$mean_contigs_length \t $sd_contigs_length \t";
    print OUT  "$mean_contig_kmer_cov \t $sd_contig_kmer_cov \t";
    print OUT  "$normalized_kmer_coverage\n";
  
  
  
	}
       
  


}



sub unify_tophits {


	my $SUB = 'unify_tophits';
      

### open temp file for writing out contigs

	my $temp_file = 'temp_all_uniq_top_contigs.txt';
	open(OUT, ">$temp_file") || die "Cant open the file $temp_file\n";

	my $uniq_contigs;
	my @duplicate_contigs;

	my @files = `ls megablast.contigs.fa.v.*tophit`;

	foreach my $file (@files) {

		### reading each file and unifying contig level info
				open(IN, "<$file") || die "Can't open file $file\n";
#				print "Reading contigs from file $file \n";

				 	while(<IN>) {
				 		chomp $_;
				 		my $contig_line = $_;
				 		my @contig_line_split = split('_',$contig_line);
				 		my $contig_name = $contig_line_split[1];


				 		if( defined $uniq_contigs->{$contig_name} ) {
#				 			print "Duplicate: $contig_name";
				 			push(@duplicate_contigs, $contig_name);
				 		}
				 		else {
				 			print OUT "$contig_line\n";
				 			$uniq_contigs->{$contig_name}=0;
				 		}
				 	}

	}



my $total_uniq_contigs = scalar keys %$uniq_contigs ;
my $dup_contigs = scalar @duplicate_contigs;

print "Total Uniq Contigs \t $total_uniq_contigs\n";
print "Duplicate Contigs \t $dup_contigs \n";




return $temp_file;

}




sub unify_taxanomy {

	my $SUB = 'unify_taxanomy';
#	my @taxa_files='test.taxa';
	my @taxa_files=`ls megablast.contigs.fa.v*taxlist`;

	my $species;
	my $phylo_tree;
	my @uniq_species;
	my @dup_species;

		foreach my $file (@taxa_files) {

				open(IN,"<$file") || die "File $file can't be opened\n";

					while(<IN>){

						chomp $_;
						my $taxa_line=$_;

						my @taxa_line_split = split(' ',$taxa_line);

						my $specie = $taxa_line_split[0];

						my @lineage = split(' : ', $taxa_line);
						@lineage=split(';',$lineage[1]);



#						print "@lineage\n", scalar @lineage;

						if(defined $species->{$specie}) {
							push(@dup_species,$specie)
						}

						else {
						    $species->{$specie}=0;
						        push(@uniq_species,$specie); 

						}

				}

               }	

print "Uniq Species \t",scalar @uniq_species,"\n";
print "Dup Species \t ", scalar @dup_species,"\n";
   
return (@uniq_species);
}






sub number_sanity_check {

	my $SUB = 'number_sanity_check';

	my $number = shift @_;
	my $contig_line = shift @_;

	my @contig_name = split (' ',$contig_line);


	if ($number=~ /^\d+[\.\d]+$/ ) {

		return $number;
	}

	else {

		print STDERR "The input is not numeric \n $contig_name[0] \t $number\n";

		return 0;
	}
}