#!/jgi/tools/bin/perl


use strict;
use POSIX 'isatty';
use Getopt::Long;
use Data::Dumper;

use vars qw( $COUNT_RESIDUES $TOP_CONTIGS $SCAFFOLDS_TO_CONTIGS  $EXTRACT_SEQUENCE  $EXTRACT_SEQUENCE_HEADER $TOP_CONTIGS_FOR_SUMMARY);
use vars qw ($SPLIT_FASTA $MIN_CONTIG_LEN $USER_DEF_OUT_FILE);
use vars qw($translate_rna_to_dna);


main();


sub main {
	
	
	check_and_get_input();

    create_fasta_summary() if $COUNT_RESIDUES;
    top_contigs($TOP_CONTIGS) if $TOP_CONTIGS;
    scaffold_to_contigs() if $SCAFFOLDS_TO_CONTIGS;
    extract_fasta_sequence($EXTRACT_SEQUENCE_HEADER) if $EXTRACT_SEQUENCE;
    show_top_N_contig_residues() if $TOP_CONTIGS_FOR_SUMMARY;
    split_fasta_by_fasta_header() if $SPLIT_FASTA;
    translate_rna_to_dna()	if $translate_rna_to_dna;
}



sub check_and_get_input() {
	
	if ( isatty(*STDIN) ) {
		 usage();
	}
	
	my %input;
	
	GetOptions(\%input,
					"top_contigs|tc:i",
					"show_top_contigs_summary|summary:i",
					"count_residues|cr!",
                    "verbose|v!",
 					"scaffolds_to_contigs|stc!",
 					"extract_sequence|esq:s",
 					"split_fasta|sf!",
 					"min_contig_len|mcl:i",
 					"out_fastq_file|of:s",
 					"translate_rna_to_dna|t_rna_dna!"
               );
	
	
	if( defined $input{'top_contigs'}){
		$TOP_CONTIGS = $input{'top_contigs'};
	}
	elsif( defined $input{'count_residues'}){
		$COUNT_RESIDUES = $input{'count_residues'};
	}
	elsif( defined $input{'show_top_contigs_summary'}){
		$TOP_CONTIGS_FOR_SUMMARY = $input{'show_top_contigs_summary'};
	}
	elsif( defined $input{'scaffolds_to_contigs'}){
		$SCAFFOLDS_TO_CONTIGS = $input{'scaffolds_to_contigs'};
	}
	elsif( defined $input{'extract_sequence'}){
		$EXTRACT_SEQUENCE_HEADER = $input{'extract_sequence'};
	}
	elsif( defined $input{'split_fasta'}){
		$SPLIT_FASTA = $input{'split_fasta'};
	}
	elsif( defined $input{'translate_rna_to_dna'}){
		$translate_rna_to_dna = 1;
	}
	else{
		usage();
	}
	
	$MIN_CONTIG_LEN = $input{'min_contig_len'} || 0;
	
	$USER_DEF_OUT_FILE = $input{'out_fastq_file'} ;
	
	
#	print Dumper(\%input);
}

sub usage {
	
	
	my $SUB = 'usage';
	
print<<USAGE;

Options:
	
top_contigs|tc  <N>							to produce a new fasta with <N> longest contigs
count_residues|cr 0|1						to produce a AGCT count of residues in contigs
show_top_contigs_summary|summary <N>		to produce a AGCT count of residues in top <N> contigs sorted by contig length
scaffold_to_contigs|stc						to split the scaffolds into contigs where N homopolymer stretch > 10 
extract_sequence | esq						extract a fasta sequence corresponding to a header
min_contig_len|mcl							only process contigs with length  > N bp
t_rna_dna									converts the RNA sequence to DNA ( U -> T)
		
Examples 
1. cat FASTA | $0  -tc 20   		 				 ##produce the top 20 contigs
2. cat FASTA | $0   -cr   							 # print contig residues summary for all contigs
3. cat FASTA | $0  -summary 5 [num of contigs]  	 #print contig residues summary for top N contigs
4. cat FASTA | $0  -summary 5 -mcl 2000 [num of contigs]    #print contig residues summary for top N contigs with min length 2000
4. cat FASTA | $0  -tc 100 -of new_contigs.fasta -mcl 2000 [num of contigs]    #create a new fasta file of top N contigs with min length 2000
	
USAGE
	



print "\n";
exit 2;
	
}

sub create_fasta_summary {
	my $SUB = 'create_fasta_summary';
	
	my $sequence;
	my $last_contig_name;
	my $last_contig_cov ;
	my $last_contig_kmer_len;
	my $header_read = 0;
	
	$/ ='>';
	print "header[1] \t length[2] \t  count_A[3] \t count_G[4] \t count_C[5] \t count_T[6] \t count_N[7] \t count_GC[8] \t GC_percent[9] \t count_AT[10] \t AT_percent[11] \t contig_kmer_depth[12] \n";
	
	while(<STDIN>){
		chop $_;
		if (/^$/) { next ;}
		$_ =~ /(^.+?\n)(.*$)/s;
		my $header = $1;
		my $sequence = $2;
		$header =~s/\n//g;
		$sequence =~s/\n//g;
		$header =~/^.*cov_([0-9]+).([0-9]{2}).*$/;
		my $contig_kmer_depth = $1.".$2"		|| 0;
	
		
		my $count_A = ($sequence =~tr/(A|a)//) 	|| 0;
		my $count_G = ($sequence =~tr/(G|g)//) 	|| 0;
		my $count_C  = ($sequence =~tr/(C|c)//) || 0;
		my $count_T = ($sequence =~tr/(T|t)//)	|| 0;
		my $count_U  = ($sequence =~tr/(U|u)//) || 0;  ##for RNA
		my $count_N  = ($sequence =~tr/(N|n)//) || 0;
		
		
		my $count_GC = $count_G + $count_C;
		my $count_AT = $count_A + $count_T;
		
		## counting the N's in the fasta sequence
		my $contig_length = $count_GC + $count_AT + $count_N + $count_U;
		
		##skip the contig if the length is > MIN contig length
		next if ($contig_length < $MIN_CONTIG_LEN );
		
		my $GC_percent = sprintf('%0.2f', ($count_GC/$contig_length)*100 );
		my $AT_percent = sprintf('%0.2f', ($count_AT/$contig_length)*100 );
		
		print "$header \t $contig_length \t $count_A \t $count_G \t $count_C \t $count_T \t $count_N \t $count_GC \t $GC_percent \t $count_AT \t $AT_percent \t $contig_kmer_depth \n";
	}
}



sub show_top_N_contig_residues {
	my $SUB = 'show_top_N_contig_residues';
	my $contigs;

	$/ ='>';	
	print "header[1] \t length[2] \t  count_A[3] \t count_G[4] \t count_C[5] \t count_T[6] \t count_N[7] \t count_GC[8] \t GC_percent[9] \t count_AT[10] \t AT_percent[11] \t contig_kmer_depth[12] \n";
	
	while(<STDIN>){
		chop $_;
		
		if (/^$/) { next ;}
		$_ =~ /(^.+?\n)(.*$)/s;
		
		my $header = $1;
		my $sequence = $2;
		$header =~s/\n//g;
		$sequence =~s/\n//g;
				
		$header=~/^NODE_(\d+?)_.*$/;
		my $contig_name =$1;

		my $contig_length = length($sequence);
		
		##skip the contig if the length is > MIN contig length
		next if ($contig_length < $MIN_CONTIG_LEN );
		
		$contigs->{$header}->{'length'} 	=	$contig_length;
		$contigs->{$header}->{'sequence'} 	=	$sequence;
	}	
	my @sorted_contigs = reverse sort { $contigs->{$a}->{'length'} <=> $contigs->{$b}->{'length'}  } keys %{$contigs};
	my $contigs_shown = 0; 
	
	foreach my $contig_name (@sorted_contigs) {
		$contigs_shown++;		
		if ( $contigs_shown > $TOP_CONTIGS_FOR_SUMMARY ) { last; }
		
		my $header =  $contig_name;
		my $sequence = $contigs->{$contig_name}->{'sequence'};
	
		$header =~s/\n//g;
		$sequence =~s/\n\s//g;
		
		
		$header =~/^.*cov_([0-9]+).([0-9]{2}).*$/;
		my $contig_kmer_depth = $1.".$2"		|| 0;
		
#		print  $header, "\t",$contig_kmer_depth,"\n";
		
		my $contig_length = length($sequence);
		
		my $count_A = ($sequence =~tr/(A|a)//) 	|| 0;
		my $count_G = ($sequence =~tr/(G|g)//) 	|| 0;
		my $count_C  = ($sequence =~tr/(C|c)//) || 0;
		my $count_T = ($sequence =~tr/(T|t)//)	|| 0;
		my $count_U  = ($sequence =~tr/(U|u)//) || 0;  ##for RNA
		my $count_N  = ($sequence =~tr/(N|n)//) || 0;
		
		
		my $count_GC = $count_G + $count_C;
		my $count_AT = $count_A + $count_T;
		
		my $GC_percent = sprintf('%0.2f', ($count_GC/$contig_length)*100 );
		my $AT_percent = sprintf('%0.2f', ($count_AT/$contig_length)*100 );
		
		
		print "$header \t $contig_length \t $count_A \t $count_G \t $count_C \t $count_T \t $count_N \t $count_GC \t $GC_percent \t $count_AT \t $AT_percent \t $contig_kmer_depth \n";
	}
}


sub top_contigs {
	
	my $SUB = 'top_contigs';
	
	my $num_top_contigs = shift @_;
	
	my $header_read = 0;
	
	
	my $OUT_FILE =  $USER_DEF_OUT_FILE  ? $USER_DEF_OUT_FILE : 'top_'.$num_top_contigs.'_contigs.fasta';
	open(OUT , ">$OUT_FILE") || die "[SUB] File $OUT_FILE can't be opened \n";
	
	my $contigs;

	$/ ='>';
	my $count_fasta =0;
	
	while(<STDIN>){
		chop $_;
		
		$count_fasta++;
		
#		print "$count_fasta ...\n";
		print "Read $count_fasta sequences ...\n" if ( $count_fasta%50000  == 0 );

		if (/^$/) { next ;}
		$_ =~ /(^.+?\n)(.*$)/s;
		
		my $header = $1;
		my $sequence = $2;
	
		$header =~s/\n//g;
		$sequence =~s/\n//g;
		
		$header=~/^NODE_(\d+?)_.*$/;
		my $contig_name =$1;
		
		my $contig_length = length($sequence);
		
		##skip the contig if the length is > MIN contig length
		next if ($contig_length < $MIN_CONTIG_LEN );
		
		$contigs->{$header}->{'length'} 	=	$contig_length;
		$contigs->{$header}->{'sequence'} 	=	$sequence;
		
	}	
	
	if (keys %{$contigs} == 0) {
		print "No contigs found with length > $MIN_CONTIG_LEN \n Will exit \n\n";
		exit 0;
	}
	
	my @sorted_contigs = reverse sort { $contigs->{$a}->{'length'} <=> $contigs->{$b}->{'length'}  } keys %{$contigs};
	my $count_contigs_to_write =0;	
	
	foreach my $contig_name (@sorted_contigs) {
	
		$count_contigs_to_write++;
	
		 if ( $count_contigs_to_write > $num_top_contigs ) { last; }
		my $header = '>'.$contig_name."\n";
		my $sequence = $contigs->{$contig_name}->{'sequence'}."\n";
		
		print OUT "$header";
		print OUT "$sequence";
	}
	print "Printed the contigs in file $OUT_FILE \n";	
}




sub extract_fasta_sequence {
	my $SUB = 'extract_fasta_sequence';
	$/ ='>';
	my $count_orig_sequence = 0;
	
	while(<STDIN>){
		chop $_;
		next if (/^$/ );
		$count_orig_sequence++;
		
		print "Read $count_orig_sequence sequences ...\n" if ( $count_orig_sequence % 50000  == 0 );

		if (/^$/) { next ;}
		$_ =~ /(^.+?\n)(.*$)/s;
		
		my $header = $1;
		my $sequence = $2;
		
		if ($EXTRACT_SEQUENCE_HEADER == $header ){
			print $sequence;
		}
	}
}


sub split_fasta_by_fasta_header {
	my $SUB = 'split_fasta_by_fasta_header';
	
	$/ ='>';
	my $count_orig_sequence = 0;
	
	while(<STDIN>){
		chop $_;
		
		next if (/^$/ );
		$count_orig_sequence++;
		print "Read $count_orig_sequence sequences ...\n" if ( $count_orig_sequence % 50000  == 0 );

		if (/^$/) { next ;}
		$_ =~ /(^.+?\n)(.*$)/s;

		my $header = $1;
		chop $header;
		my $sequence = $2;
		
		my $out_file_name = "$header".'.fasta';
		open(OUT,">$out_file_name");
		print OUT '>'."$header\n";
		print OUT "$sequence\n";
		close(OUT);
		print "Created file $out_file_name \n";
	}
}


sub translate_rna_to_dna {
	
	my $SUB = 'translate_rna_to_dna';
	$/ ='>';
	my $count_orig_sequence = 0;
	
	while(<STDIN>){
		chop $_;
		next if (/^$/ );
		$count_orig_sequence++;
	
		print "Read $count_orig_sequence sequences ...\n" if ( $count_orig_sequence % 50000  == 0 );

		if (/^$/) { next ;}
		$_ =~ /(^.+?\n)(.*$)/s;
		
		my $header = $1;
		my $sequence = $2;
		
		#translating the RNA to DNA
		# so U to T
		$sequence =~tr/Uu/TT/;
		
		#and get rid of any space in the sequence and header
		$sequence =~s/[\s\n]//g;
		$header =~s/[\n]//g;
		
		
		my @sequence_array = unpack("(A80)*",$sequence);
		chomp $sequence;
		chomp $header;
		
		print '>',$header,"\n",$sequence,"\n";
		#print '>',$header,"\n";

# the following code crashes the samtools faidx on the fasta file 
#		foreach my $line (@sequence_array){
#			chomp $line;
#			print $line,"\n";
#		}
	}
}








## To be done### Logic incomplete

sub scaffold_to_contigs {
	
	my $SUB = 'scaffold_to_contigs';
	
	$/ ='>';
	my $count_orig_sequence = 0;
	
	while(<STDIN>){
		chop $_;
		
		next if (/^$/ );
		$count_orig_sequence++;
		
		print "Read $count_orig_sequence sequences ...\n" if ( $count_orig_sequence % 50000  == 0 );

		if (/^$/) { next ;}
		$_ =~ /(^.+?\n)(.*$)/s;
		
		my $header = $1;
		my $sequence = $2;
	
		$header =~s/\n//g;
		$sequence =~s/\n//g;
		
		print  STDERR "Processing $header ..........\n";
		
		my $count_each_match = 0;
		##now read each sequeunce and split them into contigs if there is NNNNNN > 10 times 
		while( $sequence=~/(.+?)([N]{10,})/gsi) {
			
			$count_each_match++;
			
			print STDERR  "split into $count_each_match contig \n";
			
			my $contig_header = '>'.$header.'_scaffold_'.$count_orig_sequence.'contig_'.$count_each_match."\n";
			my $contig_sequence = $1."\n";
			
			print "$contig_header$contig_sequence";
		}
	}	
}