#!/jgi/tools/bin/perl

use strict;
use FindBin;
use lib "$FindBin::Bin/lib";
use JGI::QAQC::RQCdbAPI;
use JGI::QAQC::QC;
use JGI::QAQC::Utilities;
use File::Path qw(mkpath rmtree);
use Log::Log4perl qw(:easy);
use Data::Dumper;
use Getopt::Long;
use JSON;


## global variables
our ($LOGGER, $BASEPATH, @SEQ_UNITS, @library_names, @seq_units_by_user);
our ($EMAIL, $VERBOSE, $CWD, $BIO_CLASS_NAME, $USER_DEF_BASE_PATH, $DELIVER);
our ($RUN_ON_EACH_FASTQ, $SCRIPT_NAME);


##global flags
our ($cdproj);
our ($create_project_space, $create_mt_project_space);
our	($do_qc);


##main objects for accessing methods in the modules
our ($qc_obj );
our ($rqcdb_obj);

main();



sub main {
	#by default not to run anything on each fastq until specified
	$RUN_ON_EACH_FASTQ = 0;
	
	### Print the standard RQC header 
	get_standard_usage_header();
	
	## get the user arguments
	_get_user_arguments();
	
	## Instantiate the DB access
	$rqcdb_obj = JGI::QAQC::RQCdbAPI->new();
	$rqcdb_obj->logger()->info('####Program Stated######');
	
	
	##Instantiate a object of QC.pm 
	$qc_obj = JGI::QAQC::QC->new();
	
	###set the user def base path for creating project space
	if ($USER_DEF_BASE_PATH){
		$qc_obj->USER_DEF_PATH($USER_DEF_BASE_PATH);
	}
	
	##setting the verbosity
	$rqcdb_obj->verbose($VERBOSE);
	
	##set true if user has asked to create Meta Transcriptome project
	$qc_obj->META_TRANSCRIPTOME_PROJECT($create_mt_project_space);
	
	# get the lib names given the bioclass name
	if ($BIO_CLASS_NAME){
		@library_names = $rqcdb_obj->get_library_names_from_bio_classification_name($BIO_CLASS_NAME);
		print "Libraries associated with $BIO_CLASS_NAME \t @library_names \n";
	}
	
	
	
	# 	Get the final set of seq_unit_names to be included in QC report
	# 	RESULTS available in SEQ_UNITS array
	my $SEQ_UNITS_REF = $rqcdb_obj->get_final_seq_units_set(\@library_names,\@seq_units_by_user);
	print "Total number of seq units used to create DATA hash ",scalar @{$SEQ_UNITS_REF},"\n\n";
	
	##set the set units ref
	$qc_obj->SEQ_UNITS_REF($SEQ_UNITS_REF);

	## create the main hash to hold paths
	$qc_obj->create_data_holder_hash($rqcdb_obj);
	
	###create the JSON object
	my $DATA = $qc_obj->DATA();
	my $json = JSON->new->allow_nonref;
	#print $json->pretty->encode($DATA);
	
	###MAIN LOOP that reads the content from the data hash and does the QC steps
	data_looper();
	

	print Dumper($qc_obj->DATA()) if $VERBOSE;
}



sub data_looper(){
	my $SUB ='QC.pl : data_looper';

	my $DATA = $qc_obj->DATA();
	foreach my $library_name (keys %{$DATA}) {
			print STDERR "working on $library_name \n";
			
				###deliver data if needed
				$qc_obj->create_project_space($library_name) if $create_project_space;

				### do the qc 
				$qc_obj->gen_QC_report($library_name,$rqcdb_obj) if $do_qc;
				
				##cd into the project space for the library
				## exits after going to the directory
				#$qc_obj->cd_to_lib_proj_space($library_name) if ( $cdproj || $create_project_space);
				
				
				#run_the_following_command_on_each_fastq
				print "Run $RUN_ON_EACH_FASTQ \n";
				if($RUN_ON_EACH_FASTQ){
					foreach my $orig_fastq ( keys %{ $DATA->{$library_name}->{'orig_fastq'} } ){
						my $target_fastq = $DATA->{$library_name}->{'orig_fastq'}->{$orig_fastq}->{'target_fastq'};
						my $command = "$SCRIPT_NAME $target_fastq";
						print execute_system_command($rqcdb_obj->logger(),$command, $SUB);
					}
				}
	}
}


sub usage {	
	my $SUB  ='usage';
my $usage_statement = <<USAGE;
This script is meant as a main entry point for DATA QC


USAGE
print $usage_statement;
}




sub _get_user_arguments {
	my $SUB = '_get_user_arguments';

	my %temp;
	if(scalar @ARGV ==  0 ) {
		usage();
		exit 2;
	}
	GetOptions(\%temp,
					"lib_name|lib:s@",
					"seq_units|su:s@",
					"bio_class_name|bcn:s",
					"cdproj!",
					"create_project_space|cps!",
					"create_mt_project_space|mt!",
					"do_qc|qc!",
					"base_path|bp:s",
					"deliver_data|dd!",
                    "verbose|v!",
                    "email|e:i",
                    "run_on_each_fastq|run:s",
              );
    if ( defined $temp{'cdproj'}){
		$cdproj = 1;
	}
	if ( defined $temp{'create_project_space'}){
		$create_project_space = 1;
	}
 	if ( defined $temp{'do_qc'}){
		$do_qc = 1;
	}
    if ( defined $temp{'lib_name'} ){
    	@library_names = @ {$temp{'lib_name'} };
    }
    if ( defined $temp{'seq_units'} ){
    	@seq_units_by_user = @{ $temp{'seq_units'} };
    }
 	if ( defined $temp{'bio_class_name'}){
		$BIO_CLASS_NAME = $temp{'bio_class_name'};
	}
	if ( defined $temp{'base_path'}){
		$USER_DEF_BASE_PATH = $temp{'base_path'};
	}

	if ( defined $temp{'email'} ){
		$EMAIL = $temp{'email'};
	}
	else {
		$EMAIL = 0;
	}

	if ( defined $temp{'verbose'}){
		$VERBOSE = $temp{'verbose'};
	}
	else {
		$VERBOSE = 0;
	}
	
	if ( defined $temp{'deliver_data'}){
		$DELIVER = $temp{'deliver_data'};
	}
	else {
		$DELIVER = 0;
	}
	
	if ( defined $temp{'create_mt_project_space'}){
		$create_mt_project_space = 1;
	}
	else {
		$create_mt_project_space = 0
	}
	if ( defined $temp{'run_on_each_fastq'}){
		$RUN_ON_EACH_FASTQ = 1;
		$SCRIPT_NAME= $temp{'run_on_each_fastq'};
	}
	if ( (scalar @library_names == 0) && ( ! $BIO_CLASS_NAME ) && ( scalar @seq_units_by_user == 0)){
		die " You have not provided either the library name or bio_classfication_name";
	}
}