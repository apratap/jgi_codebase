#!/jgi/tools/bin/perl

use strict;
use FindBin;
use lib "$FindBin::Bin/lib";
use JGI::QAQC::RQCdbAPI;
use JGI::QAQC::Utilities;
use File::Path qw(mkpath rmtree);
use Log::Log4perl qw(:easy);
use Data::Dumper;
use Getopt::Long;


## global variables
our ($LOGGER, $BASEPATH);

main();



sub main {
	
	### Print the standard RQC header 
	get_standard_usage_header();
	
	
	###setup the logger
	config_logger();
	$LOGGER = get_logger();
	
	
	## get the user arguments
	my $library_name = get_user_arguments();
	
	## Instantiate the DB access
	my $obj = JGI::QAQC::RQCdbAPI->new();
	
	## set the library name
	$obj->library_name($library_name);
	
	
	## get the library detail using library name 
	$obj->get_library_info_from_lib_name();
	
	## also get the library detail from venonat
	$obj->get_library_info_from_venonat();
	
	##get the bio_class_name
	my $bioclass_name = $obj->get_bio_classification_name();
	##substituting all the spaces with underscrores as the names will
	##used to create dirs
	$bioclass_name =~s/\s/_/g;
	
	##get the PMO project ID
	my $pmo_project_id = $obj->get_pmo_project_id();
	
	$BASEPATH = '/house/groupdirs/genetic_analysis/rna/projects';
	chomp $BASEPATH;
	
	## Suffixing the  $bioclass_name and $pmo_project_id to $CWD
	$BASEPATH .='/'.$bioclass_name.'_'.$pmo_project_id;
	
	### Check how to set up dir infrastucture ( multiplex / non-multiplex)
	
	
	#######################################
	##MULTIPLEXED-LIBRARY
	#######################################
	if( $obj->is_library_pooled() ) {
		
		print "Library $library_name is Multiplexed \n\n";
		my $result = $obj->get_individual_library_info_from_pooled_lib();

		
		### creating the parent dir for multiplexed main lib
		my $parent_dir = $library_name.'_Mplexed_library';
		
		
		foreach my $individual_seq_unit_name ( keys %{$result}) {
		
		## getting the seq_unit_name for each individual library
		## create a new hash with all the details and return
		
			if ( exists $result->{$individual_seq_unit_name}    )  {
				
				my $library_id 			= $result->{$individual_seq_unit_name}->{'library_id'};
				my $individual_lib_name = $result->{$individual_seq_unit_name}->{'individual_lib_name'};
				
				$obj->seq_unit_name($individual_seq_unit_name);
				my $qualified_individual_seq_unit_name = $obj->get_qualified_seq_unit_name();
				
				deliver_fastq_data($individual_lib_name,$qualified_individual_seq_unit_name,$parent_dir);
			
		
		  	}
	
		}
		
	}
	#######################################
	##SINGLEPLEXED-LIBRARY
	#######################################
	else { 
			print "Library $library_name is Singeplex \n\n";
			
			my $result = $obj->get_singleplex_library_detail();
		
			foreach my $individual_seq_unit_name ( keys %{$result}) {
		
			## getting the seq_unit_name for each individual library
			## create a new hash with all the details and return
		
			if ( exists $result->{$individual_seq_unit_name}    )  {
				
				my $library_id 			= $result->{$individual_seq_unit_name}->{'library_id'};
				my $pooled_lib_name 	= $result->{$individual_seq_unit_name}->{'pooled_lib_name'};
				my $individual_lib_name = $result->{$individual_seq_unit_name}->{'library_name'};
				
				
				$obj->seq_unit_name($individual_seq_unit_name);
				my $qualified_individual_seq_unit_name = $obj->get_qualified_seq_unit_name();
				
				deliver_fastq_data($individual_lib_name,$qualified_individual_seq_unit_name);
			
		
		  	}
	
		}
			
			
	}
	
	
}

sub deliver_fastq_data {
		
		my $SUB = 'deliver_data';
		
		my $base_data_location = '/house/sdm/prod/illumina/seq_data/fastq';
	
		
		
		my $lib_name = shift @_;
		my $file_name = shift @_;
		my $parent_dir = shift @_;
	
		my ($target_dir_path, $target_file_location,$orig_file_location, $project_level_dir);
		
		
		$project_level_dir = "$BASEPATH".'/'."$parent_dir";
		$target_dir_path = "$BASEPATH".'/'."$parent_dir".'/'."$lib_name";		
		$target_file_location = $target_dir_path.'/'."$lib_name".'.'."$file_name";
		$orig_file_location = $base_data_location.'/'."$file_name";
		
		eval {
			mkpath($target_dir_path);

			chmod 0777, $target_dir_path, $project_level_dir;
	
			symlink($orig_file_location, $target_file_location);
			
			print "Linked \n $orig_file_location ----> $target_file_location \n";
			
			
		};
		
		if($@) {
			
			print "Error[$SUB] $@ \n $! \n\n";
		}
	
}

sub get_user_arguments {
	
	# need to improve this
	my $SUB = 'get_user_arguments';
	
	if ( scalar @ARGV == 1 ) {
		return shift @ARGV;
	}
	
	else {
		usage();
		exit 3;
	}
	
}

sub usage {
	
	my $SUB  ='usage';

print<<USAGE;

This script is meant to create dir for RNA-Seq data

$0 <library_name>

Will create a analysis directory/ies for the QC copying the revelant files.
If the library is multiplexed it will create sub dirs for individual multiplexed library

USAGE
	
	
	
}



	
	
