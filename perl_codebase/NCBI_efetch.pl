#!/jgi/tools/bin/perl


use strict;

use Bio::DB::EUtilities;
use Data::Dumper;
use Bio::SeqIO;

use Bio::Taxon;

use lib "/house/homedirs/a/apratap/lib/perl5/site_perl/5.10.1";

use Bio::LITE::Taxonomy::NCBI::Gi2taxid qw/new_dict/;
use Bio::LITE::Taxonomy::NCBI;




use Bio::DB::GenBank;

my @ids = qw(  EU161304 D64122.1 );
my $gbh = Bio::DB::GenBank->new();

foreach my $id( @ids ) {

 my $seq = $gbh->get_Seq_by_acc( $id );
 my $org = $seq->species;
my @classification = $org->classification;
  print "Acc : $id \n";
  print "Org : $org\n";
  print "reverse @classification \n";
}

# say join ' ' , $org->classification , "\n";



#  new_dict (in => "/house/homedirs/a/apratap/playground/NCBI_taxanomy/gi_taxid_nucl.dmp",
#            out => "/house/homedirs/a/apratap/playground/NCBI_taxanomy/gi_taxid_nucl.bin");

#my $dict = Bio::LITE::Taxonomy::NCBI::Gi2taxid->new(dict=>"/house/homedirs/a/apratap/playground/NCBI_taxanomy/gi_taxid_nucl.bin");
#
#
# my $taxid = $dict->get_taxid(203377216);
# 
# print "Tax id : $taxid \n";



##Get one from a database
#  my $taxNCBI = Bio::LITE::Taxonomy::NCBI->new(
#  								 db=>"NCBI",
#                                  nodes=> '/house/homedirs/a/apratap/playground/NCBI_taxanomy/nodes.dmp',
#                                  names=> '/house/homedirs/a/apratap/playground/NCBI_taxanomy/names.dmp',
#                                  dict=>"/house/homedirs/a/apratap/playground/NCBI_taxanomy/gi_taxid_nucl.bin"
#                                  );
#        
#  
# my @taxNCBI = $taxNCBI->get_taxonomy_from_gi(3352);
# 
#foreach my $val (@taxNCBI) {
#	
#	print "$val \n";
#}






#
#my @ids = qw( NW_001884661 EZ361133 CP000490 ) ;
#
##my @ids = qw( NW_001884661) ;
#
#my $factory = Bio::DB::EUtilities->new(-eutil => 'efetch',
#                                       -email => 'apratap@lbl.gov',
#                                       -db    => 'nucleotide',
#                                       -id    => \@ids,
#                                       -rettype => 'gb',
#              						    );
#              						    
#              						    
#              						    
#
#my $file = 'temp.gb';
#
#$factory->get_Response(-file => $file);
#
#my $seqin = Bio::SeqIO->new(-file => $file,
#							-format => 'genbank');
#
#print $seqin,"\n";
#
#while (my $seq = $seqin->next_seq() ) {
#	
#	my $species_object = $seq->species;
#	
#	my $specie_name = $species_object->node_name;
#	
#	my @lineage = $species_object->classification;
#	
#	print "$specie_name \t ",@lineage,"\n";
#	
#}


# Get one from a database
#  my $dbh = Bio::DB::Taxonomy->new(-source   => 'flatfile',
#                                  -directory=> '/scratch',
#                                  -nodesfile=> '/house/homedirs/a/apratap/playground/NCBI_taxanomy/nodes.dmp',
#                                  -namesfile=> '/house/homedirs/a/apratap/playground/NCBI_taxanomy/names.dmp');
#                                  
#                                  
#        
#
#my @ranks = qw(superkingdom class genus species);
#
#my $entrez_dbh = Bio::DB::Taxonomy->new(-source => 'entrez', -ranks=>\@ranks);
#
#
#my @taxon = $dbh->get_taxon(-taxonid=>'510516');
#
#print "Taxon : @taxon \n\n";



