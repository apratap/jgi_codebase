#!/jgi/tools/bin/perl

use strict;
use FindBin;
use lib "$FindBin::Bin/lib";
use JGI::QAQC::RQCdbAPI;
use JGI::QAQC::Utilities;
use File::Path qw(mkpath rmtree);
use Log::Log4perl qw(:easy);
use Data::Dumper;
use Getopt::Long;


## global variables
our ($LOGGER, $BASEPATH, @SEQ_UNITS, @library_names, @seq_units_by_user);
our ($EMAIL, $VERBOSE, $CWD, $BIO_CLASS_NAME, $USER_DEF_BASE_PATH, $DELIVER);


our $base_fastq_data_location = '/house/sdm/prod/illumina/seq_data/fastq/';
our $base_clip_pe_fastq_data_location = '/house/groupdirs/QAQC/rqc_archive/clip_pe/rqc/';

#main hash to track the data being delivered
our $DATA_DELIVERED;

main();


sub main {
	
	
	### Print the standard RQC header 
	get_standard_usage_header();
	
	## get the user arguments
	_get_user_arguments();
	
	## Instantiate the DB access
	my $obj = JGI::QAQC::RQCdbAPI->new();
	$obj->logger()->info('####Program Stated######');
	
	
	##setting the verbosity
	$obj->verbose($VERBOSE);
	
	if ($BIO_CLASS_NAME){
		@library_names = $obj->get_library_names_from_bio_classification_name($BIO_CLASS_NAME);
		print "Libraries associated with $BIO_CLASS_NAME \t @library_names \n";
	}
	
	
# 	Get the final set of seq_unit_names to be included in QC report
# 	RESULTS available in SEQ_UNITS array
	my $SEQ_UNITS_REF = $obj->get_final_seq_units_set(\@library_names,\@seq_units_by_user);
	@SEQ_UNITS = @{$SEQ_UNITS_REF};

	$CWD = get_cwd();
	create_analysis_stage($obj);
	
	print "\n\n\n";
	print Dumper($DATA_DELIVERED) if $VERBOSE;
}
	
	
	
	
sub create_analysis_stage {
	my $SUB = 'create_analysis_stage';
	
	my $obj = shift @_;
	
		foreach my $individual_seq_unit_name ( @SEQ_UNITS){
			
					print "\n\n";
					next if ($individual_seq_unit_name=~/UNKNOWN/);
					
					# Setting the right seq_unit for down_stream called to RQCdbAPI
					$obj->seq_unit_name($individual_seq_unit_name);
				
					###Now fetching the results for each seq_unit_name from RQC db
					$obj->get_lane_level_info();
					$obj->get_library_info_from_seq_unit_name();
					
					## also get the library detail from venonat
					$obj->get_library_info_from_venonat();
					
					###now exclusively setting up the library name parameter
					my $library_name 	= $obj->get_library_name() 				|| $obj->logger()->info("$SUB: Libray Name  can't be determined for $individual_seq_unit_name");
					$obj->library_name($library_name);
					
					my $project_type 	= $obj->get_project_type() 				|| $obj->logger()->info("$SUB: Project type can't be determined for $individual_seq_unit_name");
					my $library_type 	= determine_library_type_short_form($obj);
					my $pmo_project_id 	= $obj->get_pmo_project_id() || $obj->logger()->info("$SUB: PMO Project ID can't be determined for $individual_seq_unit_name");
					my $lib_project_id 		= $obj->get_lib_project_id() || $obj->logger()->info("$SUB: Library Project ID can't be determined for $individual_seq_unit_name");
					my $qualified_seq_unit_name = $obj->get_qualified_seq_unit_name() || $obj->logger()->info("$SUB: Qualfiied seq_unit_name name can't be determined for $individual_seq_unit_name");
					my $bioclass_name 	= $obj->get_bio_classification_name()	|| $obj->logger()->info("$SUB: Bioclassifcation name can't be determined for $individual_seq_unit_name");
					##substituting all the spaces with underscrores as the names will
					##used to create dirs
					$bioclass_name =~s/\s/_/g;

					print STDERR "working on $library_name \t $library_type \t $bioclass_name \t $individual_seq_unit_name \n";

					##METAGENOME PROGRAM
					if($project_type =~/Metagenome/){
						create_MG_project_space($obj,'metagenome',$pmo_project_id,$bioclass_name,$qualified_seq_unit_name,$library_name,$library_type)
					}
					
					##MICROBIAL PROGRAM
					if ($project_type =~/Whole Genome/){
						create_Microbe_project_space($obj,'whole_genome',$pmo_project_id,$bioclass_name,$qualified_seq_unit_name,$library_name,$library_type);
					}
			}
}



sub create_MG_project_space(){
	my $SUB = 'create_MG_project_space';
	
	my $MG_BASEPATH = '/house/groupdirs/QAQC/seq_qc/metagenomes/';

	my $obj 					= shift @_;
	my $program					= shift @_;
	my $pmo_project_id 			= shift @_;
	my $bioclass_name 			= shift @_;
	my $qualified_seq_unit_name = shift @_;
	my $library_name 			= shift @_;
	my $library_type			= shift @_;
	
	my $BASEPATH;	
	if ( $USER_DEF_BASE_PATH ) { 
		$BASEPATH = $USER_DEF_BASE_PATH 
	}
	else {
		$BASEPATH = $MG_BASEPATH;
	}
	
	my $target_dir_path;
	my $target_seq_unit_name = "$library_name".'.'."$qualified_seq_unit_name";
	
	###if the library is pooled
	if ( $obj->is_library_part_of_pool() ){ 
		my ($pooled_lib_name,$pooled_lib_bioclass_name,$pooled_lib_pmo_project_id) = $obj->get_pooled_lib_info();
		$pooled_lib_bioclass_name =~s/\s/_/g;
		print "$pooled_lib_name \t $pooled_lib_bioclass_name \t $pooled_lib_pmo_project_id \t $library_name \n" if $VERBOSE;
		$target_dir_path = $BASEPATH.'/'.$pooled_lib_bioclass_name.'_'.$pooled_lib_pmo_project_id.'/'.$pooled_lib_name.'_Mplexed_library/'.$library_name.'/';
	}
	
	### else the library is not pooled
	else {
		$target_dir_path = $BASEPATH.'/'.$bioclass_name.'_'.$pmo_project_id.'/'.$library_name.'/';
	}
	
	$DATA_DELIVERED->{$library_name}->{'target_location'} = $target_dir_path;
	$DATA_DELIVERED->{$library_name}->{'fastq'} = $target_seq_unit_name;
	$DATA_DELIVERED->{$library_name}->{'bio_class_name'} = $bioclass_name;
	$DATA_DELIVERED->{$library_name}->{'library_type'} = $library_type;
	$DATA_DELIVERED->{$library_name}->{'program'} = $program;
	
	
#	print ($base_fastq_data_location, "\n" , $target_dir_path ,"\n", $qualified_seq_unit_name,"\n" ,$target_seq_unit_name);
	deliver_fastq_data($obj,$base_fastq_data_location,$target_dir_path,$qualified_seq_unit_name,$target_seq_unit_name) if $DELIVER;
	print "\n\n\n";
}



sub create_Microbe_project_space() {
	my $SUB = 'create_Microbe_project_space';
	
	my $MICROBES_BASEPATH = '/house/groupdirs/pi/project/';
	
	my $obj 					= shift @_;
	my $program					= shift @_;
	my $pmo_project_id 			= shift @_;
	my $bioclass_name 			= shift @_;
	my $qualified_seq_unit_name = shift @_;
	my $library_name 			= shift @_;
	my $library_type			= shift @_;
	
	
	my $BASEPATH;	
	if ( $USER_DEF_BASE_PATH ) { 
		$BASEPATH = $USER_DEF_BASE_PATH 
	}
	else {
		$BASEPATH = $MICROBES_BASEPATH;
	}

	my $target_dir_path = $BASEPATH.'/'.$pmo_project_id.'/ill_dir/'."$library_type".'_'."$library_name".'/';
	my $target_seq_unit_name = "$library_name".'.'."$qualified_seq_unit_name";
	
	$DATA_DELIVERED->{$library_name}->{'target_location'} = $target_dir_path;
	$DATA_DELIVERED->{$library_name}->{'fastq'} = $target_seq_unit_name;
	$DATA_DELIVERED->{$library_name}->{'bio_class_name'} = $bioclass_name;
	$DATA_DELIVERED->{$library_name}->{'library_type'} = $library_type;
	$DATA_DELIVERED->{$library_name}->{'program'} = $program;
	
	deliver_fastq_data($obj,$base_fastq_data_location,$target_dir_path,$qualified_seq_unit_name,$target_seq_unit_name) if $DELIVER;

	###also symlink the CATG removed data to the clip_pe data
	if ($library_type eq 'clip_pe'){
		my $seq_unit_prefix = $qualified_seq_unit_name;
		$seq_unit_prefix =~s/\.fastq.gz//;
		my $CATG_removed_file_name = $seq_unit_prefix.'.jCATG.fastq.gz';
		my $target_seq_unit_name = "$library_name".'.'.$CATG_removed_file_name;
		my $target_dir_path = 	$DATA_DELIVERED->{$library_name}->{'target_location'};
		deliver_fastq_data($obj,$base_clip_pe_fastq_data_location,$target_dir_path,$CATG_removed_file_name,$target_seq_unit_name) if $DELIVER;
		
		$DATA_DELIVERED->{$library_name}->{'fastq_jCATG'} = $target_seq_unit_name;
	}
	
	
	
	##checking on assembly for standard library
	if ($library_type eq 'standard'){
	my $max_contifg_size_from_velvet_assembly = $obj->get_max_contig_size();
	
	print "$library_name \t max_contig_size \t $max_contifg_size_from_velvet_assembly \n";
	
	}
	
	
	
}
	

sub deliver_fastq_data {
		
		my $SUB = 'deliver_fastq_data';
		
		my $obj = shift @_;
		my $base_path = shift @_;
		my $target_path = shift @_;
		my $base_file_name = shift @_;
		my $target_file_name = shift @_;
	
		if (! -e $base_path.$base_file_name ) {
			$obj->logger->fatal("File path $base_path/$base_file_name is INVALID \n");
			exit 2;
		}
		eval {
			mkpath($target_path, {mode=>0775});
			chmod 0775, "$target_path";
			symlink($base_path.$base_file_name, $target_path.$target_file_name );
			print "Created: $target_path$target_file_name ...Success\n";
		};
		if($@) {
			print "Error[$SUB] $@ -> $! \n";
		}
	
}


sub determine_library_type_short_form(){
	my $SUB = 'determine_library_type_short_form';

	my $obj = shift @_;
	my $library_type_long = $obj->get_library_type() || $obj->logger()->info("$SUB: Libray type  can't be determined");
	
	if ($library_type_long=~/Illumina Std/){
		return 'standard';
	}
	if ($library_type_long=~/CLIP/){
		return 'clip_pe';
	}	
}









sub usage {
	
	my $SUB  ='usage';

my $usage_statement = <<USAGE;

This script is meant to create dir for MG  data

$0 <library_name>

Will create a analysis directory/ies for the QC copying the revelant files.
If the library is multiplexed it will create sub dirs for individual multiplexed library


USAGE

print $usage_statement;

}




sub _get_user_arguments {
	
	# need to improve this
	my $SUB = '_get_user_arguments';

	my %temp;
	
	if(scalar @ARGV ==  0 ) {
		usage();
		exit 2;
	}
	
	GetOptions(\%temp,
					"lib_name|lib:s@",
					"seq_units|su:s@",
					"bio_class_name|bcn:s",
					"base_path|bp:s",
					"deliver_data|dd!",
                    "verbose|v!",
                    "email|e:i",
              );
    
    
    if ( defined $temp{'lib_name'} ){
    	@library_names = @ {$temp{'lib_name'} };
    }
    
    if ( defined $temp{'seq_units'} ){
    	@seq_units_by_user = @{ $temp{'seq_units'} };
    	
    }
    
 	if ( defined $temp{'bio_class_name'}){
		$BIO_CLASS_NAME = $temp{'bio_class_name'};
	}
	if ( defined $temp{'base_path'}){
		$USER_DEF_BASE_PATH = $temp{'base_path'};
	}
	
	
	if ( defined $temp{'email'} ){
		$EMAIL = $temp{'email'};
	}
	else {
		$EMAIL = 0;
	}
	if ( defined $temp{'verbose'}){
		$VERBOSE = $temp{'verbose'};
	}
	else {
		$VERBOSE = 0;
	}
	
	if ( defined $temp{'deliver_data'}){
		$DELIVER = $temp{'deliver_data'};
	}
	else {
		$DELIVER = 0;
	}
	
	if ( (scalar @library_names == 0) && ( ! $BIO_CLASS_NAME ) && ( scalar @seq_units_by_user == 0)){
		die " You have not provided either the library name or bio_classfication_name";
	}
	
}




	
	