#!/jgi/tools/bin/perl

#========================================================================================
#Disclaimer: ----- 
#This program is written by Abhishek Pratap (apratap@lbl.gov)
#Script Name : edit_sam.pl
#======================================================================================== 

use strict;
use Data::Dumper;
use POSIX 'isatty';
use Getopt::Long;
use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/lib";
use FindBin;
use Log::Log4perl qw(:easy);
use JGI::QAQC::Utilities;
use Storable;
use BerkeleyDB;
use Bio::DB::Sam;
use Benchmark;


#####
our $MIN_INNER_FRAGMENT_LENGTH = 200;
our $MAX_INNER_FRAGMENT_LENGTH = 100000; 


use vars qw ( $verbose $logger $count_sam_lines_read_1  $manually_pair_read_1_read_2 $READS  $READ_COUNT  @HEADER);
use vars qw ($read_1_sam $read_2_sam @sam_header_read_1 @sam_header_read_2 $direction_data_file $direction_data_hash);
use vars qw ($FH_MATE_PAIRS $FH_CHIMERAS $FH_UNMAPPED $FH_ORPHANED $FH_PAIR_ENDS $FH_POSSIBLE_INVERSIONS);
use vars qw ( $convert_to_fastq $keep_singeltons  $count_num_mate_pairs $count_num_unmapped_pairs $count_num_paired_ends $count_num_chimeras  $count_num_unmapped_pairs); 
use vars qw ($count_num_orphaned_pairs $count_num_possible_inversions  $count_exact_rev_comp_pairs);
use vars qw ($count_num_pairs_dir_determined);
use vars qw ($count_uniq_pair $count_one_read_uniq $count_both_read_not_uniq);
use vars qw ($count_pairs_processed $missing_pairs_in_sam_files);


main();


sub main {
	#set up the logger
	config_logger();
	$logger = get_logger();

	#get the use args
	get_user_args();
	
	 #split the paried sam file and also spit out the unpaired reads
	 if ($convert_to_fastq) {
    	check_input();
    	convert_sam_to_paired_fastq();
    	print_left_over_singelton_reads() if $keep_singeltons;
    }
    
    #manually pair the read1/2 : mainly written for RNA-PET data
    pair_read_1_read_2()	if ($manually_pair_read_1_read_2);
} ##END OF MAIN

################################

sub get_user_args {
	my $SUB = 'get_user_args';
	my %input;
	GetOptions(\%input,
					"convert_to_fastq|fq:i",
					"keep_singeltons|ks:i",
					"pair_read_1_read_2|pair:i",
					"read_1_sam|r1s:s",
					"read_2_sam|r2s:s",
					"direction_file|df:s",
					"verbose|v!",
					"help|h!"
					);

	#verbose
	$verbose =  $input{'verbose'} || '0';
	
	if ( scalar keys %input == 0) {
		$logger->info("Command Line Arguments used \n", Dumper(\%input) );
		usage('Please specify the arguments');
		exit 4;
	}
	if ($input{'help'}){
		usage('Exit with user help');
		exit 0;
	}
	if ( defined $input{'convert_to_fastq'} ) {
		$convert_to_fastq = $input{'convert_to_fastq'};
	}
	if ( defined $input{'keep_singeltons'} ) {
		$keep_singeltons = $input{'keep_singeltons'};
	}
	if ($convert_to_fastq) {
		print STDERR "Converting the input SAM file to fastq..output will be in STDOUT \n";
		
		if($keep_singeltons){
			print STDERR "Singeltons will be kept \n\n";
		}
		else {
			print STDERR "Singeltons will not be kept \n";
		}
	}
	if(   $input {'pair_read_1_read_2'} ) {
		#checking on read_1_sam
		if (   ( $input{'read_1_sam'}  ) &&  ( -e $input{'read_1_sam'}   )  )  {
			$read_1_sam = $input{'read_1_sam'};
		}
		else{
			$logger->fatal("Read 1 sam file not entered \n");
			exit 3;
		}
		#checking on read_2 sam
		if (   ( $input{'read_2_sam'}  ) &&  ( -e $input{'read_2_sam'}   )  )  {
				$read_2_sam = $input{'read_2_sam'};
		}
		else{
			$logger->fatal("Read 2 sam file not entered \n");
			exit 3;
		}
		$manually_pair_read_1_read_2 = $input{'pair_read_1_read_2'};
		## file which stores the direction info
		if (defined $input{'direction_file'}){
			$direction_data_file = $input{'direction_file'};
		}
	}
}

sub check_input() {
	if ( isatty(*STDIN) ) {
		 usage();
	}
}

sub usage {
	my $SUB = 'usage';
	my $message = shift @_;
	
	print qq (
	#MESSAGE : $message

	##mannual pairing
	 $0 -pair 1 -r1s read_1.fastq_aligned.sam -r2s read_2.fastq_aligned.sam

	### extracting unmapped read pairs
	cat IBTO.1925.3.1679._phiX_174.sam | $0 -fq 1
	);
	print "\n";
	exit 2;
}

sub convert_sam_to_paired_fastq {
	
	my $SUB = 'convert_sam_to_paired_fastq';
	
	my $count_sam_lines = 0;
	my $count_read_1 = 0;
	my $count_read_2 = 0;
	my $count_pairs  = 0;
	
	
	#\$logger->info("Reached $SUB ");

	
	while(<STDIN>) {
		
		
		chomp $_;
		$count_sam_lines++;
		
		if ($count_sam_lines%100000 == 0) { print STDERR "Processed $count_sam_lines SAM lines \n";}
		
#		print $_,"\n";
		my @sam_line = split('\t',$_);
		
		#bare minimum sanity check
		if( scalar @sam_line < 11 ){
			print STDERR "WARN[$SUB] Sam line $count_sam_lines doesn't contain minimum 11 fields \n";
			next ;
		}
		
		my $read_header = $sam_line[0];
		my $flag 		= $sam_line[1];
		my $sequence 	= $sam_line[9];
		my $quality		= $sam_line[10]; 
		my $read_type;
		
		
		#taking out the trailing /1 or /2 from the header
		#making the header agnostic of the method used to align the read
		#bowtie leaves /1 /2 whereas bwa removed /1 and /2 and identification is done based on SAM flag
		$read_header  	=~s/\/[1|2].*$//;
		
#		print "$_\n\n";
#		print "$read_header \t $flag \t $sequence \t $quality \n";
		
		#this will make this work technically on any mapper which as the screening is based on read bitwise sam flag
		#check if read 1
		if( $flag & hex('0x40')) {
		
			my $read_type = 1;
			$count_read_1++;	
		
			$READS->{$read_header}->{$read_type}->{'sequence'}	=	$sequence;
			$READS->{$read_header}->{$read_type}->{'quality'}	=	$quality;
		
			##checking if the read 2 already is present in the hash
			if ( defined $READS->{$read_header}->{'2'}) {
				
				$count_pairs++;
				print_read_pair($read_header)
			}
		
		}
		
		##check if read 2
		elsif( $flag & hex('0x80')){
		
			my $read_type = 2;
			$count_read_2++;	
		
		
			$READS->{$read_header}->{$read_type}->{'sequence'}	=	$sequence;
			$READS->{$read_header}->{$read_type}->{'quality'}	=	$quality;
		
			##checking if the read 1 already is present in the hash
			if ( defined $READS->{$read_header}->{'1'}) {
				$count_pairs++;
				print_read_pair($read_header)
			}
		
			
		}
		
		else {
			
			my $read_type = 1;
			#We will just assume they are not read 1/2 ; most likely singeltons
			$READS->{$read_header}->{$read_type}->{'sequence'}	=	$sequence;
			$READS->{$read_header}->{$read_type}->{'quality'}	=	$quality;
		}
		
		
		
		
		
		
	} #end READING THE sam FILE
	
	
	my $count_singeltons = count_left_over_singelton_reads();
	
	print STDERR "=======Summary ========\n";
	print STDERR "Total number of entries in sam file \t $count_sam_lines \n";
	print STDERR "Total number of read 1 \t $count_read_1 \n";
	print STDERR "Total number of read 2 \t $count_read_2 \n";
	print STDERR "Total number of pairs \t $count_pairs \n";
	print STDERR "Total number of singeltons \t $count_singeltons \n";
	
}

sub print_read_pair {
	
	my $SUB = 'print_read_pair';
	
	my $read_header = shift @_ ;
	
	my $read_1_header 			= '@'.$read_header.'/1'."\n";
	my $read_1_sequence 		= $READS->{$read_header}->{'1'}->{'sequence'}."\n";
	my $read_1_quality_header	= '+'."\n";
	my $read_1_quality			= $READS->{$read_header}->{'1'}->{'quality'}."\n";
	
	my $read_2_header 			= '@'.$read_header.'/2'."\n";
	my $read_2_sequence 		= $READS->{$read_header}->{'2'}->{'sequence'}."\n";
	my $read_2_quality_header	= '+'."\n";
	my $read_2_quality			= $READS->{$read_header}->{'2'}->{'quality'}."\n";
	
	
	print STDOUT "$read_1_header$read_1_sequence$read_1_quality_header$read_1_quality";
	print STDOUT "$read_2_header$read_2_sequence$read_2_quality_header$read_2_quality";
	
	delete $READS->{$read_header};
}

sub print_left_over_singelton_reads {
	
#	print Dumper ($READS);
	
	my $SUB = 'print_left_over_singelton_reads';
	
	my $count_singeltons = 0;
	
	foreach my $read_header ( keys %{$READS}) { 
		foreach my $read_type ( keys %{ $READS->{$read_header} }  ){
			$count_singeltons++;
			my $full_read_header			= '@'.$read_header.'/'.$read_type."\n";
			my $read_sequence 			= $READS->{$read_header}->{$read_type}->{'sequence'}."\n";
			my $read_quality_header		= '+'."\n";
			my $read_quality			= $READS->{$read_header}->{$read_type}->{'quality'}."\n";
			
#			printing the singeltons
			print STDOUT "$full_read_header$read_sequence$read_quality_header$read_quality";
		}	
	
	}
	
	
	return ($count_singeltons);
	
	
}

sub count_left_over_singelton_reads {
	
#	print Dumper ($READS);
	my $SUB = 'count_left_over_singelton_reads';
	my $count_singeltons = 0;
	$count_singeltons = scalar keys(%{$READS});
	return ($count_singeltons);
}



##manually perform the read 1/2
sub pair_read_1_read_2 {
	my $SUB = 'pair_read_1_read_2';

	
	
	

	open (my $IN_READ_1,"< $read_1_sam") || $logger->fatal("File $read_1_sam can't be opened \n\n ");
	open (my $IN_READ_2,"< $read_2_sam") || $logger->fatal("File $read_2_sam can't be opened \n\n ");
	
	
	#counter initialization
	$count_pairs_processed 	    = 0;
	$count_num_chimeras 		= 0;
	$count_num_mate_pairs 		= 0;
	$count_num_paired_ends 		= 0;
	$count_num_unmapped_pairs 	= 0;
	$count_num_orphaned_pairs   = 0;
	$count_num_possible_inversions = 0;
	$count_num_pairs_dir_determined = 0;
	$missing_pairs_in_sam_files 	= 0;
	$count_one_read_uniq			= 0;
	$count_both_read_not_uniq       =  0;

	
	my $line_file1 = read_one_line_from_file($IN_READ_1);
	my $line_file2 = read_one_line_from_file($IN_READ_2);
	
	
	my $start = new Benchmark;
	
	while ($line_file1 && $line_file2){
		
			
		##skip the sam file headers
		 if (  ($line_file1 =~/^\@[SQ|HD].*$/ )  || ( $line_file2 =~/^\@[SQ|HD].*$/) ) {
		 	push(@HEADER,$line_file1); 
		 	$line_file1 = read_one_line_from_file($IN_READ_1);
			$line_file2 = read_one_line_from_file($IN_READ_2);
		 	next ; 
		 }
		 elsif( ($line_file1 =~/^\@[PG|RG].*$/) &&  ($line_file2 =~/^\@[PG|RG].*$/) ){ 
		 	push(@HEADER,$line_file1); 
		 	$line_file1 = read_one_line_from_file($IN_READ_1);
			$line_file2 = read_one_line_from_file($IN_READ_2);
		 	next; 
		 }
		 elsif ($count_pairs_processed == 0) {
		 	print "Sam header read : Creating the new files and handles ....";
		 	open_file_handles();
		 	print "Done \n";
		 }
		 
		
		##only increment the counter if sam line has been found
		$count_pairs_processed++;

		if ( $count_pairs_processed % 100000 == 0) {
			my $end = new Benchmark;
			my $diff = timediff($end,$start);
			print "Read pairs  $count_pairs_processed processed in  \t",timestr($diff,'all'),"\n";
			$start = new Benchmark;
		}		
		
		my $READ_PAIR_HASH_REF;
		$READ_PAIR_HASH_REF = read_sam_file(\$line_file1,1,$READ_PAIR_HASH_REF);
		$READ_PAIR_HASH_REF = read_sam_file(\$line_file2,2,$READ_PAIR_HASH_REF);
		
		if (scalar keys %{$READ_PAIR_HASH_REF} > 1) {
			print "MAJOR ERORR[$SUB] proper read pair not formed : most likely read order not preserved in two sam files \n ", keys %{$READ_PAIR_HASH_REF};
			exit 2;
		}
		implement_pairing_logic($READ_PAIR_HASH_REF);
		
		
		##read the next line
		$line_file1 = read_one_line_from_file($IN_READ_1);
		$line_file2 = read_one_line_from_file($IN_READ_2);
	}
	


	###
	print_analysis_summary();

}








##takes a file handle and return one line per call
sub read_one_line_from_file{
	my $SUB = 'read_one_line_from_file';
	my $file_handle = shift @_;
	if ( $file_handle &&  (my $line = <$file_handle>) ) {
		chomp $line;
#		print $line;
		return $line;
	}
}


##open all the file required file handles
sub open_file_handles (){
	my $SUB = 'open_file_handles';
	
	open($FH_MATE_PAIRS, ">mate_pairs.sam") || $logger->fatal("$SUB cant open file \n");
	print_sam_header($FH_MATE_PAIRS);

	open($FH_CHIMERAS, ">chimeras.sam") || $logger->fatal("$SUB cant open file \n");
	print_sam_header($FH_CHIMERAS);
	
	
	open($FH_UNMAPPED, ">unmapped.sam") || $logger->fatal("$SUB cant open file \n");
	print_sam_header($FH_UNMAPPED);
	
	open($FH_ORPHANED, ">orphaned.sam") || $logger->fatal("$SUB cant open file \n");
	print_sam_header($FH_ORPHANED);
	
	
	open($FH_PAIR_ENDS, ">paired_ends.sam") || $logger->fatal("$SUB cant open file \n");
	print_sam_header($FH_PAIR_ENDS);
	
	
	open($FH_POSSIBLE_INVERSIONS, ">possible_inversions.sam") || $logger->fatal("$SUB cant open file \n");
	print_sam_header($FH_POSSIBLE_INVERSIONS);
	
	
	
}


sub print_analysis_summary {
	my $SUB = 'print_analysis_summary';
	
	$logger->info("\n");
	$logger->info("#reads(r1+r1) \t",$count_pairs_processed);
	$logger->info("#uniq_pairs \t $count_uniq_pair");
	$logger->info("#one_read_uniq	\t $count_one_read_uniq ");
	$logger->info("#both_reads_NOT_uniq \t $count_both_read_not_uniq");
	$logger->info("#unmapped_pairs \t $count_num_unmapped_pairs");
#	$logger->info("#incomplete pair \t $ $missing_pairs_in_sam_files");
	$logger->info("#mate_pairs \t $count_num_mate_pairs  ");
	$logger->info("#paired_ends \t  $count_num_paired_ends ");
	$logger->info("#orphaned_pairs \t $count_num_orphaned_pairs ");
	$logger->info("#Chimeric_pairs \t $count_num_chimeras ");
	$logger->info("#Inversion pairs \t $count_num_possible_inversions ");
	$logger->info("#pairs_dir_determined \t $count_num_pairs_dir_determined ");
	
}


sub sam_header_sanity_check {
	my $SUB = 'sam_header_sanity_check';
	
	$logger->info("$SUB");
	$logger->info("$SUB checking if the header in both the SAM file are should ..they shud be same");
	
	#test 1
	if (scalar @sam_header_read_1 != scalar @sam_header_read_2 ){
		$logger->fatal("$SUB number of entries in the sam headers for read 1/2 sam files are different..they shud be equal \n !!Please check");
		exit 4;
	}
	#test 2
	for(my $i =0; $i <=  $#sam_header_read_1; $i++ ) {
		if( $sam_header_read_1[$i]  ne $sam_header_read_2[$i]) {
			$logger->fatal("$SUB SAM header entry at line $i are not same $sam_header_read_1[$i] != $sam_header_read_2[$i] !!Please check");
			exit 4;
		}
	}
	$logger->info("Success");
}


sub print_sam_header {
	my $SUB = 'print_sam_header';
	my $FILE_HANDLE = shift @_;
	for(my $i =0; $i <=  $#HEADER; $i++ ) {
		print $FILE_HANDLE $HEADER[$i]."\n";
	}
}




#the actual pairing algo
sub implement_pairing_logic {
	my $SUB = 'implement_pairing_logic';
	
	my $HASH_REF = shift @_;
	
##actually pairing start
#	Biological Conditions
#	1. Same Chromosome
#	2. Mate pair : reads pointing outwards ( <----- ------>)
#	3. Check for multiple mappings and 
	
	NEXT_READ:
	foreach  my $READ ( keys %{$HASH_REF}  ) {
		
				###for counting the uniq/non uniq read pairs
				my $read_1_num_mappings =   scalar keys %{ $HASH_REF->{$READ}->{'1'} };
				my $read_2_num_mappings =   scalar keys %{ $HASH_REF->{$READ}->{'2'} };
				
				if  (  ($read_1_num_mappings == 1) && ( $read_2_num_mappings == 1) ){
					$count_uniq_pair++;
				}
				elsif (  ($read_1_num_mappings > 1) && ( $read_2_num_mappings > 1) ) {
					$count_both_read_not_uniq++;
				}
				elsif ( ($read_1_num_mappings > 1) && ( $read_2_num_mappings == 1) ) {
					$count_one_read_uniq++;
				}
				elsif ( ($read_1_num_mappings  == 1) && ( $read_2_num_mappings > 1) ) {
					$count_one_read_uniq++;
				}
				elsif ( ($read_1_num_mappings  == 0) || ( $read_2_num_mappings == 0) ) {
					$missing_pairs_in_sam_files++;
				}
	
			#get all the read 1 mappings positions
			foreach my $read_1_pos ( keys %{ $HASH_REF->{$READ}->{'1'} }) {
				my $read_1_sam_flag = $HASH_REF->{$READ}->{'1'}->{$read_1_pos}->{'orig_sam_flag'};
				my $ref_name_read_1 = $HASH_REF->{$READ}->{'1'}->{$read_1_pos}->{'ref_name'};

				$logger->fatal("$READ/1 doesn't have sam_flag" ) unless defined $read_1_sam_flag;
				$logger->fatal("$READ/1 doesn't have ref_name" ) unless defined $ref_name_read_1 ;
					#get all the read 2 mappings positions
					foreach my $read_2_pos ( keys %{ $HASH_REF->{$READ}->{'2'} }) {
						my $read_2_sam_flag = $HASH_REF->{$READ}->{'2'}->{$read_2_pos}->{'orig_sam_flag'};
						my $ref_name_read_2 = $HASH_REF->{$READ}->{'2'}->{$read_2_pos}->{'ref_name'};
								
						$logger->fatal("$READ/2 doesn't have sam_flag" ) unless defined $read_2_sam_flag;
						$logger->fatal("$READ/2 doesn't have ref_name" ) unless defined $ref_name_read_2 ;
						
#						print "$READ/1 $read_1_pos \t $read_1_sam_flag \t $ref_name_read_1 \n" if $verbose;
#						print "$READ/2 $read_2_pos \t $read_2_sam_flag \t $ref_name_read_2 \n" if $verbose;
						
						print "Comparing $READ $read_1_pos \t $read_1_sam_flag \t $ref_name_read_1 to $read_2_pos \t $read_2_sam_flag \t $ref_name_read_2 \n" if $verbose;
								
						#####################################
						#checking the conditions
						#################################direction.txt_hash_ref###
								
						#1. if both the reads are unmapped print and skip the pair
						if ( if_unmapped_pair_print_to_sam_file ($READ,$read_1_pos,$read_2_pos,$read_1_sam_flag,$read_2_sam_flag,$HASH_REF)  ){
#							print "unmapped_pair_found \n";
							next NEXT_READ;
						}
						#2. if only one read maps 
						if ( if_orphaned_pair_print_to_sam_file($READ,$read_1_pos,$read_2_pos,$read_1_sam_flag,$read_2_sam_flag,$HASH_REF)) {
#							print "orphaned_pair_found \n";
							next NEXT_READ;
						}
						#3. check for mate pairs and paired end
						if ($ref_name_read_1 eq $ref_name_read_2){
								#3-A Now check if it is a mate pair					
								if ( if_mate_pair_print_to_sam_file($READ,$read_1_pos,$read_2_pos,$read_1_sam_flag,$read_2_sam_flag,$HASH_REF) ) {
#									print "mate_pair_found \n";
									next NEXT_READ;
								}
								#3-B Now check if it is a paired end
								if( if_pair_end_print_to_sam_file($READ,$read_1_pos,$read_2_pos,$read_1_sam_flag,$read_2_sam_flag,$HASH_REF) ) {
#									print "paired_end_found \n";
									next NEXT_READ;
								}
						}
					
						#4. chimera read pair
						if ( if_chimera_pair_print_to_sam_file($READ,$read_1_pos,$read_2_pos,$read_1_sam_flag,$read_2_sam_flag,$ref_name_read_1,$ref_name_read_2,$HASH_REF) ){
							next NEXT_READ;
						}
						#5. possible inversions
						if ( if_possible_inversion_print_to_sam_file($READ,$read_1_pos,$read_2_pos,$read_1_sam_flag,$read_2_sam_flag,$ref_name_read_1,$ref_name_read_2,$HASH_REF) ){
							next NEXT_READ;
						}
						
					}
			}
	}
	


}





sub if_unmapped_pair_print_to_sam_file {
	my $SUB = 'if_unmapped_pair_print_to_sam_file';
	
	my $READ 				= shift @_;
	my $read_1_pos			= shift @_;
	my $read_2_pos  		= shift @_;
	my $read_1_sam_flag 	= shift @_;
	my $read_2_sam_flag		= shift @_;
	my $HASH_REF			= shift @_;
	
	my $new_sam_flag_read_1;
	my $new_sam_flag_read_2;
	
	#both reads map but not on the same chromosome
	if ( ($read_1_sam_flag & hex(0x04) ) &&  ($read_2_sam_flag & hex(0x04) ) ) {
		$count_num_unmapped_pairs++;
			#fixing the insert size
					$HASH_REF->{$READ}->{'1'}->{$read_1_pos}->{'template_len'} = 0;
					$HASH_REF->{$READ}->{'2'}->{$read_2_pos}->{'template_len'} = 0;
		
		my $new_sam_flag_read_1 = 1 + 4 + 64 ; # ( read is paired, unmapped, first in pair);
		my $new_sam_flag_read_2 = 1 + 4 + 128 ; # ( read is paired, unmapped , second in pair);
		
		$HASH_REF->{$READ}->{'1'}->{$read_1_pos}->{'new_sam_flag'} = $new_sam_flag_read_1;
		$HASH_REF->{$READ}->{'2'}->{$read_2_pos}->{'new_sam_flag'} = $new_sam_flag_read_2;
		
#		print "$SUB : read 1 sam flag $new_sam_flag_read_1 \n";
#		print "$SUB : read 2 sam flag $new_sam_flag_read_2 \n";
#		print get_sam_line($READ,$read_1_pos,$read_2_pos,1);

		print $FH_UNMAPPED get_sam_line($READ,$read_1_pos,$read_2_pos,1,$HASH_REF),"\n";
		print $FH_UNMAPPED get_sam_line($READ,$read_2_pos,$read_1_pos,2,$HASH_REF),"\n";
		
		delete $HASH_REF->{$READ};
		return 1
	}
	else{
		return 0;
	}
}



sub if_orphaned_pair_print_to_sam_file {
	
	my $SUB = 'if_orphaned_pair_print_to_sam_file';
	
	my $READ 				= shift @_;
	my $read_1_pos			= shift @_;
	my $read_2_pos  		= shift @_;
	my $read_1_sam_flag 	= shift @_;
	my $read_2_sam_flag		= shift @_;
	my $HASH_REF			= shift @_;
	
	my $new_sam_flag_read_1;
	my $new_sam_flag_read_2;
	
	#read 1 is not mapped and read 2 is mapped
	if ( ($read_1_sam_flag & hex(0x04) ) &&  (~$read_2_sam_flag ) ) {
		
		$count_num_orphaned_pairs++;
		
		#fixing the insert size
		$HASH_REF->{$READ}->{'1'}->{$read_1_pos}->{'template_len'} = 0;
		$HASH_REF->{$READ}->{'2'}->{$read_2_pos}->{'template_len'} = 0;
		
		#read 2 maps in forward direction
		if ( $read_2_sam_flag == 0 ) {
				
				#fixing the sam flag
				$new_sam_flag_read_1 = 1 + 4 + 64 ; # ( read is paired, unmapped, first in pair);
				$new_sam_flag_read_2 = 1 + 8 + 128 ; # ( read is paired, mapped_in_forward, mate_unmapped , second in pair);
				
				$HASH_REF->{$READ}->{'1'}->{$read_1_pos}->{'new_sam_flag'} = $new_sam_flag_read_1;
				$HASH_REF->{$READ}->{'2'}->{$read_2_pos}->{'new_sam_flag'} = $new_sam_flag_read_2;
				
				
				#fixing the ref_next #col7		
				$HASH_REF->{$READ}->{'1'}->{$read_1_pos}->{'ref_next'} = $HASH_REF->{$READ}->{'2'}->{$read_2_pos}->{'ref_name'};
				
				print $FH_ORPHANED get_sam_line($READ,$read_1_pos,$read_2_pos,1,$HASH_REF),"\n";
				print $FH_ORPHANED get_sam_line($READ,$read_2_pos,$read_1_pos,2,$HASH_REF),"\n";
				
			return 1;
				
		}
		#read 2 maps in reverse direction
		elsif ( $read_2_sam_flag &  hex(0x10) ){
					
				#fixing the sam flag
				$new_sam_flag_read_1 = 1 + 4 + 32 + 64 ; # ( read is paired, unmapped, mate_reversed, first in pair);
				$new_sam_flag_read_2 = 1 + 8 + 16 + 128 ; # ( read is paired, reversed , mate_unmapped,  second in pair);
				
				$HASH_REF->{$READ}->{'1'}->{$read_1_pos}->{'new_sam_flag'} = $new_sam_flag_read_1;
				$HASH_REF->{$READ}->{'2'}->{$read_2_pos}->{'new_sam_flag'} = $new_sam_flag_read_2;
				
				
				#fixing the ref_next #col7		
				$HASH_REF->{$READ}->{'1'}->{$read_1_pos}->{'ref_next'} = $HASH_REF->{$READ}->{'2'}->{$read_2_pos}->{'ref_name'};
				
				
				print $FH_ORPHANED get_sam_line($READ,$read_1_pos,$read_2_pos,1,$HASH_REF),"\n";
				print $FH_ORPHANED get_sam_line($READ,$read_2_pos,$read_1_pos,2,$HASH_REF),"\n";
				
				
			return 1;
	
		}
	}
		
	#read 1 is mapped and red 2 is not mapped
	elsif ( ~$read_1_sam_flag  &&  ($read_2_sam_flag  & hex(0x04) ) ) {
		
		$count_num_orphaned_pairs++;
		
		#fixing the insert size
		$HASH_REF->{$READ}->{'1'}->{$read_1_pos}->{'template_len'} = 0;
		$HASH_REF->{$READ}->{'2'}->{$read_2_pos}->{'template_len'} = 0;
		
		
		if ( $read_1_sam_flag == 0 ) {
				
				#fixing the sam flag 
				$new_sam_flag_read_1 = 1 + 8 + 64 ; # ( read is paired, mapped_in_forward, mate unmapped, first in pair);
				$new_sam_flag_read_2 = 1 + 4 + 128 ; # ( read is paired, unmapped , second in pair);
				$HASH_REF->{$READ}->{'1'}->{$read_1_pos}->{'new_sam_flag'} = $new_sam_flag_read_1;
				$HASH_REF->{$READ}->{'2'}->{$read_2_pos}->{'new_sam_flag'} = $new_sam_flag_read_2;
				
				#fixing the ref_next #col7		
				$HASH_REF->{$READ}->{'2'}->{$read_2_pos}->{'ref_next'} = $HASH_REF->{$READ}->{'1'}->{$read_1_pos}->{'ref_name'};
				
				print $FH_ORPHANED get_sam_line($READ,$read_1_pos,$read_2_pos,1,$HASH_REF),"\n";
				print $FH_ORPHANED get_sam_line($READ,$read_2_pos,$read_1_pos,2,$HASH_REF),"\n";
			
			return 1;
				
		}
		elsif ( $read_1_sam_flag &  hex(0x10) ){
			
				#fixing the sam flag 
				$new_sam_flag_read_1 = 1 + 8 + 16 + 64 ; # ( read is paired, reversed, first in pair);
				$new_sam_flag_read_2 = 1 + 4 + 32 + 128 ; # ( read is paired, unmapped, reversed , mate_mappred_in_reverse,  second in pair);
				$HASH_REF->{$READ}->{'1'}->{$read_1_pos}->{'new_sam_flag'} = $new_sam_flag_read_1;
				$HASH_REF->{$READ}->{'2'}->{$read_2_pos}->{'new_sam_flag'} = $new_sam_flag_read_2;
				
				
				#fixing the ref_next #col7		
				$HASH_REF->{$READ}->{'2'}->{$read_2_pos}->{'ref_next'} = $HASH_REF->{$READ}->{'1'}->{$read_1_pos}->{'ref_name'};
							
				
				print $FH_ORPHANED get_sam_line($READ,$read_1_pos,$read_2_pos,1,$HASH_REF),"\n";
				print $FH_ORPHANED get_sam_line($READ,$read_2_pos,$read_1_pos,2,$HASH_REF),"\n";
				
			return 1;
	
		}
	}
	
	else{
		return 0;
	}
} #END if_orphaned_pair_print_to_sam_file



sub if_mate_pair_print_to_sam_file {
	my $SUB = 'if_mate_pair_print_to_sam_file';
	
	my $READ 				= shift @_;
	my $read_1_pos			= shift @_;
	my $read_2_pos  		= shift @_;
	my $read_1_sam_flag 	= shift @_;
	my $read_2_sam_flag		= shift @_;
	my $HASH_REF 			= shift @_;
	
#	print "Read $READ \n";
	
	if ( ( $read_1_sam_flag & hex(0x10) ) &&    ($read_2_sam_flag == 0)   ) { 
#			print "Reads are facing opposite direction \n";
			#this is mate pair ( outies )
			#condition 1 for mate pairs
			# when read 1 is mapped on the -ve strand and read 2 on the positive strand
			# in this case for the pair to be outies (MP) the read 1 mapping position coordinate has to be less than that of read 2
			
			if ( $read_1_pos  < $read_2_pos) {
					
					++$count_num_mate_pairs;
					
					#fixing the insert size
					my $length_read_2 = length( $HASH_REF->{$READ}->{'2'}->{$read_2_pos}->{'sequence'} );
					my $insert_size = ($read_2_pos-$read_1_pos) + $length_read_2 + 1;
					
					if ( ($insert_size > $MAX_INNER_FRAGMENT_LENGTH ) || ($insert_size < $MIN_INNER_FRAGMENT_LENGTH)  ) {
						return 0;
					}
					$HASH_REF->{$READ}->{'1'}->{$read_1_pos}->{'template_len'} = $insert_size ;  
					$HASH_REF->{$READ}->{'2'}->{$read_2_pos}->{'template_len'} = ($insert_size * -1); ##making the insert size negative to make it work with SAM SPEC
					
					#fixing the samflag
					my $new_sam_flag_read_1 = 1 + 2 + 16 + 64 ; # ( read is paired, properly aligned, rev_comp, first in pair);
					my $new_sam_flag_read_2 = 1 + 2 + 32 + 128 ; # ( read is paired, properly aligned, mate_is_rev_comp, second in pair);
					
					$HASH_REF->{$READ}->{'1'}->{$read_1_pos}->{'new_sam_flag'} = $new_sam_flag_read_1;
					$HASH_REF->{$READ}->{'2'}->{$read_2_pos}->{'new_sam_flag'} = $new_sam_flag_read_2;
										
					#fixing the RNEXT #col7 of sam file
					$HASH_REF->{$READ}->{'1'}->{$read_1_pos}->{'ref_next'} = '=';
					$HASH_REF->{$READ}->{'2'}->{$read_2_pos}->{'ref_next'} = '=';
							
							
					
					my $direction_read_1 = $HASH_REF->{$READ}->{'1'}->{$read_1_pos}->{'direction'};
					my $direction_read_2 = $HASH_REF->{$READ}->{'2'}->{$read_2_pos}->{'direction'};
					
					my ($read_1_color,$read_2_color);
					($read_1_color,$read_2_color) = determine_read_color($direction_read_1,$direction_read_2);
					
#					print "1:$direction_read_1 \t 2:$direction_read_2 \t color \t 1:$read_1_color \t 2:$read_2_color \n";
										
					my $read_1_sam_line = get_sam_line($READ,$read_1_pos,$read_2_pos,1,$HASH_REF);
					my $read_2_sam_line = get_sam_line($READ,$read_2_pos,$read_1_pos,2,$HASH_REF);
										
					
					if ($read_1_color && $read_2_color){
						
						$count_num_pairs_dir_determined++;
						
						$read_1_sam_line .= "\t".'YC:Z:'."$read_1_color";
						$read_2_sam_line .= "\t".'YC:Z:'."$read_2_color";
						
						print $FH_MATE_PAIRS $read_1_sam_line,"\n";
						print $FH_MATE_PAIRS $read_2_sam_line,"\n";
					}
								
					delete $HASH_REF->{$READ};				
					return 1;
		     }
		     else {
		     	return 0;
		     }
	}
	elsif ( ( $read_1_sam_flag  == 0) &&  ( $read_2_sam_flag & hex(0x10) )  ) {
		
#			print "Reads are facing opposite direction \n";
		
			# the if conditions will be reverse for the above if block
			# this is mate pair ( outies )
			if ( $read_1_pos   > $read_2_pos) {
												
				++$count_num_mate_pairs;
																				
				#fixing the insert size
				my $length_read_1 = length( $HASH_REF->{$READ}->{'1'}->{$read_1_pos}->{'sequence'} );
				my $insert_size = ($read_1_pos-$read_2_pos) + $length_read_1;
				
				if ( ($insert_size > $MAX_INNER_FRAGMENT_LENGTH ) || ($insert_size < $MIN_INNER_FRAGMENT_LENGTH)  ) {
						return 0;
				}
				
				$HASH_REF->{$READ}->{'1'}->{$read_1_pos}->{'template_len'} =  ($insert_size * -1);##making the insert size negative to make it work with SAM SPEC
				$HASH_REF->{$READ}->{'2'}->{$read_2_pos}->{'template_len'} =  $insert_size ; 
						
				#fixing the samflag
				my $new_sam_flag_read_1 = 1 + 2 + 32 + 64 ; # ( read is paired, properly aligned, mate_is_rev_comp, first in pair);
				my $new_sam_flag_read_2 = 1 + 2 + 16 + 128 ; # ( read is paired, properly aligned, rev_comp, second in pair);
				
				$HASH_REF->{$READ}->{'1'}->{$read_1_pos}->{'new_sam_flag'} = $new_sam_flag_read_1;
				$HASH_REF->{$READ}->{'2'}->{$read_2_pos}->{'new_sam_flag'} = $new_sam_flag_read_2;
				
				
				#fixing the RNEXT #col7 of sam file
				$HASH_REF->{$READ}->{'1'}->{$read_1_pos}->{'ref_next'} = '=';
				$HASH_REF->{$READ}->{'2'}->{$read_2_pos}->{'ref_next'} = '=';
				
				
				my $direction_read_1 = $HASH_REF->{$READ}->{'1'}->{$read_1_pos}->{'direction'};
				my $direction_read_2 = $HASH_REF->{$READ}->{'2'}->{$read_2_pos}->{'direction'};
					
				
				my ($read_1_color,$read_2_color);
					($read_1_color,$read_2_color) = determine_read_color($direction_read_1,$direction_read_2);
					
#				print "1:$direction_read_1 \t 2:$direction_read_2 \t color \t 1:$read_1_color \t 2:$read_2_color \n";
				
									
				my $read_1_sam_line = get_sam_line($READ,$read_1_pos,$read_2_pos,1,$HASH_REF);
				my $read_2_sam_line = get_sam_line($READ,$read_2_pos,$read_1_pos,2,$HASH_REF);
									
				if ($read_1_color && $read_2_color){
					
					$count_num_pairs_dir_determined++;
					
					$read_1_sam_line .= "\t".'YC:Z:'."$read_1_color";
					$read_2_sam_line .= "\t".'YC:Z:'."$read_2_color";
					
					print $FH_MATE_PAIRS $read_1_sam_line,"\n";
					print $FH_MATE_PAIRS $read_2_sam_line,"\n";
				}
				
				return 1;
		}
		else {
			return 0;
		}
	}
}




sub if_pair_end_print_to_sam_file {
	
	my $SUB = 'if_pair_end_print_to_sam_file';
	
	my $READ 				= shift @_;
	my $read_1_pos			= shift @_;
	my $read_2_pos  		= shift @_;
	my $read_1_sam_flag 	= shift @_;
	my $read_2_sam_flag		= shift @_;
	my $HASH_REF 			= shift @_;
	
	if (  ( $read_1_sam_flag & hex(0x10) ) &&   ( $read_2_sam_flag == 0 )   ) { 
			#this is mate pair ( outies )
			#condition 1 for mate pairs
			# when read 1 is mapped on the -ve strand and read 2 on the positive strand
			# in this case for the pair to be outies (MP) the read 1 mapping position coordinate has to be less than that of read 2
			
			if ( $read_1_pos  >  $read_2_pos) {
					$count_num_paired_ends++; 
					#fixing the insert size
					my $length_read_1 = length( $HASH_REF->{$READ}->{'1'}->{$read_1_pos}->{'sequence'} );
					my $insert_size = ($read_1_pos-$read_2_pos) + $length_read_1 + 1;
					
					$HASH_REF->{$READ}->{'1'}->{$read_1_pos}->{'template_len'} = ($insert_size * -1); ; ##making the insert size negative to make it work with SAM SPEC 
					$HASH_REF->{$READ}->{'2'}->{$read_2_pos}->{'template_len'} = $insert_size;
					
					
					#fixing the samflag
					my $new_sam_flag_read_1 = 1 + 2 + 16 + 64 ; # ( read is paired, properly aligned, rev_comp, first in pair);
					my $new_sam_flag_read_2 = 1 + 2 + 32 + 128 ; # ( read is paired, properly aligned, mate_is_rev_comp, second in pair);
					
					$HASH_REF->{$READ}->{'1'}->{$read_1_pos}->{'new_sam_flag'} = $new_sam_flag_read_1;
					$HASH_REF->{$READ}->{'2'}->{$read_2_pos}->{'new_sam_flag'} = $new_sam_flag_read_2;
										
					#fixing the RNEXT #col7 of sam file
					$HASH_REF->{$READ}->{'1'}->{$read_1_pos}->{'ref_next'} = '=';
					$HASH_REF->{$READ}->{'2'}->{$read_2_pos}->{'ref_next'} = '=';
										
					print $FH_PAIR_ENDS get_sam_line($READ,$read_1_pos,$read_2_pos,1,$HASH_REF),"\n";
					print $FH_PAIR_ENDS get_sam_line($READ,$read_2_pos,$read_1_pos,2, $HASH_REF),"\n";
					
					delete $HASH_REF->{$READ};	
					return 1;
		     }
		     
		     else {
		     	return 0;
		     }
	
	}
	elsif ( ( $read_1_sam_flag == 0 ) &&   ($read_2_sam_flag & hex(0x10) ) ) {
		
		# the if conditions will be reverse for the above if block
		# this is mate pair ( outies )
		if ( $read_2_pos   > $read_1_pos) {
											
			++$count_num_paired_ends;
																			
			#fixing the insert size
			my $length_read_2 = length( $HASH_REF->{$READ}->{'2'}->{$read_2_pos}->{'sequence'} );
			my $insert_size = ($read_2_pos-$read_1_pos) + $length_read_2 + 1;
			
			$HASH_REF->{$READ}->{'1'}->{$read_1_pos}->{'template_len'} =   $insert_size;
			$HASH_REF->{$READ}->{'2'}->{$read_2_pos}->{'template_len'} =  ($insert_size * -1);  ##making the insert size negative to make it work with SAM SPEC 
					
			#fixing the samflag
			my $new_sam_flag_read_1 = 1 + 2 + 32 + 64 ; # ( read is paired, properly aligned, mate_is_rev_comp, first in pair);
			my $new_sam_flag_read_2 = 1 + 2 + 16 + 128 ; # ( read is paired, properly aligned, rev_comp, second in pair);
			
			$HASH_REF->{$READ}->{'1'}->{$read_1_pos}->{'new_sam_flag'} = $new_sam_flag_read_1;
			$HASH_REF->{$READ}->{'2'}->{$read_2_pos}->{'new_sam_flag'} = $new_sam_flag_read_2;
			
		
			#fixing the RNEXT #col7 of sam file
			$HASH_REF->{$READ}->{'1'}->{$read_1_pos}->{'ref_next'} = '=';
			$HASH_REF->{$READ}->{'2'}->{$read_2_pos}->{'ref_next'} = '=';
			
			print $FH_PAIR_ENDS get_sam_line($READ,$read_1_pos,$read_2_pos,1,$HASH_REF),"\n";
			print $FH_PAIR_ENDS get_sam_line($READ,$read_2_pos,$read_1_pos,2,$HASH_REF),"\n";
			
			delete $HASH_REF->{$READ};	
			return 1;
		}
		else {
			return 0;
		}
	}

}



sub get_sam_line {
	
	my $SUB  = 'get_sam_line';
	
	my $read_header		= shift @_;
	my $map_position 	= shift @_; 
	my $map_pos_next	= shift @_;
	my $read_type		= shift @_;
	my $HASH_REF 			= shift @_;
	
	my $read_type_2;
	
	if ($read_type == 1) { 
		$read_type_2 = 2;
	}
	elsif ($read_type == 2){
		$read_type_2 = 1;
	} 
	else {
		$logger->fatal("$SUB : Read type passed is not 1/2 .. will crash ! Sorry : \n\n");
		exit 4;
	}
	

	my $sam_flag 		= 	$HASH_REF->{$read_header}->{$read_type}->{$map_position}->{'new_sam_flag'}  || $HASH_REF->{$read_header}->{$read_type}->{$map_position}->{'orig_sam_flag'};
	
	unless( defined $sam_flag ) {
		print " No sam flag : $read_header \t $map_position \t $read_type \n";
	}
	
	my $ref_name 		= 	$HASH_REF->{$read_header}->{$read_type}->{$map_position}->{'ref_name'};
	my $map_Q	 		=	$HASH_REF->{$read_header}->{$read_type}->{$map_position}->{'map_Q'};
	my $CIGAR 	 		= 	$HASH_REF->{$read_header}->{$read_type}->{$map_position}->{'CIGAR'};
	my $ref_next		= 	$HASH_REF->{$read_header}->{$read_type}->{$map_position}->{'ref_next'};
	#map_pos_next will be here : already got from the caller routine
	my $template_len	=	$HASH_REF->{$read_header}->{$read_type}->{$map_position}->{'template_len'};
    my $sequence 		=   $HASH_REF->{$read_header}->{$read_type}->{$map_position}->{'sequence'};
    my $quality			=	$HASH_REF->{$read_header}->{$read_type}->{$map_position}->{'quality'}; 
   	my $TAGS			=	$HASH_REF->{$read_header}->{$read_type}->{$map_position}->{'TAGS'}; 
   	
   	
   	my $SAM_LINE = qq ($read_header\t$sam_flag\t$ref_name\t$map_position\t$map_Q\t$CIGAR\t$ref_next\t$map_pos_next\t$template_len\t$sequence\t$quality\t$TAGS) ;
	
	return $SAM_LINE;
	
	
}

sub read_sam_file {
	
	my $SUB  = 'read_sam_file';
	
	
	my $sam_line  = shift @_;
	my @sam_line = split('\t',${$sam_line} );
	my $user_defined_read_type = shift @_;
	my $HASH_REF = shift @_;
	
	#bare minimum sanity check
		if( scalar @sam_line < 11 ){
			print STDERR "WARN[$SUB] Sam line @sam_line doesn't contain minimum 11 fields \n @sam_line \n\n";
			next ;
		}
		
		my $read_header 	= $sam_line[0];
		#trimming the trailing /1 or /2 or anything beyond that from the read header
		$read_header =~s/\/[1|2].*$//;
		
		
		#checking if the read header has direction
		my $direction = 'none';
		if ($read_header =~/dir/){
			$read_header =~/^.+\_dir=(.+).*$/;
			$direction = $1; 
			
			##now getting rid of the direction from the read header before it is inserted into a hash
			$read_header=~s/\_dir=.*$//;
		}
		

		
		my $sam_flag 		= $sam_line[1];
		my $ref_name 		= $sam_line[2];
		my $map_position	= $sam_line[3];
		my $map_Q			= $sam_line[4];
		my $CIGAR			= $sam_line[5];
		my $ref_next		= $sam_line[6];
		my $map_pos_next	= $sam_line[7];
		my $template_len	= $sam_line[8];	
		my $sequence 		= $sam_line[9];
		my $quality			= $sam_line[10];
		my $TAGS			= join ("\t", @sam_line[11..$#sam_line] );
		
		
		##use user defined read type when you know 
		my $read_type  		=  $user_defined_read_type || get_read_type($sam_flag);
		
		
		$HASH_REF->{$read_header}->{$read_type}->{$map_position}->{'orig_sam_flag'} 		= $sam_flag;
		$HASH_REF->{$read_header}->{$read_type}->{$map_position}->{'map_Q'} 				= $map_Q;
		$HASH_REF->{$read_header}->{$read_type}->{$map_position}->{'ref_name'} 			= $ref_name;
		$HASH_REF->{$read_header}->{$read_type}->{$map_position}->{'CIGAR'} 				= $CIGAR;
		$HASH_REF->{$read_header}->{$read_type}->{$map_position}->{'ref_next'} 			= $ref_next;
		$HASH_REF->{$read_header}->{$read_type}->{$map_position}->{'map_pos_next'} 		= $map_pos_next;
		$HASH_REF->{$read_header}->{$read_type}->{$map_position}->{'template_len'} 		= $template_len;
		$HASH_REF->{$read_header}->{$read_type}->{$map_position}->{'sequence'} 			= $sequence;	
		$HASH_REF->{$read_header}->{$read_type}->{$map_position}->{'quality'} 				= $quality;
		$HASH_REF->{$read_header}->{$read_type}->{$map_position}->{'TAGS'} 			    = $TAGS;
		$HASH_REF->{$read_header}->{$read_type}->{$map_position}->{'direction'} 			= $direction;

#		
#		if ( defined $HASH_REF->{$read_header}->{$read_type} ) {
#			 $HASH_REF->{$read_header}->{$read_type}->{'count'}++;
#		}
#		else{
#			$HASH_REF->{$read_header}->{$read_type}->{'count'} = 1;
#		}
return $HASH_REF;
}



sub determine_read_color {
	my $SUB = 'determine_read_color';
	
	my $direction_read_1 	= shift @_;
	my $direction_read_2 	= shift @_;
	
	
	# 5_prime === GREEN
	# 3_prime === ORANGE

	
	
	if  ( ($direction_read_1 eq '5_prime') && ($direction_read_2 ne  '5_prime')){
#		print "Got 1:$direction_read_1 \t 2:$direction_read_2 \t Return \t 1:green \t 2:orange \n";
		return('green','orange');
	}
	elsif  ( ($direction_read_1 eq '3_prime') && ($direction_read_2 ne  '3_prime')){
#		print "Got 1:$direction_read_1 \t 2:$direction_read_2 \t Return \t 1:orange \t 2:green \n";
		return('orange','green');
	}
	elsif  ( ($direction_read_2 eq '5_prime') && ($direction_read_1 ne  '5_prime')){
#		print "Got 1:$direction_read_1 \t 2:$direction_read_2 \t Return \t 1:orange \t 2:green \n";
		return('orange','green');
	}
	elsif  ( ($direction_read_2 eq '3_prime') && ($direction_read_1 ne  '3_prime')){
#		print "Got 1:$direction_read_1 \t 2:$direction_read_2 \t Return \t 1:green \t 2:orange \n";
		return('green','orange');
	}
	elsif  ( ($direction_read_1 eq 'not_determined') && ($direction_read_2 eq  'not_determined')){
#		print "Got 1:$direction_read_1 \t 2:$direction_read_2 \t Return \t 1:0 \t 2:0 \n";
		return(0,0);
	}
	elsif ( ($direction_read_1 eq 'none') && ($direction_read_2 eq  'none')){
#		print "Got 1:$direction_read_1 \t 2:$direction_read_2 \t Return \t 1:0 \t 2:0 \n";
		#no color
		return(0,0);
	}
	elsif  ( ($direction_read_1 eq '3_prime') && ($direction_read_2 eq  '3_prime')){
#		print "Got 1:$direction_read_1 \t 2:$direction_read_2 \t Return \t 1:0 \t 2:0 \n";
		##most likely a binning problem
		return(0,0);
	}
	elsif  ( ($direction_read_1 eq '5_prime') && ($direction_read_2 eq  '5_prime')){
#		print "Got 1:$direction_read_1 \t 2:$direction_read_2 \t Return \t 1:0 \t 2:0 \n";
		##most likely a binning problem
		return(0,0);
	}
	else{
		# another problem
		print STDERR "Can't resolve direction to color 1:$direction_read_1 \t 2:$direction_read_2 \n";
	}
		
}

sub get_read_type {
	my $SUB = 'get_read_type';
	my $sam_flag = shift @_;
	#check if read 1
		if( $sam_flag & hex('0x40')) {
		return 1;
		}
	##check if read 2
		elsif( $sam_flag & hex('0x80')){
			return 2;
		}
	#error case
		else {
			return 'unmapped';
		}
}



sub if_chimera_pair_print_to_sam_file {
	
	my $SUB = 'if_chimera_pair_print_to_sam_file';
	
	my $READ 				= shift @_;
	my $read_1_pos			= shift @_;
	my $read_2_pos  		= shift @_;
	my $read_1_sam_flag 	= shift @_;
	my $read_2_sam_flag		= shift @_;
	my $ref_name_read_1		= shift @_;
	my $ref_name_read_2		= shift @_;
	my $HASH_REF			= shift @_;
	
	my $new_sam_flag_read_1;
	my $new_sam_flag_read_2;
	
	#both reads map but not on the same chromosome
	if ( $ref_name_read_1 ne $ref_name_read_2 )  {
		
		$count_num_chimeras++;
			
			#fixing the insert size
					$HASH_REF->{$READ}->{'1'}->{$read_1_pos}->{'template_len'} = 0;
					$HASH_REF->{$READ}->{'2'}->{$read_2_pos}->{'template_len'} = 0;
			
			
			
			#fixing the samflag
			if( $read_1_sam_flag & hex(0x0)  && $read_2_sam_flag & hex(0x10)) {
				$new_sam_flag_read_1 = 1 + 32 + 64 ; # ( read is paired, mate_rev_comp, maps_in_forward_dir, first in pair);
				$new_sam_flag_read_2 = 1 + 16 + 128 ; # ( read is paired, rev_comp, second in pair);
				
			}
			elsif (  $read_1_sam_flag & hex(0x10) && $read_2_sam_flag & hex(0x0) ){
				$new_sam_flag_read_1 = 1 + 16 + 64 ; # ( read is paired,   rev_comp, first in pair);
				$new_sam_flag_read_2 = 1 + 32 + 128 ; # ( read is paired, mate_rev_comp, maps_in_forward_dir,  second in pair);
							}
			elsif (  $read_1_sam_flag & hex(0x0) && $read_2_sam_flag & hex(0x0) ){
				$new_sam_flag_read_1 = 1 + 64 ; # ( read is paired, maps_in_forward_dir, first in pair);
				$new_sam_flag_read_2 = 1 + 128 ; # ( read is paired, maps_in_forward_dir, second in pair);
				
			}
			elsif (  $read_1_sam_flag & hex(0x10) && $read_2_sam_flag & hex(0x10) ){
				$new_sam_flag_read_1 = 1 + 16 +  32 + 64 ; # ( read is paired, rev_comp , mate_rev_comp,  first in pair);
				$new_sam_flag_read_2 = 1 + 16 +  32 + 128 ; # ( read is paired, rev_comp, mate_rev_comp, second in pair);
			}
		
		#fixing the sam flag	
		$HASH_REF->{$READ}->{'1'}->{$read_1_pos}->{'new_sam_flag'} = $new_sam_flag_read_1;
		$HASH_REF->{$READ}->{'2'}->{$read_2_pos}->{'new_sam_flag'} = $new_sam_flag_read_2;
		
		
		#fixing the ref_next #col7		
		$HASH_REF->{$READ}->{'1'}->{$read_1_pos}->{'ref_next'} = $HASH_REF->{$READ}->{'2'}->{$read_2_pos}->{'ref_name'};
		$HASH_REF->{$READ}->{'2'}->{$read_2_pos}->{'ref_next'} = $HASH_REF->{$READ}->{'1'}->{$read_1_pos}->{'ref_name'};
		
		
		#printing the final sam lines to file
		print $FH_CHIMERAS get_sam_line($READ,$read_1_pos,$read_2_pos,1,$HASH_REF),"\n";
		print $FH_CHIMERAS get_sam_line($READ,$read_2_pos,$read_1_pos,2,$HASH_REF),"\n";
		
		delete $HASH_REF->{$READ};
	return 1;
	}
	
	else { 
		return 0;
	}
	
}




sub if_possible_inversion_print_to_sam_file {
	
	my $SUB = 'if_possible_inversion_print_to_sam_file';
	
	my $READ 				= shift @_;
	my $read_1_pos			= shift @_;
	my $read_2_pos  		= shift @_;
	my $read_1_sam_flag 	= shift @_;
	my $read_2_sam_flag		= shift @_;
	my $ref_name_read_1		= shift @_;
	my $ref_name_read_2		= shift @_;
	my $HASH_REF			= shift @_;
	
	my $new_sam_flag_read_1;
	my $new_sam_flag_read_2;
	
	#both reads map but not on the same chromosome
	if ( ($read_1_sam_flag == $read_2_sam_flag  ) && ( $ref_name_read_1 eq $ref_name_read_2 ) ) {
		
		$count_num_possible_inversions++;
			
			#fixing the insert size
			$HASH_REF->{$READ}->{'1'}->{$read_1_pos}->{'template_len'} = 0;
			$HASH_REF->{$READ}->{'2'}->{$read_2_pos}->{'template_len'} = 0;
			
			
			
			#fixing the samflag
			if(  ( $read_1_sam_flag & hex('0x0') ) &&  ( $read_2_sam_flag & hex('0x0') ) )  {
				
				$new_sam_flag_read_1 = 1 + 64 ; # ( read is paired, mate_rev_comp, maps_in_forward_dir, first in pair);
				$new_sam_flag_read_2 = 1 + 128 ; # ( read is paired, rev_comp, second in pair);
				
			}
			elsif (  ( $read_1_sam_flag & hex('0x10') ) && ( $read_2_sam_flag & hex('0x10') )  ){
				
				$new_sam_flag_read_1 = 1 + 16 + 32 + 64 ; # ( read is paired,   rev_comp, first in pair);
				$new_sam_flag_read_2 = 1 + 16 + 32 + 128 ; # ( read is paired, mate_rev_comp, maps_in_forward_dir,  second in pair);
			}
			
		#fixing the sam flag	
		$HASH_REF->{$READ}->{'1'}->{$read_1_pos}->{'new_sam_flag'} = $new_sam_flag_read_1;
		$HASH_REF->{$READ}->{'2'}->{$read_2_pos}->{'new_sam_flag'} = $new_sam_flag_read_2;
		
		
		#fixing the ref_next #col7		
		$HASH_REF->{$READ}->{'1'}->{$read_1_pos}->{'ref_next'} = $HASH_REF->{$READ}->{'2'}->{$read_2_pos}->{'ref_name'};
		$HASH_REF->{$READ}->{'2'}->{$read_2_pos}->{'ref_next'} = $HASH_REF->{$READ}->{'1'}->{$read_1_pos}->{'ref_name'};
		
		#printing the final sam lines to file
		print $FH_POSSIBLE_INVERSIONS get_sam_line($READ,$read_1_pos,$read_2_pos,1,$HASH_REF),"\n";
		print $FH_POSSIBLE_INVERSIONS get_sam_line($READ,$read_2_pos,$read_1_pos,2,$HASH_REF),"\n";
		
	
	return 1;
	}
	
	else { 
		return 0;
	}
	
}


#sub get_direction_data_using_hash_ref {
#	my $SUB = 'get_direction_data_using_hash_ref';
#
#	my $hash_ref_file = $direction_data_file.'_hashref_storable.hash';
#	
##	if ( ! 1) {
#	if ( -e $hash_ref_file  ) {
#		print "Retrieving the hash ds from file $hash_ref_file \t";
#		$direction_data_hash = retrieve($hash_ref_file);
#		print "....Done\n";
##		print Dumper ($direction_data_hash) if $verbose;
#	}
#	else {
#			open(IN,"< $direction_data_file") || die "File $direction_data_file  can't be opened \n";
#			my $counter = 0;
#			
#			while(<IN>){
#				$counter++;
#				if ($counter % 100000  == 0){
#					print "Processed $counter reads \n";
#				}
#				my $line = $_;
#				$line =~/^@(.+)\/(\d{1}).*dir=(.+?)\s+.*$/;
#				my $read_header=$1;
#				my $read_type = $2;
#				my $direction = $3;
#				
#				
#				$direction_data_hash->{$read_header}->{$read_type} = $direction;
#		}
##		print Dumper($direction_data_hash) if $verbose;
#		store $direction_data_hash, $hash_ref_file;
#		close(IN);
#	}
#}
#
#
#sub get_direction_data_using_BerkeleyDB {
#	my $SUB = 'get_direction_data_using_BerkeleyDB';
#
#	my $berkeley_db_file = $direction_data_file.'_berkeleyDB.dbm';
#	
#	if ( -e $berkeley_db_file ) {
#		print "Tieing the hash from BerkeleyDB database \t";
#		tie my %read_direction_hash, 'BerkeleyDB::Hash', -Filename => $berkeley_db_file, -Flags => DB_RDONLY or die "Couldn't tie database: $BerkeleyDB::Error";
#		
#		$direction_data_hash = \%read_direction_hash;
#		
#		
#		print Dumper($direction_data_hash);
#	}
#	else {
#		
#		tie my %read_direction_hash, 'BerkeleyDB::Hash', -Filename => "$berkeley_db_file", -Flags => DB_CREATE or die "Couldn't tie database: $BerkeleyDB::Error";
#		
#		open(IN,"< $direction_data_file") || die "File $direction_data_file  can't be opened \n";
#		my $counter = 0;
#		while(<IN>){
#			$counter++;
#			
#			if ($counter % 100000  == 0){
#				print "Processed $counter reads \n";
#			}
#			
#			my (@split_line) = split('\t',$_);
#			my $read_header = $split_line[0];
#			my $direction = $split_line[1];
#			
#			$read_header=~/^@(.+)$/;
#			$read_header=$1;
##			my $read_type = $2;
#			$direction =~/^(dir=)(.*)$/;
#			$direction = $2;
#		
#			
#			$read_direction_hash{$read_header} = $direction;
#			
#		}
#		
#		$direction_data_hash = \%read_direction_hash;
#		untie %read_direction_hash;
#		close(IN);
#		
#	}
#		
#		
#		
#}

















###if the read header is not contained in the direction hash
#	if ( ! $direction_data_hash->{$read_header}){
#		print "WARN[$SUB] Read header $read_header is not contained in the direction_hash \n";
#	}	
#	elsif ( $direction_data_hash->{$read_header}->{$read_type} ){
#		$direction_read =  $direction_data_hash->{$read_header}->{$read_type};
#	}
#	else {
#		$direction_read = 'none';
#	}
#	
#	
##	print "$read_header \t read_type $read_type \t $direction_read  \t";
#	
#	if ( $direction_read=~/3_prime/ ){
##		print "Returning $DIR_3_PRIME_COLOR \n";
#	$count_num_reads_direction_determined++;
#		return $DIR_3_PRIME_COLOR;
#	}
#	elsif( $direction_read =~/5_prime/) {
##		print "Returning $DIR_5_PRIME_COLOR \n";
#	$count_num_reads_direction_determined++;
#		return $DIR_5_PRIME_COLOR;
#		
#	}
#	elsif ($direction_read =~/not_determined|none/ ){
#			###try the direction of other read\
#			if ( $direction_data_hash->{$read_header}->{$mate_read_type} ){
#				my $direction_other_read =  $direction_data_hash->{$read_header}->{$mate_read_type};
#				
#				##here we will return the opposite colors since the direction of the mate will be opposite to that of the read
#				if ( $direction_other_read=~/3_prime/ ){
#				#print "Returning $DIR_5_PRIME_COLOR \n";
#					$count_num_reads_direction_determined++;
#					return $DIR_5_PRIME_COLOR;
#				}
#				elsif( $direction_other_read =~/5_prime/) {
#				#print "Returning $DIR_3_PRIME_COLOR \n";
#					$count_num_reads_direction_determined++;
#					return $DIR_3_PRIME_COLOR;
#				}
#				else {
#				#print "Returning $DIRECTION_NOT_DETERMINED_COLOR \n";
#					return $DIRECTION_NOT_DETERMINED_COLOR;
#				}
#				
#				
#			}
#			else {
#				return $DIRECTION_NOT_DETERMINED_COLOR;
#			}
#			
#				
#				
#		
#	}
	

	
