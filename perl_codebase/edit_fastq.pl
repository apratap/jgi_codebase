#!/jgi/tools/bin/perl

#========================================================================================
#Disclaimer: ----- 
#This program is written by Abhishek Pratap (apratap@lbl.gov)
#Script Name : clean_fastq.pl
#======================================================================================== 


use strict;
use Data::Dumper;
use POSIX 'isatty';
use Getopt::Long;


use vars qw ($clean_fastq $split_fastq $prefix $paired $fastq_type);



main();


sub main {
	get_user_args();
    read_parse_input();
}


sub get_user_args {	
	my $SUB = 'get_user_args';
	check_input();
	my %input;
	GetOptions(\%input,
					"clean_fastq|cfq!",
					"split_fastq|sfq!",
					"paired!",
					"prefix|p:s",
					"verbose|v!",
					"fq_type:s",
					);
		
	$clean_fastq 	=	$input{'clean_fastq'};
	$split_fastq	=	$input{'split_fastq'};
	$paired 		=	$input{'paired'};
	$prefix			= 	$input{'prefix'} || 'out';
	$fastq_type     =   $input{'fq_type'} || 0;
	
	
	if ($fastq_type != "new" || $fastq_type != "old"){
		print STDERR "Please specify the fastq type (old|new) \n";
		exit 2;
	}
	
	
	if ($split_fastq && !$paired ){
		print STDERR "Splitting the input fastq file \n";
	}
	
	if ($split_fastq  && $paired  ){
		print STDERR "Splitting the input fastq file and keeping only paired reads \n";
	}
	
	if ($clean_fastq) {
		print STDERR "Cleaning the input fastq to produce Paired End Reads and remove the Single Reads into separate file \n";
	}
	
	if ( (! $split_fastq) &&  ( ! $clean_fastq ) ){
		usage();
		exit 3;
	}
}




sub check_input() {
	if ( isatty(*STDIN) ) {
		 usage();
	}
}



sub usage {
	my $SUB = 'usage';
	print qq (
	
	clean_fastq|cfq			clean fastq to paired fastq
	split_fastq|sfq			split a fastq into read 1 and 2
	paired					can be combined with split_fastq to only split paired reads
	prefix|p				prefix of the new fastq being created
	verbose|v				for more details
	
#########
#Examples
#########	

1. cat FASTQ | $0 -sfq 1 -p 'prefix'[default: 'out']
# will create 
	-prefix_read1.fastq
	-prefix_read2.fastq
	

2. cat FASTQ | $0 -sfq 1 -paired  -p 'prefix' [default: 'out']
# will create same as 1 but only paired reads will be kept
## The script will also print out stats at the end of parsing.	
	
3. cat FASTQ | $0 -cfq 1 -p 'prefix'
 	- prefix_paired_reads.fastq will have all the correct pairs
	- prefix_singletons.fastq file will have all the singletons
## The script will also print out stats at the end of parsing.
	
);

	print "\n";
	exit 2;
}

sub read_parse_input {
    my $SUB='read_parse_input';
    my $count_proper_pair = 0;
	my $reads;
	
	## SPLITTING THE FASTQ
	if ( $split_fastq ){	
		my $read_1_fastq = $prefix.'_read_1.fastq';
		my $read_2_fastq = $prefix.'_read_2.fastq';
		open(READ1, "> $read_1_fastq" ) || die "$SUB : Read 1 file $read_1_fastq can't be opened \n";
		open(READ2, "> $read_2_fastq" ) || die "$SUB : Read 2 file $read_2_fastq can't be opened \n";
	}
	
	##cleaning the fastq
	if ( $clean_fastq ) {
		my $cleaned_fastq_file = $prefix.'_paired_reads.fastq';	
		open(PAIRED_READS, "> $cleaned_fastq_file" ) || die "$SUB : File can't be opened \n";
		
		my $singeltons_fastq = $prefix.'_singletons.fastq';
		open (SINGLETONS, "> $singeltons_fastq");
		
	}
	
	my $counter =0;
	my $singleton_reads = 0;
	while(<STDIN>) {
     $counter++;
     if ($counter%1000000 == 0 ){
     	print "Processed $counter reads \n";
     }
    
	my $header= $_;
	chomp $header;
	my $seq=<STDIN>;
	chomp $seq;
	my $qual_header=<STDIN>;
	chomp $qual_header;
	my $quality = <STDIN>;
	chomp $quality;

	my $full_read =$header."\n".$seq."\n".$qual_header."\n".$quality;
		
		#determining the read type
		#	@2390:1:1101:3262:22583/1
		#   @HISEQ08:235:D142MACXX:7:1101:1445:2177 2:N:0:CGATGT
		# one regular expression to catch both the read headers
		$header =~/^(.+)[\/\s](\d).*$/;
		 
		#determining the read header
		my $header_only = $1;
		my $read1_OR_read2 =$2;	
		
		
		if ($header_only eq ''){
			#assuming the read is a single end read as the regex match was not successful 
			if($clean_fastq){
				print SINGLETONS "$full_read\n";
				$singleton_reads++;
				next;
			}
		}
		
		##just split the fastq  "AS-IS" : not checking for pairing
		if ($split_fastq && !$paired){		
			if ($read1_OR_read2 == '1') {
				print READ1 "$full_read\n";
			}
			if ($read1_OR_read2  == '2'){
				print READ2 "$full_read\n";
			}
		}
		
		##splitting the fastq and keeping only good pairs
		if ( $split_fastq && $paired ) {			
			$reads->{$header_only}->{$read1_OR_read2}=$full_read;
			
			if(  (defined $reads->{$header_only}->{'1'})  && (defined $reads->{$header_only}->{'2'}) )  {
						$count_proper_pair++;
					
						print READ1 "$reads->{$header_only}->{'1'}\n";
						print READ2 "$reads->{$header_only}->{'2'}\n";
						delete $reads->{$header_only};
			}
		}
		
		###cleaning the fastq by keeping only good pairs
		if ( $clean_fastq ) {			
			$reads->{$header_only}->{$read1_OR_read2}=$full_read;
			if(         (defined $reads->{$header_only}->{'1'})  && (defined $reads->{$header_only}->{'2'})     )  {
						$count_proper_pair++;
#						print "A pair found \n";
						print PAIRED_READS "$reads->{$header_only}->{'1'}\n";
						print PAIRED_READS "$reads->{$header_only}->{'2'}\n";
						delete $reads->{$header_only};
			}
			else{
				$singleton_reads++;
			}
		}
		
	} ## END while loop
	
	if (  ($split_fastq && $paired) || ($clean_fastq)  ) {
	###printing the summary
		my $total_reads = ($count_proper_pair*2) + $singleton_reads;
		print STDERR "#Reads \t $total_reads\n\n";
		print STDERR "#proper_pairs \t $count_proper_pair\n";
		print STDERR "#Singeltons \t $singleton_reads\n";
		
		my $fastq_cleaning_log = $prefix.'_pairing.log';
		open (OUT, "> $fastq_cleaning_log");
		print OUT "#Reads \t $total_reads\n";
		print OUT "#proper_pairs \t $count_proper_pair\n";
		print OUT "#Singeltons \t $singleton_reads\n";
		close(OUT);
	}
	
	### print out the singeltons
	if ( $clean_fastq ) {
			foreach my $read ( keys %{$reads}) {
				print SINGLETONS $reads->{$read}->{'1'},"\n" if ( defined $reads->{$read}->{'1'} ); 
				print SINGLETONS $reads->{$read}->{'2'},"\n" if ( defined $reads->{$read}->{'2'} );
			}
			close(SINGLETONS);	
		}
}