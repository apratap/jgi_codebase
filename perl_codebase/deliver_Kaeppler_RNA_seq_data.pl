#!/jgi/tools/bin/perl

use strict;
use FindBin;
use lib "$FindBin::Bin/lib";
use JGI::QAQC::RQCdbAPI;
use JGI::QAQC::Utilities;
use File::Path qw(mkpath rmtree);
use Log::Log4perl qw(:easy);
use Data::Dumper;
use Getopt::Long;
use LWP::Simple;


## global variables
our ($LOGGER, $BASEPATH, $VERBOSE, $BASE_UPLOAD_PATH, $SEQ_UNIT, $FC_ID);
our (@library_names,@seq_units_by_user, @SEQ_UNITS, $index_to_well_location_map);

main();



sub main {
	
	### Print the standard RQC header 
	get_standard_usage_header();
	
	###setup the logger
	config_logger();
	$LOGGER = get_logger();
	
	## get the user arguments
	get_user_arguments();


	## Instantiate the DB access
	my $obj = JGI::QAQC::RQCdbAPI->new();
	
		
# 	Get the final set of seq_unit_names for which data needs to be delivered
# 	RESULTS available in SEQ_UNITS array
	my $SEQ_UNITS_REF = $obj->get_final_seq_units_set(\@library_names,\@seq_units_by_user);
	
	
	##get the standard index to well map for a 96 well plate from RQC
#	$index_to_well_location_map = $obj->get_index_to_well_map();
#	      'ATCAAG' => {
#                        'index_name' => 'IJ010',
#                        'index_sequence' => 'ATCAAG',
#                        'location' => 'B2'
#                      }
#	
	
	
		foreach my $seq_unit_name ( @{$SEQ_UNITS_REF} ) {
			next if ($seq_unit_name=~/^$/);	
			#following two lines not needed the script shud support non-indexed data
			#skipping if the seq unit is not for a indexed library or UNKNOWN
			#next if ($seq_unit_name !~/[AGCT]/ );
			
			#skip the UNKOWN file
			next if ($seq_unit_name=~/UNKNOWN/ );
			deliver_data($obj,$seq_unit_name)
		}
}
	

sub deliver_data {
	my $SUB = 'deliver_data';
	
	my $obj = shift @_;
	my $SEQ_UNIT = shift @_;
	
	
	## set the library name
	$obj->seq_unit_name($SEQ_UNIT);
	## get the library detail using library name 
	$obj->get_library_info_from_seq_unit_name();	
	## get the lane level info for this seq_unit
	$obj->get_lane_level_info();
	## also get the library detail from venonat
	$obj->get_library_info_from_venonat();
	##get the seq_unit_info from ITS DW
	$obj->get_seq_unit_info_from_ITS_DW();	
	##get the info from SDM webservice
	$obj->get_seq_unit_info_SDM_webservice();
			
			
	
	
#	Get the total number of reads for this seq_unit
	my $read_count = $obj->get_total_read_count();
#	Get the libarary name for this seq_unit_name
	my $library_name = $obj->get_library_name();
#	get the source DNA number
	my $source_dna_number = $obj->get_source_dna_number();
	
	
	my $SAMPLE_NAME 			= $obj->seq_unit_info_from_ITS_DW()->{$SEQ_UNIT}->{'SAMPLE_NAME'} || 'NA';
	my $PLATE_LOCATION 			= $obj->seq_unit_info_from_ITS_DW()->{$SEQ_UNIT}->{'PLATE_LOCATION'} || 'NA';
	my $FLOWCELL_BARCODE 		= $obj->seq_unit_info_from_ITS_DW()->{$SEQ_UNIT}->{'FLOWCELL_BARCODE'} || 'NA';
	my $INDEX_NAME 				= $obj->seq_unit_info_from_ITS_DW()->{$SEQ_UNIT}->{'INDEX_NAME'} || 'NA';
	my $INDEX_SEQ 				= $obj->seq_unit_info_from_ITS_DW()->{$SEQ_UNIT}->{'INDEX_SEQUENCE'} || 'NA';
	my $NCBI_ORGANISM_NAME 		= $obj->seq_unit_info_from_ITS_DW()->{$SEQ_UNIT}->{'NCBI_ORGANISM_NAME'} || 'NA';
 	
# 	appending the FC ID to path
	my $current_upload_path = $BASE_UPLOAD_PATH .'/'.$FLOWCELL_BARCODE;


	
	#get the seq unit path from SDM web service
	# this part can be slow
	my $seq_unit_location = $obj->get_sdm_seq_unit_path();
	my $qualified_individual_seq_unit_name = $obj->get_qualified_seq_unit_name();
	my $full_path_seq_unit = $seq_unit_location.'/'.$qualified_individual_seq_unit_name;
	
##	Delivery file name
	my $delivery_file_name = "$library_name".'.'.$qualified_individual_seq_unit_name;
	
	my $status = symlink_fastq_data($full_path_seq_unit, $delivery_file_name, $current_upload_path);
	
	if ($status) { 	
		write_LIBRARIES_file($library_name,$INDEX_NAME,$source_dna_number,$NCBI_ORGANISM_NAME,$delivery_file_name,$INDEX_SEQ,$current_upload_path,$PLATE_LOCATION); 
		print "$library_name,$INDEX_NAME,$source_dna_number,$NCBI_ORGANISM_NAME,$delivery_file_name,$INDEX_SEQ,$current_upload_path,$PLATE_LOCATION ....Delivered \n "
	}
}
	
	


sub symlink_fastq_data {
		my $SUB = 'symlink_fastq_data';
		
		my $full_path_seq_unit  = shift @_;
		my $delivery_file_name  = shift @_;
		my $path_where_to_upload  = shift @_;
	

		eval {
			mkpath($path_where_to_upload);
			chmod 0777, $path_where_to_upload
		};
		if($@) {
				print "Error[$SUB] $@ \n $! \n\n";
		}
		unless ( -d $path_where_to_upload ) { 
			print "Error[$SUB] Path $path_where_to_upload doesn't exists !!! Will Skip \n";
			die "$! \n\n";
		}

		if( -e $path_where_to_upload."/".$delivery_file_name ) { 
			print "Warn[$SUB] File $delivery_file_name already symlinked in $path_where_to_upload \n";
			return 0;
		}
		else{
			my $symlink_del_status = eval { symlink("$full_path_seq_unit","$path_where_to_upload/$delivery_file_name") , 1 };			
			if($@) {
					print "Error[$SUB] $@  \n\n";
			}
			if( $symlink_del_status ) {
				return 1;
			}
		}
}


sub write_LIBRARIES_file {
	my $SUB = 'write_LIBRARIES_file';
	my $library_name = shift @_;
	my $index_name = shift @_;
	my $source_dna_number = shift @_;
	my $bioclass_name = shift @_;
	my $delivery_file_name = shift @_;
	my $index_sequence = shift @_;
	my $path_to_write_file = shift @_;
	my $well = shift @_;
	
	my $out_file = "$path_to_write_file".'/README';
	if( -e $out_file ){
		open(OUT, ">> $out_file ") || die "Error[$SUB] File $out_file can't be opened \n $! \n\n";
	}
	else {
		open(OUT, "> $out_file ") || die "Error[$SUB] File $out_file can't be opened \n $! \n\n";
		print OUT 'Library_Name '."\t".'Index_Num'."\t".' SampleID '."\t".'Bioclass Name'."\t".' Fastq_File '."\t".'Index Sequence'."\t".'Well-Location'."\n";
	}	
	print OUT "$library_name \t $index_name \t $source_dna_number \t $bioclass_name \t $delivery_file_name \t $index_sequence \t $well  \n";
close (OUT);
print "Finished creating/appending LIBRARIES file $out_file \n";
}





sub get_user_arguments {
	
	my $SUB = 'get_user_arguments';
	my %temp;
	if(scalar @ARGV ==  0 ) {
		usage();
		exit 2;
	}
	
	GetOptions(\%temp,
					"lib_name|lib:s@",
					"seq_units|su:s@",
                    "upload_path|up:s",
                    "verbose|v!",
                    "email|e:i",
              );
    
    
    if ( defined $temp{'lib_name'} ){
    	@library_names = @ {$temp{'lib_name'} };
 
    }
    
    if ( defined $temp{'seq_units'} ){
    	@seq_units_by_user = @{ $temp{'seq_units'} };
    	
    }
    
    if( defined $temp{'upload_path'} ){
    	$BASE_UPLOAD_PATH = $temp{'upload_path'};
	}
	else{
		print "Error";
		usage();
		exit 2;
	}
	
	if ( defined $temp{'verbose'}){
		$VERBOSE = $temp{'verbose'};
	}
	else {
		$VERBOSE = 0;
	}
}
	
	
sub usage {
	
	my $SUB  ='usage';
	my $script_name =`basename $0`;
	chomp $script_name;
	
print<<MSG;
USAGE:

Basic:
$script_name -lib <lib_name> -su <seq_unint_name> -up <upload_path>

Notes:
-lib can accept both singleplex or multiplex lib names
-su can accept the exact seq_unit_name (2042.7.1728.TAGCTT.fastq.srf) OR (2042.7.*) to include all the files from lane 7 from run 2042
MSG
}



	
	