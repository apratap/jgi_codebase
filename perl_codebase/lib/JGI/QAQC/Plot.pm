package JGI::QAQC::Plot;


####
#wrapper for plotting simple graphs 




use strict;
use FindBin;
use lib "$FindBin::Bin/../../../lib";
use JGI::QAQC::Utilities;
use Log::Log4perl qw(:easy);
use File::Temp qw/ tempfile tempdir/;
use File::Path qw(mkpath rmtree);
use Statistics::Descriptive::Discrete;
use Data::Dumper;
use Moose;


use Chart::Math::Axis;


use Chart::Clicker;
use Chart::Clicker::Axis;
use Chart::Clicker::Context;
use Chart::Clicker::Data::DataSet;
use Chart::Clicker::Data::Marker;
use Chart::Clicker::Data::Series;
use Geometry::Primitive::Rectangle;
use Chart::Clicker::Renderer::StackedBar;
use Graphics::Color::RGB;
use Geometry::Primitive::Circle;






########




sub plot_series_data_as_lines(){
	
	my $SUB = 'plot_series_data_as_lines';
	
	my $self 						= shift @_;
	my $data_array_of_array_ref     = shift @_;
	my $data_series_name_ref		= shift @_;
	my $chart_title					= shift @_;
	my $x_axis_label				= shift @_;
	my $y_axis_label				= shift @_;
	my $x_axis_tick_labels			= shift @_;
	my $output_file_name			= shift @_;
	my $scale_y_axis				= shift @_ || 0;
	

#	print $data_array_of_array_ref, $data_series_name_ref, $chart_title, $x_axis_label, $y_axis_label,"\n\n";
	

	my $cc = Chart::Clicker->new(width => 1500, height=> 900, format=> 'png');
	
	my @all_series;
	my @numeric_keys;
	my @all_y_axis_values;
	my $counter = 0;
	 
	foreach my $data_array_ref ( @{$data_array_of_array_ref}){
		
		my $series_name = $data_series_name_ref->[$counter];
		$counter++;
		
		my $num_var_in_array = scalar @{$data_array_ref};
		@numeric_keys = (1..$num_var_in_array);
		
		@all_y_axis_values = (@all_y_axis_values,@{$data_array_ref});
		
		my $series = Chart::Clicker::Data::Series->new(keys=>\@numeric_keys, values=>$data_array_ref,name=>$series_name);
		push(@all_series,$series);
		
	}
	
	
	
	my $ds = Chart::Clicker::Data::DataSet->new(series => \@all_series );
	
	#creating the title
	$cc->title->text($chart_title);
	$cc->title->font->size('25');
	$cc->title->padding->bottom(20);
	$cc->add_to_datasets($ds);
		
	
	my $font_x_axis = Graphics::Primitive::Font->new({size => 10,});
	my $font_y_axis = Graphics::Primitive::Font->new({size => 12,});
		
		
		
	my $defctx = $cc->get_context('default');

#	updating the y-axis
	my ($tick_values_array_ref,$tick_labels_array_ref)=$self->get_tick_values_and_labels(\@all_y_axis_values);

	$defctx->range_axis->label($y_axis_label);
	$defctx->range_axis->tick_values($tick_labels_array_ref);
	$defctx->range_axis->tick_labels($tick_labels_array_ref);
	$defctx->range_axis->tick_font($font_y_axis);
	
#	updating the x-axis
	$defctx->domain_axis->label($x_axis_label);
	$defctx->domain_axis->tick_labels($x_axis_tick_labels);
	$defctx->domain_axis->tick_values(\@numeric_keys);
	$defctx->domain_axis->tick_label_angle(4.7123);
	$defctx->domain_axis->tick_font($font_x_axis);
	


	$defctx->renderer->shape(Geometry::Primitive::Circle->new({
																	radius => 3,
																	})
							);
		
	$defctx->renderer->shape_brush(  Graphics::Primitive::Brush->new(
																	width => 1,
																	color => Graphics::Color::RGB->new(red=>1, green=> 1, blue=> 1)	
																		));
		
	$defctx->renderer->brush->width(1);
	
	$cc->write_output($output_file_name);
	
}



##not working as expected
sub plot_series_data_as_stackedBars(){
	
	my $SUB = 'plot_series_data_as_stackedBars';
	
	my $self 						= shift @_;
	my $data_array_of_array_ref     = shift @_;
	my $data_series_name_ref		= shift @_;
	my $chart_title					= shift @_;
	my $x_axis_label				= shift @_;
	my $y_axis_label				= shift @_;
	my $x_axis_tick_labels			= shift @_;
	my $output_file_name			= shift @_;
	my $scale_y_axis				= shift @_ || 0;
	

#	print $data_array_of_array_ref, $data_series_name_ref, $chart_title, $x_axis_label, $y_axis_label,"\n\n";
	

	my $cc = Chart::Clicker->new(width => 1500, height=> 900, format=> 'png');
	
	my @all_series;
	my @numeric_keys;
	my @all_y_axis_values;
	my $counter = 0;
	 
	foreach my $data_array_ref ( @{$data_array_of_array_ref}){
		
		my $series_name = $data_series_name_ref->[$counter];
		$counter++;
		
		my $num_var_in_array = scalar @{$data_array_ref};
		@numeric_keys = (1..$num_var_in_array);
		
		@all_y_axis_values = (@all_y_axis_values,@{$data_array_ref});
		
		my $series = Chart::Clicker::Data::Series->new(keys=>\@numeric_keys, values=>$data_array_ref,name=>$series_name);
		push(@all_series,$series);
		
	}
	
	
	
	my $ds = Chart::Clicker::Data::DataSet->new(series => \@all_series );
	
	#creating the title
	$cc->title->text($chart_title);
	$cc->title->font->size('20');
	$cc->title->padding->bottom(20);
	$cc->add_to_datasets($ds);
		
	
	my $font_x_axis = Graphics::Primitive::Font->new({size => 10,});
	my $font_y_axis = Graphics::Primitive::Font->new({size => 12,});
		
		
		
	my $defctx = $cc->get_context('default');

#	updating the y-axis
	my ($tick_values_array_ref,$tick_labels_array_ref)=$self->get_tick_values_and_labels(\@all_y_axis_values);


	my $area = Chart::Clicker::Renderer::StackedBar->new(opacity => .8);
	$area->brush->width(3);
	$defctx->range_axis->label($y_axis_label);
	$defctx->range_axis->tick_values($tick_labels_array_ref);
	$defctx->range_axis->tick_labels($tick_labels_array_ref);
	$defctx->range_axis->tick_font($font_y_axis);
	$defctx->range_axis->fudge_amount(.05);
	
	
#	updating the x-axis
	$defctx->domain_axis->label($x_axis_label);
	$defctx->domain_axis->tick_labels($x_axis_tick_labels);
	$defctx->domain_axis->tick_values(\@numeric_keys);
	$defctx->domain_axis->tick_label_angle(4.7123);
	$defctx->domain_axis->tick_font($font_x_axis);
	$defctx->domain_axis->fudge_amount(.01);

	
	$defctx->renderer($area);
	
	
	$cc->write_output($output_file_name);
	
}












###not giving appropraite answers
##buggy
sub get_tick_values_and_labels(){
	my $SUB = 'get_axis_layout';
	
	my $self = shift @_;
	my $data_array_ref = shift @_;
	
	my @data = @{$data_array_ref};
	
#	print "Got the following data \n\n @data \n ";
	
	my $axis_scaling = Chart::Math::Axis->new();
	$axis_scaling->set_maximum_intervals(25);
	$axis_scaling->add_data(@data);
		
		
	# Get the values for the axis
#  	print "Top of axis: "     . $axis_scaling->top           . "\n";
#  	print "Bottom of axis: "  . $axis_scaling->bottom        . "\n";
#  	print "Tick interval: "   . $axis_scaling->interval_size . "\n";
# 	print "Number of ticks: " . $axis_scaling->ticks         . "\n";
	
	my @tick_values = (1..$axis_scaling->ticks);	
	
	
	my @tick_labels;
	push(@tick_labels,$axis_scaling->bottom);
	my $interval = $axis_scaling->interval_size;
	
	my $temp_store = $axis_scaling->bottom;
	foreach (my $i=1 ; $i <= ($axis_scaling->ticks ); $i++ ){
		$temp_store += $interval;
		push(@tick_labels, $temp_store);
	}
	
#	print "Tick values \n @tick_values \n\n";
#	print "Tick labels \n @tick_labels \n\n";
	
	return (\@tick_values,\@tick_labels);
}




##########################################
####### OLD ATTEMPTS : Test code
##########################################
		
		
#		my $x_axis = Chart::Clicker::Axis->new({	
#													label_font  => Graphics::Primitive::Font->new,
#													brush => Graphics::Primitive::Brush->new,
#													orientation => 'vertical',
#													position => 'left',
#													visible => 1,
#													tick_labels => \@check,
#												});
		
		
#		 my $font = Graphics::Primitive::Font->new({
#    												family => 'Arial',
#    												size => 7,
#    												slant => 'normal'
#  												  });
		
		
		
	
#		my $cc = Chart::Clicker->new(width => 1500, height=> 900, format=> 'png');
#		my $series1 = Chart::Clicker::Data::Series->new(keys=>\@chart_keys, values=>\@total_artifacts_percentage,name=>'1.Total');
#		my $series2 = Chart::Clicker::Data::Series->new(keys=>\@chart_keys, values=>\@illumina_artifacts,name=>'2. Illumina_Artifacts');
#		my $series3 = Chart::Clicker::Data::Series->new(keys=>\@chart_keys, values=>\@jgi_contaminants,name=>'3. JGI Contaminants');
#		my $series4 = Chart::Clicker::Data::Series->new(keys=>\@chart_keys, values=>\@rRNA_contaminants,name=>'4. rRNA');
##		my $series5 = Chart::Clicker::Data::Series->new(keys=>\@chart_keys, values=>\@ecoli_contaminants,name=>'5. E.coli');
#		my $ds = Chart::Clicker::Data::DataSet->new(series => [$series1,$series2,$series3,$series4 ]);
#		$cc->title->text('Contamination Report');
#		$cc->title->font->size('25');
#		$cc->title->padding->bottom(20);
#		$cc->add_to_datasets($ds);
#		
#		
#		
##		my $x_axis = Chart::Clicker::Axis->new({	
##													label_font  => Graphics::Primitive::Font->new,
##													brush => Graphics::Primitive::Brush->new,
##													orientation => 'vertical',
##													position => 'left',
##													visible => 1,
##													tick_labels => \@check,
##												});
#		
#		
#		 my $font = Graphics::Primitive::Font->new({
#    												family => 'Arial',
#    												size => 7,
#    												slant => 'normal'
#  												  });
#		
#		
#		my $font_x_axis = Graphics::Primitive::Font->new({size => 9,});
#		my $font_y_axis = Graphics::Primitive::Font->new({size => 11,});
#		
#		
#		my $defctx = $cc->get_context('default');
#		$defctx->range_axis->label('Contamination-Percentage');
##		$defctx->range_axis->label->font->size(15);
##		$defctx->range_axis->height(150);
##		$defctx->range_axis->width(500);
#		$defctx->range_axis->tick_values(\@chart_keys);
#		$defctx->range_axis->tick_font($font_y_axis);
#		
#		
##		$defctx->domain_axis->range->min( scalar(@seq_unit_prefix) );  ##gives out a blank graph
#		$defctx->domain_axis->label('Library Names');
#		$defctx->domain_axis->tick_labels(\@seq_unit_prefix);
#		$defctx->domain_axis->tick_values(\@chart_keys);
#		$defctx->domain_axis->tick_label_angle(4.7123);
#		$defctx->domain_axis->tick_font($font_x_axis);
#		
#		$defctx->renderer->shape(Geometry::Primitive::Circle->new({
#																	radius => 3,
#																	})
#								);
#		
#		$defctx->renderer->shape_brush(
#										Graphics::Primitive::Brush->new(
#																		width => 1,
#																		color => Graphics::Color::RGB->new(red=>1, green=> 1, blue=> 1)	
#																		)	
#										);
#		
#		$defctx->renderer->brush->width(1);
#		$cc->write_output('sample_graph.png');
		
		

no Moose;
1;