package JGI::QAQC::Utilities;

use Exporter;
@ISA = qw(Exporter);
## Exporting the methods by default
@EXPORT = qw(	config_logger 
				var_check 
				file_check 
				execute_system_command 
				guess_user_email 
				send_text_email
				get_standard_usage_header
				convert_HUMAN_NCBI_geneNames_to_HUGO
				get_temp_dir
				get_cwd
				commify
				get_username
				get_timeStamp
				rev_comp_dna
			);


use Log::Log4perl qw(:easy);
use strict;
use Getopt::Long;
use File::Temp qw/ tempfile tempdir/;
use MIME::Lite;
use Moose;




sub config_logger {
	# Configuration in a string ...
  	my $conf = q(
  					log4perl.rootLogger                = INFO, Logfile, Screen
    				log4perl.appender.Logfile          = Log::Log4perl::Appender::File
    				log4perl.appender.Logfile.filename =  out.log
    				log4perl.appender.Logfile.layout   = Log::Log4perl::Layout::PatternLayout
    				log4perl.appender.Logfile.layout.ConversionPattern = [%d] %F %L %m%n
    				log4perl.appender.Screen         = Log::Log4perl::Appender::Screen
    				log4perl.appender.Screen.stderr  = 0
    				log4perl.appender.Screen.layout = Log::Log4perl::Layout::SimpleLayout
  				);

 # Logging as    [millisecs] source-filename line-number class - message newline
 # ... passed as a reference to init()
 	Log::Log4perl::init( \$conf );
}







sub execute_system_command {
	
	my $SUB='execute_system_command';

	my $logger = shift@_;
	
	my $command_to_execute = shift @_;
	my $from_subroutine = shift @_ || 'NA';
	
	unless ($command_to_execute) {
		$logger->fatal("Command to execute not passed\n");
		die "Exiting ..";
	}
	
	$command_to_execute =~s/[\r\n\t]//g;
	$logger->info("Running Command : $command_to_execute");

	my $output = `$command_to_execute`;
	
	if ( $? == -1) {
		$logger->fatal("Failed to execute to command: $! \n");
		exit 3;
	}
	elsif( $? && 127 ){
#		my $exit_value = $? >> 8;
		$logger->info("Success : $from_subroutine Exited with value $?");
	}
	else{
		my $exit_value = $? >> 8;
		$logger->info("[From $from_subroutine]: Exited with value $exit_value \n");	
	}
	
	return ($output);
}

sub var_check {
	
	my $SUB ='var_check';
	
	my $logger = shift@_;
	
	my $actual_variable_value = shift @_;
	my $literal_name_var = shift @_;
	my $from_suboroutine = shift @_;
	
	unless ($actual_variable_value) {		
		$logger->fatal("Variable $literal_name_var not set in Subroutine $from_suboroutine \n Current Value is blank \n\n Will exit !!!!");
		exit 3;
	}
}


sub file_check {
	
	my $SUB ='file_check';
	
	my $logger = shift @_;
	my $file = shift @_;
	
	
	unless (-e $file) {		
		$logger->fatal("File $file not present in the current location.. Progam Will exit !!!!\n\n");
		exit 3;
	}
}




sub guess_user_email {
	
	my $SUB = 'guess_user_email';
	my $user = `whoami`;
	chomp $user;
	my $email = $user.'@lbl.gov';
	return ($email);
	
}

sub get_username {
	
	my $SUB = 'get_username';
	my $username= `whoami`;
	chomp $username;
	return $username;
	
}


sub rev_comp_dna {
	
	my $SUB = 'rev_comp_dna';
	
	my $input_dna = shift @_ ;
	
	my $reverse_comp_dna = reverse ($input_dna);
	
	$reverse_comp_dna =~ tr /AGCTagct/TCGAtcga/;
	
	return ($reverse_comp_dna);
	
	
}

sub get_timeStamp {
	
	my $SUB = 'get_timeStamp';
	
	my @months = qw(Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec);
 	my @weekDays = qw(Sun Mon Tue Wed Thu Fri Sat Sun);
 	my ($second, $minute, $hour, $dayOfMonth, $month, $yearOffset, $dayOfWeek, $dayOfYear, $daylightSavings) = localtime();
 	my 	$year = 1900 + $yearOffset;
 	my $theTime = "$hour:$minute:$second, $weekDays[$dayOfWeek] $months[$month] $dayOfMonth, $year";
 	return $theTime; 
	
	
}


sub get_standard_usage_header {
	
my $SUB = 'standard_usage_header';

my $script_name = shift @_ || "NA";
	
system('clear');

my $USAGE_HEADER= qq(
========================================================================================
This program is written by Abhishek Pratap at QAQC Group ,DOE-Joint Genome Institute
email : apratap[at]lbl.gov
Disclaimer : 
======================================================================================== 
);

print STDERR $USAGE_HEADER,"\n";

	
}

sub commify {     
	
	my $SUB ='commify';
	
	my $input = shift @_ || 0;
	my $text = reverse $input;    
	$text =~ s/(\d\d\d)(?=\d)(?!\d*\.)/$1,/g;     
	return scalar reverse $text; 

}




sub get_temp_dir {
	
	my $SUB = 'get_temp_dir';
	my $temp_dir = tempdir(DIR=>"/scratch");
	print $temp_dir;
}




sub get_cwd {
	
	my $SUB='get_cwd';
	my $pwd =`pwd`;
	chomp $pwd;
	return $pwd	
}





sub send_text_email {
	
	my $sub = 'send_text_email';
	
	my $from_email 			= shift @_;
	my $to_email 			= shift @_;
	my $subject				= shift @_;
	my $email_text_message	= shift @_;
	
	my $cc_email = 'apratap@lbl.gov';
	
	
	 my $msg = MIME::Lite->new(
        From     => $from_email,
        To       => $to_email,
        Cc       => $cc_email,
        Subject  => $subject,
        Data	 => $email_text_message
   
    );
    $msg->send; # send via default
	
}






sub convert_HUMAN_NCBI_geneNames_to_HUGO {
	
	my $SUB = 'convert_HUMAN_NCBI_geneNames_to_HUGO';
		
	my $annotation_file_location = '/local/sequence/reference/refFalt/hg_18_genes_Aug14.refflat';
	my $GENES;
	my $count=0;
	
	my $logger = get_logger();
	
	$logger->info ("This subroutine $SUB will return a hash with NCBI gene accessions and keys and HUGO gene names as its value\n");
		
	#### Reading the annotation information
	open(IN,"< $annotation_file_location") || die "ERROR[$SUB] : File $annotation_file_location could not be opened \n $! \n\n";
	
	while (<IN>) {
		
		chop $_;
			
		## ignore comments
		if (/^#.*$/) { next; }
		
		### although the file has > 2 cols we are using first two cols
		my ($gene_name,$gene_id)=split("\t",$_);
		$GENES->{$gene_id}=$gene_name;
		$count++;	
	}
	
	$logger->info("Returned GENE hash with $count NCBI genes read from file $annotation_file_location ");
	return $GENES;
}




no Moose;
1;


















