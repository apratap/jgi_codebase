package JGI::QAQC::BWA;

use strict;
use Log::Log4perl qw(:easy);
use FindBin;
use lib "$FindBin::Bin/../../../lib";
use JGI::QAQC::Utilities;
use POSIX;



#### settting the up GRID_CONFIG environment variable before loading the Grid::Request module
BEGIN {



my $grid_config_file="$FindBin::Bin/lib/JGI/QAQC".'/grid_config.txt';
$ENV{'GRID_CONFIG'}=$grid_config_file;



}
#use Grid::Request;


### Now using moose
use Moose;



config_logger();
my $logger = get_logger(); 


### Class Variables 
has 'fastq_read1' => (
	is  	=> 'rw',
	isa 	=>  'Str',
);
has 'fastq_read2' => (
	is => 'rw',
	isa=>'Str',
);
has 'num_threads' => (
	is => 'rw',
	isa=>'Str',
);
has 'ref_dbase' => (
	is => 'rw',
	isa => 'Str',
);
has 'sai_read1' => (
	is => 'rw',
	isa => 'Str',
	
);
has 'sai_read2' => (
	is => 'rw',
	isa => 'Str',
	
);
has 'out_sam' => (
	is => 'rw',
	isa => 'Str',
);

has 'out_bam' => (
	is => 'rw',
	isa => 'Str',
);
has 'force_run' => (
	is => 'rw',
	isa => 'Str',
);
has 'grid' => (
	is => 'rw',
	isa => 'Str',
);
has 'email' => (
	is => 'rw',
	isa => 'Str',
);
has 'email_body_message' => (
	is => 'rw',
	isa => 'Str',
);
has 'combined_fastq' => (
	is => 'rw',
	isa => 'Str',
);
has 'verbose'   => (
	is =>'rw',
	isa => 'Str',
);
has 'seed_edit_distance'   => (
	is =>'rw',
	isa => 'Str',
	default => 2
);
has 'max_edit_distance'   => (
	is =>'rw',
	isa => 'Str',
	default => .03
);
has 'out_sorted_bam' => (
	is => 'rw',
	isa => 'Str',
);
has 'pwd' => (
	is => 'rw',
	isa => 'Str',
);
has 'index_bam_file' => (
	is => 'rw',
	isa => 'Str',
);
has 'bamstats_file' => (
	is => 'rw',
	isa => 'Str',
);
has 'read_count_file' => (
	is => 'rw',
	isa => 'Str',
);
has 'fai_file' => (
	is => 'rw',
	isa => 'Str',
);
has 'sorted_bam_prefix' => (
	is => 'rw',
	isa => 'Str',
);
has 'unmapped_reads_fastq_file' => (
	is => 'rw',
	isa => 'Str',
);
has 'quality_ascii_64' => (
	is => 'rw',
	isa => 'Str',
);



sub set_out_fileNames(){
	my $SUB = 'set_out_fileNames';
	my $self = shift @_;
	
	my $combined_fastq 	= $self->combined_fastq();
	my $fastq_read1		= $self->fastq_read1();
	my $fastq_read2		= $self->fastq_read2();
	my $out_sam			= $self->out_sam();
	my $ref_dbase 		= $self->ref_dbase();
	
	if ($combined_fastq){
		$fastq_read1 = $combined_fastq.'.read1.fastq';
		$fastq_read2 = $combined_fastq.'.read2.fastq';
		$self->fastq_read1($fastq_read1);
		$self->fastq_read2($fastq_read2);
	}
	
	#print "Final : read 1 : $fastq_read1, read 2: $fastq_read2\n";
	if (! $out_sam){
		$out_sam = $fastq_read1; 
		$out_sam =~s/(_read1)?.fastq.*//;
		$out_sam = $out_sam.'.sam';
		$self->out_sam($out_sam);
		$logger->info("Out sam/bam file will be named $out_sam \n");
	}
	
	

	my $sai_read1 = $fastq_read1.'.sai';
	my $sai_read2 = 0;	
	$sai_read2 = $fastq_read2.'.sai' if ($fastq_read2);
	

	my $prefix = $out_sam;
	$prefix		=~s/.sam//;
	my $out_bam_file 					= $prefix.'.bam';
	my $sorted_bam_prefix 				= $prefix.'_sorted';
	my $out_sorted_bam_file 			= $sorted_bam_prefix.'.bam';
	my $fai_file 						= $ref_dbase.'.fai';
	my $index_bam_file					= $out_sorted_bam_file.'.bai';
	my $bamstats_file 					= $out_bam_file.'.alignstats';
	my $read_count_file					= $out_bam_file.'.readCounts';
	my $unmapped_reads_fastq_file		= $prefix.'_unmapped_reads.fastq';
	
	$self->out_bam($out_bam_file);
	$self->out_sorted_bam($out_sorted_bam_file);
	$self->index_bam_file($index_bam_file);
	$self->bamstats_file($bamstats_file);
	$self->read_count_file($read_count_file);
	$self->fai_file($fai_file);
	$self->sai_read1($sai_read1);
	$self->sai_read2($sai_read2);
	$self->sorted_bam_prefix($sorted_bam_prefix);
	$self->unmapped_reads_fastq_file($unmapped_reads_fastq_file);
	
	$logger->info("[$SUB]: Created output file names \n $sai_read1 \n $sai_read2 \n $out_bam_file \n $out_sorted_bam_file \n $index_bam_file \n $bamstats_file \n $read_count_file \n $fai_file \n");
}




sub split_combined_fastq_gz {
	my $SUB = 'split_combined_fastq_gz';
	my $self = shift @_;
	$logger->info("SUB[$SUB] Splitting Combined FASTQ into read 1/2\n") if $self->verbose();
	my $combined_fastq = $self->combined_fastq() || die "Error[$SUB] combined_fastq name not set \n $! \n";
	my $cat_method;
	
	if( -e $combined_fastq ) {
		#make the read1/read2 fastq file names
		my $prefix =$combined_fastq;
		if ( $prefix =~/^.*\.gz$/) {
			$prefix    =~s/\.fastq\.gz//;
			$cat_method = 'zcat';
		}
		if ( $prefix =~ /^.*\.fastq/ ) {
			$prefix    =~s/\.fastq//;
			$cat_method = 'cat';
		}
		if ( $prefix =~ /^.*\.fq/ ) {
			$prefix    =~s/\.fq//;
			$cat_method = 'cat';
		}
		
		my $fastq_read_1 = $self->fastq_read1();
		my $fastq_read_2 = $self->fastq_read2();
		
		
		##checking if the files are already present unless it is a force_run 		
		if(      (   (-e $fastq_read_1) && (-e $fastq_read_2)   )   &&  ( ! $self->force_run() )   ) {
			$logger->info("[$SUB] : Expected output $fastq_read_1 and $fastq_read_2 already exists \n ");
		}
		
		### if index_bam_file exists will assume mapping done successfully
		elsif( (-e $self->index_bam_file() )  && ( ! $self->force_run() )    ){
			$logger->info("[$SUB]: Index bam file already present ..assuming mapping done..Use force flag if force_run \n ");
		}
		else {
			my $command_read1 = "$cat_method $combined_fastq | grep -E \'^@.*\/1$\' -A3 | sed \'\/^--\$\/d\' > $fastq_read_1";
			my $command_read2 =  "$cat_method $combined_fastq | grep -E \'^@\.*\/2$\' -A3 | sed \'\/^--\$\/d\' > $fastq_read_2";
			execute_system_command($logger,$command_read1, $SUB);
			execute_system_command($logger,$command_read2, $SUB);
		}
	}
	else {
		$logger->fatal("[$SUB]: File $combined_fastq not present \n");
		exit 2;
	}
}


sub aln {
	my $SUB = 'aln';
	my $self = shift @_ || die "ERROR[$SUB] Method not called with a object";
	my $fastq_read1 	= $self->fastq_read1();
	my $fastq_read2 	= $self->fastq_read2();
	my $ref_dbase 		= $self->ref_dbase();
	my $num_threads 	= $self->num_threads();
	my $force_run		= $self->force_run();
	my $grid			= $self->grid();
	my $seed_edit_distance 	= $self->seed_edit_distance(); 
	my $max_edit_distance 	= $self->max_edit_distance(); 
	
	
	my $quality_64;
	if ($self->quality_ascii_64() ){
		$quality_64 = '-I'
	}
	
	
	my $index_bam_file = $self->index_bam_file();
	if (  (-e $index_bam_file )    && ( ! $self->force_run() ) ) {
		$logger->info("[$SUB]: Index bam file already present ..assuming alignment was done earlier ..use force flag to force run\n");
		return 1;
	}
	
	
	
	file_check($logger,$ref_dbase);
	
	### if the input is single read
	if ( ( $fastq_read1 )  && (! $fastq_read2 ) ){
		file_check($logger,$fastq_read1);
		my $sai_read1 = $self->sai_read1();
		if (! -e $sai_read1 || $force_run ) {
			if ($grid ) {
				run_BWA_on_grid_threadedQ($num_threads,$ref_dbase,$fastq_read1,$sai_read1);
			}
			else{
			   	my $command = qq ( bwa aln $quality_64 -k $seed_edit_distance -n $max_edit_distance -t $num_threads $ref_dbase $fastq_read1 > $sai_read1 );
				$logger->info("Running Command $command");
				execute_system_command($logger,$command, $SUB);
			}
		}
		else{
			$logger->info(".....Expected output of bwa aln -> $sai_read1 is present");
		}
	}
	if ( $fastq_read1 && $fastq_read2  ){
		file_check($logger,$fastq_read1);
		file_check($logger,$fastq_read2);
		
		my $sai_read1 = $self->sai_read1();
		my $sai_read2 = $self->sai_read2();
		
		if (  (! -e $sai_read1  || ! -e $sai_read2 ) || $force_run ) {
			##run it on grid if specified by the user else locally
			if ($grid ) {
				$logger->info("Running BWA on Grid\n");
				run_BWA_on_grid_threadedQ($num_threads,$ref_dbase,$fastq_read1,$sai_read1,$fastq_read2,$sai_read2);
			}
			else{
			   	my $command_read1 = qq ( bwa aln $quality_64 -k $seed_edit_distance -n $max_edit_distance  -t $num_threads $ref_dbase $fastq_read1 > $sai_read1 );
			   	my $command_read2 = qq ( bwa aln $quality_64 -k $seed_edit_distance -n $max_edit_distance  -t $num_threads $ref_dbase $fastq_read2 > $sai_read2 );
				$logger->info("Running locally");
				execute_system_command($logger,$command_read1,$SUB);
				execute_system_command($logger,$command_read2, $SUB);
				}
		}
		else{
			$logger->info( ".....Expected output of this bwa aln on PE data -> ($sai_read1 and $sai_read2) is present");
		}
	}
}

sub samse {
	
   	my $SUB = 'samse';
   	my $self = shift @_ || die "Method $SUB was not called with class object \n";
	  	
	my $fastq_read1 	= $self->fastq_read1();
	my $ref_dbase 		= $self->ref_dbase();
	my $sai_read1		= $self->sai_read1();
	my $out_sam			= $self->out_sam();
	my $force_run		= $self->force_run();
	
	
	my $index_bam_file = $self->index_bam_file();
	if (  (-e $index_bam_file )    && ( ! $self->force_run() ) ) {
		$logger->info("[$SUB]: Index bam file already present ..assuming alignment was done earlier ..use force flag to force run\n");
		return 1;
	}
	
	
	
	my $command = qq(bwa samse $ref_dbase $sai_read1 $fastq_read1 -f $out_sam);
	if ( ! -e $out_sam || $force_run ) {
		execute_system_command($logger,$command, $SUB);
	}
	else {
		$logger->info( ".....Expected output of command samse -> ($out_sam) already present..Will skip running the command");
	}

}

sub sampe {
   	my $SUB = 'sampe';
   	my $self = shift @_ || die "Method $SUB was not called with class object \n";
	my $fastq_read1 	= $self->fastq_read1();
	my $fastq_read2 	= $self->fastq_read2();
	my $ref_dbase 		= $self->ref_dbase();
	my $sai_read1		= $self->sai_read1();
	my $sai_read2		= $self->sai_read2();
	my $force_run		= $self->force_run();
	my $out_sam			= $self->out_sam();
	
	my $index_bam_file = $self->index_bam_file();
	if (  (-e $index_bam_file )    && ( ! $self->force_run() ) ) {
		$logger->info("[$SUB]: Index bam file already present ..assuming alignment was done earlier ..use force flag to force run\n");
		return 1;
	}
	

	my $command = qq(bwa sampe $ref_dbase $sai_read1 $sai_read2 $fastq_read1 $fastq_read2 > $out_sam);
	
	if ( ! -e $out_sam || $force_run ) {
		execute_system_command($logger,$command,$SUB);
	}
	else {
		$logger->info(".....Expected output of command  sampe ($out_sam) already present..Will skip running the command");
	}
}

sub check_if_reference_is_indexed {
	
	my $SUB = 'check_if_reference_is_indexed';
	
	my $self = shift @_ || die "ERROR[$SUB] Class object not passed \n";
	my $ref_dbase = shift @_ || die "ERROR[$SUB]";
	
	$logger->info("SUB[$SUB] Checking if the reference is indexed 1/2\n") if $self->verbose();
	$logger->info("Value of ref_dbase $ref_dbase\n") if $self->verbose();
	
	chomp $ref_dbase;
	
	
	## These are the files we expect to be present for the ref fasta in order for BWA to use the reference and align FASTQ
	my @needed_file_extensions = ('amb','ann','bwt','pac','rbwt','rpac','rsa','sa', 'fai');
	
	my $ref_dbase_path = `dirname $ref_dbase`;
	my $ref_dbase_file = `basename $ref_dbase`;
	
	chomp $ref_dbase_file;
	chomp $ref_dbase_path;
	
	##checking of the 
	foreach my $file_ext ( @needed_file_extensions ) {
		
		my $file_name = $ref_dbase_file.'.'.$file_ext;
		my $full_path = $ref_dbase_path.'/'.$file_name;
		
		unless ( -e $full_path ) {
			$logger->fatal("\n BWA index file $full_path for this reference $ref_dbase_file not found \n Please index the dbase using bwa with appropriate algo based on the size of the dbase");
			exit 2;
		}
		else {
			
			#$logger->info("Checking for file : $full_path .. Success");
		}
		
	}
	
	
	
}

sub sam_to_bam {
	
	my $SUB = 'sam_to_bam';
	
	my $self = shift @_ || die "ERROR[$SUB] : method not called from a class object";
	
	my $sam_file	= $self->out_sam();
	my $ref_dbase 	= $self->ref_dbase();
	my $force_run	= $self->force_run();
	my $fai_file 			= $self->fai_file();
	my $out_bam_file 		= $self->out_bam();
	my $sorted_bam 			= $self->out_sorted_bam();
	my $sorted_bam_prefix 	= $self->sorted_bam_prefix();
	my $bamstats_file 		= $self->bamstats_file();
	my $index_bam_file		= $self->index_bam_file();
	
	
	my $pwd =`pwd`;
	chomp $pwd;
	$self->pwd($pwd);
	
	
	if (  (-e $index_bam_file )    && ( ! $self->force_run() ) ) {
		$logger->info("[$SUB]: Index bam file already present ..assuming alignment was done earlier ..use force flag to force run\n");
		return ($pwd,$sorted_bam);
	}
	
	unless ( -e $fai_file ) { 
		$logger->fatal("ERROR[$SUB] FAI file $fai_file not present as expected !!!! Will Exit");
		die;
	}
	
	my $command1 = "samtools view -bS -t $fai_file $sam_file -o $out_bam_file";
	my $command2 = "samtools sort $out_bam_file $sorted_bam_prefix";
	my $command3 = "samtools index $sorted_bam";
	my $command4 = "samtools flagstat $sorted_bam | tee $bamstats_file";
	
	
	### convert sam to bam
	if ( ! -e $out_bam_file || $force_run ) {execute_system_command($logger,$command1, $SUB); }
	else { $logger->info(".....Expected output $out_bam_file already present ..will not run the command"); }
	
	### sort the bam
	if  ( ! -e $sorted_bam || $force_run ) {execute_system_command($logger,$command2, $SUB); }
	else { $logger->info(".....Expected output $sorted_bam already present ..will not run the command"); }
	
	### index the bam
	if ( ! -e $index_bam_file || $force_run ) {execute_system_command($logger,$command3, $SUB); }
	else { $logger->info(".....Expected output $index_bam_file already present ..will not run the command"); }
	
	## reporting the basic alignment stats
	if ( ! -e $bamstats_file  || $force_run ) { 
		my $output= execute_system_command($logger,$command4, $SUB);
		print "Output is $output\n\n";
		my $message = $self->email_body_message();
		$message 	= $message."\n$output";
		$self->email_body_message($message);
	}
	else { $logger->info(".....Expected output $bamstats_file already present ..will not run the command"); }
	
	return ($pwd,$sorted_bam);
}  ### END SUB sam_to_bam



sub cleanUP(){
	my $SUB = 'cleanUP';
	my $self = shift @_ || die "ERROR[$SUB] : method not called from a class object";

	my $combined_fastq  = $self->combined_fastq() || 0;
	my $fastq_read1 	= $self->fastq_read1();
	my $fastq_read2 	= $self->fastq_read2() || 0 ;
	my $sai_read1		= $self->sai_read1();
	my $sai_read2		= $self->sai_read2();
	my $out_sam			= $self->out_sam();
	my $out_bam 		= $self->out_bam();
	
	unlink($fastq_read1) if $combined_fastq;
	unlink($fastq_read2) if $combined_fastq;
	unlink($sai_read1);
	unlink($sai_read2);
	unlink($out_sam);
	unlink($out_bam);
		
	$logger->info("[$SUB]: Cleaned out \n $fastq_read1 \n $fastq_read2 \n $sai_read1 \n $sai_read2 \n $out_sam \n $out_bam \n\n\n ");
}




sub count_reads_per_reference(){
	my $SUB = 'count_reads_per_reference';
	my $self = shift @_;
	my $out_sorted_bam = $self->out_sorted_bam();
	my $read_count_file    = $self->read_count_file();
	
	if (  ( ! -e $read_count_file) || ( $self->force_run() )  ){	
		my $command = "samtools view $out_sorted_bam | cut -f3 | uniq -c | awk '{print \$2\"\\t\"\$1}' > $read_count_file";
		execute_system_command($logger,$command,$SUB); 
		
		#read the file to sum the total reads
		open(IN,"< $read_count_file") || die "File $read_count_file can't be opened \n";
		my $total_reads = 0;
		my $hash_ref;
		
		while(<IN>){
			chomp $_;
			my ($reference,$read_count_per_reference) = split("\t",$_);
#			print " $reference \t $read_count_per_reference\n";
			$total_reads += $read_count_per_reference;
			$hash_ref->{$reference} = $read_count_per_reference;
		}
		close(IN);
		
		#open the file and put #reads as well as percetange reads
		open(OUT, "> $read_count_file");
		foreach my $reference ( keys %{$hash_ref}){
			print OUT ($reference,"\t",$hash_ref->{$reference},"\t", sprintf('%2.4f', ($hash_ref->{$reference}/$total_reads)*100 ) ,"\n");
		}
		close(OUT);		
	}
	else {
		$logger->info("[$SUB]: Expected output $read_count_file already present ..use force_run is needed");
	}
}




sub create_unmapped_reads_fastq_from_bam(){
	my $SUB = 'create_unmapped_reads_fastq_from_bam';
	my $self = shift @_;
	my $out_sorted_bam = $self->out_sorted_bam();
	my $unmapped_reads_fastq_file = $self->unmapped_reads_fastq_file();
	
	if (  ( ! -e $unmapped_reads_fastq_file) || ( $self->force_run() )  ){	
		my $command = "samtools view -f 4 $out_sorted_bam | /house/homedirs/a/apratap/bin/perl_scripts/edit_sam.pl -fq 1 > $unmapped_reads_fastq_file ";
		execute_system_command($logger,$command,$SUB); 
	}
	else {
		$logger->info("[$SUB]: Expected output $unmapped_reads_fastq_file already present ..use force_run is needed");
	}
}


#sub run_BWA_on_grid_threadedQ {
#
#	my $SUB = 'run_BWA_on_grid_threadedQ';
#	
#	### Setting the GRID_CONFIG param to read the config file
#	
#	
#	my $num_threads			= shift @_;
#	my $reference			= shift @_;
#	my $fastq_read1			= shift @_ ;
#	my $sai_read1			= shift @_;
#	my $fastq_read2			= shift @_;
#	my $sai_read2			= shift @_;	 
#	my $error_file		   	= shift @_ || 'error.log';
#
## Create a request object. The "project" is required.
#	my $request = Grid::Request->new ( project => 'nonrnd.p' );
#
############################################
##### running the alignment for Read 1
##############################################
#
#	if ($fastq_read1) {
#	
#		# Set up the executable and job attributes.
#		$request->set_command('/jgi/tools/bin/bwa');
#		$request->set_output("$sai_read1");
#		$request->set_error("$error_file");
#	
## passing the SGE level instructions to qsub
#		$request->pass_through("-v PATH -cwd -pe pe_slots $num_threads -l long.c,ram.c=45G");
#
## Add the command line parameters for the executable.
#		$request->add_param("aln");
#		$request->add_param("-t");
#		$request->add_param("$num_threads");
#		$request->add_param("$reference");
#		$request->add_param("$fastq_read1");
#	}
#	else {
#		$logger->fatal("ERROR[$SUB]: Fastq file for read 1 not passed Will exit");
#		exit 2;
#	}
#	
#	
#
############################################
##### running the alignment for Read 2
##############################################
#
#	if ( $fastq_read2) {
#		
#	# if two reads are present then creating a new command to process them asynchronously 
#
#		$request->new_command();
#		$request->set_command('/jgi/tools/bin/bwa');
#		$request->set_output("$sai_read2");
#		$request->set_error("$error_file");
#	
## passing the SGE level instructions to qsub
#		$request->pass_through("-v PATH -cwd -pe pe_slots $num_threads -l long.c,ram.c=45G");
#
## Add the command line parameters for the executable.
#		$request->add_param("aln");
#		$request->add_param("-t");
#		$request->add_param("$num_threads");
#		$request->add_param("$reference");
#		$request->add_param("$fastq_read2");
#	}
#	
#	else {
#		$logger->info("Read 2 fastq file was not given!! Assuming single read dataset ... will run BWA now");
#	}
#		
## Submit the job.
#	my @ids = $request->submit();
#	$logger->info("Submitted jobs to SGE with job ids     @ids");
#	
## Wait for the job to complete.
#	$request->wait_for_request();
#
## Job is now completed. Check the results.
#	my $state = $request->get_state();
#	#	my $job_time = $request->end_time();
#
#	$logger->info("Job finished with state: $state");
#
#} ### END 




sub create_igv_cov_track {	
	my $SUB = 'create_igv_cov_track';
	my $self = shift @_ || die "ERROR[$SUB] Class object not passed \n";
}


1;
no Moose;

__PACKAGE__->meta->make_immutable;
