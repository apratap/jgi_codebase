package JGI::QAQC::RQCdbAPI;


use strict;
use FindBin;
use lib "$FindBin::Bin/../../../lib";
use JGI::QAQC::Utilities;
use Log::Log4perl qw(:easy);
use DBI;
use Getopt::Long;
use File::Temp qw/ tempfile tempdir/;
use MIME::Lite;
use Statistics::Descriptive::Discrete;
use Data::Dumper;
use REST::Client;
use JSON;
use Moose;





### Class Variables 

has 'verbose' => (
	is  	=> 'rw',
	isa 	=>  'Str',
);

has 'library_name' => (
	is  	=> 'rw',
	isa 	=>  'Str',
);


has 'run_id' => (
	is  	=> 'rw',
	isa 	=>  'Str',
);


has 'seq_unit_name' => (
	is  	=> 'rw',
	isa 	=>  'Str',
);

has 'database_handle' => (
	is	=> 'ro',
	required =>1,
	default => sub { connectDB(); }
);


has 'ITS_DW_handle' => (
	is	=> 'ro',
	required =>1,
	default => sub { get_ITS_DW_handle(); }
);



has 'full_lane_results' => (
	is =>'rw',
);


has 'library_results' => (
	is =>'rw',
);

has 'library_results_venonat' => (
	is => 'rw',
);


has 'seq_unit_info_from_SDM_webservice' =>(
	is => 'rw',
);

has 'seq_unit_info_from_ITS_DW' =>(
	is => 'rw',
);


has 'seq_units_per_run' => (
	is =>'rw',
);


has 'seq_units_per_library' => (
	is =>'rw',
);


has 'logger' => (
	is => 'ro',
	default => sub { config_logger(); get_logger(); }
);

has 'verbose' => (
	is => 'rw',	
);

### auto class initializer
sub BUILD {
	my $SUB = ' BUILD';
	my $PWD='pwd';
	my $self = shift @_;
}




=pod

=head2 Subroutine : connectDB
Used to connect to RQC DB. Auto used by  the RQCdbAPI when class RQCdbAPI is setup
=cut


sub connectDB {
	my $self = shift ;
	my $database='rqc';
	#my $host='redwood.jgi-psf.org';
	my $host='sequoia.jgi-psf.org';
	### DBI Connection settings
	my $dsn ="DBI:mysql:$database:$host";
	my $user_name='rqc_ro';
	my $password='rqcro';
	my %conn_attrs=(RaiseError=>1,PrintError=>1,AutoCommit=>0);
	
	##Connect to the database 
	my $dbh = DBI->connect_cached($dsn,$user_name,$password,\%conn_attrs) 
				or
			  die " Could not connect to $database".DBI->errstr();
	print STDERR "RQC Database Connected\n"; 
return $dbh;
}



sub get_ITS_DW_handle() {
   my $DB               = "dwprd1";
   my $NA_shared        = "dw_user";
   my $PW_shared        = "dw_user";

   $ENV{ORACLE_HOME}    = "/jgi/tools/oracle_client/DEFAULT";
   $ENV{TNS_ADMIN}      = "/jgi/tools/oracle_client/DEFAULT/network/admin";
   $ENV{TWO_TASK}       = $DB;


   my $ITS_DW_handle = "";
   unless( $ITS_DW_handle = DBI->connect("dbi:Oracle:$DB","$NA_shared","$PW_shared" ) ) {
  		 die "cannot establish ITS Data Warehouse connection to $DB\n";
        }
   else{
   		print "Connected to ITS Data Warehouse \n";
   }
return $ITS_DW_handle;
}


sub close_dbase_connection {
	my $SUB = 'close_dbase_connection';
	my $self = shift;
	$self->database_handle->disconnect();
}


=pod

=head2 Subroutine : get_lane_level_info

To run the lane level query on RQC database for "seq_unit_name"

=item1	
	set the seq_unit_name
	$obj->seq_unit_name($seq_unit_name);

=item2	
	run the query to get all lane level info,"\n"
    $obj->get_lane_level_info();
    
return a big hash with stats_name = KEY and stats_value = value

stats_name 								stats_value
'read base count text 2', 				'1583.7.1455.s0.05.r2.fastq.base.stats'
'read qhist text', 						'1583.7.1455.s0.05.fastq.qhist'
'read 20mer uniqueness plot', 			'1583.7.1455.merSampler.m20.e25000.png'
'read bwa aligned duplicate percent', 	'19.4'
'read length 1', 						'151'
'read GC std', 							'10.87'

Return : $result hash reference
Also sets class attribute full_lane_results = $result

=cut

sub get_lane_level_info {
	my $SUB = 'get_lane_level_info';
	
	my $self = shift @_ ;
	my $seq_unit_name  = $self->seq_unit_name() || die "Error[$SUB] no value for seq_unit_name \n";
	my $verbose		= $self->verbose();
	my $result;
	my $query = qq (
					select stats.stats_name, stats.stats_value from rqc_analysis_stats stats,
					m2m_analysis_seq_unit m2m
					where 
					m2m.analysis_id = stats.analysis_id
					and
					m2m.seq_unit_name = "$seq_unit_name"
					);
	
	my $sth = $self->database_handle->prepare($query);
	eval {
		$sth->execute();
	};
	if ($@) {
		warn $@;
#		$sth->RaiseError();
	}
	## if all good fetch the data
	else {
		### ERROR CHECK
		if ( $sth->rows() == '0' ) {
				$self->logger()->info("ERROR[$SUB] Seq unit $self->seq_unit_name fetched 0 rows\n");
				$self->logger()->info($query);
			next;
		}
		while ( my $value = $sth->fetchrow_hashref() ) {
				unless ( defined $result->{$$value{'stats_name'}} ) {
					$result->{$$value{'stats_name'}}= $$value{'stats_value'};
				}
				else {
					$self->logger()->warn("[$SUB] Double value found in data") if $verbose;
					$self->logger()->warn("Value 1 : $$value{'stats_name'} \t ", $$value{'stats_value'}) if $verbose;
					$self->logger()->warn("Value 2 : $$value{'stats_name'} \t ",$result->{$$value{'stats_name'}}) if $verbose ;
				}
		}
		$self->full_lane_results($result);
	#   return a hash ref with keys=stats_name and value=stats_value from the DB query
		return $result;
	}
}





sub get_seq_units_name_from_run_id {
	my $SUB = 'get_seq_units_name_from_run_id';
	my $self = shift @_ ;
	my $run_id  = $self->run_id() || die "Error[$SUB] no value for seq_unit_name \n";
	my $result;
	my $query = qq (       
							select distinct seq.seq_unit_name, seq.run_id, seq.run_section,seq.rqc_status_id 
							from seq_units seq, 
									(
										select  max(external_data_change_time) as time, 
										seq_unit_name, run_section, seq_unit_id
        								from seq_units 
        									where run_id="$run_id" and seq_unit_name LIKE  "$run_id%" group by run_section 
        							) max_run_time
    						where
    						seq.external_data_change_time = max_run_time.time
    						and 
                            seq.seq_unit_name = max_run_time.seq_unit_name
                            and 
                            seq.run_section = max_run_time.run_section
                            and seq.seq_unit_id = max_run_time.seq_unit_id
    				);
	my $seq_units_per_run;
	eval {
		$seq_units_per_run = $self->database_handle->selectall_hashref($query,'run_section');
	};
	if ($@) {
		warn $@;	
		$self->logger()->info(" Following query were run : $query");
	}
	if ( keys %{$seq_units_per_run} == 0) {
		
			$self->logger()->fatal("ERROR[$SUB] No rows fetched for run id $run_id \n ");
			$self->logger()->info("Please check if run has been processed by RQC\n ");
			$self->logger()->info(" Following query were run : $query");
			die "\n";
	}
#		Storing the returned hash as a class attribute		
		$self->seq_units_per_run($seq_units_per_run);
		return $seq_units_per_run;
}

#   Expects to return a following hash structure 
##	$VAR1 = {
#          '1633.1.1511.srf' => {
#                                 'run_section' => '1',
#                                 'seq_unit_name' => '1633.1.1511.srf',
#                                 'run_id' => '1633'
#                               },
#          '1633.6.1508.srf' => {
#                                 'run_section' => '6',
#                                 'seq_unit_name' => '1633.6.1508.srf',
#                                 'run_id' => '1633'
#                               },
#          '1633.8.1510.srf' => {
#                                 'run_section' => '8',
#                                 'seq_unit_name' => '1633.8.1510.srf',
#                                 'run_id' => '1633'
#                               },


sub get_seq_units_name_from_user_entered_seq_unit_pefix {
	my $SUB = 'get_seq_units_name_from_user_entered_seq_unit_pefix';
	my $self = shift @_ ;
	my $seq_unit_pattern = shift @_ || $self->logger->fatal('No seq unit prefix passed \n');
	
	my @seq_units_names_based_prefix;
	my $sth;
	my @rqc_status_id_per_seq_unit;
	my $query = qq (       
						select seq.seq_unit_name, seq.rqc_status_id
						from 
						seq_units seq 
						where seq.seq_unit_name LIKE "$seq_unit_pattern"
    				);
	eval {
		$sth   = $self->database_handle->prepare($query);
		$sth->execute();
	};
	
	if ($@) {
		warn $@;	
		$self->logger()->info(" Following query were run : $query");
	}
		while ( my $seq_unit =  $sth->fetchrow_hashref() ) {
			my $seq_unit_name = $seq_unit->{'seq_unit_name'};
			my $rqc_status	  = $seq_unit->{'rqc_status_id'};
			
			push(@seq_units_names_based_prefix, $seq_unit_name);
			push(@rqc_status_id_per_seq_unit, $rqc_status);
		}
	return (\@seq_units_names_based_prefix );
}



sub get_seq_units_name_from_library_name {
	my $SUB = 'get_seq_units_name_from_library_name';
	my $self = shift @_ ;
	my $library_name  = $self->library_name() || die "Error[$SUB] no value for Library Name \n";
	my $result;
	my $query = qq (       
							select seq.seq_unit_name, seq.is_multiplex , seq.run_date , seq.rqc_status_id   
							from 
							library_info lib  , seq_units seq 
							where lib.library_name = "$library_name"
							and
							seq.rqc_library_id = lib.library_id		
							order by seq.run_date ASC		
    				);
	
	my $seq_units_per_library;
	my $sth;
	eval {
		$sth   = $self->database_handle->prepare($query);
		$sth->execute();
	};

	if ($@) {
		warn $@;	
		$self->logger()->info(" Following query were run : $query");
	}
	
	if ( $sth->rows() == 0) {
			$self->logger()->fatal("ERROR[$SUB] No rows fetched for run id $library_name \n ");
			$self->logger()->info("Please check if Library $library_name has been processed by RQC\n ");
			$self->logger()->info(" Following query were run : $query");
			return (0,0);
	}
	
	
#	 Reading each sql returned row one by one to preserve the order of run_date by ASC
# 	 Will then return two array reference
#	1. Will have seq_unit_name organized by run_date
# 	2. rqc_status_id : will have the corresponding rqc_status_id
# 	Not returning a hash since the order of seq_unit_name is essential to mantain

	my @seq_units_names;
	my @rqc_status_id_per_seq_unit;
	while ( my $seq_unit =  $sth->fetchrow_hashref() ) {
			
			my $seq_unit_name = $seq_unit->{'seq_unit_name'};
			my $rqc_status	  = $seq_unit->{'rqc_status_id'};
			
			push(@seq_units_names, $seq_unit_name);
			push(@rqc_status_id_per_seq_unit, $rqc_status);
			
		}
	
	
	return (\@seq_units_names, \@rqc_status_id_per_seq_unit);

#  DB query is expected to return a following hash structure
# which will be parsed and put into two arrays ( seq_unit_names and rqc_status_id ) and returned
# seq_unit_name   ; multiplex_info ; run_date ; rqc_status_id
#'219.8.217.srf', '0', '2009-09-18 00:00:00', '4'
#'211.3.550.srf', '0', '2009-09-18 00:00:00', '4'
#'211.3.551.srf', '0', '2009-09-18 00:00:00', '4'
#'211.5.550.srf', '0', '2009-09-18 00:00:00', '4'
#'211.1.550.srf', '0', '2009-09-18 00:00:00', '4'
#'211.8.551.srf', '0', '2009-09-18 00:00:00', '4'

}



=pod

=head2 Subroutine : get_library_info_from_seq_unit_name

To run the library level query on RQC database for "seq_unit_name"

=item1	
	set the seq_unit_name
	$obj->seq_unit_name($seq_unit_name);

=item2	
	run the query to get all library level info,"\n"
    $obj->get_library_info_from_seq_unit_name();
    
return a single row with following values
library_id
library_name
activity
bio_classification_id
bio_classification_name
etc 


Return : $result hash reference
Also sets class attribute library_results = $result

=cut


sub get_library_info_from_seq_unit_name {
	my $SUB = 'get_library_info_from_seq_unit_name';
	my $self = shift @_ ;
	my $seq_unit_name  = $self->seq_unit_name();
	my $result;
	my $query = qq (
					select *
					from library_info lib, seq_units seq
					where
					seq.seq_unit_name = "$seq_unit_name"
					and
					seq.rqc_library_id = lib.library_id					
					);
	
	my $sth = $self->database_handle->prepare($query);
	eval {
		$sth->execute();
	};
	if ($@) {
		warn $@;
	}
	## if all good fetch the data
	else {
		### ERROR CHECK
		if ( $sth->rows() == '0' ) {
				print "ERROR[$SUB] Seq unit ".$self->seq_unit_name()." fetched 0 rows\n";
			exit;
		}
		if ( $sth->rows() > 1 ) {
				print "ERROR[$SUB] Seq unit $self->seq_unit_name fetched > 1 rows \n There should be only one row expected $@\n\n ";
			exit;
		}
	my $result = $sth->fetchrow_hashref();
	$self->library_results($result);
	}
}


sub get_rqc_status_message {	
	my $SUB = 'get_rqc_status_message';
	my $self = shift @_ ;
	my $query = qq (       
							select * from rqc_status;
    				);
	my $rqc_status_message;
	eval {
		$rqc_status_message = $self->database_handle->selectall_hashref($query,'rqc_status_id');
	};
	
	if ($@) {
		warn $@;	
		$self->logger()->info(" Following query were run : $query");
	}
#		Storing the returned hash as a class attribute		
#		$self->rqc_status_message($rqc_status_message);
		return $rqc_status_message;
}



=pod

=head2 Subroutine : get_library_info_from_lib_name

To get the library level info from RQC database for "library_name"

Idea very much same as get_library_info_from_seq_unit_name

=item1	
	set the seq_unit_name
	$obj->library_name($library_name;

=item2	
	run the query to get all library level info,"\n"
    $obj->get_library_info_from_lib_name();
    
return a single row with following values
library_id
library_name
activity
bio_classification_id
bio_classification_name
etc 


Return : $result hash reference
Also sets class attribute library_results = $result

=cut


sub get_library_info_from_lib_name {
	my $SUB = 'get_library_info_from_lib_name';
	my $self = shift @_ ;
	my $library_name  = $self->library_name();
	my $result;
	my $query = qq (
					select  *  from library_info lib, seq_units seq
                                where
                                lib.library_name = "$library_name"
                                and
                                seq.rqc_library_id = lib.library_id
                                        	and seq.rqc_status_change_time = ( 
                                            									select max(seq.rqc_status_change_time) from 
                                            									library_info lib, seq_units seq
                                        										where
                                        										lib.library_name = "$library_name"
                                        										and
                                        										seq.rqc_library_id = lib.library_id  
                                        									)
					);
	
	my $sth = $self->database_handle->prepare($query);
	eval {
		$sth->execute();
	};
	if ($@) {
		warn $@;
		$self->logger->info("Following querywas used \n $query \n\n");
	}
	if ( $sth->rows() == '0' ) {
				print "ERROR[$SUB] Seq unit $self->seq_unit_name fetched 0 rows\n";
				$self->logger->info("SQL query : $query");
				exit;
		}
	else {
		my $result = $sth->fetchrow_hashref();
		$self->library_results($result);
	}	
}


sub get_library_info_from_venonat {
	
	my $SUB = 'get_library_info_from_venonat';
	my $self = shift @_ ;
	my $library_name  = $self->get_library_name();
	my $result;
	my $command = qq (
						fetch_library $library_name
					);
	my @output_lib_info_from_venonat ;
	eval {  @output_lib_info_from_venonat = `$command`;  };    
	warn $@ if $@;
		
		foreach my $line (@output_lib_info_from_venonat ) {
		
			if ($line =~/^No data for library.*$/) { 
				$result->{'bio_classification_name'} = undef;	
				$result->{'sample_number'} = undef;
			}
			if ( $line =~/^.*(source_dna.bio_class_name:\s+)(.+?)$/) {
				$result->{'bio_classification_name'} = $2;
			}
			if ( $line =~/^.*(source_dna.source_dna_number:\s+)(.+?)$/) {
				$result->{'sample_number'} = $2;
			}	
			if ( $line =~/^.*(pmo.program_name:\s+)(.+?)$/) {
				$result->{'program_name'} = $2;
			}
			if ( $line =~/^.*(pmo.program_year:\s+)(.+?)$/) {
				$result->{'program_year'} = $2;
			}
			if ( $line =~/^.*(pmo.pmo_project_id:\s+)(.+?)$/) {
				$result->{'pmo_project_id'} = $2;
			}
			if ( $line =~/^.*(library.project_id:\s+)(.+?)$/) {
				$result->{'lib_project_id'} = $2;
			}
		}	
#	Setting the library results from venonat 	
		$self->library_results_venonat($result);	
}










###############
#SUB
###############

sub get_seq_unit_info_from_ITS_DW(){
	
	my $SUB = 'get_seq_unit_info_from_ITS_DW';
	my $self = shift @_;
	my $seq_unit_name = $self->seq_unit_name();
	$seq_unit_name = '\''.$seq_unit_name.'\'';
	
	my $seq_unit_info_ITS_DW = '';
	my $query = qq (       
						select * from  all_inclusive_report air 
						INNER JOIN LIBRARY_STOCK ls
						ON air.library_stock_id = ls.library_stock_id
						INNER JOIN SDM_SEQUENCE_UNIT ssu
						on air.seq_unit_name = ssu.seq_unit_name 
						where air.seq_unit_name = $seq_unit_name
						 
    				);
	eval {
		$seq_unit_info_ITS_DW = $self->ITS_DW_handle->selectall_hashref($query,'SEQ_UNIT_NAME');
	};
	if ($@) {
		warn $@;	
		print " Following query was run : $query";
	}
	$self->seq_unit_info_from_ITS_DW($seq_unit_info_ITS_DW);
}






###############
#SUB
###############
sub get_seq_unit_info_SDM_webservice {
	
	my $SUB = 'get_seq_unit_info_SDM_webservice';
	my $self = shift @_;
	my $seq_unit_name = $self->seq_unit_name();
	
	my $client = REST::Client->new();
	$client->setTimeout(10);
	$client->setHost('http://sdm.jgi-psf.org');
	$client->GET('/cgi-bin/rqc_seq_unit_info.cgi?action=info&seq_unit_file_name='.$seq_unit_name);
	
	if ($client->responseCode() != 200){
		print '[$SUB]:ERROR: response code:',$client->responseCode(). "\n message:".$client->responseContent();
		print 'http://sdm.jgi-psf.org/cgi-bin/rqc_seq_unit_info.cgi?platform=Illumina&action=info&seq_unit_file_name=',$seq_unit_name;
    }
 	else {
 		if (! $client->responseContent()){
 			print 'No response received for seq_unit : ',$seq_unit_name;
 		}
 		else{
 			my $response = from_json($client->responseContent());
 			$self->seq_unit_info_from_SDM_webservice($response);
 		}
 	}
}
#example output:
#$VAR1 = {
#          'physical_run_barcode' => 'C0UTCACXX',
#          'run_date' => '2012-08-03 15:24:02',
#          'sequencer_model' => 'HiSeq',
#          'library_name' => 'ABGQ',
#          'physical_run_id' => '6104',
#          'analysis_task_id' => '6602',
#          'is_multiplex' => '0',
#          'physical_run_section' => '7',
#          'geneus_segment_id' => '47',
#          'file_name' => '6104.7.36431.fastq.gz',
#          'file_status' => 'Local',
#          'platform' => 'Illumina',
#          'fs_location' => '/house/sdm/prod/illumina/seq_data/fastq/00/00/61/04',
#          'sequence_unit_name' => '6104.7.36431.srf',
#          'parent_library_name' => undef
#        };
#





sub get_sdm_seq_unit_path {
	my $SUB = 'get_sdm_seq_unit_path';
	my $self = shift @_;
	return $self->seq_unit_info_from_SDM_webservice()->{'fs_location'} || 'NA';
}




## For a given seq_unit_name if we know the file name in rqc format then the program will return the full path of the equivalent file 
## stored under the basepth. It helps locate any ancillary analysis for each seq_unit
sub get_qualified_rqc_filename {
	
	my $SUB = 'get_qualified_rqc_filename';
	
	my $self = shift @_;
	my  $in_filename = shift @_;
	my $seq_unit_name = $self->seq_unit_name();
	
	my $basepath = '/house/groupdirs/QAQC/rqc_archive/analysis_files/rqc/';
	my $result;
	
	my $query = qq (
					select rqc.analysis_file_id from rqc_analysis_files rqc, m2m_analysis_seq_unit m2m where 
					m2m.analysis_id = rqc.analysis_id 
					and m2m.seq_unit_name = "$seq_unit_name"
					and rqc.file_name= "$in_filename"
					order by rqc.analysis_file_id desc limit 1
					
					);
	
	my $sth = $self->database_handle->prepare($query);
	
	eval {
		$sth->execute();
	};
	
	if ($@) {
		warn $@;
	}
	
	## if all good fetch the data
	else {
		### ERROR CHECK
		if ( $sth->rows() == '0' ) {
				$self->logger()->fatal( "ERROR[$SUB] No rqc qualified name found for file $in_filename \n");
				die "$! \n";
		}
		if ( $sth->rows() > 1 ) {
				$self->logger()->fatal( "ERROR[$SUB] More than one rqc qualified file name found for file_name : $in_filename for seq_unit $seq_unit_name \n $query ");
				die "$! \n";
		}
		
	my $result = $sth->fetchrow_hashref();
	my $qualified_rqc_filename = "$basepath".$$result{'analysis_file_id'};
	
		if ( -e $qualified_rqc_filename  ) { 
			return $qualified_rqc_filename;
		}
		else {
			$self->logger()->warn("qualified rqc file name $qualified_rqc_filename doesn't exist \n\n");
			return 'NA';
		}

	}
	
}




########################
#SUB: 
########################
##returns the hash reference to standard JGI indexes on a illumina plate
sub get_index_to_well_map() {
	
	my $SUB = 'get_index_to_well_map';
	
	my $self = shift @_ ;
	my $index_to_well_location_hashref;
	
	##trimming the last bast off the index_sequence
	my $query = qq (       	
						select location,index_name,substring(default_sequence,1,6) as index_sequence from multiplex_index where index_type='JGI'
    				);
	
	eval {
		$index_to_well_location_hashref = $self->database_handle->selectall_hashref($query,'index_sequence');
	};
	if ($@) {
		warn $@;	
		$self->logger()->info(" Following query were run : $query");
	}
	if ( keys %{$index_to_well_location_hashref} == 0) {
		
			$self->logger()->fatal("ERROR[$SUB] No rows fetched  \n ");
			$self->logger()->info("Please check if run has been processed by RQC\n ");
			$self->logger()->info(" Following query were run : $query");
			die "\n";
	}
	else {
		return $index_to_well_location_hashref
	}
}



##returns the hash reference to standard JGI indexes on a illumina plate
sub get_index_info() {
	
	my $SUB = 'get_index_info';
	my $self = shift @_ ;
	my $index_to_well_location_hashref;
	
	##trimming the last bast off the index_sequence
	my $query = qq (       	
						select location,index_name,substring(default_sequence,1,6) as index_sequence from multiplex_index where index_type='JGI'
    				);
	
	eval {
		$index_to_well_location_hashref = $self->database_handle->selectall_hashref($query,'index_sequence');
	};
	if ($@) {
		warn $@;	
		$self->logger()->info(" Following query were run : $query");
	}
	if ( keys %{$index_to_well_location_hashref} == 0) {
		
			$self->logger()->fatal("ERROR[$SUB] No rows fetched  \n ");
			$self->logger()->info("Please check if run has been processed by RQC\n ");
			$self->logger()->info(" Following query were run : $query");
			die "\n";
	}
	else {
		return $index_to_well_location_hashref
	}
}





sub get_Q30_percent {
	my $SUB = 'get_Q30_percent';
	my $self = shift @_;
	if (defined $self->full_lane_results->{'read Q30'}){
		return $self->full_lane_results->{'read Q30'};
	}
}


sub get_GC_content {
	my $SUB = 'get_GC_content';
	
	my $self = shift @_;
	my $GC_content;
	if ( defined $self->full_lane_results->{'read GC mean'} ){
		$GC_content = $self->full_lane_results->{'read GC mean'};
	}
	else {
		warn "WARN[$SUB]: No value for key (read GC mean)..will return NA\n";
	 	$GC_content = 'NA';
	}
	
	if ( defined $self->full_lane_results->{'read GC std'} ){
		$GC_content .= ' +/- '.$self->full_lane_results->{'read GC std'};
	}
	else {
		warn "WARN[$SUB]: No value for key (read GC std)..will return NA\n";
		$GC_content .=  ' +/- NA';
	}
	
	return $GC_content;
}


sub get_num_reads_sampled {
	my $SUB = 'get_num_reads_sampled';
	my $self = shift @_;
	
	if (defined $self->full_lane_results->{'read 20mer sample size'}){
		my $num_reads_sampled = $self->full_lane_results->{'read 20mer sample size'};
		return (sprintf('%.02f', ($num_reads_sampled/1000000) ));
	}
	else {
		warn "WARN[$SUB] No value for the key (read 20mer sample size) .. will return NA\n";
		return 'NA';
	}
}


sub get_20mer_uniq_start_mers_percent {
	my $SUB = 'get_20mer_uniq_start_mers_percent';
	my $self = shift @_;
	
	if (defined $self->full_lane_results->{'read 20mer percentage starting mers'}){
		return $self->full_lane_results->{'read 20mer percentage starting mers'};
	}
	else {
		warn "WARN[$SUB] No value for the key (read 20mer percentage starting mers) .. will return NA\n";
		return 'NA';
	}
}


sub get_20mer_uniq_random_mers_percent {
	my $SUB = 'get_20mer_uniq_random_mers_percent';

	my $self = shift @_;
	if (defined $self->full_lane_results->{'read 20mer percentage random mers'}){
		return $self->full_lane_results->{'read 20mer percentage random mers'};
	}
	else {
		warn "WARN[$SUB] No value for the key (read 20mer percentage random mers) .. will return NA\n";
		return 'NA';
	}
}


sub get_bwa_algn_dup_percent {
	my $SUB = 'get_bwa_algn_dup_percent';
	my $self = shift @_;
	
	if (defined $self->full_lane_results->{'read bwa aligned duplicate percent'}){
		return $self->full_lane_results->{'read bwa aligned duplicate percent'};
	}
	else {
		warn "WARN[$SUB] No value for the key (read bwa aligned duplicate percent) .. will return NA\n";
		return 'NA';
	}
}


sub get_lib_complexity {
	my $SUB = 'get_lib_complexity';
	my $self = shift @_;
	
	my $lib_complexity = 'NA';
	if (defined $self->full_lane_results->{'read 20mer percentage random mers'}){
		$lib_complexity = $self->full_lane_results->{'read 20mer percentage random mers'}.'%   :  ';
	}
	if (defined $self->full_lane_results->{'read 20mer percentage starting mers'}){
		$lib_complexity .= $self->full_lane_results->{'read 20mer percentage starting mers'}.'%';
	}
	return $lib_complexity;
}




sub get_percent_illumina_artifacts {
	my $SUB = 'get_percent_illumina_artifacts';
	my $self = shift @_;
	if (defined $self->full_lane_results->{'illumina read percent contamination artifact'}){
		return sprintf("%.2f" , $self->full_lane_results->{'illumina read percent contamination artifact'});
	}
	else {
		warn "WARN[$SUB] No value for the key (illumina read percent contamination artifact) .. will return NA\n";
		return 'NA';
	}
	
}


sub get_percent_reads_map_Ecoli {
	my $SUB = 'get_percent_reads_map_Ecoli';
	my $self = shift @_;
	if (defined $self->full_lane_results->{'illumina read percent contamination ecoli combined'}){
		return sprintf("%.2f" , $self->full_lane_results->{'illumina read percent contamination ecoli combined'});
	}
	else {
		warn "WARN[$SUB] No value for the key (illumina read percent contamination ecoli combined) .. will return NA\n";
		return 'NA';
	}
}


sub get_percent_reads_map_fosmid {
	my $SUB = 'sub get_percent_reads_map_fosmid';
	my $self = shift @_;
	if (defined $self->full_lane_results->{'illumina read percent contamination fosmid'}){
		return sprintf("%.2f" , $self->full_lane_results->{'illumina read percent contamination fosmid'});
	}
	else {
		warn "WARN[$SUB] No value for the key (illumina read percent contamination fosmid) .. will return NA\n";
		return 'NA';
	}
}

sub get_percent_jgi_contaminants {
	my $SUB = 'get_percent_jgi_contaminants';
	my $self = shift @_;
	if (defined $self->full_lane_results->{'illumina read percent contamination contaminants'}){
		return sprintf("%.2f" , $self->full_lane_results->{'illumina read percent contamination contaminants'});
	}
	else {
		warn "WARN[$SUB] No value for the key (illumina read percent contamination contaminants) .. will return NA\n";
		return 'NA';
	}
}

sub get_percent_rRNA_contaminants {
	my $SUB = 'get_percent_rRNA_contaminants';
	my $self = shift @_;
	if (defined $self->full_lane_results->{'illumina read percent contamination rrna'}){
		return sprintf("%.2f" , $self->full_lane_results->{'illumina read percent contamination rrna'});
	}
	else {
		warn "WARN[$SUB] No value for the key (read artifact percent) .. will return NA\n";
		return 'NA';
	}
}



sub get_percent_chloroplast_contaminants {
	my $SUB = 'get_percent_chloroplast_contamination';
	my $self = shift @_;
	if (defined $self->full_lane_results->{'illumina read percent contamination plastid'}){
		return sprintf("%.2f" , $self->full_lane_results->{'illumina read percent contamination plastid'});
	}
	else {
		warn "WARN[$SUB] No value for the key (illumina read percent contamination plastid) .. will return NA\n";
		return 'NA';
	}
}



sub get_percent_mito_contaminants {
	my $SUB = 'get_percent_mito_contamination';
	my $self = shift @_;
	if (defined $self->full_lane_results->{'illumina read percent contamination mitochondrion'}){
		return sprintf("%.2f" , $self->full_lane_results->{'illumina read percent contamination mitochondrion'});
	}
	else {
		warn "WARN[$SUB] No value for the key (illumina read percent contamination mitochondrion) .. will return NA\n";
		return 'NA';
	}
}




sub get_kmer_size {
	
	my $SUB = 'get_kmer_size';
	
	my $self = shift @_;
	
	if (defined $self->full_lane_results->{'assembly kmer'}){
		return $self->full_lane_results->{'assembly kmer'};
	}
	else {
		warn "WARN[$SUB] No value for the key (assembly kmer) .. will return NA\n";
		return 'NA';
	}
	
}


sub get_min_contig_len {
	
	my $SUB = 'get_min_contig_len';
	
	my $self = shift @_;
	
	if (defined $self->full_lane_results->{'assembly min contig length'}){
		return $self->full_lane_results->{'assembly min contig length'};
	}
	else {
		warn "WARN[$SUB] No value for the key (assembly min contig length) .. will return NA\n";
		return 'NA';
	}
	
}


sub get_N50 {
	
	my $SUB = 'get_N50';
	
	my $self = shift @_;
	
	if (defined $self->full_lane_results->{'assembly n50'}){
		return $self->full_lane_results->{'assembly n50'};
	}
	else {
		warn "WARN[$SUB] No value for the key (assembly n50) .. will return NA\n";
		return 'NA';
	}
	
}

sub get_max_contig_size {
	
	my $SUB = 'get_contig_size';
	
	my $self = shift @_;
	
	if (defined $self->full_lane_results->{'assembly max contig length'}){
		return $self->full_lane_results->{'assembly max contig length'};
	}
	else {
		warn "WARN[$SUB] No value for the key (assembly max contig length ) .. will return NA\n";
		return 'NA';
	}
	
}


sub get_contig_count {
	
	my $SUB = 'get_contig_count';
	
	my $self = shift @_;
	
	if (defined $self->full_lane_results->{'assembly node number'}){
		return $self->full_lane_results->{'assembly node number'};
	}
	else {
		warn "WARN[$SUB] No value for the key (assembly node number ) .. will return NA\n";
		return 'NA';
	}
	
}


sub get_contigs_total_len {
	
	my $SUB = 'get_contigs_total_len';
	
	my $self = shift @_;
	
	if (defined $self->full_lane_results->{'assembly all node length'}){
		return $self->full_lane_results->{'assembly all node length'};
	}
	else {
		warn "WARN[$SUB] No value for the key (assembly all node length ) .. will return NA\n";
		return 'NA';
	}
	
}


sub get_total_read_count {
	
	my $SUB = 'get_total_read_count';
	
	my $self = shift @_;
	
	if (defined $self->full_lane_results->{'read count'}){
		return $self->full_lane_results->{'read count'};
	}
	else {
		warn "WARN[$SUB] No value for the key (read count) .. will return NA\n";
		return 0;
	}
	
}


sub mean_assembly_GC {
	
	my $SUB = 'mean_assembly_GC';
	
	my $self = shift @_;
	my $assembly_GC_content;
	
	if ( defined $self->full_lane_results->{'assembly GC mean'} ){
		$assembly_GC_content = $self->full_lane_results->{'assembly GC mean'};
	}
	else {
		warn "WARN[$SUB]: No value for key (assembly GC mean)..will return NA\n";
	 	$assembly_GC_content = 'NA';
	}
	
	if ( defined $self->full_lane_results->{'assembly GC std'} ){
		$assembly_GC_content .= ' +/- '.$self->full_lane_results->{'assembly GC std'};
	}
	else {
		warn "WARN[$SUB]: No value for key (assembly GC std)..will return NA\n";
		$assembly_GC_content .=  ' +/- NA';
	}
	
	return $assembly_GC_content;
	
}


sub get_kmer_depth_90x {
	
	my $SUB = 'get_kmer_depth_90x';
	
	my $self = shift @_;
	my $kmer_depth;
	
	if ( defined $self->full_lane_results->{'assembly depth mean 90'} ){
		$kmer_depth = $self->full_lane_results->{'assembly depth mean 90'};
	}
	else {
		warn "WARN[$SUB]: No value for key (assembly depth mean 90)..will return NA\n";
	 	$kmer_depth = 'NA';
	}
	
	if ( defined $self->full_lane_results->{'assembly depth std 90'} ){
		$kmer_depth .= ' +/- '.$self->full_lane_results->{'assembly depth std 90'};
	}
	else {
		warn "WARN[$SUB]: No value for key (assembly depth std 90)..will return NA\n";
		$kmer_depth .=  ' +/- NA';
	}
	
	return $kmer_depth;
	
}


sub get_nucleotide_seq_depth {
	
	my  $SUB = 'get_nucleotide_seq_depth';
	my $self = shift @_; 
	
	my $read_1_len	=	$self->get_read_1_len();
	my $read_2_len	=	$self->get_read_2_len();
	
	if( ($read_1_len == $read_2_len)  ) {
		
	my $kmer_depth  = $self->get_kmer_depth_90x();
	my $kmer_size  = $self->get_kmer_size();
	
	
	my ($kmer_depth_without_SD, $sign , $kmer_depth_SD) = split( ' ', $kmer_depth);
	
		my $nucleotide_seq_depth = sprintf ( '%.02f', ( ($kmer_depth_without_SD * $read_1_len)/($read_1_len - $kmer_size + 1) )   );
		return $nucleotide_seq_depth;		
	}
	else {
		print "WARN[$SUB] : Read 1($read_1_len) and Read 2($read_2_len) are not equal. Will not calculate nuc seq depth\n";
		return 'NA';
	}
	
	
}


sub get_read_1_len {
	
	
	my $SUB = 'get_read_1_len';
	
	my $self = shift @_ ;
	
	if ( defined $self->full_lane_results->{'read length 1'} ){
		return $self->full_lane_results->{'read length 1'};
	}
	else {
		warn "WARN[$SUB]: No value for key (read length 1)..will return NA\n";
	 	return 'NA';
	}
}

sub get_read_2_len {
	
	
	my $SUB = 'get_read_2_len';
	
	my $self = shift @_ ;
	
	my $verbose = $self->verbose();
	
	if ( defined $self->full_lane_results->{'read length 2'} ){
		return $self->full_lane_results->{'read length 2'};
	}
	else {
		warn "WARN[$SUB]: No value for key (read length 2)..will return NA\n" if $verbose;
	 	return 'NA';
	}
}

sub get_num_blastable_contigs {
	
	my $SUB = 'get_num_blastable_contigs';
	
	my $self = shift @_;
	
	if (defined $self->full_lane_results->{'assembly contig number'}){
		return $self->full_lane_results->{'assembly contig number'};
	}
	else {
		warn "WARN[$SUB] No value for the key (assembly contig number) .. will return NA\n";
		return 'NA';
	}
	
}




sub get_ {
	
	my $SUB = 'get_num_NT_tophits';
	
	my $self = shift @_;
	
	if (defined $self->full_lane_results->{'assembly top hits of nt'}){
		return $self->full_lane_results->{'assembly top hits of nt'};
	}
	else {
		warn "WARN[$SUB] No value for the key (assembly top hits of nt) .. will return NA\n";
		return 'NA';
	}
	
}




sub get_num_NT_tophits {
	
	my $SUB = 'get_num_NT_tophits';
	
	my $self = shift @_;
	
	if (defined $self->full_lane_results->{'assembly top hits of nt'}){
		return $self->full_lane_results->{'assembly top hits of nt'};
	}
	else {
		warn "WARN[$SUB] No value for the key (assembly top hits of nt) .. will return NA\n";
		return 'NA';
	}
	
}


sub get_read1_qreport_filename   {
	my $SUB = 'get_read1_qreport_filename';

	my $self = shift @_;

	if (defined $self->full_lane_results->{'read qual pos qrpt 1'}){
		return $self->full_lane_results->{'read qual pos qrpt 1'};
	}
	else {
		warn "WARN[$SUB] No value for the key (read qual pos qrpt 1) .. will return NA\n";
		return 'NA';
	}
}


sub get_read2_qreport_filename   {
	my $SUB = 'get_read2_qreport_filename';
	
	my $self = shift @_;
	
	if (defined $self->full_lane_results->{'read qual pos qrpt 2'}){
		return $self->full_lane_results->{'read qual pos qrpt 2'};
	}
	else {
		warn "WARN[$SUB] No value for the key (read qual pos qrpt 2) .. will return NA\n";
		return 'NA';
	}
}



## will return the fastx toolkit generated filename for the read 1
## which stores the base composition per cycle of read 1
sub get_read1_basecount_filename   {
	
	
	my $SUB = 'get_read1_basecount_filename';
	
	my $self = shift @_;
	
	if (defined $self->full_lane_results->{'read base count text 1'}){
		return $self->full_lane_results->{'read base count text 1'};
	}
	else {
		warn "WARN[$SUB] No value for the key (read base count text 1) .. will return NA\n";
		return 'NA';
	}
	
}


## will return the fastx toolkit generated filename for the read 2
## which stores the base composition per cycle of read 2
sub get_read2_basecount_filename   {
	
	
	my $SUB = 'get_read2_basecount_filename';
	
	my $self = shift @_;
	
	my $verbose = $self->verbose();
	
	if (defined $self->full_lane_results->{'read base count text 2'}){
		return $self->full_lane_results->{'read base count text 2'};
	}
	else {
		warn "WARN[$SUB] No value for the key (read base count text 2) .. will return NA\n" if $verbose;
		return 'NA';
	}
	
}


## given a read base count file name specified by user the routine
## will return mean +/- stddev A percent
## between the start cycle and end cycle specified by the user 
## if none given it defaults to full read;
sub get_A_percent {
	
	
	my $SUB = 'get_A_percent';
	
	my $self = shift @_ ;
	
	my $read_basecount_filename  	= shift @_ ;
	
	my $report_from_what_cycle		= shift @_ || 1;		
	my $report_till_what_cycle 		= shift @_ || $self->get_read_1_len();
	
	
	my $A_percent ;
	
						
		my $full_qualified_read_basecount_filename 	= $self->get_qualified_rqc_filename($read_basecount_filename);
		
		open(IN,"< $full_qualified_read_basecount_filename") || die "Error[$SUB] Can't open file $full_qualified_read_basecount_filename \n";
		
		my @percent_A_per_cycle;
		my $which_cycle = 1;
		
		## lopping of the 
		while(<IN>){
			
			
			## skip the header line
			if (/^#.*?/) { next ; }
			
			# keep skipping the cycles 
			if ($which_cycle < $report_from_what_cycle ) { $which_cycle++; next; }
			
			## break the loop at which cycle = cycle specified by the user..
			if ( $which_cycle > $report_till_what_cycle ) { last; }
			
			my @temp = split(' ', $_);
			
			#temp[3] is the percent_A per cycle
			push(@percent_A_per_cycle, $temp[3]);
			
			$which_cycle++;
		}
		
		
		my $stat = Statistics::Descriptive::Discrete->new();
   	  	$stat->add_data(@percent_A_per_cycle);
    	$A_percent = sprintf("%.2f",$stat->mean());
    	$A_percent .= ' +/- '. sprintf("%.2f",$stat->standard_deviation() ). ' %';
		
		return $A_percent;
	
}

## given a read base count file name specified by user the routine
## will return mean +/- stddev T percent
## between the start cycle and end cycle specified by the user 
## if none given it defaults to full read;
sub get_T_percent {
	
	
	my $SUB = 'get_T_percent';
	
	my $self = shift @_ ;
	
	my $read_basecount_filename  = shift @_ ;
	
	my $report_from_what_cycle		= shift @_ || 1;		
	my $report_till_what_cycle 		= shift @_ || $self->get_read_1_len();
	
	my $T_percent ;
	
						
		my $full_qualified_read_basecount_filename 	= $self->get_qualified_rqc_filename($read_basecount_filename);
		
		open(IN,"< $full_qualified_read_basecount_filename") || die "Error[$SUB] Can't open file $full_qualified_read_basecount_filename \n";
		
		my @percent_T_per_cycle;
		
		my $which_cycle = 1;
		
		## lopping of the 
		while(<IN>){
			
			## skip the header line
			if (/^#.*?/) { next ; }
			
			# keep skipping the cycles 
			if ($which_cycle < $report_from_what_cycle ) { $which_cycle++; next; }
			
			## break the loop at which cycle = cycle specified by the user..
			if ( $which_cycle > $report_till_what_cycle ) { last; }
			
			my @temp = split(' ', $_);
			
			#temp[5] is the percent_T per cycle
			push(@percent_T_per_cycle, $temp[5]);
			
			$which_cycle++;
		}
		
		
		my $stat = Statistics::Descriptive::Discrete->new();
   	  	$stat->add_data(@percent_T_per_cycle);
    	$T_percent = sprintf("%.2f",$stat->mean());
    	$T_percent .= ' +/- '. sprintf("%.2f",$stat->standard_deviation() ). ' %';
		
		return $T_percent;
	
}



## given a read base count file name specified by user the routine
## will return mean +/- stddev G percent
## between the start cycle and end cycle specified by the user 
## if none given it defaults to full read;
sub get_G_percent {
	
	
	my $SUB = 'get_G_percent';
	
	my $self = shift @_ ;
	
	my $read_basecount_filename  = shift @_ ;
	
	my $report_from_what_cycle		= shift @_ || 1;		
	my $report_till_what_cycle 		= shift @_ || $self->get_read_1_len();
	
	my $G_percent ;
	
						
		my $full_qualified_read_basecount_filename 	= $self->get_qualified_rqc_filename($read_basecount_filename);
		
		open(IN,"< $full_qualified_read_basecount_filename") || die "Error[$SUB] Can't open file $full_qualified_read_basecount_filename \n";
		
		my @percent_G_per_cycle;
		my $which_cycle = 1;
		
		## lopping of the 
		while(<IN>){
			
			## skip the header line
			if (/^#.*?/) { next ; }
			
			# keep skipping the cycles 
			if ($which_cycle < $report_from_what_cycle ) { $which_cycle++; next; }
			
			## break the loop at which cycle = cycle specified by the user..
			if ( $which_cycle > $report_till_what_cycle ) { last; }
			
			
			my @temp = split(' ', $_);
			
			#temp[7] is the percent_G per cycle
			push(@percent_G_per_cycle, $temp[7]);
			
			$which_cycle++;
		}
		
		
		my $stat = Statistics::Descriptive::Discrete->new();
   	  	$stat->add_data(@percent_G_per_cycle);
    	$G_percent = sprintf("%.2f",$stat->mean());
    	$G_percent .= ' +/- '. sprintf("%.2f",$stat->standard_deviation() ). ' %';
		
		return $G_percent;
	
}


# given a read base count file name specified by user the routine
## will return mean +/- stddev G percent
## between the start cycle and end cycle specified by the user 
## if none given it defaults to full read;
sub get_C_percent {
	
	
	my $SUB = 'get_C_percent';
	
	my $self = shift @_ ;
	
	my $read_basecount_filename  = shift @_ ;
	
	my $report_from_what_cycle		= shift @_ || 1;		
	my $report_till_what_cycle 		= shift @_ || $self->get_read_1_len();
	
	my $C_percent ;
	
						
		my $full_qualified_read_basecount_filename 	= $self->get_qualified_rqc_filename($read_basecount_filename);
		
		open(IN,"< $full_qualified_read_basecount_filename") || die "Error[$SUB] Can't open file $full_qualified_read_basecount_filename \n";
		
		my @percent_C_per_cycle;
		my $which_cycle = 1;
		
		## lopping of the 
		while(<IN>){
			
			## skip the header line
			if (/^#.*?/) { next ; }
			
			# keep skipping the cycles 
			if ($which_cycle < $report_from_what_cycle ) { $which_cycle++; next; }
			
			## break the loop at which cycle = cycle specified by the user..
			if ( $which_cycle > $report_till_what_cycle ) { last; }
			my @temp = split(' ', $_);
			
			#temp[9] is the percent_C per cycle
			push(@percent_C_per_cycle, $temp[9]);
			
			$which_cycle++;
		}
		
		
		my $stat = Statistics::Descriptive::Discrete->new();
   	  	$stat->add_data(@percent_C_per_cycle);
    	$C_percent = sprintf("%.2f",$stat->mean());
    	$C_percent .= ' +/- '. sprintf("%.2f",$stat->standard_deviation() ). ' %';
		
		return $C_percent;
	
}

# given a read base count file name specified by user the routine
## will return mean +/- stddev N percent
## between the start cycle and end cycle specified by the user 
## if none given it defaults to full read;
sub get_N_percent {
	
	
	my $SUB = 'get_N_percent';
	
	my $self = shift @_ ;
	
	my $read_basecount_filename  = shift @_ ;
	
	my $report_from_what_cycle		= shift @_ || 1;		
	my $report_till_what_cycle 		= shift @_ || $self->get_read_1_len();
	
	my $N_percent ;
	
						
		my $full_qualified_read_basecount_filename 	= $self->get_qualified_rqc_filename($read_basecount_filename);
		
		open(IN,"< $full_qualified_read_basecount_filename") || die "Error[$SUB] Can't open file $full_qualified_read_basecount_filename \n";
		
		my @percent_N_per_cycle;
		my $which_cycle = 1;
		
		## lopping of the 
		while(<IN>){
			
			## skip the header line
			if (/^#.*?/) { next ; }
			
			# keep skipping the cycles 
			if ($which_cycle < $report_from_what_cycle ) { $which_cycle++; next; }
			
			## break the loop at which cycle = cycle specified by the user..
			if ( $which_cycle > $report_till_what_cycle ) { last; }
			
			my @temp = split(' ', $_);
			
			#temp[11] is the percent_C per cycle
			push(@percent_N_per_cycle, $temp[11]);
		
		
			$which_cycle++;
		}
		
		
		my $stat = Statistics::Descriptive::Discrete->new();
   	  	$stat->add_data(@percent_N_per_cycle);
    	$N_percent = sprintf("%.2f",$stat->mean());
    	$N_percent .= ' +/- '. sprintf("%.2f",$stat->standard_deviation() ). ' %';
		
		return $N_percent;
	
}


## Should work only for RNA Seq libraries 
sub get_num_reads_matched_adapter {
	
	my $SUB = 'get_num_reads_matched_adapter';
	
	my $self = shift @_;
	
	if (defined $self->full_lane_results->{'pcr_reads_projected'}){
		return $self->full_lane_results->{'pcr_reads_projected'};
	}
	else {
		warn "WARN[$SUB] No value for the key (pcr_reads_projected) .. will return NA\n";
		return 'NA';
	}
	
}


## Should work only for RNA Seq libraries 
sub get_num_reads_matched_rRNA {
	
	my $SUB = 'get_num_reads_matched_rRNA';
	
	my $self = shift @_;
	
	if (defined $self->full_lane_results->{'rRNA_reads_projected'}){
		return $self->full_lane_results->{'rRNA_reads_projected'};
	}
	else {
		warn "WARN[$SUB] No value for the key (rRNA_reads_projected) .. will return NA\n";
		return 'NA';
	}
	
}


## Should work only for RNA Seq libraries 
sub get_num_reads_matched_transcriptome {
	
	my $SUB = 'get_num_reads_matched_transcriptome';
	
	my $self = shift @_;
	
	if (defined $self->full_lane_results->{'transcriptome_mapped_reads'}){
		return $self->full_lane_results->{'transcriptome_mapped_reads'};
	}
	else {
		warn "WARN[$SUB] No value for the key (transcriptome_mapped_reads) .. will return NA\n";
		return 'NA';
	}
	
}


## Should work only for RNA Seq libraries 
sub get_num_reads_matched_genome {
	
	my $SUB = 'get_num_reads_matched_genome';
	
	my $self = shift @_;
	
	if (defined $self->full_lane_results->{'genome_map_reads'}){
		return $self->full_lane_results->{'genome_map_reads'};
	}
	else {
		warn "WARN[$SUB] No value for the key (genome_map_reads) .. will return NA\n";
		return 'NA';
	}
	
}


## Should work only for RNA Seq libraries 
sub get_insert_size_RNA_lib {
	
	my $SUB = 'get_insert_size_RNA_lib';
	
	my $self = shift @_;
	my $insert_size_RNA_lib;
	
	if ( defined $self->full_lane_results->{'insert_size'} ){
		$insert_size_RNA_lib = $self->full_lane_results->{'insert_size'};
	}
	else {
		warn "WARN[$SUB]: No value for key (insert_size)..will return NA\n";
	 	$insert_size_RNA_lib = 'NA';
	}
	
	if ( defined $self->full_lane_results->{'insert_size_std_dev'} ){
		$insert_size_RNA_lib .= ' +/- '.$self->full_lane_results->{'insert_size_std_dev'};
	}
	else {
		warn "WARN[$SUB]: No value for key (insert_size_std_dev)..will return NA\n";
		$insert_size_RNA_lib .=  ' +/- NA';
	}
	
	return $insert_size_RNA_lib;
	
}


## Should work only for RNA Seq libraries 
sub get_transcriptome_SE_percent {
	
	my $SUB = 'get_transcriptome_SE_percent';
	
	my $self = shift @_;
	
	if (defined $self->full_lane_results->{'transcriptome_se_percent'}){
		return $self->full_lane_results->{'transcriptome_se_percent'};
	}
	else {
		warn "WARN[$SUB] No value for the key (transcriptome_se_percent) .. will return NA\n";
		return 'NA';
	}
	
}

## Should work only for RNA Seq libraries 
sub get_transcriptome_PE_percent {
	
	my $SUB = 'get_transcriptome_PE_percent';
	
	my $self = shift @_;
	
	if (defined $self->full_lane_results->{'transcriptome_pe_total_percent'}){
		return $self->full_lane_results->{'transcriptome_pe_total_percent'};
	}
	else {
		warn "WARN[$SUB] No value for the key (transcriptome_pe_total_percent) .. will return NA\n";
		return 'NA';
	}
	
}



## Should work only for RNA Seq libraries 
sub get_transcriptome_PE_conflict_percent {
	
	my $SUB = 'get_transcriptome_PE_conflict_percent';
	
	my $self = shift @_;
	
	if (defined $self->full_lane_results->{'transcriptome_pe_conflict_percent'}){
		return $self->full_lane_results->{'transcriptome_pe_conflict_percent'};
	}
	else {
		warn "WARN[$SUB] No value for the key (transcriptome_pe_conflict_percent) .. will return NA\n";
		return 'NA';
	}
	
}


## Should work only for RNA Seq libraries 
sub get_transcriptome_PE_agree_percent {
	
	my $SUB = 'get_transcriptome_PE_agree_percent';
	my $self = shift @_;
	
	if (defined $self->full_lane_results->{'transcriptome_pe_agree_percent'}){
		return $self->full_lane_results->{'transcriptome_pe_agree_percent'};
	}
	else {
		warn "WARN[$SUB] No value for the key (transcriptome_pe_agree_percent) .. will return NA\n";
		return 'NA';
	}
	
}



# Returns the library name for a corresponding seg_unit_name
sub get_library_name {
	
	my $SUB = 'get_library_name';
	
	my $self = shift @_;
	
	if (exists $self->library_results->{'library_name'}){
		return $self->library_results->{'library_name'};
	}
	else {
		warn "WARN[$SUB] No value for the key (library_name) .. will return NA\n";
		return 'NA';
	}
	
}

# Returns the bio_classification_name for a corresponding seg_unit_name
sub get_bio_classification_name {
	
	my $SUB = 'get_bio_classification_name';
	
	my $self = shift @_;
		
	
	# first try to get the info from venonat results
	if (defined $self->library_results_venonat->{'bio_classification_name'}){
		return $self->library_results_venonat->{'bio_classification_name'};
	}
	# if not then get the info from RQC dbase library result
	elsif (defined $self->library_results->{'ncbi_organism_name'}){
		return $self->library_results->{'ncbi_organism_name'};
	}
	else {
		warn "WARN[$SUB] No value for the key (ncbi_organism_name) .. will return NA\n";
		return 'NA';
	}
	
}

# Returns the program_name a corresponding to a library
sub get_pmo_program_name {
	
	my $SUB = 'get_pmo_program_name';
	my $self = shift @_;
		
	# first try to get the info from venonat results
	if (defined $self->library_results_venonat->{'program_name'}){
		return $self->library_results_venonat->{'program_name'};
	}
	else {
		warn "WARN[$SUB] No value for the key (program_name) .. will return NA\n";
		return 'NA';
	}
	
}

# Returns the program_year  a corresponding to a library
sub get_pmo_program_year {
	
	my $SUB = 'get_pmo_program_year';
	my $self = shift @_;
		
	# first try to get the info from venonat results
	if (defined $self->library_results_venonat->{'program_year'}){
		return $self->library_results_venonat->{'program_year'};
	}
	else {
		warn "WARN[$SUB] No value for the key (program_year) .. will return NA\n";
		return 'NA';
	}
	
}


# Returns the pmo_project_id for a corresponding seg_unit_name
sub get_pmo_project_id {
	
	my $SUB = 'get_pmo_project_id';
	
	my $self = shift @_;

	# first check if results from venonat has the information
	if (defined $self->library_results_venonat->{'pmo_project_id'} ){
		return $self->library_results_venonat->{'pmo_project_id'};
	}
	#then try RQC db
	elsif (defined $self->library_results->{'pmo_project_id'} ){
		return $self->library_results->{'pmo_project_id'};
	}
	else {
		warn "WARN[$SUB] No value for the key (pmo_project_id) .. will return NA\n";
		return 'NA';
	}
}


# Returns the project_id  AND NOT PMO PROJECT ID for a corresponding seg_unit_name
sub get_lib_project_id {
	
	my $SUB = 'get_lib_project_id';
	
	my $self = shift @_;

	# first check if results from venonat has the information
	if (defined $self->library_results_venonat->{'lib_project_id'} ){
		return $self->library_results_venonat->{'lib_project_id'};
	}
	#then try RQC db
	##in RQC db i think library_project_id is equivalent to sample_number
	elsif (defined $self->library_results->{'sample_number'} ){
		return $self->library_results->{'sample_number'};
	}
	else {
		warn "WARN[$SUB] No value for the key (project_id) .. will return NA\n";
		return 'NA';
	}
}



#Returns the project type for a corresponding  seq unit name
sub get_project_type {
	my $SUB = 'get_project_type';
	
	my $self = shift @_;
	
	# first check if results from venonat has the information
	if (defined $self->library_results_venonat->{'project_type'} ){
		return $self->library_results_venonat->{'project_type'};
	}
	#then try RQC db
	elsif (defined $self->library_results->{'project_type'} ){
		return $self->library_results->{'project_type'};
	}
	else {
		warn "WARN[$SUB] No value for the key (project_type) .. will return NA\n";
		return 'NA';
	}
}


sub get_taxa_kingdom {
	my $SUB = 'get_taxa_kingdom';
	
	my $self = shift @_;
	
	# first check if results from venonat has the information
	if (defined $self->library_results_venonat->{'kingdom'} ){
		return $self->library_results_venonat->{'kingdom'};
	}
	#then try RQC db
	elsif (defined $self->library_results->{'kingdom'} ){
		return $self->library_results->{'kingdom'};
	}
	else {
		warn "WARN[$SUB] No value for the key (kingdom) .. will return NA\n";
		return 'NA';
	}
}


sub get_source_dna_type {
	my $SUB = 'get_source_dna_type';
	
	my $self = shift @_;
	# first check if results from venonat has the information
	if (defined $self->library_results_venonat->{'source_dna_type'} ){
		return $self->library_results_venonat->{'source_dna_type'};
	}
	#then try RQC db
	elsif (defined $self->library_results->{'source_dna_type'} ){
		return $self->library_results->{'source_dna_type'};
	}
	else {
		warn "WARN[$SUB] No value for the key (source_dna_type) .. will return NA\n";
		return 'NA';
	}
}



# Returns the source dna number  for a corresponding seq_unit_name
sub get_source_dna_number {
	
	my $SUB = 'get_source_dna_number';
	
	my $self = shift @_;
	# first check if results from venonat has the information
	if (defined $self->library_results_venonat->{'sample_number'} ){
		return $self->library_results_venonat->{'sample_number'};
	}
	# else get it from RQC dbase
	elsif (defined $self->library_results->{'sample_number'} ){
		return $self->library_results->{'sample_number'};
	}
	else {
		warn "WARN[$SUB] No value for the key (sample_number) .. will return NA\n";
		return 'NA';
	}
}



# Returns the library_type for a corresponding seg_unit_name
sub get_library_type {
	
	my $SUB = 'get_library_type';
	
	my $self = shift @_;
	
	if (defined $self->library_results->{'library_type'}){
		return $self->library_results->{'library_type'};
	}
	else {
		warn "WARN[$SUB] No value for the key (library_type) .. will return NA\n";
		return 'NA';
	}
	
}

# Returns the lane yield in Mbp for each seq_unit_name if length read1=read2
sub get_lane_yield {
	my $SUB = 'get_lane_yield';
	
	my $self = shift @_;
	my $read_count 	=	$self->get_total_read_count();
	my $read_1_len	=	$self->get_read_1_len();
	my $read_2_len	=	$self->get_read_2_len();
#	If only read 1 is present : Assuming run is single read
	if( ($read_1_len) && $read_2_len eq 'NA'){
		my $lane_yield = sprintf ( '%.02f', ( ($read_count * $read_1_len )/(1000000) )  ) if ($read_count);
		return $lane_yield || 0;		
	}
#	Run is PE
	elsif ($read_1_len == $read_2_len ) {
		my $lane_yield = sprintf ( '%.02f', ( ($read_count * $read_2_len )/(1000000) )  )if ($read_count);
		return $lane_yield || 0;
	}
	
#	Run is PE but read 1 length is not == to read 2 len : Using the avg  of the 2 to calculate yield
	elsif ( $read_1_len != $read_2_len ) {
		my $read_len_to_use = ($read_1_len + $read_2_len ) / 2; 
		my $lane_yield = sprintf ( '%.02f', ( ($read_count * $read_len_to_use )/(1000000) )  );
		return $lane_yield;	
	}
	elsif ( $read_1_len == 'NA'){
		print "WARN[$SUB] : Read 1($read_1_len) == NA . Will not calculate total yield for this lane\n";
		return 'NA';
	}
	else {
		print "WARN[$SUB] : Read 1 $read_1_len and  Read 2 $read_2_len. Will not calculate total yield for this lane\n";
		return 'NA';
	}
}


sub get_run_config {
	my $SUB = 'get_run_config';
	my $self = shift @_;
	
	my $read_1_len	=	$self->get_read_1_len();
	my $read_2_len	=	$self->get_read_2_len();
	#	If only read 1 is present : Assuming run is single read
	if( ($read_1_len) && $read_2_len eq 'NA'){
		return '1x'."$read_1_len";
	}
	elsif( $read_1_len == $read_2_len   ) {
		return '2x'."$read_2_len";	
	}
	elsif ( (  $read_1_len != $read_2_len ) && ( $read_1_len ne'NA') ){
		my $read_len_to_use = $read_1_len > $read_2_len ?  $read_1_len : $read_2_len ;
		return '2x'."$read_len_to_use";
	}
	else {
		print "WARN[$SUB] : Read 1($read_1_len) and Read 2($read_2_len)not falling in the if conditions for $SUB . Will not return NA \n";
		return 'NA';
	}
}


sub get_qualified_seq_unit_name {
	my $SUB = 'get_qualified_seq_unit_name';
	my $self = shift @_ ;
	my $qualified_seq_unit_name = $self->seq_unit_name();
	$qualified_seq_unit_name=~s/\.srf/\.fastq.gz/;
	return ($qualified_seq_unit_name );
	
}


sub get_seq_unit_path {
	my $SUB = 'get_seq_unit_path';
	my $self = shift @_ ;
	my $qualified_seq_unit_name = $self->seq_unit_name();
	
	$qualified_seq_unit_name=~s/\.srf/\.fastq.gz/;
	return ($qualified_seq_unit_name );
	
}





## generating lower of read1/2 the Q20 read length
sub get_min_Q20_readlen_of_read_1_2 {
	my $SUB = 'get_min_Q20_readlen_of_read_1_2';
	
	my $self = shift @_ ;
	my $read_1_len	=	$self->get_read_1_len();
	my $read_2_len	=	$self->get_read_2_len();
	
#	If run is SE
	if ( ($read_1_len ) && $read_2_len eq 'NA' ){
		
#		print "Run is SE \n";
		my $Q20_len_read1=  $self->get_Q20_readlen_read_1();
		
#		print "Read 1 len is $read_1_len \t returning $read_1_len \n";
		return ( $Q20_len_read1);
	}

#	If run is PE
	elsif( ($read_1_len ne 'NA') && ($read_2_len ne 'NA' )  ) {
		
#		print "Run iss PE \n";
		
		my $Q20_len_read1=  $self->get_Q20_readlen_read_1();
		my $Q20_len_read2=  $self->get_Q20_readlen_read_2();
	
#		print "Read 1 $Q20_len_read1 \t Read 2 $Q20_len_read2 returning ", ($Q20_len_read1 >= $Q20_len_read2 ) ? $Q20_len_read2 : $Q20_len_read1;
		print "\n";
		
		return (  ($Q20_len_read1 >= $Q20_len_read2 ) ? $Q20_len_read2 : $Q20_len_read1 );
	}
	
	else {
		$self->logger()->warn("[$SUB] Read 1($read_1_len) and Read 2($read_2_len) are not equal. Will not calculate overall Q20\n");
		return 'NA';
	}
	
	
}


sub get_Q20_readlen_read_1 {
	my $SUB = 'get_Q20_readlen_read_1';

	my $self = shift @_ ;
	my $read_1_len	=	$self->get_read_1_len();
	my $read1_qreport_filename =   $self->get_read1_qreport_filename();
	my $qualified_rqc_read1_filename = $self->get_qualified_rqc_filename($read1_qreport_filename);
	my $Q20_len_read1=0;

	open(IN_READ1,"< $qualified_rqc_read1_filename") || die " Error[$SUB] Could not open the file $qualified_rqc_read1_filename "; 	
		for( my $cycle = 0 ; $cycle<= $read_1_len; $cycle++ ) {
			my @cycle_read_1 = split(' ',<IN_READ1>);
			## Skipping the header line which is the first line in the fastx toolkit generated 
			## file that I am reading
			next if ( $cycle == 0); 
			my $mean_Q_read1 = $cycle_read_1[5];
			if ($mean_Q_read1 >= 19.90 ){
				$Q20_len_read1 = $cycle;
			}
		}
		return $Q20_len_read1;
}


sub get_Q20_readlen_read_2 {
	my $SUB = 'get_Q20_readlen_read_2';
	
	my $self = shift @_ ;
	my $read_2_len	=	$self->get_read_2_len();
	my $read2_qreport_filename =   $self->get_read2_qreport_filename();
	my $qualified_rqc_read2_filename = $self->get_qualified_rqc_filename($read2_qreport_filename);
	my $Q20_len_read2=0;
	
	open(IN_READ2,"< $qualified_rqc_read2_filename") || die " Error[$SUB] Could not open the file $qualified_rqc_read2_filename "; 	
		for( my $cycle = 0 ; $cycle<= $read_2_len; $cycle++ ) {
			my @cycle_read_2 = split(' ',<IN_READ2>);
			## Skipping the header line which is the first line in the fastx toolkit generated 
			## file that I am reading
			next if ( $cycle == 0); 
			my $mean_Q_read2 = $cycle_read_2[5];
			if ($mean_Q_read2 >= 19.90 ){
				$Q20_len_read2 = $cycle;
			}
		}
		return $Q20_len_read2;
}






##returns > 0 if library name is actually a pooled library name
##updated: Oct 17 : now gets the info from ITS-DW
sub is_lib_a_parent_pooled_lib {
	my $SUB = 'is_lib_a_parent_pooled_lib';
	my $self = shift @_;
	my $library_name = shift @_ ;
	
	my $count;
	
	if (! $library_name){
		$library_name = $self->library_name();
	}
	print "Reached SUB $SUB : library name $library_name \n" if $self->verbose();
	my $query = qq ( select count(*) from all_inclusive_report where pool_name = '$library_name' );
	
	eval {
		$count = $self->ITS_DW_handle->selectrow_array($query);
	};
	if ($@) {
		warn $@,"\t",$!;	
	}
	else {
		return $count;		
	}
}

##created Oct 17: 2012
#given a library is parent pooled library, get all the individual seq uunits from the pool
sub get_individual_seq_units_name_from_parent_library_name {
	my $SUB = 'get_individual_seq_units_name_from_parent_library_name';

	my $self = shift @_;
	my $library_name = shift @_ ;
	if (! $library_name){
		$library_name = $self->library_name();
	}
	
	my $query_results ;
	print "Reached SUB $SUB : library name $library_name \n" if $self->verbose();
	my $query = qq ( select seq_unit_name,library_name,pool_name from all_inclusive_report where pool_name = '$library_name' );

	eval {
		$query_results = $self->ITS_DW_handle->selectall_hashref($query,'SEQ_UNIT_NAME');
	};
	if ($@) {
		warn $@,"\t",$!;	
	}
	else {
		#just return the seq_units which are keys
		return keys %{$query_results};
	}
}



###returns TRUE (> 0) if the library is part of a pool
sub is_library_part_of_pool {
	my $SUB = 'is_library_part_of_pool';
	my $self = shift @_;
	my $library_name = shift @_;
	
	#if the user has not supplied with the library name
	if (! $library_name){
		$library_name = $self->library_name();
	}

	print "Reached SUB $SUB : library name $library_name \n"  if $self->verbose();
	my $num_individual_lib;
	
	my $query = qq (
					select count(*) from multiplex_individual_lib where individual_lib_name="$library_name"
					);
	eval {
		$num_individual_lib = $self->database_handle->selectrow_array($query);
	};
	if ($@) {
		warn $@,"\t",$!;	
	}
	else {
		return $num_individual_lib;		
	}
}



##given a pooled library either individual or pooled returns the pooled library name, bioclass and pmo_project_id
sub get_pooled_lib_info {
	my $SUB = 'get_pooled_lib_info';
	my $self = shift @_;
	my $library_name = $self->library_name();
	my $seq_unit_name = $self->seq_unit_name();
	my $results;
	
#	my $query = qq (
#					select distinct pooled_lib_name from multiplex_individual_lib where pooled_lib_name = "$library_name" OR individual_lib_name="$library_name"
#					);

#changed on May 14 
#	my $query = qq (
#						select library_name, bio_classification_name,pmo_project_id from library_info 
#								where library_name = ( 
#														select distinct pooled_lib_name from multiplex_individual_lib, library_info 
#														where pooled_lib_name = "$library_name" 
#														OR 
#														individual_lib_name ="$library_name")
#					);
	##currently no easy way to get the pooled library name
	## given individual library seq unit name
	
	##so the workaround is to build the pooled library seq_unit_name from individual library seq unit name
	##by taking out the index sequence

	my $pooled_library_seq_unit_name = $self->seq_unit_name();
	if($pooled_library_seq_unit_name=~/[AGCT]{1,}/){
		$pooled_library_seq_unit_name =~ s/([AGCT]{1,})(.srf)//;
		$pooled_library_seq_unit_name .= 'srf';	
	}
	
	my $query = qq (  select library_name,ncbi_organism_name,pmo_project_id from library_info 
								where library_id = (select rqc_library_id from seq_units where seq_unit_name ="$pooled_library_seq_unit_name")
					);
	eval {
		$results = $self->database_handle->selectrow_arrayref($query);
	};
	if ($@) {
		warn $@,"\t",$!;
		print "Query \n $query \n\n";	
		print "Seq unit name \t $seq_unit_name \n\n";
	}
	else {
			return @{$results};
	}
}









sub get_individual_library_info_from_pooled_lib {
	my $SUB = 'get_individual_library_info_from_pooled_lib';
	my $self = shift @_;
	my $library_name = $self->library_name();
	my $result;
	my $query = qq (
					select mlib.pooled_lib_name, mlib.individual_lib_name, linfo.library_id, seq.seq_unit_name 
					from 
					multiplex_individual_lib mlib, 
					seq_units seq,
					library_info linfo 
					where pooled_lib_name="$library_name"
					and 
					linfo.library_name = mlib.individual_lib_name 
					and 
					seq.rqc_library_id = linfo.library_id
				 );
	
##### sample output of the above query for input library name = HYWN
#'HYWN', 'HYTZ', '25160', '1629.5.1482.CCGATGTAGT.srf'
#'HYWN', 'HYTY', '25162', '1629.5.1482.CCATGTGACA.srf'
#'HYWN', 'HYTX', '25163', '1629.5.1482.TAACAGTCGC.srf'
#'HYWN', 'HYTW', '25165', '1629.5.1482.TAAAGGCGGT.srf'
#'HYWN', 'HYUA', '25161', '1629.5.1482.GTGAATGCCA.srf'
#'HYWN', 'HYUB', '25166', '1629.5.1482.AACTACGGTC.srf'
#'HYWN', 'HYUC', '25167', '1629.5.1482.TTAGCAGTGC.srf'
#'HYWN', 'HYUF', '25164', '1629.5.1482.GTGATGCATC.srf'
	
	eval {
		$result = $self->database_handle->selectall_hashref($query,'seq_unit_name');
	};
	if ($@) {
		warn $@;	
	}
	else {
		return $result;
	}
	
#### Exepected output of above database query

#	 '1629.5.1482.TAACAGTCGC.srf' => {
#                                            'seq_unit_name' => '1629.5.1482.TAACAGTCGC.srf',
#                                            'library_id' => '25163',
#                                            'pooled_lib_name' => 'HYWN',
#                                            'individual_lib_name' => 'HYTX'
#                                          },
#          '1629.5.1482.CCATGTGACA.srf' => {
#                                            'seq_unit_name' => '1629.5.1482.CCATGTGACA.srf',
#                                            'library_id' => '25162',
#                                            'pooled_lib_name' => 'HYWN',
#                                            'individual_lib_name' => 'HYTY'
#                                          },
#          '1629.5.1482.AACTACGGTC.srf' => {
#                                            'seq_unit_name' => '1629.5.1482.AACTACGGTC.srf',
#                                            'library_id' => '25166',
#                                            'pooled_lib_name' => 'HYWN',
#                                            'individual_lib_name' => 'HYUB'
#                                          },
#          '1629.5.1482.CCGATGTAGT.srf' => {
#                                            'seq_unit_name' => '1629.5.1482.CCGATGTAGT.srf',
#                                            'library_id' => '25160',
#                                            'pooled_lib_name' => 'HYWN',
#                                            'individual_lib_name' => 'HYTZ'
#                                          }	
}


sub get_singleplex_library_detail {
	my $SUB = 'get_singleplex_library_detail';
	my $self = shift @_ ;
	my $library_name = $self->library_name();
	my $result;

#	The query will also return the fastqs which have been two times processed by RQC. This is because the query is written in a way
#   to return multiple fastq's for individual library which are part of the pool (eg : CISB)
#   I could not think of a sql query which could work for both libraries such as CIOY ( multiple analysis) and CISB ( multiple fastq for a library due to pooling across different lanes )
	my $query = qq (
						select  linfo.library_id, seq.seq_unit_name, linfo.library_name 
						from  
						seq_units seq,
						library_info linfo 
						where linfo.library_name = "$library_name"
						and 
						linfo.library_id = seq.rqc_library_id
				  );

### sample output from the above query for library HYAF
#library_id        	seq_unit_name		library_name
#24965				1583.7.1455.srf		HYAF

	eval {
		$result = $self->database_handle->selectall_hashref($query,'seq_unit_name');
	};
	if ($@) {
		warn $@;	
	}
	else {
		return $result;
	}

}


## For a pooled library this will return the seq_unit_name for main library
sub get_multiplex_lib_seq_unit_name {
	my $SUB = 'get_multiplex_lib_seq_unit_name';
	my $self = shift @_ ;
	my $library_name = $self->library_name();
	my $result;
	my $query = qq (
						select seq.seq_unit_name from 
						library_info linfo, 
						seq_units seq
						where library_name = "$library_name" and
						seq.rqc_library_id = linfo.library_id
						and seq.is_multiplex = 1
				  );

### sample output from the above query for library HYWN
#1629.5.1482.srf

	eval {
		$result = $self->database_handle->selectall_hashref($query,'seq_unit_name');
	};
	if ($@) {
		warn $@;	
	}
	else {
		#exactly one seq_unit_name is returned
		if ( keys %{$result }  == 1 ){
			## converting an array to scalar value 
			my @seq_unit_names = keys %{$result};
			return ( $seq_unit_names[0]);
		}
		##more than one seq_unit_name is returned
		elsif( keys %{$result} >= 1) {
			print "Following seq unints were present for the library \n";
			print "Please type the index number of the seq unit for report generation \n";
			## converting an array to scalar value 
			my @seq_unit_names = keys %{$result};
			print "index_num \t seq_unit_name \n";
			my $count = 0;
			foreach my $seq_unit_name (@seq_unit_names) {
				$count++;
				print "$count. \t $seq_unit_name\n";
			}
			  print "Please enter the index number ";
			  my $index_num = <>;
			  if ($index_num <= $count && $index_num>= 1) {
			  	print "Selected $seq_unit_names[$index_num-1] seq_unit_name for QC report \n\n";
			  }
			return ( $seq_unit_names[$index_num-1] );
		} 
		##no seq_unit_name is returned
		else {
			print "ERROR[$SUB] : No seq_units for for lib $library_name \n Will print the hash dump and exit";
			print Dumper($result);
			exit 3;
		}
	}
}



## For a individual library in a pool this will return the multiplex info : index_type, index_name etc
sub get_multiplex_info_for_given_seq_unit_name {
	my $SUB = 'get_multiplex_info_for_given_seq_unit_name';
	my $self = shift @_ ;
	my $library_name = $self->seq_unit_name();
	my $result;
	my $query = qq (
						select seq.seq_unit_name from 
						library_info linfo, 
						seq_units seq
						where library_name = "$library_name" and
						seq.rqc_library_id = linfo.library_id
						and seq.is_multiplex = 1
				  );
				  
### sample output from the above query for library HYWN
#1629.5.1482.srf

	eval {
		$result = $self->database_handle->selectall_hashref($query,'seq_unit_name');
	};
	if ($@) {
		warn $@;	
	}
	else {
		
		#exactly one seq_unit_name is returned
		if ( keys %{$result }  == 1 ){
			## converting an array to scalar value 
			my @seq_unit_names = keys %{$result};
			return ( $seq_unit_names[0]);
		}
		
		##more than one seq_unit_name is returned
		elsif( keys %{$result} >= 1) {
			
			print "Following seq unints were present for the library \n";
			print "Please type the index number of the seq unit for report generation \n";
			
			## converting an array to scalar value 
			my @seq_unit_names = keys %{$result};
			
			print "index_num \t seq_unit_name \n";
			my $count = 0;
			foreach my $seq_unit_name (@seq_unit_names) {
				$count++;
				print "$count. \t $seq_unit_name\n";
			}
			  print "Please enter the index number ";
			  my $index_num = <>;
			  
			  if ($index_num <= $count && $index_num>= 1) {
			  	print "Selected $seq_unit_names[$index_num-1] seq_unit_name for QC report \n\n";
			  }
		
			return ( $seq_unit_names[$index_num-1] );
			
		} 
		
		##no seq_unit_name is returned
		else {
			
			print "ERROR[$SUB] : No seq_units for for lib $library_name \n Will print the hash dump and exit";
			print Dumper($result);
			exit 3;
		}
	}
}


### given a ncbi_organism_name returns all the library names associated with it 
sub get_library_names_from_bio_classification_name {
	my $SUB = 'get_library_names_from_bio_classification_name';
	my $self = shift @_;
	my $bio_classification_name = shift @_;
	my $result;
	my $query = qq (
						select library_name from library_info 
						where ncbi_organism_name = "$bio_classification_name"
				  );
	my $sth = $self->database_handle->prepare($query);
	eval {
		$sth->execute();
	};
	if ($@) {
		warn $@;	
	}
	else {
		my @library_names;
		while( my $lib = $sth->fetchrow_array() ){
			push(@library_names,$lib);
		}
		return @library_names;
	}
}







####################
##set_final_seq_unit_set
## accepts : array of library names and sequnit names defined by user
## outputs the total number of seq units present in that library
#######################
sub get_final_seq_units_set {
	my $SUB = 'get_final_seq_units_set';
	
	my $self = shift @_;
	my @library_names = @{ shift @_ };
	my @seq_units_by_user = @{ shift @_ }; 
	my $count = 0;
	my @SEQ_UNITS;
	
	my @BAD_LIBS;
	
	#	Get the seq_units for a library and ask user which ones to QC
	if (scalar @library_names > 0 ){
			foreach my $libs (@library_names ){
				#if the lib names are comma separated
				foreach my $library_name (split(',',$libs)){
					next if $library_name=~/^$/;  #skip the blank ones 
					$self->library_name($library_name);
					if( $self->is_lib_a_parent_pooled_lib() ) {
						$self->logger->info("Library $library_name is Multiplexed \n");
						#get the seq_unit_name of the parent library
						my ($seq_units_from_parent_lib_name, $rqc_status) = $self->get_seq_units_name_from_library_name();
						@SEQ_UNITS = (@SEQ_UNITS, @{$seq_units_from_parent_lib_name});
						
						#get all the seq_units_for the individual libs in the pool
						my (@seq_units_from_parent_lib_name) = $self->get_individual_seq_units_name_from_parent_library_name();
						@SEQ_UNITS = (@SEQ_UNITS, @seq_units_from_parent_lib_name);
					}
					##SINGLEPLEXED-LIBRARY
					else { 
						my ($seq_units_from_lib_name, $rqc_status) = $self->get_seq_units_name_from_library_name();
						if (! $seq_units_from_lib_name ){
							push(@BAD_LIBS,$library_name);
						}
						else{
							@SEQ_UNITS = (@SEQ_UNITS,  @{$seq_units_from_lib_name} );	
						}
						
					}
				}	
			}
	}
# 	Get the seq_units entered by user 
	if ( scalar @seq_units_by_user > 0 ) {
		foreach my $seq_unit (@seq_units_by_user){
			if($seq_unit =~/\*/){
				my $seq_unit_pattern = $seq_unit;
				$seq_unit_pattern =~s/\*/\%/g;
				print "pattern is $seq_unit_pattern \n";
				###get seq unit names from RQC database with the prefix mentioned by the user
				my @seq_units_names_based_prefix = @{ $self->get_seq_units_name_from_user_entered_seq_unit_pefix($seq_unit_pattern) };
				@SEQ_UNITS = (@SEQ_UNITS, @seq_units_names_based_prefix);
			}
			else {
				push (@SEQ_UNITS,$seq_unit);
			}	
		}
	}	
	if (scalar @BAD_LIBS > 0){
		open (OUT, '>BAD_LIBS.txt') || die 'Error[$SUB] : cant open a file for writing bad library names \n'; 
		print "$SUB: following libs were not found in the database\n\n";
		foreach my $each_bad_lib (@BAD_LIBS){
			print "$each_bad_lib \n";
			print OUT "$each_bad_lib \n";
		}
		close(OUT);
	}
	return (\@SEQ_UNITS);
}


################
##END of Module
################

no Moose;
1;







####
	#OLD CODE
	####
					
#					##get the lane prefix from pooled library seq unit name and extract all the individual libraries associated with it
#					foreach my $pooled_library_seq_unit_name ( @{$seq_units_from_lib_name}){
#
#						# if seq unit has AGAGCTAG then it is already part of the pool and doesnt need further look up
#						if ($pooled_library_seq_unit_name =~/[ACGT]{4,}/){ 
#							next ;
#						}	
#						$pooled_library_seq_unit_name =~/^(\d{4}\.\d\.\d{4}).+?$/;
#						my $pooled_library_prefix = $1.'%';
#						my @seq_units_names_based_prefix = @{ $self->get_seq_units_name_from_user_entered_seq_unit_pefix($pooled_library_prefix) };
#							
#					}



#
#	
#sub get_index_info_by_lib_and_index_name{
#	
#	
#	my $SUB = 'get_index_info_by_lib_and_index_name';
#	
#	my $obj = shift @_;
#	my $lib_name = shift @_;
#	my $index_sequence = shift @_;
#	
#	my $result;
#	
#	my $query = qq (
#					select * from multiplex_individual_lib where individual_lib_name="$lib_name" and index_sequence="$index_sequence"
#					);
#	
###expected output	
###### sample output of the above query for input library name = CNTO and index = CAGATC
##'2801', 'CNTO', 'IT007', 'CAGATC', 'CNTO', 'Std PE', 'Zea mays', 'MC', '2011-10-20 17:35:39', 'Maize plate well A7'
#	
#	eval {
#		$result = $obj->database_handle->selectall_arrayref($query);
#	};
#	if ($@) {
#		warn $@;	
#	}
#	else {
#		#we expect only one row to be returned for each index and library name combination
#		if(scalar @{$result} == 1){
#			my @query_result = @{ ${$result}[0]  };   # this is a bit complicated : extracting the first array from an array of array references
#			my $index_name = $query_result[2] || 'NA';
#			my $notes = $query_result[9] || 'NA';
#			$notes =~s/\s/_/g;
#			return($index_name,$notes);
#		}
#		else {
#			print "[$SUB] unexpected result from the query : \n $query";
#			print Dumper($result);
#			return('NA','NA');
#		}
#	}
#}
	


	####
	#END OLD CODE
	####