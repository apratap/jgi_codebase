package JGI::QAQC::QC;


use strict;
use FindBin;
use lib "$FindBin::Bin/../../../lib";
use JGI::QAQC::Utilities;
use Log::Log4perl qw(:easy);
use DBI;
use Getopt::Long;
use File::Temp qw/ tempfile tempdir/;
use File::Path qw(mkpath rmtree);
use MIME::Lite;
use Statistics::Descriptive::Discrete;
use Data::Dumper;
use Moose;



###moose variables

has 'VERBOSE' => (
	is  	=> 'rw',
	isa 	=>  'Str',
);


has 'SEQ_UNITS_REF' => (
	is  	=> 'rw',
	isa 	=>  'ArrayRef',
);


has 'DATA' => (
	is  	=> 'rw',
	isa 	=>  'HashRef',
);


has 'USER_DEF_PATH' => (
	is  	=> 'rw',
	isa 	=>  'Str',
);

has 'DEF_FASTQ_PATH' =>(
	is  	=> 'rw',
	isa 	=>  'Str',
	default => '/house/sdm/prod/illumina/seq_data/fastq/',
);

has 'DEF_FASTQ_CLIPPE_PATH' =>(
	is  	=> 'rw',
	isa 	=>  'Str',
	default => '/house/groupdirs/QAQC/rqc_archive/clip_pe/rqc/',
);


has 'META_TRANSCRIPTOME_PROJECT' =>(
	is  	=> 'rw',
	isa 	=>  'Str',
	default => 0,
);





###methods

sub create_data_holder_hash {
	my $SUB = 'create_data_holder_hash';
	
	
	my $self = shift @_;
	my $rqc_obj = shift @_;
	
	
	my $count_seq_unit = 0;
	my $SEQ_UNITS_REF = $self->SEQ_UNITS_REF();
	

		foreach my $individual_seq_unit_name ( @{$SEQ_UNITS_REF}){
					$count_seq_unit++;
					next if ($individual_seq_unit_name=~/UNKNOWN/);
					
					# Setting the right seq_unit for down_stream called to RQCdbAPI
					$rqc_obj->seq_unit_name($individual_seq_unit_name);
				
					###Now fetching the results for each seq_unit_name from RQC db
					$rqc_obj->get_lane_level_info();
					$rqc_obj->get_library_info_from_seq_unit_name();
					
					## also get the library detail from venonat
					$rqc_obj->get_library_info_from_venonat();
					
					##now fecthing the seq_unit info from SDM web service
					$rqc_obj->get_seq_unit_info_SDM_webservice();
					
					
					##get the seq_unit_info from ITS DW
					$rqc_obj->get_seq_unit_info_from_ITS_DW();
			
			
					
					###now exclusively setting up the library name parameter
					my $library_name 	= $rqc_obj->get_library_name() 						|| $rqc_obj->logger()->info("$SUB: Libray Name  can't be determined for $individual_seq_unit_name");
					$rqc_obj->library_name($library_name);
										
					my $dna_type 				= $rqc_obj->get_source_dna_type() 						|| $rqc_obj->logger()->info("$SUB: DNA type can't be determined for $individual_seq_unit_name");
					my $project_type 			= $rqc_obj->get_project_type() 						|| $rqc_obj->logger()->info("$SUB: Project type can't be determined for $individual_seq_unit_name");
					my $library_type 			= $self->determine_library_type_short_form($rqc_obj);
					my $pmo_project_id 			= $rqc_obj->get_pmo_project_id() 					|| $rqc_obj->logger()->info("$SUB: PMO Project ID can't be determined for $individual_seq_unit_name");
					my $lib_project_id 			= $rqc_obj->get_lib_project_id() 				|| $rqc_obj->logger()->info("$SUB: Library Project ID can't be determined for $individual_seq_unit_name");
					my $qualified_seq_unit_name = $rqc_obj->get_qualified_seq_unit_name() 	|| $rqc_obj->logger()->info("$SUB: Qualfiied seq_unit_name name can't be determined for $individual_seq_unit_name");
					my $bioclass_name 			= $rqc_obj->get_bio_classification_name()			|| $rqc_obj->logger()->info("$SUB: Bioclassifcation name can't be determined for $individual_seq_unit_name");
					
					#substituting all the spaces with underscrores as the names will
					##used to create dirs
					$bioclass_name =~s/[\s\/]/_/g;
					
					#removing the ; from the bioclass name
					$bioclass_name =~s/[\;,=\)\(\%]//g;

					
					# getting info from the ITS query
					my $material_type = $rqc_obj->seq_unit_info_from_ITS_DW()->{$individual_seq_unit_name}->{'MATERIAL_TYPE'} || 'NA';
					
					
					print "\t Project : $project_type \t DNA_type $dna_type \n" if $self->VERBOSE();
					
					
#					###Plant Program
#					if( ($dna_type !~/RNA/) && ($taxa_kingdom =~ /Plant/) ){
#						print "Fetching details for Plant Program seq : $count_seq_unit : $individual_seq_unit_name from RQC db to create the data hash \n" ;
#						$self->create_Plant_project_hash($rqc_obj,'Plant',$pmo_project_id,$bioclass_name,$qualified_seq_unit_name,$library_name,$library_type)
#					}
					###RNA Program
					if( ($dna_type =~/RNA|EST/)  || ($material_type=~/RNA/) ){
						print "Fetching details for RNA Program seq : $count_seq_unit : $individual_seq_unit_name from RQC db to create the data hash \n" ;
						$self->create_RNA_project_hash($rqc_obj,'RNA',$pmo_project_id,$bioclass_name,$qualified_seq_unit_name,$library_name,$library_type)
					}
					##METAGENOME PROGRAM
					elsif($project_type =~/Metagenome/){
						print "Fetching details for Metagenome Program seq : $count_seq_unit : $individual_seq_unit_name from RQC db to create the data hash \n" ;
						$self->create_MG_project_hash($rqc_obj,'metagenome',$pmo_project_id,$bioclass_name,$qualified_seq_unit_name,$library_name,$library_type)
					}
					
					##MICROBIAL PROGRAM
#					elsif ( ($dna_type !~/RNA/) && ($project_type =~/Whole Genome|Prokaryotic|Eukaryotic/) ){
#						print "Fetching details for Microbial Program seq : $count_seq_unit : $individual_seq_unit_name from RQC db to create the data hash \n" ;
#						$self->create_Microbe_project_hash($rqc_obj,'whole_genome',$lib_project_id,$bioclass_name,$qualified_seq_unit_name,$library_name,$library_type);
#					}
					else {
						print "[WARN]: Can't determine project\dna type : $project_type \t $dna_type \t $individual_seq_unit_name \t $library_name \t $library_type \n";
						print "Defaulting to MG project type \n\n";
						$self->create_MG_project_hash($rqc_obj,'metagenome',$pmo_project_id,$bioclass_name,$qualified_seq_unit_name,$library_name,$library_type);
					}
			}
}




sub determine_library_type_short_form(){
	my $SUB = 'determine_library_type_short_form';

	my $self = shift @_;
	my $rqcdb_obj = shift @_;
	my $library_type_long = $rqcdb_obj->get_library_type() || $rqcdb_obj->logger()->info("$SUB: Libray type  can't be determined");
	
	if ($library_type_long=~/Illumina Std/){
		return 'standard';
	}
	if ($library_type_long=~/CLIP/){
		return 'clip_pe';
	}	
}



sub create_Microbe_project_hash() {
	my $SUB = 'create_Microbe_project_hash';
	
	my $MICROBES_BASEPATH = '/house/groupdirs/pi/project/';
	
	my $self					= shift @_;
	
	my $rqcdb_obj 				= shift @_;
	my $program					= shift @_;
	my $lib_project_id 			= shift @_;
	my $bioclass_name 			= shift @_;
	my $qualified_seq_unit_name = shift @_;
	my $library_name 			= shift @_;
	my $library_type			= shift @_;
	
	my $USER_DEF_BASE_PATH				 = $self->USER_DEF_PATH();
	my $DATA							 = $self->DATA();
	my $VERBOSE							 = $self->VERBOSE();
	my $base_fastq_data_location 		 = $rqcdb_obj->get_sdm_seq_unit_path();
	my $base_clip_pe_fastq_data_location = $self->DEF_FASTQ_CLIPPE_PATH();
	
	
	my $BASEPATH;
	if ( $USER_DEF_BASE_PATH ) { 
		$BASEPATH = $USER_DEF_BASE_PATH 
	}
	else {
		$BASEPATH = $MICROBES_BASEPATH;
	}

	my $project_path = $BASEPATH.'/'.$lib_project_id.'/';
	
	my $target_dir_path = $BASEPATH.'/'.$lib_project_id.'/ill_dir/'."$library_type".'_'."$library_name".'/';
	my $target_seq_unit_name = "$library_name".'.'."$qualified_seq_unit_name";
	
	$DATA->{$library_name}->{'project_path'} 		= $project_path;
	$DATA->{$library_name}->{'base_location'} 		= $base_fastq_data_location;
	$DATA->{$library_name}->{'target_location'} 	= $target_dir_path;
	$DATA->{$library_name}->{'bio_class_name'} 		= $bioclass_name;
	$DATA->{$library_name}->{'library_type'} 		= $library_type;
	$DATA->{$library_name}->{'program'} 			= $program;
	$DATA->{$library_name}->{'orig_fastq'}->{$qualified_seq_unit_name}->{'target_fastq'} 		= $target_seq_unit_name;
	


	###also symlink the CATG removed data to the clip_pe data
	if ($library_type eq 'clip_pe'){
		my $seq_unit_prefix = $qualified_seq_unit_name;
		$seq_unit_prefix =~s/\.fastq.gz//;
		my $CATG_removed_file_name = $seq_unit_prefix.'.jCATG.fastq.gz';
		my $target_seq_unit_name = "$library_name".'.'.$CATG_removed_file_name;
		my $target_dir_path = 	$DATA->{$library_name}->{'orig_fastq'}->{$qualified_seq_unit_name}->{'target_location'};
		
		$DATA->{$library_name}->{'orig_fastq'}->{$qualified_seq_unit_name}->{'target_fastq_jCATG'} = $target_seq_unit_name;
		$DATA->{$library_name}->{'orig_fastq'}->{$qualified_seq_unit_name}->{'base_fastq_jCATG'} = $CATG_removed_file_name;
		$DATA->{$library_name}->{'base_clippe_location'} = $base_clip_pe_fastq_data_location;
	}
	
	##checking on assembly for standard library
	if ($library_type eq 'standard'){
	my $max_contifg_size_from_velvet_assembly = $rqcdb_obj->get_max_contig_size();

	
	}
	
	
	$self->DATA($DATA);
	
	
}


sub create_Plant_project_hash(){
	my $SUB = 'create_Plant_project_hash';
	
	my $MG_BASEPATH = '/house/groupdirs/QAQC/seq_qc/plants/';

	my $self 						= shift @_;

	my $rqcdb_obj 					= shift @_;
	my $program						= shift @_;
	my $pmo_project_id 				= shift @_;
	my $bioclass_name 				= shift @_;
	my $qualified_seq_unit_name 	= shift @_;
	my $library_name 				= shift @_;
	my $library_type				= shift @_;
	
	my $USER_DEF_BASE_PATH		= $self->USER_DEF_PATH();
	my $DATA					= $self->DATA();
	my $VERBOSE					= $self->VERBOSE();
	my $base_fastq_data_location = $rqcdb_obj->get_sdm_seq_unit_path();
	
	my $BASEPATH;	
	if ( $USER_DEF_BASE_PATH ) { 
		$BASEPATH = $USER_DEF_BASE_PATH 
	}
	else {
		$BASEPATH = $MG_BASEPATH;
	}
	
	
	my $target_dir_path;
	my $project_path;
	my $target_seq_unit_name = "$library_name".'.'."$qualified_seq_unit_name";
	
	
	###if library is a parent pooled library
	if (my $val= $rqcdb_obj->is_lib_a_parent_pooled_lib()){
		$target_dir_path = $BASEPATH.'/'.$bioclass_name.'_'.$pmo_project_id.'/'.$library_name.'_Mplexed_library/';
		$project_path = $BASEPATH.'/'.$bioclass_name.'_'.$pmo_project_id.'/';
	
	}
	###if the library part of a pooled
	elsif ( $rqcdb_obj->is_library_part_of_pool() ){ 
		my ($pooled_lib_name,$pooled_lib_bioclass_name,$pooled_lib_pmo_project_id) = $rqcdb_obj->get_pooled_lib_info();
		$pooled_lib_bioclass_name =~s/\s/_/g;
		
		$project_path = $BASEPATH.'/'.$pooled_lib_bioclass_name.'_'.$pooled_lib_pmo_project_id.'/';
		$target_dir_path = $BASEPATH.'/'.$pooled_lib_bioclass_name.'_'.$pooled_lib_pmo_project_id.'/'.$pooled_lib_name.'_Mplexed_library/'.$library_name.'/';
		print "$pooled_lib_name \t $pooled_lib_bioclass_name \t $pooled_lib_pmo_project_id \t $library_name \n $target_dir_path \n\n" if $VERBOSE;
	}
	### else the library is not pooled
	else {
		$target_dir_path 	= $BASEPATH.'/'.$bioclass_name.'_'.$pmo_project_id.'/'.$library_name.'/';
		$project_path 		= $BASEPATH.'/'.$bioclass_name.'_'.$pmo_project_id.'/';
	}	
	
	
	$DATA->{$library_name}->{'project_path'} 		= $project_path;
	$DATA->{$library_name}->{'base_location'} 		= $base_fastq_data_location;
	$DATA->{$library_name}->{'target_location'} 	= $target_dir_path;
	$DATA->{$library_name}->{'bio_class_name'} 		= $bioclass_name;
	$DATA->{$library_name}->{'library_type'} 		= $library_type;
	$DATA->{$library_name}->{'program'} 			= $program;
	$DATA->{$library_name}->{'orig_fastq'}->{$qualified_seq_unit_name}->{'target_fastq'} 		= $target_seq_unit_name;
	
	
	$self->DATA($DATA);
	
	print ($base_fastq_data_location, "\n" , $target_dir_path ,"\n", $qualified_seq_unit_name,"\n" ,$target_seq_unit_name) if $VERBOSE;
}

sub create_MG_project_hash(){
	my $SUB = 'create_MG_project_hash';
	
	my $MG_BASEPATH = '/house/groupdirs/QAQC/seq_qc/metagenomes/';

	my $self 						= shift @_;

	my $rqcdb_obj 					= shift @_;
	my $program						= shift @_;
	my $pmo_project_id 				= shift @_;
	my $bioclass_name 				= shift @_;
	my $qualified_seq_unit_name 	= shift @_;
	my $library_name 				= shift @_;
	my $library_type				= shift @_;
	
	my $USER_DEF_BASE_PATH		= $self->USER_DEF_PATH();
	my $DATA					= $self->DATA();
	my $VERBOSE					= $self->VERBOSE();
	my $base_fastq_data_location = $rqcdb_obj->get_sdm_seq_unit_path();
	
	my $BASEPATH;	
	if ( $USER_DEF_BASE_PATH ) { 
		$BASEPATH = $USER_DEF_BASE_PATH 
	}
	else {
		$BASEPATH = $MG_BASEPATH;
		
	}
	
	
	my $target_dir_path;
	my $project_path;
	my $target_seq_unit_name = "$library_name".'.'."$qualified_seq_unit_name";
	
	$target_dir_path 	= $BASEPATH.'/'.$bioclass_name.'_'.$pmo_project_id.'/'.$library_name.'/';
	$project_path 		= $BASEPATH.'/'.$bioclass_name.'_'.$pmo_project_id.'/';
	
	
## May 15, 2012
## not creating the multiplexed library folder ( test implemenation)
##

#	###if library is a parent pooled library
#	if (my $val= $rqcdb_obj->is_lib_a_parent_pooled_lib()){
#		$target_dir_path = $BASEPATH.'/'.$bioclass_name.'_'.$pmo_project_id.'/'.$library_name.'_Mplexed_library/';
#		$project_path = $BASEPATH.'/'.$bioclass_name.'_'.$pmo_project_id.'/';
#	}
#	###if the library part of a pooled
#	elsif ( $rqcdb_obj->is_library_part_of_pool() ){ 
#		my ($pooled_lib_name,$pooled_lib_bioclass_name,$pooled_lib_pmo_project_id) = $rqcdb_obj->get_pooled_lib_info();
#		$pooled_lib_bioclass_name =~s/\s/_/g;
#		
#		$project_path = $BASEPATH.'/'.$pooled_lib_bioclass_name.'_'.$pooled_lib_pmo_project_id.'/';
#		$target_dir_path = $BASEPATH.'/'.$pooled_lib_bioclass_name.'_'.$pooled_lib_pmo_project_id.'/'.$pooled_lib_name.'_Mplexed_library/'.$library_name.'/';
#		print "$pooled_lib_name \t $pooled_lib_bioclass_name \t $pooled_lib_pmo_project_id \t $library_name \n $target_dir_path \n\n" if $VERBOSE;
#	}
#	### else the library is not pooled
#	else {
	
#	}	
	
	$DATA->{$library_name}->{'project_path'} 		= $project_path;
	$DATA->{$library_name}->{'target_location'} 	= $target_dir_path;
	$DATA->{$library_name}->{'base_location'} 		= $base_fastq_data_location;
	$DATA->{$library_name}->{'bio_class_name'} 		= $bioclass_name;
	$DATA->{$library_name}->{'library_type'} 		= $library_type;
	$DATA->{$library_name}->{'program'} 			= $program;
	$DATA->{$library_name}->{'orig_fastq'}->{$qualified_seq_unit_name}->{'target_fastq'} 		= $target_seq_unit_name;
	
	$self->DATA($DATA);
	
	print ($base_fastq_data_location, "\n" , $target_dir_path ,"\n", $qualified_seq_unit_name,"\n" ,$target_seq_unit_name) if $VERBOSE;
}



sub create_RNA_project_hash(){
	my $SUB = 'create_RNA_project_hash';
	
	my $RNA_BASEPATH = '/house/groupdirs/genetic_analysis/rna/projects/';

	my $self 						= shift @_;

	my $rqcdb_obj 					= shift @_;
	my $program						= shift @_;
	my $pmo_project_id 				= shift @_;
	my $bioclass_name 				= shift @_;
	my $qualified_seq_unit_name 	= shift @_;
	my $library_name 				= shift @_;
	my $library_type				= shift @_;
	
	my $USER_DEF_BASE_PATH		= $self->USER_DEF_PATH();
	my $DATA					= $self->DATA();
	my $VERBOSE					= $self->VERBOSE();
	my $base_fastq_data_location = $rqcdb_obj->get_sdm_seq_unit_path();
	
	my $BASEPATH;	
	if ( $USER_DEF_BASE_PATH ) { 
		$BASEPATH = $USER_DEF_BASE_PATH 
	}
	else {
		$BASEPATH = $RNA_BASEPATH;
	}
	
	
	my $target_dir_path;
	my $project_path;
	my $target_seq_unit_name = "$library_name".'.'."$qualified_seq_unit_name";
	
	
	###if library is a parent pooled library
	if (my $val= $rqcdb_obj->is_lib_a_parent_pooled_lib()){
		$target_dir_path = $BASEPATH.'/'.$bioclass_name.'_'.$pmo_project_id.'/'.$library_name.'_Mplexed_library/';
		$project_path = $BASEPATH.'/'.$bioclass_name.'_'.$pmo_project_id.'/';
	
	}
	###if the library part of a pooled
	elsif ( $rqcdb_obj->is_library_part_of_pool() ){ 
		my ($pooled_lib_name,$pooled_lib_bioclass_name,$pooled_lib_pmo_project_id) = $rqcdb_obj->get_pooled_lib_info();
		$pooled_lib_bioclass_name =~s/\s/_/g;
		
		$project_path = $BASEPATH.'/'.$pooled_lib_bioclass_name.'_'.$pooled_lib_pmo_project_id.'/';
		$target_dir_path = $BASEPATH.'/'.$pooled_lib_bioclass_name.'_'.$pooled_lib_pmo_project_id.'/'.$pooled_lib_name.'_Mplexed_library/'.$library_name.'/';
		print "$pooled_lib_name \t $pooled_lib_bioclass_name \t $pooled_lib_pmo_project_id \t $library_name \n $target_dir_path \n\n" if $VERBOSE;
	}
	### else the library is not pooled
	else {
		$target_dir_path 	= $BASEPATH.'/'.$bioclass_name.'_'.$pmo_project_id.'/'.$library_name.'/';
		$project_path 		= $BASEPATH.'/'.$bioclass_name.'_'.$pmo_project_id.'/';
	}	
	
	
	###if meta_transcriptome project
	if ($self->META_TRANSCRIPTOME_PROJECT() ){
		my $mt_assembly_dir = $target_dir_path . '/meta_transcriptome_assembly/';
		$DATA->{$library_name}->{'mt_assembly_dir'}	= $mt_assembly_dir;
	}
	
	$DATA->{$library_name}->{'project_path'} 		= $project_path;
	$DATA->{$library_name}->{'base_location'} 		= $base_fastq_data_location;
	$DATA->{$library_name}->{'target_location'} 	= $target_dir_path;
	$DATA->{$library_name}->{'bio_class_name'} 		= $bioclass_name;
	$DATA->{$library_name}->{'library_type'} 		= $library_type;
	$DATA->{$library_name}->{'program'} 			= $program;
	$DATA->{$library_name}->{'orig_fastq'}->{$qualified_seq_unit_name}->{'target_fastq'} 		= $target_seq_unit_name;
	$self->DATA($DATA);
	
	print ($base_fastq_data_location, "\n" , $target_dir_path ,"\n", $qualified_seq_unit_name,"\n" ,$target_seq_unit_name) if $VERBOSE;
}







sub create_project_space {
		
		my $SUB = 'create_project_space';
		my $self		= shift @_;
		my $library_name	= shift @_;
		my $DATA 		= $self->DATA();
	
		foreach my $qualified_seq_unit_name ( keys %{$DATA->{$library_name}->{'orig_fastq'}} ) {
			
			my  $base_path 				= $DATA->{$library_name}->{'base_location'};
			my  $base_file_name			= $qualified_seq_unit_name;
			my  $target_path 			= $DATA->{$library_name}->{'target_location'}; 
			my  $target_file_name		= $DATA->{$library_name}->{'orig_fastq'}->{$qualified_seq_unit_name}->{'target_fastq'};
			
			$self->deliver_fastq_data($base_path,$base_file_name,$target_path,$target_file_name);
			
			##if clipe-pe library deliver CATG cleaned CLIP-pe data
			my 	$library_type				= $DATA->{$library_name}->{'library_type'};
			
			if ($library_type=~/clip/){
				print "CLIP-PE library : will try to deliver clip-pe fastq ...";
				my  $base_path 				= $DATA->{$library_name}->{'base_clippe_location'};
				my  $base_file_name			= $DATA->{$library_name}->{'orig_fastq'}->{$qualified_seq_unit_name}->{'base_fastq_jCATG'};
				my  $target_path 			= $DATA->{$library_name}->{'target_location'}; 
				my  $target_file_name		= $DATA->{$library_name}->{'orig_fastq'}->{$qualified_seq_unit_name}->{'target_fastq_jCATG'};
				if ($base_path && $base_file_name && $target_path && $target_file_name ){
					$self->deliver_fastq_data($base_path,$base_file_name,$target_path,$target_file_name);	
				}
				else{
					print "failed \n";
				}
				
			}
			
			#create the Meta transcriptome assembly space
			if ( defined $DATA->{$library_name}->{'mt_assembly_dir'}){
				my  $base_path 				= $DATA->{$library_name}->{'base_location'};
				my  $base_file_name			= $qualified_seq_unit_name;
				my  $target_path 			= $DATA->{$library_name}->{'mt_assembly_dir'}; 
				my  $target_file_name		= $DATA->{$library_name}->{'orig_fastq'}->{$qualified_seq_unit_name}->{'target_fastq'};
				$self->deliver_fastq_data($base_path,$base_file_name,$target_path,$target_file_name);
			}
		}
}


sub cd_to_lib_proj_space {
	my $SUB ='cd_to_lib_proj_space';

	my $self		= shift @_;
	my $library_name	= shift @_;
	my $DATA 		= $self->DATA();
	
	my $project_path = $DATA->{$library_name}->{'project_path'};
	chomp $project_path;
	
	if (! -e $project_path ){
		warn("Project Path $project_path is not existent \n");
		warn("You probably want to deliver data and create project space before looking for you :) \n\n");
		exit 2;
	}
	else{
		eval {
			chomp $project_path;
			print("cd $project_path");
			#`cd $project_path`;
			chdir($project_path);
		};
		if($@){ print "Error[$SUB] $@: $!\n\n";}
	}
}





sub  deliver_fastq_data {
		my $SUB = 'deliver_fastq_data';
		
		my $self		= shift @_;
		my $base_path = shift @_;
		my $base_file_name = shift @_;
		my $target_path 	= shift @_;
		my $target_file_name = shift @_;
		
		my $full_seq_unit_path = "$base_path".'/'."$base_file_name";
		if (! -e $full_seq_unit_path ) {
			warn("File path $full_seq_unit_path is INVALID \n");
			exit 2;
		}
		eval {
			mkpath($target_path, {mode=>0775});
			chmod 0775, "$target_path";
			symlink($full_seq_unit_path, $target_path.$target_file_name );
			print "Created: $target_path$target_file_name ...Success\n";
		};
		if($@) {
			print "Error[$SUB] $@ -> $! \n";
		}
}





sub gen_QC_report {
	my $SUB = 'gen_QC_report';
	
	my $base_QC_script = $FindBin::Bin.'/gen_MG_base_QC_report.pl';
	my $base_pooled_lib_QC_script = $FindBin::Bin.'/gen_pooled_lib_QC_report.pl';
	

		my $self					= shift @_;
		my $library_name			= shift @_;	
		my $rqcdb_obj				= shift @_;
		my $CWD 					= `pwd`;
		my $DATA 					= $self->DATA();
		my $target_path 			= $DATA->{$library_name}->{'target_location'}; 
		
		if( $rqcdb_obj->is_lib_a_parent_pooled_lib($library_name)){
			chdir $target_path;
			`$base_pooled_lib_QC_script -lib $library_name`;
			chdir $CWD;
		}
		else{
			chdir $target_path;
			`$base_QC_script -lib $library_name`;
			chdir $CWD;
		}
}





no Moose;

1;