#!/jgi/tools/bin/perl

use strict;
use FindBin;
use lib "$FindBin::Bin/lib";
use JGI::QAQC::RQCdbAPI;
use JGI::QAQC::Utilities;
use File::Path qw(mkpath rmtree);
use Log::Log4perl qw(:easy);
use Data::Dumper;
use Getopt::Long;


## global variables
our ($LOGGER, $CWD);
our ($library_name);
our ($bioclassication_summary_line_per_lane);
our ($read_quality_per_lane);
our ($GC_content_per_lane);
our ($bwa_dup_rate_per_lane);
our ($artifcat_per_lane);
our ($failed_lanes);
our ($read_counts_per_lane);
our ($lanes_analyzed_by_RQC);
our ($total_lanes_sequenced);
our (@read_counts_per_lane, @read_Q20 , @GC_content , @artifacts );





main();


sub main {
	
	### Print the standard RQC header 
	get_standard_usage_header();
	
	
	###setup the logger
	config_logger();
	$LOGGER = get_logger();
	
	
	## get the user arguments
	$library_name = get_user_arguments();
	
	$LOGGER->info("############Started###############");
	
	## Instantiate the Class RQCdbAPI
	my $obj = JGI::QAQC::RQCdbAPI->new();
	
	## set the run_id
	$obj->library_name($library_name);
	
#    set the verbosity level
	$obj->verbose(0);
		
	$CWD = get_cwd();
	
	my ($seq_unit_names_ref, $rqc_status_id_per_seq_unit_ref) = $obj->get_seq_units_name_from_library_name();
	
#	Stores the seq_unit_names for library organized by run_date
	my @seq_unit_names = @$seq_unit_names_ref;

#	Stores the corresponding rqc_status_id for each seq_unit_name
	my @rqc_status_id_per_seq_unit = @$rqc_status_id_per_seq_unit_ref; 
	
		
#	Get one time to use below
	my $rqc_status_message = $obj->get_rqc_status_message();
	
#	set Counter=0;
	$lanes_analyzed_by_RQC=0;
	
#	get the count of total seq_unit_name returned which will equal total lanes sequenced
	$total_lanes_sequenced= scalar @seq_unit_names;
	
#  open a file handle to store the tsv dat for downstream analysis
	my $out_file = 'README.'.$library_name.'.csv';
	open(OUT, "> $out_file") || die "File $out_file could not be opened \n";



	for ( my $i=0; $i <= $#seq_unit_names ; $i++) {
		

			my $seq_unit_name = $seq_unit_names[$i];
			my $rqc_status_id = $rqc_status_id_per_seq_unit[$i];
			
			
			print "\n\n";
			$LOGGER->info("Generating QC numbers for lane $seq_unit_name of library $library_name");
						
			
#			Skip a lane if RQC has not processed it
			unless ( $rqc_status_id == 11 ) {
				
				$LOGGER->info("Will skip processing \t $seq_unit_name");
				$LOGGER->info("Rqc_status_id = $rqc_status_id");
				$LOGGER->info("Interpretation : $rqc_status_message->{$rqc_status_id}->{'rqc_status_name'}\n\n");
				
				$failed_lanes .= "$seq_unit_name has not been proceesed by RQC : Rqc status -> $rqc_status_id : \t ".$rqc_status_message->{$rqc_status_id}->{'rqc_status_name'}."\n";
				next;
			}
		
			
			
#			adding one to num of lanes processed by RQC
			$lanes_analyzed_by_RQC++;
			
			## Setting the right seq_unit for down_stream called to RQCdbAPI
			$obj->seq_unit_name($seq_unit_name);
			
			
			###Now fetching the results for each library from RQC db
			$obj->get_lane_level_info();
			$obj->get_library_info_from_seq_unit_name();
			
		
			$read_counts_per_lane	.= "$seq_unit_name\t\t".$obj->get_library_name()."\t\t".commify($obj->get_total_read_count())."\n"; 
			push ( @read_counts_per_lane, $obj->get_total_read_count());			
			
					
			$bioclassication_summary_line_per_lane   .=  $obj->get_lane_yield().' Mbp'."\t".$obj->get_qualified_seq_unit_name()."\n";
			
			$read_quality_per_lane .=  "$seq_unit_name\t\t".$obj->get_library_name()."\t\t". $obj->get_Q30_percent()."\t\t".'Q20 rl = '.$obj->get_min_Q20_readlen_of_read_1_2()."\n";
			
			push(@read_Q20, $obj->get_min_Q20_readlen_of_read_1_2());
			
			
			$GC_content_per_lane   .= "$seq_unit_name\t\t".$obj->get_library_name()."\t\t".$obj->get_GC_content()."\n";
			
			push (@GC_content, $obj->get_GC_content());
			
			$bwa_dup_rate_per_lane .= "$seq_unit_name\t\t".$obj->get_library_name()."\t\t".$obj->get_bwa_algn_dup_percent().' % '."\n";
			
			
			$artifcat_per_lane  .= "$seq_unit_name\t\t".$obj->get_library_name()."\t\t".$obj->get_percent_illumina_artifacts().' % '."\n";
			push(@artifacts, $obj->get_percent_illumina_artifacts() );
			
			my ($gc_content_without_stdev, $stdev) = split(/\s*\+\/-\s*/, $obj->get_GC_content() );
			
			print OUT $seq_unit_name."\t".$obj->get_read_1_len."\t".$obj->get_total_read_count()."\t".$obj->get_min_Q20_readlen_of_read_1_2()."\t".$gc_content_without_stdev."\t".$obj->get_percent_illumina_artifacts()."\n";
				
	
		}

		
		
#		# Generate the QC report and email it , locally store it
		generateReport($obj);
		
		
#		plot the results
		generateplots();
	
		$LOGGER->info("############END###############");
	
		
}
	



sub get_user_arguments {
	
	# need to improve this
	my $SUB = 'get_user_arguments';
	
	if ( scalar @ARGV == 1 ) {
		return shift @ARGV;
	}
	
	else {
		usage();
		exit 3;
	}
	
}

sub usage {
	
	my $SUB  ='usage';

print<<USAGE;

$0  <<LIBRAY_NAME>>

generate the Main pooled library QC report

USAGE
	
}

sub generateReport {
	
	my $SUB = 'generateReport';
	
	my $obj = shift @_ || die " Class obj not passed \n\n";
	
	my $REPORT;
	
	
	$REPORT .= '1.) Library  : '."$library_name \n\n";
	
		
		
	$REPORT .= '2)  Read Counts '."\n\n";
	$REPORT .= $read_counts_per_lane."\n\n";
		

	$REPORT .= '2) Lanes sequenced :  '.$total_lanes_sequenced."\t\t".'|  Lanes Analyzed : '."\t $lanes_analyzed_by_RQC\n";
	$REPORT .= 'bioclassification,    library,    library type,    Mbp,    run configuration,       file name'."\n\n\n";
	$REPORT .= $bioclassication_summary_line_per_lane."\n\n\n";
	
	
	$REPORT .= '3) Read Level QC Metrics'."\n\n";
	$REPORT .= "\n";
	$REPORT .= '3.1) Read quality'; 
	$REPORT .= "\n";
	$REPORT .= '3.1.1) Quality score are :';
	$REPORT .= "\n\n";
	$REPORT .= 'Library :       Library Name          :   %reads_Q30    :     Q20_read_length min of read1/2'."\n";
	$REPORT .= $read_quality_per_lane."\n\n\n";
	
	
	
	

	$REPORT .= '3.2) Read GC Content'."\n\n";
	$REPORT .= 'Library'."\t\t".'FastQ'."\t\t".'GC_content'."\n";
	$REPORT .=  $GC_content_per_lane."\n\n\n";
	
	$REPORT .= '3.3) Uniqueness - 5% sampled 50bp read vs. read alignment using BWA'."\n";
	$REPORT .= $bwa_dup_rate_per_lane."\n\n";
	
	
	$REPORT .= '3.4) Percent Illumina artifacts :'."\n";
	$REPORT .= $artifcat_per_lane."\n\n";
	
	
	
	$REPORT .= 'Possible Failures : Following Lanes were not reported'."\n\n";
	$REPORT .=	$failed_lanes."\n\n";
	

	
	
	my $filename = 'README.'.$library_name;
	open(OUT,">$filename") || die "Error[$SUB] : Could not open the file $filename \n\n";
	
	print (OUT $REPORT);
	close(OUT);
	
	my $SUBJECT = 'Library '."$library_name".' Basic Report Template generated';
	
	send_text_email (guess_user_email(),guess_user_email(),$SUBJECT,$REPORT);
	
	
	$obj->logger()->info("\n\n============================================================");
	$obj->logger()->info("QC Base Template email sent to user email :", guess_user_email() );
	$obj->logger()->info("Also generated the base QC template $filename in the current dir\n");
	
	
	
}

	