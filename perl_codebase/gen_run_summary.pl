#!/jgi/tools/bin/perl

use strict;
use FindBin;
use lib "$FindBin::Bin/lib";
use JGI::QAQC::RQCdbAPI;
use JGI::QAQC::Utilities;
use File::Path qw(mkpath rmtree);
use Log::Log4perl qw(:easy);
use Data::Dumper;
use Getopt::Long;


## global variables
our ($LOGGER, $CWD);
our ($run_id);
our ($bioclassication_summary_line_per_lane);
our ($read_quality_per_lane);
our ($GC_content_per_lane);
our ($bwa_dup_rate_per_lane);
our ($artifcat_per_lane);
our ($failed_lanes);
our ($read_counts_per_lane);
our ($lanes_analyzed_by_RQC);
our ($A_percent_per_lane_12_cycles);
our ($G_percent_per_lane_12_cycles);
our ($C_percent_per_lane_12_cycles);
our ($T_percent_per_lane_12_cycles);
our ($N_percent_per_lane_12_cycles);
our ($A_percent_per_lane_post_cycle_12);
our ($G_percent_per_lane_post_cycle_12);
our ($C_percent_per_lane_post_cycle_12);
our ($T_percent_per_lane_post_cycle_12);
our ($N_percent_per_lane_post_cycle_12);





main();


sub main {
	
	### Print the standard RQC header 
	get_standard_usage_header();
	
	
	###setup the logger
	config_logger();
	$LOGGER = get_logger();
	
	
	## get the user arguments
	$run_id = get_user_arguments();
	
	$LOGGER->info("############Started###############");
	
	## Instantiate the Class RQCdbAPI
	my $obj = JGI::QAQC::RQCdbAPI->new();
	
	## set the run_id
	$obj->run_id($run_id);
		
		
	$CWD = get_cwd();
	
	$obj->get_seq_units_name_from_run_id();
	
	my $seq_units_name_per_run = $obj->seq_units_per_run();
	
#	Get one time to use below
	my $rqc_status_message = $obj->get_rqc_status_message();
	
	$lanes_analyzed_by_RQC=0;
	
		foreach my $lane  ( sort keys %{$seq_units_name_per_run}) {
			
				
			my $individual_seq_unit_name = $seq_units_name_per_run->{$lane}->{'seq_unit_name'};
			my $rqc_status_id = $seq_units_name_per_run->{$lane}->{'rqc_status_id'};
			
			print "\n\n";
			$LOGGER->info("Generating QC numbers for lane $individual_seq_unit_name");
			
			
			
#			Skip a lane if RQC has not processed it
			unless ( $rqc_status_id == 11 ) {
				
				print "Will skip processing this lane $lane \t $individual_seq_unit_name \n";
				print "Rqc_status_id = $rqc_status_id\n";
				print  "Interpretation : $rqc_status_message->{$rqc_status_id}->{'rqc_status_name'}";
				
				$failed_lanes .= 'Lane  '."$lane \t $individual_seq_unit_name has not been proceesed by RQC : Rqc status -> $rqc_status_id : \t ".$rqc_status_message->{$rqc_status_id}->{'rqc_status_name'}."\n";
				next;
			}
		
			
#			adding one to num of lanes processed by RQC
			$lanes_analyzed_by_RQC++;
			
			## Setting the right seq_unit for down_stream called to RQCdbAPI
			$obj->seq_unit_name($individual_seq_unit_name);
			
			
			###Now fetching the results for each library from RQC db
			$obj->get_lane_level_info();
			$obj->get_library_info_from_seq_unit_name();
			
			## also get the library detail from venonat
			$obj->get_library_info_from_venonat();
			
		
			$read_counts_per_lane	.= "$lane\t\t".$obj->get_library_name()."\t\t".commify($obj->get_total_read_count())."\n"; 
			
			$bioclassication_summary_line_per_lane   .= $lane."\t".$obj->get_bio_classification_name()."\t\t".$obj->get_library_name()."\t".$obj->get_library_type()."\t".$obj->get_lane_yield().' Mbp'."\t".$obj->get_run_config()."\t".$obj->get_qualified_seq_unit_name()."\n";
			
			$read_quality_per_lane .=  "$lane\t\t".$obj->get_library_name()."\t\t". $obj->get_Q30_percent()."\t\t".'Q20 rl = '.$obj->get_min_Q20_readlen_of_read_1_2()."\n";
			
			$GC_content_per_lane   .= "$lane\t\t".$obj->get_library_name()."\t\t".$obj->get_GC_content()."\n";
			
			$bwa_dup_rate_per_lane .= "$lane\t\t".$obj->get_library_name()."\t\t".$obj->get_bwa_algn_dup_percent().' % '."\n";
			
			$artifcat_per_lane  .= "$lane\t\t".$obj->get_library_name()."\t\t".$obj->get_percent_illumina_artifacts().' % '."\n";
			
			
			my $read_1_basecount_filename = $obj->get_read1_basecount_filename();
			my $read_2_basecount_filename = $obj->get_read2_basecount_filename();
#			
#			$A_percent_per_lane_12_cycles .= $obj->get_A_percent($read_1_basecount_filename,1, 12)."\t\t".  $obj->get_A_percent($read_2_basecount_filename,1, 12)."\n";
#			$G_percent_per_lane_12_cycles .= "\t\t".$obj->get_G_percent($read_1_basecount_filename,1, 12)."\t\t".  $obj->get_G_percent($read_2_basecount_filename,1, 12)."\n";
#			$C_percent_per_lane_12_cycles .= $lane."\t\t".$obj->get_C_percent($read_1_basecount_filename,1, 12)."\t\t".  $obj->get_C_percent($read_2_basecount_filename,1, 12)."\n";
#			$T_percent_per_lane_12_cycles .= $lane."\t\t".$obj->get_T_percent($read_1_basecount_filename,1, 12)."\t\t".  $obj->get_T_percent($read_2_basecount_filename,1, 12)."\n";
#			$N_percent_per_lane_12_cycles .= $lane."\t\t".$obj->get_N_percent($read_1_basecount_filename,1, 12)."\t\t".  $obj->get_N_percent($read_2_basecount_filename,1, 12)."\n";
#			
#			
#			$A_percent_per_lane_post_cycle_12.= $lane."\t\t".$obj->get_A_percent($read_1_basecount_filename,	13,$obj->get_read_1_len())."\t\t".  $obj->get_A_percent($read_2_basecount_filename,13,$obj->get_read_2_len() )."\n";
#			$G_percent_per_lane_post_cycle_12.= $lane."\t\t".$obj->get_G_percent($read_1_basecount_filename,	13,$obj->get_read_1_len())."\t\t".  $obj->get_G_percent($read_2_basecount_filename,13,$obj->get_read_2_len() )."\n";
#			$C_percent_per_lane_post_cycle_12.= $lane."\t\t".$obj->get_C_percent($read_1_basecount_filename,	13,$obj->get_read_1_len())."\t\t".  $obj->get_C_percent($read_2_basecount_filename,13,$obj->get_read_2_len() )."\n";
#			$T_percent_per_lane_post_cycle_12.= $lane."\t\t".$obj->get_T_percent($read_1_basecount_filename,	13,$obj->get_read_1_len())."\t\t".  $obj->get_T_percent($read_2_basecount_filename,13,$obj->get_read_2_len() )."\n";
#			$N_percent_per_lane_post_cycle_12.= $lane."\t\t".$obj->get_N_percent($read_1_basecount_filename,	13,$obj->get_read_1_len())."\t\t".  $obj->get_N_percent($read_2_basecount_filename,13,$obj->get_read_2_len() )."\n";
#    	
			
	
		}

		
		
#		# Generate the QC report and email it , locally store it
		generateReport($obj);
	
		$LOGGER->info("############END###############");
	
		
}
	



sub get_user_arguments {
	
	# need to improve this
	my $SUB = 'get_user_arguments';
	
	if ( scalar @ARGV == 1 ) {
		return shift @ARGV;
	}
	
	else {
		usage();
		exit 3;
	}
	
}

sub usage {
	
	my $SUB  ='usage';

print<<USAGE;

$0  <<RUN_ID>>

generate the Main pooled library QC report

USAGE
	
}

sub generateReport {
	
	my $SUB = 'generateReport';
	
	my $obj = shift @_ || die " Class obj not passed \n\n";
	
	my $REPORT;
	
	
	$REPORT .= '1.) Run ID : '."$run_id \n\n";
	
		
		
	$REPORT .= '2)  Read Counts '."\n\n";
	$REPORT .= $read_counts_per_lane."\n\n";
		
	

		
		
		
	
	$REPORT .= '2) Lanes sequenced : 8'."\t\t".'|  Lanes Analyzed : '."\t $lanes_analyzed_by_RQC\n";
	$REPORT .= 'bioclassification,    library,    library type,    Mbp,    run configuration,       file name'."\n\n\n";
	$REPORT .= $bioclassication_summary_line_per_lane."\n\n\n";
	
	
	$REPORT .= '3) Read Level QC Metrics'."\n\n";
	$REPORT .= "\n";
	$REPORT .= '3.1) Read quality'; 
	$REPORT .= "\n";
	$REPORT .= '3.1.1) Quality score are :';
	$REPORT .= "\n\n";
	$REPORT .= 'Library :       Library Name          :   %reads_Q30    :     Q20_read_length min of read1/2'."\n";
	$REPORT .= $read_quality_per_lane."\n\n\n";
	
	
	
	

	$REPORT .= '3.2) Read GC Content'."\n\n";
	$REPORT .= 'Library'."\t\t".'FastQ'."\t\t".'GC_content'."\n";
	$REPORT .=  $GC_content_per_lane."\n\n\n";
	
	$REPORT .= '3.3) Uniqueness - 5% sampled 50bp read vs. read alignment using BWA'."\n";
	$REPORT .= $bwa_dup_rate_per_lane."\n\n";
	
	
	$REPORT .= '3.4) Percent Illumina artifacts :'."\n";
	$REPORT .= $artifcat_per_lane."\n\n";
	
	
	
	$REPORT .= 'Possible Failures : Following Lanes were not reported'."\n\n";
	$REPORT .=	$failed_lanes."\n\n";
	
#	$REPORT .= '4.1) Nucleotide Composition  : '."\n\n";
	
	
	
#	$REPORT .= '				Read 1(first 12 cycles)			Read 2(first 12 cycles)'."\n";
#    
#  	$REPORT .= $A_percent_per_lane_12_cycles;
#  	$REPORT .= $G_percent_per_lane_12_cycles;
#  	$REPORT .= $C_percent_per_lane_12_cycles;
#  	$REPORT .= $T_percent_per_lane_12_cycles;
#  	$REPORT .= $N_percent_per_lane_12_cycles;
#  	
#	
#	$REPORT .= '				Read 1(post cycle 12)			Read 2(post cycle 12)'."\n";
#    $REPORT .= $A_percent_per_lane_post_cycle_12;
#    $REPORT .= $G_percent_per_lane_post_cycle_12;
#    $REPORT .= $C_percent_per_lane_post_cycle_12;
#    $REPORT .= $T_percent_per_lane_post_cycle_12;
#    $REPORT .= $N_percent_per_lane_post_cycle_12;
        
	
#	print $REPORT,"\n\n\n";
	
	
	my $filename = 'README.'.$run_id;
	open(OUT,">$filename") || die "Error[$SUB] : Could not open the file $filename \n\n";
	
	print (OUT $REPORT);
	close(OUT);
	
	my $SUBJECT = 'Run '."$run_id".' Basic Report Template generated';
	
	send_text_email (guess_user_email(),guess_user_email(),$SUBJECT,$REPORT);
	
	
	$obj->logger()->info("\n\n============================================================");
	$obj->logger()->info("QC Base Template email sent to user email :", guess_user_email() );
	$obj->logger()->info("Also generated the base QC template $filename in the current dir\n");
	
	
	
}

	