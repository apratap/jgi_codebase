#!/jgi/tools/bin/perl

use strict;
use FindBin;
use lib "$FindBin::Bin/lib";
use JGI::QAQC::RQCdbAPI;
use JGI::QAQC::Utilities;
use Data::Dumper;
use Getopt::Long;


use vars qw($verbose @library_names @seq_units_by_user $VERBOSE $EMAIL $SEQ_UNITS_REF $IS_RUN_PE);


our ($bioclassication_summary_line_per_seq_unit);
our ($read_quality_per_seq_unit);
our ($GC_content_per_seq_unit);
our ($read_Count_per_seq_unit);
our ($bwa_dup_rate_per_seq_unit);
our ($artifcats_mapping_reads_per_seq_unit);
our ($nucleotide_content);
our ($num_reads_mapped_transcriptome_per_seq_unit);
our ($num_reads_mapped_genome_per_seq_unit);
our ($contamination_per_seq_unit);
our ($assembly_stats_per_seq_unit);
our ($assembly_GC_content_per_seq_unit);
our	($assembly_kmer_depth_per_seq_unit);
our	($sequenced_depth_per_seq_unit);
our ($megablast_results_per_seq_unit);
our	($bwa_duplicates_percent_per_seq_unit);
our ($mer20_uniqueness_per_seq_unit);
our ($RQC_report_link);


##for plotting
our (@library_names);
our (@seq_units_names);
our (@num_reads);
our (@Q20_readlen);



main();



sub main {
	
	
	my $SUB = 'main';
	
	### Print the standard RQC header 
	get_standard_usage_header();
	
	
	## get the user arguments
	_get_user_arguments();
	
	## Instantiate the DB access
	my $obj = JGI::QAQC::RQCdbAPI->new();
	$obj->logger()->info('####Program Stated######');
	
# 	Get the final set of seq_unit_names to be included in QC report
# 	RESULTS available in SEQ_UNITS array
	$SEQ_UNITS_REF = $obj->get_final_seq_units_set(\@library_names,\@seq_units_by_user);
	
	#getting the details for each $seq_unit for the individual library that 
	foreach my $seq_unit_name ( @{$SEQ_UNITS_REF}){
			
			## Setting the right seq_unit for down_stream called to RQCdbAPI
			$obj->seq_unit_name($seq_unit_name);
		
			###Now fetching the results for each seq_unit_name from RQC db
			$obj->get_lane_level_info();
			$obj->get_library_info_from_seq_unit_name();
			
			## also get the library detail from venonat
			$obj->get_library_info_from_venonat();
			
	
#			# check if run is PE or SE
   			 my $read_1_len = $obj->get_read_1_len();
    		 my $read_2_len = $obj->get_read_2_len();
    
    		if($read_1_len && ($read_2_len == 'NA')  ){
    			$IS_RUN_PE = 0
    		}
    		elsif($read_1_len == $read_2_len ){
    			$IS_RUN_PE = 1;
    		}	
	
		
			$bioclassication_summary_line_per_seq_unit   .= $obj->get_bio_classification_name().', '.$obj->get_library_name().', '.$obj->get_library_type().', '.$obj->get_lane_yield().' Mbp'.', '.$obj->get_run_config().', '.$obj->get_qualified_seq_unit_name()."\n";
			
			$read_Count_per_seq_unit   	.=$obj->get_library_name()."\t\t".$obj->get_qualified_seq_unit_name()."\t\t".commify($obj->get_total_read_count())."\n";
			
			$read_quality_per_seq_unit .=  $obj->get_library_name()."\t\t".$obj->get_qualified_seq_unit_name()."\t\t". $obj->get_Q30_percent().'  : Q20 rl = '.$obj->get_min_Q20_readlen_of_read_1_2()."\n";
			
			$GC_content_per_seq_unit   .= $obj->get_library_name()."\t\t".$obj->get_qualified_seq_unit_name()."\t\t".$obj->get_GC_content()."\n";
			
			$mer20_uniqueness_per_seq_unit .= $obj->get_library_name()."\t\t".$obj->get_qualified_seq_unit_name()."\t\t".$obj->get_num_reads_sampled()."\t\t\t". $obj->get_20mer_uniq_start_mers_percent().' %'."\t\t\t".$obj->get_20mer_uniq_random_mers_percent().' %'."\n"; 
			
			$bwa_duplicates_percent_per_seq_unit .=  $obj->get_library_name()."\t\t".$obj->get_qualified_seq_unit_name()."\t\t".$obj->get_bwa_algn_dup_percent().' % '."\n";
			
			$contamination_per_seq_unit  .= $obj->get_library_name()."\t\t".$obj->get_qualified_seq_unit_name()."\t\t\t".$obj->get_percent_illumina_artifacts().' % '."\t\t\t".$obj->get_percent_jgi_contaminants().' % '."\t\t\t".$obj->get_percent_rRNA_contaminants().' % '."\n";
			
			$assembly_stats_per_seq_unit .= $obj->get_library_name()."\t\t".$obj->get_qualified_seq_unit_name()."\t\t".'Final graph has  '.commify($obj->get_contig_count()).' nodes and n50 of '.$obj->get_N50().' max '. commify($obj->get_max_contig_size()). ' total '.commify($obj->get_contigs_total_len()). ' using '.commify($obj->get_total_read_count()).' reads'."\n";	 
			
			$assembly_GC_content_per_seq_unit .= $obj->get_library_name()."\t\t".$obj->get_qualified_seq_unit_name()."\t\t".$obj->mean_assembly_GC().'%'."\n";
			
			
			$assembly_kmer_depth_per_seq_unit .= $obj->get_library_name()."\t\t".$obj->get_qualified_seq_unit_name()."\t\t".$obj->get_kmer_depth_90x().'x'."\n";
			
			$sequenced_depth_per_seq_unit	  .=  $obj->get_library_name()."\t\t".$obj->get_qualified_seq_unit_name()."\t\t".$obj->get_nucleotide_seq_depth().'x'."\n";
			
			$megablast_results_per_seq_unit   .= $obj->get_library_name()."\t\t".$obj->get_qualified_seq_unit_name()."\t\t".'Megablast of '.commify($obj->get_num_blastable_contigs()). ' contigs produced '.commify($obj->get_num_NT_tophits()).' top hits to nt.'."\n";
			
			$RQC_report_link				  .= $obj->get_library_name(). "\t\t".'http://rqc.jgi-psf.org/cgi-bin/display_report_illumina.cgi?seqUnit='.$seq_unit_name."\n";
			
			
			my $read_1_basecount_filename = $obj->get_read1_basecount_filename();
			my $read_2_basecount_filename = $obj->get_read2_basecount_filename() if $IS_RUN_PE;
			
		
			$nucleotide_content .= "\n";
			$nucleotide_content  .= $obj->get_qualified_seq_unit_name()."\n";
    		$nucleotide_content .= 'A Percent :			'.$obj->get_A_percent($read_1_basecount_filename,1,$obj->get_read_1_len())."\t\t";
   		 	$nucleotide_content .=  $obj->get_A_percent($read_2_basecount_filename,1,$obj->get_read_2_len() ) if ($IS_RUN_PE);
    		$nucleotide_content .= "\n";
    		$nucleotide_content .= 'T Percent :		'.$obj->get_T_percent($read_1_basecount_filename,1,$obj->get_read_1_len())."\t\t";
    		$nucleotide_content .=  $obj->get_T_percent($read_2_basecount_filename,1,$obj->get_read_2_len() ) if $IS_RUN_PE ;
    		$nucleotide_content .= "\n";
    		$nucleotide_content .= 'G Percent :		'.$obj->get_G_percent($read_1_basecount_filename,1,$obj->get_read_1_len())."\t\t";
    		$nucleotide_content .=  $obj->get_G_percent($read_2_basecount_filename,1,$obj->get_read_2_len() ) if($IS_RUN_PE);
    		$nucleotide_content .= "\n";
    		$nucleotide_content .= 'C Percent :		'.$obj->get_C_percent($read_1_basecount_filename,1,$obj->get_read_1_len())."\t\t";
    		$nucleotide_content .=  $obj->get_C_percent($read_2_basecount_filename,1,$obj->get_read_2_len() ) if $IS_RUN_PE;
    		$nucleotide_content .= "\n";
    		$nucleotide_content .= 'N Percent :		'.$obj->get_N_percent($read_1_basecount_filename,1,$obj->get_read_1_len())."\t\t"; 
    		$nucleotide_content .= $obj->get_N_percent($read_2_basecount_filename,1,$obj->get_read_2_len() ) if $IS_RUN_PE;
    		$nucleotide_content .= "\n\n";

	}

	## Create the REPORT
	createBaseReport($obj);
	
	$obj->logger()->info("####Program Finished######\n\n")
}


sub _get_user_arguments {
	
	# need to improve this
	my $SUB = '_get_user_arguments';
	my %temp;
	
	if(scalar @ARGV ==  0 ) {
		usage();
		exit 2;
	}
	
	GetOptions(\%temp,
					"lib_name|lib:s@",
					"seq_units|su:s@",
                    "prefix|p:s",
                    "verbose|v!",
                    "email|e:i",
              );
    
    
    if ( defined $temp{'lib_name'} ){
    	@library_names = @ {$temp{'lib_name'} };
    }
    
    if ( defined $temp{'seq_units'} ){
    	@seq_units_by_user = @{ $temp{'seq_units'} };
    }
	
	if ( defined $temp{'verbose'}){
		$VERBOSE = $temp{'verbose'};
	}
	else {
		$VERBOSE = 0;
	}
	
	if ( defined $temp{'email'} ){
		$EMAIL = $temp{'email'};
	}
	else {
		$EMAIL = 0;
	}
	
	if ( (scalar @library_names == 0) && ( scalar @seq_units_by_user == 0 )  ){
		die " You have not provided either the library name or seq_unit_name/s to generate a QC template on.";
	}
}


sub usage {
	
print<<USAGE;

-------------------------------------------------
Metagenomic Library QC template generator script 
Standard options for script
-------------------------------------------------


1.	For single library
$0 -lib <lib_name> -email <0|1 : default 1>

2. 	For multiple libraries
$0 -lib <lib_name1> -lib <lib_name2> -lib <lib_name3> -email <0|1 : default 1>

3.	If you want to specify seq_units_name
$0 -su <seq_unit_name1> -su <seq_unit_name2> -email <0|1 : default 1>


4.	You can also club library names and seq_unit_names if needed 
$0 	
-lib <lib_name1> -lib <lib_name2> -lib <lib_name3>  
-su <seq_unit_name1> -su <seq_unit_name2>
-email <0|1 : default 1>


What is a SEQ_UNIT_NAME ? 
Full seq unit name is the full lane name as per RQC. Also shown on the RQC page post library name based search.
Example: For library HZON  with one lane of data 1690.5.1538.fastq.gz : seq_unit_name will be 1690.5.1538.srf
	
USAGE

}


sub createBaseReport {
	
	my $SUB = 'createBaseReport';
	
	my $obj = shift @_;
	
	my $REPORT;
	
	$REPORT .= 'ACTION:'."\n\n\n";
	$REPORT .= '1) Comments: <PASS|FAIL>'."\n";
	$REPORT .= 'Run base output <low|high>.'."\n";
	$REPORT .= 'Read quality <OK|low|high>.'."\n";
	$REPORT .= 'Target genome <is | not> present in BLAST hits.'."\n";
	$REPORT .= 'Assembly nucleotide coverage is '. $obj->get_nucleotide_seq_depth().'x using 1 lane(s) of data'."\n";
	$REPORT .= '<Y|N> evidence of process contamination'."\n";

	$REPORT .= "\n\n\n";
	

	$REPORT .= 'QC dir: ' .get_cwd()."\n";
	$REPORT .= 'Passed FASTQ files: '.get_cwd()."\n\n\n";
	
	
	$REPORT .= 'RQC Report link/ library[ies]'."\n";
	$REPORT .= $RQC_report_link."\n\n\n";
	
	$REPORT .= '2) Lanes sequenced : '.scalar @$SEQ_UNITS_REF."\n";
	$REPORT .= 'bioclassification, library, library type, Mbp, run configuration, file name'."\n";
	$REPORT .= $bioclassication_summary_line_per_seq_unit."\n";
	
	
	$REPORT .= '3) Read Level QC Metrics';
	$REPORT .= "\n";
	$REPORT .= '3.1) Read quality'; 
	$REPORT .= "\n";
	$REPORT .= '3.1.1) Quality score';
	$REPORT .= "\n";
	$REPORT .= 'Run.lane.basecall    :   percent of scores >= Q30%   :  Q20 rl min(read 1/2)';
	$REPORT .= "\n";
	$REPORT .= $read_quality_per_seq_unit."\n";
	
	
	$REPORT .= '3.2) Nucleotide Composition  : <<Comment -if any >>'."\n";
	$REPORT .= $nucleotide_content."\n";
	$REPORT .="\n";
		
	
	$REPORT .= '3.3) Read GC Content'."\n";
	$REPORT .= '3.3.1) << uni/bi/tri>>>modal peak'."\n";
	$REPORT .= '3.3.2) Mean read GC : '."\n";
	$REPORT .= $GC_content_per_seq_unit."\n";
	
	
	$REPORT .= '3.4) Uniqueness - 20mer sampling'."\n";
	$REPORT .= 'Library		Fastq			#reads sampled(in-millions)		%start-20mers 	%random-20mers'."\n";
	$REPORT .= $mer20_uniqueness_per_seq_unit."\n";
	$REPORT .="\n";
	
	
	$REPORT .= '3.5) Uniqueness - 5% sampled 50bp read vs. read alignment using BWA'."\n";
	$REPORT .= '3.5.1) Percent of 50bp duplicate reads :'."\n";
	$REPORT .= $bwa_duplicates_percent_per_seq_unit;
	$REPORT .= "\n";
	
	
	$REPORT .= '3.6) Contamination'."\n";
	$REPORT .= '							illumina_artifacts		jgi_contam		rRNA'."\n";
	$REPORT .= $contamination_per_seq_unit."\n";
	
	$REPORT .= "\n\n";
	
	$REPORT .= '4) Assembly level QC metrics'."\n";
	$REPORT .= '4.1) Contig metrics'."\n";
	$REPORT .= '4.1.1) 1 lane(s) of data assembled using Velvet'."\n";
	
	## commify is coming from JGI::QAQC::Utilities
	$REPORT .= '4.1.2) Assembly parameters : kmer '.$obj->get_kmer_size().' , minimum contig length '.$obj->get_min_contig_len()."\n";
	$REPORT .= '4.1.3) Assembly summary '."\n";
	$REPORT .= $assembly_stats_per_seq_unit."\n";

	
	$REPORT .= '4.2) Sample validation and contamination'."\n";
	$REPORT .= $megablast_results_per_seq_unit;
	$REPORT .= '<<<<INSERT TOP HITS >>>>'."\n\n";
	
	
	$REPORT .= '4.2.1) Top hits <<are|not>> consistent wqith genome of interest'."\n";
	$REPORT .= '4.2.2) <Y|N> evidence of major contamination.';
	
	$REPORT .= "\n\n";
	
	$REPORT .= '4.3) Assembly GC content'."\n";
	$REPORT .= '4.3.1) <<uni/bi/tri >>modal peak'."\n";
	$REPORT .= '4.3.2) Mean Assembly GC '."\n";
	$REPORT .= $assembly_GC_content_per_seq_unit."\n";
	
	
	
	$REPORT .= '4.4) Depth'."\n";
	
	$REPORT .= '4.4.1) Kmer Depth :'."\n";
	$REPORT .=  $assembly_kmer_depth_per_seq_unit;
	$REPORT .="\n"; 
	
	$REPORT .= '4.4.2) Target nucleotide depth : NA'."\n\n";
	
	
	$REPORT .= '4.4.3) Sequenced nucleotide depth :'."\n";
	$REPORT .= $sequenced_depth_per_seq_unit;
	$REPORT .= "\n";
	
	$REPORT .= 'Report generated by '.get_username().' at time '.get_timeStamp()."\n";
	
	my $suffix = get_suffix_for_report_generation();
	
	my $filename = 'README.'.$suffix;
	open(OUT,">$filename") || die "Error[$SUB] : Could not open the file $filename \n\n";
	
	print (OUT $REPORT);
	close(OUT);
	
	$obj->logger->info("QC template for library $suffix generated in file $filename \n\n ");
	
	if ( $EMAIL ){
		my $SUBJECT = 'MG Library '."$suffix".' Base QC Template generated';
		send_text_email (guess_user_email(),guess_user_email(),$SUBJECT,$REPORT);
		$obj->logger()->info("QC Base Template email sent to user email :", guess_user_email() );
	}
	
	
}

sub get_suffix_for_report_generation {
	my $SUB = 'get_suffix_for_report_generation';
	
	if (  ( scalar @library_names == 1 ) && (scalar @seq_units_by_user == 0) ) {
		return pop(@library_names);
	}	
	else{
		return 'mulitpleLibs.QC.report';
	}
}










