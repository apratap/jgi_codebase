#!/jgi/tools/bin/perl


use strict;
use Data::Dumper;




main();


sub main {

    read_parse_input();
    

}




sub read_parse_input {

    my $SUB='read_parse_input';
    
   
    my $count_proper_pair = 0;

	my $reads;
	
	my $read1_fastq_file = 'read1.fastq';
	my $read2_fastq_file = 'read2.fastq';
	
	open(READ1,">$read1_fastq_file") || die "File $read1_fastq_file could not be opened \n";
	open(READ2, ">$read2_fastq_file") || die "File $read1_fastq_file could not be opened \n";

	
    while(<STDIN>) {
       
	my $header= $_;
	chomp $header;
	my $seq=<STDIN>;
	chomp $seq;
	my $qual_header=<STDIN>;
	chomp $qual_header;
	my $quality = <STDIN>;
	chomp $quality;

	my $full_read =$header."\n".$seq."\n".$qual_header."\n".$quality;
	
		$header =~/^.+?\/(\d).*$/;
		my $read1_OR_read2 =$1;
		
		## removing the read1/2 info
		my $header_only=$header;
		$header_only=~s/\/\d//;
		
		$reads->{$header_only}->{$read1_OR_read2}=$full_read;
		

			if(         (defined $reads->{$header_only}->{'1'})  && (defined $reads->{$header_only}->{'2'})     )  {
						$count_proper_pair++;
#						print "A pair  found \n";
					
						
						print READ1 "$reads->{$header_only}->{'1'}\n";
						print READ2 "$reads->{$header_only}->{'2'}\n";
						
						delete $reads->{$header_only};
			}			
	
	} ## END while loop



###printing the summary
print STDERR "#proper_pairs \t $count_proper_pair\n";

my $total_singeltons = scalar keys %{$reads};
print STDERR "#Singeltons \t $total_singeltons\n";

my $total_reads = ($count_proper_pair*2) + $total_singeltons;
print STDERR "#Reads \t $total_reads\n\n";




### print out the singeltons

open (OUT, ">singeltons.fq");

	foreach my $read ( keys %{$reads}) {
		
		print READ1 $reads->{$read}->{'1'},"\n" if ( defined $reads->{$read}->{'1'} ); 
		print READ2 $reads->{$read}->{'2'},"\n" if ( defined $reads->{$read}->{'2'} );
	}


}

