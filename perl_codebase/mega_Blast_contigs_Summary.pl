#!/jgi/tools/bin/perl

use strict;

use Statistics::Descriptive::Discrete;
use FindBin;
use lib "$FindBin::Bin/lib";
use JGI::QAQC::Utilities;
use Bio::DB::GenBank;
use Getopt::Long;
use lib "/house/homedirs/a/apratap/lib/perl5/site_perl/5.10.1";
use Bio::LITE::Taxonomy::NCBI::Gi2taxid qw/new_dict/;
use Bio::LITE::Taxonomy::NCBI;
use Data::Dumper;
use Log::Log4perl qw(:easy);

use vars qw($LOGGER $READ_LEN $KMER_LEN $VERBOSE $MAX_TAXA_LEVEL $MIN_TAXA_LEVEL $contig_hash $annotated_contigs_file $uniq_tophits_file $OUT_TAXA_FILE);



main();


sub main {

	config_logger();
	$LOGGER=get_logger();


#	get/set user args
	_parse_user_arguments();

	print "\n\n\n";
#	##get the names of uniq tophits contigs across all databases
	$uniq_tophits_file= unify_tophits();
	print "\n\n\n";

#	create a new file for unique contigs with taxa information
#	The header of the file will have the column names
	 ($annotated_contigs_file, $contig_hash) = properly_annotate_contigs($uniq_tophits_file);


#	create a tree for taxonomy with each taxa having the contigs which belong to it and 
#  	and count of total contigs that hit each taxa
	my $TAXA_TREE = build_contig_tree_based_on_taxonaomy();

#	print a summary of hits
	my $taxa_distribution_file = 'taxa_distribution.tsv';
	open ( $OUT_TAXA_FILE, ">$taxa_distribution_file") || $LOGGER->fatal("Can't open the file $taxa_distribution_file ");
	print $OUT_TAXA_FILE  "current_level \t taxa \t GC_content \t kmer_cov \t length \t #contig_base_match_blast_hit\n";
	analyse_contig_tree_recursively($TAXA_TREE);


}


sub _parse_user_arguments {

	my $SUB='_parse_user_arguments';
	my %temp;

	if(scalar @ARGV ==  0 ) {
		usage();
		exit 3;
	}

	GetOptions(\%temp,
					"tophits|th=s@",
                    "read_len|rl=i",
                    "velvet_kmer_size|vkmr=i",
                    "max_taxa_level|max_tl=i",
                    "min_taxa_level|min_tl=i",
                    "verbose|v!",
              );
    
    
    unless ( $temp{'read_len'} &&  $temp{'velvet_kmer_size'} ) {
    	
    	$LOGGER->fatal("Read length and velvet k-mer size both are needed\n");
    	die ;
    }
    
    
    $READ_LEN	= 	$temp{'read_len'};
    $KMER_LEN 	= 	$temp{'velvet_kmer_size'};
    $VERBOSE 	= 	$temp{'verbose'} || 0;
    $MAX_TAXA_LEVEL	=	$temp{'max_taxa_level'} || 6;
    $MIN_TAXA_LEVEL	=	$temp{'min_taxa_level'} || 1;
    

} ### End 


sub usage {

	my $SUB 	=	'usage';

print<<USAGE

$0 still to write

USAGE


}





sub unify_tophits {


	my $SUB = 'unify_tophits';
      
     $LOGGER->info(" IN SUB : $SUB ");

### open file for writing out contigs both uniq and non unique

	my $uniq_contigs_file = 'unique_tophits_across_all_dbases.txt';
	open(OUT_UNIQ, ">$uniq_contigs_file") || die "Cant open the file $uniq_contigs_file\n";

	my $non_uniq_contigs_file = 'non_unique_tophits_across_all_dbases.txt';
	open(OUT_NON_UNIQ, ">$non_uniq_contigs_file") || die "Cant open the file $non_uniq_contigs_file\n";


	my $uniq_contigs;
	my @duplicate_contigs;

	my @files = `ls *tophit`;



	foreach my $file (@files) {

		chomp $file;
		$LOGGER->info("Reading contigs from file \t $file");
		### reading each file and unifying contig level info
				open(IN, "<$file") || die "Can't open file $file\n";
#				print "Reading contigs from file $file \n";

				 	while(<IN>) {
				 		chomp $_;
				 		my $contig_line = $_;
				 		my @contig_line_split = split('_',$contig_line);
				 		my $contig_name = $contig_line_split[1];

				 		if( defined $uniq_contigs->{$contig_name} ) {
							print OUT_NON_UNIQ "$contig_line\n";
				 			push(@duplicate_contigs, $contig_name);
				 		}
				 		elsif ( ! defined $uniq_contigs->{$contig_name} ) {
				 			print OUT_UNIQ "$contig_line\n";
				 			$uniq_contigs->{$contig_name}=1;
				 		}
				 		else {
				 			$LOGGER->warn("SUB : $SUB Error : $contig_line");
				 		}

				 	}
	}


close(OUT_UNIQ);
close(OUT_NON_UNIQ);

my $total_uniq_contigs = scalar keys %{$uniq_contigs} ;
my $dup_contigs = scalar @duplicate_contigs;

$LOGGER->info("Total Uniq Contigs \t $total_uniq_contigs");
$LOGGER->info("Duplicate Contigs \t $dup_contigs ");
$LOGGER->info("DONE SUB : $SUB"); 

return $uniq_contigs_file;


}



###For each contig will return a hash with lineage and specie name
### Input is read from the $uniq_tophits_file
### output is a hash with key contig name and references to lineage 

sub properly_annotate_contigs {


	my $SUB = 'properly_annotate_contigs';

	my $force_run 			= shift @_ || 0;


	### Read the file created by the SUB : unify_tophits
	open (IN ,"< $uniq_tophits_file") || die "Error[$SUB} file $uniq_tophits_file can't be opened \n";

#	Get the contig GC content from a different file created by residues.pl
	my $contig_GC_content=get_contig_GC_content_from_file();


	### Name of the output file
	my $out_file = 'unique_annotated_tophits_across_all_dbases.txt';		


#	my %annotated_contigs will store all contig names who have been previously annotated in $out_file
#	they will be skipped during the re-run
	my %annotated_contigs;

# 	Main contig hash storing all the info per contig
	my $contig_hash;

#	Checking if the expected output already exists in full in the file
	if( -e $out_file ){

		$LOGGER->info("Expected outfile $out_file of this method $SUB already exists .. \nWill skip running $SUB for contigs already annotated and present in the file \n\n ");
		open(READIN, "< $out_file ") || die "Error[$SUB] out_file $out_file can't be opened \n ";

		while(<READIN>) {
			chomp $_;
			next if(/^contigs.*$/);
			my @temp = split("\t",$_);
			my $contig_name = $temp[0];
			my $organism_name = $temp[3];

			$contig_name =~s/\s+?//g;
			$organism_name =~s/\s+?//g;

			$annotated_contigs{$contig_name} = $organism_name;

		}

		close (READIN);
		open(OUT, ">> $out_file") || die "Could not open file $out_file $! \n\n";


	}
	else {

		open(OUT, "> $out_file") || die "Could not open file $out_file $! \n\n";

#		Print the header of outfile if this is the first time creating the file
     	my @HEADER_WORDS 	= ("contigs","kmer_length","kmer_coverage","organism","lineage", "NCBI_ACCESSION", "NCBI_GI", "Escore","match_len","percent_identity");
     	my $HEADER			= join("\t", @HEADER_WORDS)."\n";
     	print OUT $HEADER;
	}


	$LOGGER->info( "Creating the first method handle to query genbank and access lineage by accession number...");
	##First Method : Slow but needed when NCBI Accession is only given
	###db handle to extract the taxanomy when the NCBI_Accession is present
  	my $gbh = Bio::DB::GenBank->new();
  	print "Done\n";



	$LOGGER->info("Creating the second method to give lineage based on Bio::Lite::Taxonomy ....");
	##Second Method :: WAY FASTER
	## To get taxonomy when the NCBI_GI  is given
  	my $taxNCBI = Bio::LITE::Taxonomy::NCBI->new(
  								 db=>"NCBI",
                                  nodes=> '/house/homedirs/a/apratap/playground/NCBI_taxanomy/nodes.dmp',
                                  names=> '/house/homedirs/a/apratap/playground/NCBI_taxanomy/names.dmp',
                                  dict=>"/house/homedirs/a/apratap/playground/NCBI_taxanomy/gi_taxid_nucl.bin"
                                  );
    print "Done\n";
        
        
  
#  	Initializing few counters
  	my $contigs_allready_annotated = 0;
  	my $contigs_annotated_from_NCBI_GI =0;
  	my $contigs_annotated_from_NCBI_Accession=0;
  
  
  
#  	NCBI Accession cache
#	A cache to store accession ID and there corresponding lineage so
	my %NCBI_Accession_cache;
  
  	my $line=0;
	while(<IN>) {

		$line++;
		print "line $line read\n";

		chomp $_;

		my($contig_info,$blast_hit,$Escore,$match_len,$percent_identity,$contig_length)=split(' ',$_);

		my ($NCBI_ACCESSION, $NCBI_GI, @LINEAGE);


		my @contig_info_split=split('_',$contig_info);
#	 
		my $contig_name 		   = $contig_info_split[1];
		my $contig_kmer_length     = $contig_info_split[3];
		my $contig_kmer_coverage   = $contig_info_split[5];

#		 removing the whitespace if any
		$contig_name=~s/\s+?//g;
		$contig_kmer_length=~s/\s+?//g;
		$contig_kmer_coverage=~s/\s+?//g;



#				Prepare a single contig hash with all the info for that contig
		if( defined $contig_hash->{$contig_name} ) {
#			Possible problem as we expect to see each contig only once
			print "Error[$SUB] Contig name : $contig_name seen already once\n\n";
			die " Please check to see how non-uniq contig names got in the $uniq_tophits_file file";
		}
		else {
#			print "Storing contig $contig_name in hash  \n";
#			store the contig info
			$contig_hash->{$contig_name}->{'contig_kmer_length'}	=	$contig_kmer_length;
			$contig_hash->{$contig_name}->{'contig_kmer_coverage'}	=	sprintf('%.02f' , $contig_kmer_coverage);
			$contig_hash->{$contig_name}->{'Escore'}				=	$Escore;
			$contig_hash->{$contig_name}->{'match_length'}			=	$match_len;
			$contig_hash->{$contig_name}->{'percent_identity'}		=	$percent_identity;
			
			
			if ( exists $contig_GC_content->{$contig_name} ){
				$contig_hash->{$contig_name}->{'GC_percent'}		= 	$contig_GC_content->{$contig_name}->{'GC_percent'};
				$contig_hash->{$contig_name}->{'length'}			= 	$contig_GC_content->{$contig_name}->{'length'};
			
				delete $contig_GC_content->{$contig_name};
			}
			else{
				print "WARN[$SUB] Can't find GC contig for contig $contig_name in hash generated from get_contig_GC_content \n";
			}
			
		}


		
		

#		extracting the GI and NCBI Accession from blast hit
		if( defined $annotated_contigs{$contig_name}) {

			print "Contig $contig_name already annotated ..Will skip\n";
			$contigs_allready_annotated++;

#			since the contig is already annotated the organism name will be pushed to contig_hash
#			as the routine will jump and not do calculate any lineage
			$contig_hash->{$contig_name}->{'organism'}		=	$annotated_contigs{$contig_name};

			next;
		}


#		If blast hit had NCBI GI 		
		if( $blast_hit =~ /^gi\|(.+?)\|(.+?)\|(.+?)\|.*$/) {

			$NCBI_GI  		= $1;
			$NCBI_ACCESSION = $3;

			print "Fetching the lineage for Contig $contig_name with NCBI GI : $NCBI_GI\n";
			##Using second method which is faster 
			my @classification = $taxNCBI->get_taxonomy_from_gi($NCBI_GI);

#			# no need to reverse the classification to get lineage for this method
			@LINEAGE = @classification;

			$contigs_annotated_from_NCBI_GI++;
		}


#		If blast hit had NCBI accession		
		elsif( $blast_hit =~ /^(.+?)\.(.+?)\.(.+?)__.*$/    ) {

			$NCBI_ACCESSION  = $1;
			$NCBI_GI		 = 'NA';

			my @classification;

#			Checking if a query has already been done for a given accession
			if ( exists $NCBI_Accession_cache{$NCBI_ACCESSION} ) {

				print "Found taxa lineage for Accession $NCBI_ACCESSION in cache\n";
				@classification = @{$NCBI_Accession_cache{$NCBI_ACCESSION}};
			}
			else{
				print "Fetching the lineage for Contig $contig_name with NCBI Accession : $NCBI_ACCESSION \n";
			## Using the first method to get taxanomy when GI is not present
				my $seq = $gbh->get_Seq_by_acc( $NCBI_ACCESSION );
 				my $org = $seq->species;
				@classification = $org->classification;

				$NCBI_Accession_cache{$NCBI_ACCESSION}=\@classification;
			}


			#		reverse the classification to get the lineage begining from superkingdom
			@LINEAGE = reverse(@classification);

			$contigs_annotated_from_NCBI_Accession++;
		}
		else {
			print "Warn [$SUB] Could not extract GI or Accession \n $blast_hit \n\n";
			next;
		}

#		fix the spaces in lineage
		@LINEAGE = fix_spaces_in_lineage_terms(\@LINEAGE);

#		Taking out the organism name from the lineage.
		my $organism_name = pop(@LINEAGE);


#	setting up the organism name for contigs which were not previously annotated
$contig_hash->{$contig_name}->{'organism'}  = $organism_name;	

print ".";
#print "$contig_name \t $contig_kmer_length \t $contig_kmer_coverage \t $organism_name \t @LINEAGE \t $NCBI_ACCESSION \t $NCBI_GI \t $Escore \t $match_len \t $percent_identity \n";

##printing each annotated contig		
print OUT "$contig_name\t$contig_kmer_length\t$contig_kmer_coverage\t$organism_name\t@LINEAGE\t$NCBI_ACCESSION\t$NCBI_GI\t$Escore\t$match_len\t$percent_identity\n";

	}

print "\n\n\n";
print "Annotation Results \n";
print "Total contigs pre-annotated \t\t $contigs_allready_annotated \n";
print "Total Contigs annotated using NCBI GI \t\t $contigs_annotated_from_NCBI_GI \n";
print "Total Contigs annotated using NCBI Accession \t\t $contigs_annotated_from_NCBI_Accession \n\n\n";

close(OUT);
close(IN);	

return ($out_file,$contig_hash);

}


### To fix the spaces in each level of lineage
### The return array will have _ for spaces in the each value of lineage array
sub fix_spaces_in_lineage_terms {

	my $SUB  ='fix_spaces_in_lineage_terms';

	my @array = @{shift @_};

#	print "IN SUB @array\n";

	my @return_array;

	foreach my $value(@array) {
		$value=~s/\s/_/g;
		push(@return_array,$value);
	}

#	print "Return from SUB @return_array \n";
return @return_array;

}



##Build a tree that will have all the details about each level of lineage and which contig falls into it
sub build_contig_tree_based_on_taxonaomy {

	my $SUB ='build_contig_tree_based_on_taxonaomy';

	my $TAXA_TREE;

	open(READIN, "< $annotated_contigs_file ") || die "Error[$SUB] Annotated Contigs file $annotated_contigs_file can't be opened \n";

	while(<READIN>) {

		chomp $_;

		##Skip the header line
		next if (/^contig.*$/);

		my @each_contig = split ("\t",$_);

		my $contig_name = $each_contig[0];
		$contig_name=~s/\s+?//;
		my @lineage 	= split(' ',$each_contig[4]);

#		print "$contig_name \t $each_contig[4] \n";

		build_lineage_tree($TAXA_TREE,\@lineage,\$contig_name,);

	}

	print Dumper($TAXA_TREE);
	return $TAXA_TREE;

}




sub build_lineage_tree {

	
	$_[0]				||= {};
	
	my $phylo_tree		= 	shift @_;
	my @lineage 		=	@{shift @_} ;
	my $contig_name		=	${shift @_} ;		
	
	my $depth=0;

	next if(scalar @lineage == 0 );

# dynamically building the perl hash of hash : concept of autovivification
# Reference link : http://stackoverflow.com/questions/6193866/accessing-hash-reference-in-perl

	my $level_count= 1;
	for (@lineage) {
		
		last if ( $level_count > $MAX_TAXA_LEVEL );
		
		$phylo_tree = $phylo_tree->{$_} ||= {};
		++$phylo_tree->{count};
		$phylo_tree->{'level'} = $level_count;
		push( @{$phylo_tree->{'contigs'}}, $contig_name);
		
		$level_count++;
		
	}
	

}




sub analyse_contig_tree_recursively {

	my $SUB = 'analyse_contig_tree_recursively';

	my $TAXA_TREE = shift @_;	

	
	foreach ( keys %{$TAXA_TREE} ){
		
		if (  ( $_ eq 'contigs' ) || ( $_ eq 'count') || ($_ eq 'level')  )   {  next; }
		
		if ( $TAXA_TREE->{$_}->{'level'} == 1 ) { print "\n\n"; } 
			
		if (ref $TAXA_TREE->{$_} eq 'HASH') {
				
					my @contigs 										= 	@{ $TAXA_TREE->{$_}->{'contigs'} };
					my $current_level									= $TAXA_TREE->{$_}->{'level'};
	 				my ($total_bases_contigs,@length_per_contig) 		= _get_total_bases_in_contigs(\@contigs,$contig_hash);
	 				my $approx_num_reads								= _get_num_reads_from_contigs_kmer_cov(\@contigs,$contig_hash);
	 				my @GC_content										= _get_GC_content_in_contigs(\@contigs,$contig_hash);
	 				my @kmer_cov_per_contig								= _get_kmer_cov_per_contig(\@contigs,$contig_hash);
	 				my @lenth_per_contig								= _get_length_per_contig(\@contigs,$contig_hash);
	 				my ($total_bases_that_match_blast_hit, @total_len_of_contig_that_match_blast_hit)		= _get_total_bases_that_match_blasthit_in_contigs(\@contigs,$contig_hash);

	 				my $percent_identity = sprintf('%.2f', ($total_bases_that_match_blast_hit/$total_bases_contigs) * 100 ).' %';
					
					my $num_contigs	 	= $TAXA_TREE->{$_}->{'count'};
					my $SD_kmer_cov 	= _get_SD(\@kmer_cov_per_contig);
					my $mean_kmer_cov	= _get_mean(\@kmer_cov_per_contig);
					
					my $SD_GC 		= _get_SD(\@GC_content);
					my $mean_GC		= _get_mean(\@GC_content);


					print "\t"x($current_level-1), "$_ \t  $num_contigs \t ",$percent_identity,"\t", commify($total_bases_contigs),"\t",$mean_kmer_cov,'+/-',$SD_kmer_cov,"\t\t",$mean_GC,'+/-',$SD_GC,"\n";
					
					write_taxa_distribution_file($OUT_TAXA_FILE, $current_level,$_,\@GC_content,\@kmer_cov_per_contig,\@lenth_per_contig,\@total_len_of_contig_that_match_blast_hit); 
			
					analyse_contig_tree_recursively($TAXA_TREE->{$_} ) if ($current_level < $MAX_TAXA_LEVEL );

		}
		else {
			$LOGGER->fatal("$_ doesn't have a hash reference in the TAX_TREE hash. Possible mistake in the TAXA_TREE  construction \n");
		}
		
	}
	
}





sub write_taxa_distribution_file {
	
	my $SUB = 'write_taxa_distribution_file';
	
	my $filehandle = shift @_;
	my $current_level = shift @_;
	my $taxa = shift @_;
	my @GC_content = @{shift @_};
	my @kmer_cov_per_contig = @{shift @_};
	my @length_per_contig	= @{shift @_};
	my @total_len_of_contig_that_match_blast_hit = @{shift @_};
	
#	Ideally the num of elements in @GC_content and @kmer_cov_per_contig should be same 
	for( my $i = 0 ; $i <= $#GC_content ; $i++ ){
		print $filehandle "$current_level \t $taxa \t $GC_content[$i] \t $kmer_cov_per_contig[$i] \t $length_per_contig[$i] \t $total_len_of_contig_that_match_blast_hit[$i]\n";
	}
}


#return the hash with contig_name(key) with GC_percent, length, kmer_coverage
sub get_contig_GC_content_from_file {
	
	my $SUB = 'get_contig_GC_and_kmer_cov';
	
	$LOGGER->info("IN SUB : $SUB ");
	my $contig_GC_content_file = 'contigs.residues';
		
	#generate the contigs.residues file
	if( ! -e $contig_GC_content_file  )	{
		if ( -e 'contigs.fa') {
			my $command =  qq ( cat contigs.fa | /house/homedirs/a/apratap/dev/eclipse_workspace/perl_scripts/edit_fasta.pl -cr > contigs.residues);	
			execute_system_command($LOGGER,$command);	
		}
		else{
			die "Error[$SUB] Expected input file contigs.fa which is output of velvet not present \n";
		}
	}
	else{
		$LOGGER->info("Using the file $contig_GC_content_file for GC and Kmer information");
	}
	
		
	### Read the file to get the GC content of contig
	open(GC_CONTENT, "< $contig_GC_content_file ") || die "Error readin file $contig_GC_content_file \n";
	
	my $GC_CONTENT_HASH;
	my $contig_count = 0;
	$LOGGER->info("Reading $contig_GC_content_file  file ");
	
	while (<GC_CONTENT>) {
		chomp $_;
		# for counting for how many contigs GC content has been read
		$contig_count++;
		
		my @temp = split ("\t",$_);
		$temp[0] =~/^NODE_(\d+?)_.*$/;
		my $contig_name = $1;
		$contig_name =~s/\s//g;
		
		my $contig_GC_percent = $temp[8];		
		$contig_GC_percent =~s/\s//g;
		
		my $contig_length = $temp[1];		
		$contig_length =~s/\s//g;
		
		my $contig_kmer_coverage = $temp[11];		
		$contig_kmer_coverage =~s/\s//g;
		
		
		unless (exists $GC_CONTENT_HASH->{$contig_name}) {
			$GC_CONTENT_HASH->{$contig_name}->{'GC_percent'}		= $contig_GC_percent;
			
		}
		else{
			$LOGGER->warn("WARN[$SUB] Contig $contig_name seen twice in file $contig_GC_content_file" );
		}
	}
	
	$LOGGER->info("Stored GC content for $contig_count contigs in GC_CONTENT_HASH");
	$LOGGER->info("OUT SUB : $SUB ");

	return $GC_CONTENT_HASH;
}

sub _get_total_bases_in_contigs {

	my $SUB = '_get_total_bases_in_contigs';


	my @contigs 		= 	@{shift @_};
	my $contig_hash 	=	shift @_;

	my $total_bases_in_contigs;
	my @length_per_contig;

	foreach my $contig (@contigs) {

		unless(defined $contig_hash->{$contig} ) { 
			$LOGGER->fatal("[$SUB] Contig $contig should have been present in contig_hash..check SUB : properly_annotate_contigs which generates this hash\n");
		}

		my $contig_actual_len = $contig_hash->{$contig}->{'contig_kmer_length'} + $KMER_LEN -1;

		$total_bases_in_contigs +=  $contig_actual_len;	
		push(@length_per_contig, $contig_actual_len);
	}

	return ($total_bases_in_contigs,\@length_per_contig)


}

sub _get_GC_content_in_contigs {

	my $SUB = '_get_GC_content_in_contigs';


	my @contigs 		= 	@{shift @_};
	my $contig_hash 	=	shift @_;

	my @GC_content_per_contig;

	foreach my $contig (@contigs) {

		unless(defined $contig_hash->{$contig} ) { 
			$LOGGER->fatal("[$SUB] Contig $contig should have been present in contig_hash..check SUB : properly_annotate_contigs which generates this hash\n");
		}

		my $GC_content = $contig_hash->{$contig}->{'GC_percent'};

		push(@GC_content_per_contig,$GC_content);
	}

	return @GC_content_per_contig;



}



sub _get_mean(){
	my $SUB  = '_get_mean()';
	
	my @data = @{shift @_};	
	my $stats = new Statistics::Descriptive::Discrete;
	$stats->add_data(@data);
	return sprintf("%.2f", $stats->mean())|| 'NA';
}


sub _get_SD(){
	my $SUB  = '_get_variation()';

	my @data = @{shift @_};
	my $stats = new Statistics::Descriptive::Discrete;
	$stats->add_data(@data);
	return sprintf("%.2f", $stats->standard_deviation()) || 'NA';
}

sub _get_total_bases_that_match_blasthit_in_contigs {

	my $SUB = '_get_total_bases_that_match_blasthit_in_contigs';


	my @contigs 		= 	@{shift @_};
	my $contig_hash 	=	shift @_;

	my $total_contig_bases_that_match_blast_hit;
	my @total_bases_that_match_per_contig;

	foreach my $contig (@contigs) {

		unless(defined $contig_hash->{$contig} ) { 
			$LOGGER->fatal("[$SUB] Contig $contig should have been present in contig_hash..check SUB : properly_annotate_contigs which generates this hash\n");
		}

		my $contig_actual_matching_bases  = $contig_hash->{$contig}->{'match_length'};

		$total_contig_bases_that_match_blast_hit +=  $contig_actual_matching_bases;	
		
		push(@total_bases_that_match_per_contig, $contig_actual_matching_bases);

	}

	return ($total_contig_bases_that_match_blast_hit,@total_bases_that_match_per_contig);

}

sub _get_length_per_contig {

	my $SUB = '_get_length_per_contig';


	my @contigs 		= 	@{shift @_};
	my $contig_hash 	=	shift @_;

	my @length_per_contig;	

	foreach my $contig (@contigs) {

		unless(exists $contig_hash->{$contig} ) { 
			$LOGGER->fatal("[$SUB] Contig $contig should have been present in contig_hash..check SUB : properly_annotate_contigs which generates this hash\n");
		}

		my $kmer_coverage = $contig_hash->{$contig}->{'length'} ;
		push(@length_per_contig,$kmer_coverage);
	}
	return @length_per_contig;

}

sub _get_kmer_cov_per_contig {

	my $SUB = '_get_kmer_cov_per_contig';


	my @contigs 		= 	@{shift @_};
	my $contig_hash 	=	shift @_;

	my @kmer_coverge_per_contig;	

	foreach my $contig (@contigs) {

		unless(exists $contig_hash->{$contig} ) { 
			$LOGGER->fatal("[$SUB] Contig $contig should have been present in contig_hash..check SUB : properly_annotate_contigs which generates this hash\n");
		}

		my $kmer_coverage = $contig_hash->{$contig}->{'contig_kmer_coverage'} ;
		push(@kmer_coverge_per_contig,$kmer_coverage);
	}
	return @kmer_coverge_per_contig;

}

sub _get_num_reads_from_contigs_kmer_cov {

	my $SUB = '_get_num_reads_from_contigs_kmer_cov';
	my @contigs 		= 	@{shift @_};
	my $contig_hash 	=	shift @_;

	my $approx_num_reads;

	foreach my $contig (@contigs) {
		unless(exists $contig_hash->{$contig} ) { 
			$LOGGER->fatal("[$SUB] Contig $contig should have been present in contig_hash..check SUB : properly_annotate_contigs which generates this hash\n");
		}
		my $nucleotide_coverage = ( $contig_hash->{$contig}->{'contig_kmer_coverage'} * $READ_LEN )/ ( $READ_LEN-$KMER_LEN );
		$approx_num_reads +=  sprintf( '%0.0f', $nucleotide_coverage/$READ_LEN  );	
	}
	return $approx_num_reads;
}



sub number_sanity_check {

	my $SUB = 'number_sanity_check';

	my $number = shift @_;
	my $contig_line = shift @_;
	my @contig_name = split (' ',$contig_line);

	if ($number=~ /^\d+[\.\d]+$/ ) {
		return $number;
	}
	else {
		print STDERR "The input is not numeric \n $contig_name[0] \t $number\n";
		return 0;
	}
}