#!/jgi/tools/bin/perl

#========================================================================================
#Disclaimer: ----- 
#This program is written by Abhishek Pratap (apratap@lbl.gov)
#Script Name : clean_fastq.pl
#======================================================================================== 


use strict;
use Data::Dumper;
use POSIX 'isatty';
use Getopt::Long;
use FindBin;
use lib "$FindBin::Bin/lib";
use JGI::QAQC::Utilities;


use vars qw ($substring $substring_rev_comp $num_mismatches $min_overlap_at_the_end_of_read $min_trimmed_read_len);



main();


sub main {

	check_fastq_input();
	 
	$substring 			= 'CTGCTGTACCGTACATCCGCCTTGGCCGTACAGCAG';
	$substring_rev_comp = 'CTGCTGTACGGCCAAGGCGGATGTACGGTACAGCAG';
	
	$num_mismatches = 2;
	$min_overlap_at_the_end_of_read = 10;
	$min_trimmed_read_len = 25;
	 
    read_parse_input();
}



sub check_fastq_input() {
	if ( isatty(*STDIN) ) {
		 usage();
	}
}



sub usage {
	my $SUB = 'usage';
	
my $USAGE = <<USAGE;

#Usage:
cat <fastq_file_with_linker> | $0 
USAGE

print $USAGE;
exit 2;

}

sub read_parse_input {
    my $SUB='read_parse_input';

	my $total_read_pairs_processed =0;
	my $read_pairs_with_linker = 0;
	my $read_pairs_with_linker_direction_determined = 0;
	my $read_pairs_with_linker_gt_min_length = 0;
	my $read_pairs_with_linker_on_one_mate = 0;
	my $reads_with_no_linker =0;
	
	# 
	my $read_1_where_linker_found = 'read_1_with_linker_min_length'.$min_trimmed_read_len.'_minoverlap_'.$min_overlap_at_the_end_of_read.'.txt';	
	my $read_2_where_linker_found = 'read_2_with_linker_min_length'.$min_trimmed_read_len.'_minoverlap_'.$min_overlap_at_the_end_of_read.'.txt';	
	my $good_read_pairs_linker_cleaned = 'read_pairs_linker_cleaned_min_readLen_'.$min_trimmed_read_len.'_minoverlap_'.$min_overlap_at_the_end_of_read.'.fastq';
	my $read_pairs_with_linker_found_on_one_mate = 'read_pairs_with_linker_found_on_one_mate_min_readLen_'.$min_trimmed_read_len.'_minoverlap_'.$min_overlap_at_the_end_of_read.'.fastq';
	my $good_read_pairs_linker_cleaned_rev_comp = 'read_pairs_linker_cleaned_revComp_min_readLen_'.$min_trimmed_read_len.'_minoverlap_'.$min_overlap_at_the_end_of_read.'.fastq';
	my $bad_read_pairs_with_no_linker = 'bad_read_pairs_with_no_linker_found.fastq';
	
	
	
	#open all the file handles
	open (out_matched_read_1, ">$read_1_where_linker_found") || die "ERROR[$SUB] File $read_1_where_linker_found can't be opened \n";
	open (out_matched_read_2, ">$read_2_where_linker_found") || die "ERROR[$SUB] File   $read_1_where_linker_found can't be opened \n";
	open (FH_out_good_read_pairs_where_linker_found_file, ">$good_read_pairs_linker_cleaned") || die " ERROR[$SUB] File  $good_read_pairs_linker_cleaned can't be opened \n";
	open (FH_out_read_pairs_where_linker_found_on_one_mate_only, ">$read_pairs_with_linker_found_on_one_mate") || die "ERROR[$SUB] File $read_pairs_with_linker_found_on_one_mate  can't be opened \n";
	open (FH_out_good_read_pairs_rev_comp_where_linker_found_file, ">$good_read_pairs_linker_cleaned_rev_comp") || die " ERROR[$SUB] File  $good_read_pairs_linker_cleaned_rev_comp can't be opened \n";
    open (FH_out_bad_read_pairs_no_linker, ">$bad_read_pairs_with_no_linker") || die " ERROR[$SUB] File  $bad_read_pairs_with_no_linker can't be opened \n";
    
    
    
    while(<STDIN>) {
     $total_read_pairs_processed++;
     if ($total_read_pairs_processed % 10000 == 0){
     	print "Processed $total_read_pairs_processed read pairs \n";
     }
  	chop $_;
		my ($read_1_header, $read_1_quality, $read_1, $full_read_1);
		my ($read_2_header, $read_2_quality, $read_2, $full_read_2);
		
		if(/^$/) { next;}
		
		#get read 1
		if(/^@.*\/1/){
			$read_1_header = $_;
			chomp $read_1_header;
				
			$read_1 =<STDIN>;
			chomp $read_1;
			
			my $junk_line = <STDIN>;  #quality header read 1
			$read_1_quality = <STDIN>  ; #quality line read 1
			chomp $read_1_quality;
			$full_read_1 = "$read_1_header"."\n" ."$read_1"."\n".'+'."\n"."$read_1_quality"."\n";
	
			$read_2_header = <STDIN>;
			chomp $read_2_header;

				if( $read_2_header =~/^@.*\/2/ ){
					$read_2 =<STDIN>;
					chomp $read_2;
					$junk_line = <STDIN>;  #quality header read 2
					$read_2_quality = <STDIN>  ; #quality line read 2
					chomp $read_2_quality;
					$full_read_2 = "$read_2_header"."\n" ."$read_2"."\n".'+'."\n"."$read_2_quality"."\n";
				}
		}
		
			#Read 1
			# searching for query string
#			print "$read_1_header\n";
			my ($all_match_start_positions_read_1_ref,$num_mistmatch_per_start_position_read_1_ref)   = find_substring_with_n_mismatches($read_1,$substring,$substring_rev_comp,$num_mismatches, $min_overlap_at_the_end_of_read);
			my @all_match_start_positions_read_1 		= @$all_match_start_positions_read_1_ref;
			my @num_mistmatch_per_start_position_read_1	= @$num_mistmatch_per_start_position_read_1_ref;
			
			#Read 2
#			print "$read_2_header\n";
			# searching for query string
			my ($all_match_start_positions_read_2_ref,$num_mistmatch_per_start_position_read_2_ref)   = find_substring_with_n_mismatches($read_2,$substring,$substring_rev_comp,$num_mismatches, $min_overlap_at_the_end_of_read);
			my @all_match_start_positions_read_2		= @$all_match_start_positions_read_2_ref;
			my @num_mistmatch_per_start_position_read_2	= @$num_mistmatch_per_start_position_read_2_ref;
			
			
			
			##read1 / read 2 both have linker in them
			if (  ( @all_match_start_positions_read_1  )  && (@all_match_start_positions_read_2) ) {
			
				$read_pairs_with_linker++;
			
				my $best_match_start_position_read1 = $all_match_start_positions_read_1[0];
				my $best_match_start_position_read2 = $all_match_start_positions_read_2[0];
	
				
				my ($pre_linker_nuc_dimer_read1)     			= substr($read_1, ($best_match_start_position_read1-2), 2 );
				my $trimmed_read_1_without_linker				= substr($read_1, 0, ($best_match_start_position_read1-2) );   ## removing the pre-linker nucleotide also thus extra -2
				my $trimmed_read_1_quality						= substr($read_1_quality,0,($best_match_start_position_read1-2)  );
				
			
				my ($pre_linker_nuc_dimer_read2)     	= substr($read_2, ($best_match_start_position_read2-2), 2 );
				my $trimmed_read_2_without_linker				= substr($read_2, 0, ($best_match_start_position_read2-2) );
				my $trimmed_read_2_quality						= substr($read_2_quality,0,($best_match_start_position_read2-2));
				
				
				
				##annotate read header with direction
				my $result = annotate_reads_header(\$read_1_header,\$read_2_header,$pre_linker_nuc_dimer_read1,$pre_linker_nuc_dimer_read2);
				if ($result) { $read_pairs_with_linker_direction_determined++; }
				
				
				print out_matched_read_1"$read_1_header \t ",join(',',@all_match_start_positions_read_1), "\t", join(',' , @num_mistmatch_per_start_position_read_1), "\t $pre_linker_nuc_dimer_read1\n"; 
				print out_matched_read_2 "$read_2_header \t ",join(',',@all_match_start_positions_read_2 ),"\t",join(',', @num_mistmatch_per_start_position_read_2),"\t $pre_linker_nuc_dimer_read2\n"; 
				
				
				# only print to fastq if min read length condition is met
				
					if  (   ( $best_match_start_position_read1 >= $min_trimmed_read_len )  && ( $best_match_start_position_read2 >= $min_trimmed_read_len) ) {
						
						$read_pairs_with_linker_gt_min_length++;
						
						print FH_out_good_read_pairs_where_linker_found_file "$read_1_header\n$trimmed_read_1_without_linker\n".'+'."\n$trimmed_read_1_quality\n";
						print FH_out_good_read_pairs_where_linker_found_file "$read_2_header\n$trimmed_read_2_without_linker\n".'+'."\n$trimmed_read_2_quality\n";
								
						# printing the rev comp of the read and reversing the quality				
						print FH_out_good_read_pairs_rev_comp_where_linker_found_file "$read_1_header\n".rev_comp_dna($trimmed_read_1_without_linker)."\n".'+'."\n".reverse($trimmed_read_1_quality)."\n";
						print FH_out_good_read_pairs_rev_comp_where_linker_found_file "$read_2_header\n".rev_comp_dna($trimmed_read_2_without_linker)."\n".'+'."\n".reverse($trimmed_read_2_quality)."\n";
						
					}				
			}
			
		
			
			##read 1 has linker and read 2 doesn't
			elsif ( ( @all_match_start_positions_read_1     ) && ( ! @all_match_start_positions_read_2  ) ) {
				
				$read_pairs_with_linker_on_one_mate++;
				
#				Formulating the trimmed read 1				
				my $best_match_start_position_read1 = $all_match_start_positions_read_1[0];
				
				my ($pre_linker_nuc_dimer_read1)     			= substr($read_1, ($best_match_start_position_read1-2), 2 );
				my $trimmed_read_1_without_linker				= substr($read_1, 0, ($best_match_start_position_read1-2) );
				my $trimmed_read_1_quality						= substr($read_1_quality,0,($best_match_start_position_read1-2));
				
				##annotate read header with direction
				annotate_reads_header(\$read_1_header,\$read_2_header,$pre_linker_nuc_dimer_read1,'NN');

				print out_matched_read_1"$read_1_header \t ",join(',',@all_match_start_positions_read_1), "\t", join(',' , @num_mistmatch_per_start_position_read_1), "\t $pre_linker_nuc_dimer_read1\n"; 
				
				
				if  (   $best_match_start_position_read1 >= $min_trimmed_read_len    ) {
#					printing the trimmed read 1 as the linker was found
					print FH_out_read_pairs_where_linker_found_on_one_mate_only "$read_1_header\n$trimmed_read_1_without_linker\n".'+'."\n$trimmed_read_1_quality\n";
				
#					Printing read 2 as is , as no linker was found within our search threshold
					print FH_out_read_pairs_where_linker_found_on_one_mate_only "$read_2_header\n$read_2\n".'+'."\n$read_2_quality\n";
				}
				
			}
			
			##read 2 has linker and read 1 doesn't
			elsif ( ( ! @all_match_start_positions_read_1     ) && (  @all_match_start_positions_read_2  ) ) {
				
				$read_pairs_with_linker_on_one_mate++;
		
#				Formulating the trimmed read 2
				

				my $best_match_start_position_read2 = $all_match_start_positions_read_2[0];

				my ($pre_linker_nuc_dimer_read2)     	= substr($read_2, ($best_match_start_position_read2-2), 2 );
				my $trimmed_read_2_without_linker				= substr($read_2, 0, ($best_match_start_position_read2-2) );
				my $trimmed_read_2_quality						= substr($read_2_quality,0,($best_match_start_position_read2-2));

				##annotate read header with direction
				annotate_reads_header(\$read_1_header,\$read_2_header,'NN',$pre_linker_nuc_dimer_read2);
				
			   	print out_matched_read_2 "$read_2_header \t",join(',',@all_match_start_positions_read_1 ),"\t",join(',', @num_mistmatch_per_start_position_read_2),"\t $pre_linker_nuc_dimer_read2\n"; 
			
			
				if  (   $best_match_start_position_read2 >= $min_trimmed_read_len   ) {
				
#					Printing read 1 as is as no linker was found within our search threshold
					print FH_out_read_pairs_where_linker_found_on_one_mate_only "$read_1_header\n$read_1\n".'+'."\n$read_1_quality\n";
			
#					printing the trimmed read 2 as the linker was found
					print FH_out_read_pairs_where_linker_found_on_one_mate_only "$read_2_header\n$trimmed_read_2_without_linker\n".'+'."\n$trimmed_read_2_quality\n";
				
				}
			
			
			}
			else {
				
#				Linker is not present in either read 1 or read 2 or both reads
				$reads_with_no_linker++;
				
				##annotate read header with direction
				annotate_reads_header(\$read_1_header,\$read_2_header,'NN','NN');
				
#				Printing read 1 as is as no linker was found within our search threshold
				print FH_out_bad_read_pairs_no_linker "$read_1_header\n$read_1\n".'+'."\n$read_1_quality\n";
				
#				Printing read 2 as is as no linker was found within our search threshold
				print FH_out_bad_read_pairs_no_linker "$read_2_header\n$read_2\n".'+'."\n$read_2_quality\n";
				
			}
	
		
	} ## END while loop




open(OUT_SUMMARY, ">linker_processing_summary.txt");

my $SUMMARY;

$SUMMARY = 
qq{
=======================================================================
Summary
Total number of read pairs processed \t\t\t $total_read_pairs_processed
Total number of pairs in which linker is found \t\t\t $read_pairs_with_linker 
Total number of pairs in which linkder is found and direction determined \t\t\t $read_pairs_with_linker_direction_determined
Total Number of pairs in which linker is found and trimmed read > $min_trimmed_read_len \t\t\t $read_pairs_with_linker_gt_min_length 
Total Number of pairs where linker found on one mate \t\t\t $read_pairs_with_linker_on_one_mate
Total Individual read where NO linker is found \t\t\t $reads_with_no_linker 
};

print $SUMMARY;
print OUT_SUMMARY $SUMMARY;

close OUT_SUMMARY
	
}




sub annotate_reads_header {
	
	
	my $SUB = 'annotate_reads_header';
	
	my $read_1_header_reference = shift @_;
	my $read_2_header_reference = shift @_;
	
	my $pre_linker_nuc_dimer_read1 = shift @_;
	my $pre_linker_nuc_dimer_read2 = shift @_;

	${$read_1_header_reference} =~/^(.+?)\/(\d{1}).*$/;
	my $read_header = $1;
#	print "$$read_1_header_reference \n";
#	print "Read header \t $1 \n";
	


	my $pre_linker_dimer_5_prime	 = 	{ 	
											'AC' 	=> 1,
											'CC'	=> 1,
											'CT'	=> 1,
											'GC'	=> 1,
											'TC'	=> 1,
											'TG'	=> 1,	  
										};
											
											
	my $pre_linker_dimer_3_prime	 = 	{ 	
											'AA' 	=> 1,
											'CA'	=> 1,
											'GA'	=> 1,
											'TA'	=> 1,
										};
	
	
	
	
	
	
	
	
	if(  $pre_linker_dimer_5_prime->{$pre_linker_nuc_dimer_read1}  ) {
		
		##fixing the headers
		${$read_1_header_reference} = $read_header.'_dir=5_prime/1';
		${$read_2_header_reference} = $read_header.'_dir=3_prime/2';
		
		return 1
	}
	
	elsif (  $pre_linker_dimer_5_prime->{$pre_linker_nuc_dimer_read2}  ) {
		
		##fixing the headers
		${$read_1_header_reference} = $read_header.'_dir=3_prime/1';
		${$read_2_header_reference} = $read_header.'_dir=5_prime/2';
		
		return 1
		
	}
	elsif (  $pre_linker_dimer_3_prime->{$pre_linker_nuc_dimer_read1}  ) {
		
		##fixing the headers
		${$read_1_header_reference} = $read_header.'_dir=3_prime/1';
		${$read_2_header_reference} = $read_header.'_dir=5_prime/2';
		
		return 1
		
	}
	elsif (  $pre_linker_dimer_3_prime->{$pre_linker_nuc_dimer_read2}  ) {
		
		##fixing the headers
		${$read_1_header_reference} = $read_header.'_dir=5_prime/1';
		${$read_2_header_reference} = $read_header.'_dir=3_prime/2';
		
		return 1
		
	}
	else {
		
		##fixing the headers
		${$read_1_header_reference}  = $read_header.'_dir=not_determined/1';
		${$read_2_header_reference}  = $read_header.'_dir=not_determined/2';
		
		return 0
	}
	
}








sub find_substring_with_n_mismatches {
	
	my $SUB = 'find_substring_with_n_mismatches';

#	$ss is the search string
#	$qs is the query string
	
	my ($ss, $qs, $qs_rev_comp, $max_mismatch, $min_overlap_at_the_end_of_read) = @_;
	
	my $m = my @ss 			= unpack('(a)*', $ss);
	my $n = my @qs 			= unpack ('(a)*', $qs);
	my @qs_rev_comp	= unpack ('(a)*', $qs_rev_comp);
	
	my $substring_location = 0;
	my $all_possible_matches;
	
	my @all_match_start_positions;
	my @all_num_mistmatch_per_start_position;


#   main comparison here
	OUTER:
	for my $i (0 .. $m-1) {
		
#		print "@ss[$i..$m-1] \n";
		
		my $num_mismatches=0;
		
		my $bases_left_in_search_string_to_compare = $m-1-$i;
		
		
#		The following will allow to compare only right number of bases in the substring when it is nearing the end of main search string
#		eg:
#		AACATGTAGCTGATCTACGTGATCGTAGCTGATCATCGTAGTCTACGTGATCGATCGTAGCTGTACGTCAG
#														   CGATCGTAGCTGTACGTCAGACGATCGTATCGATCATCGA
#	                                                                           -------------------- ( these bases from the query string will not be used for comparison)

		my $bases_to_match_in_query_string = $bases_left_in_search_string_to_compare > $n-1 ? $n-1 : $bases_left_in_search_string_to_compare;
		
		for my $j (0..$bases_to_match_in_query_string) {
			
				if ( ($ss[$i+$j] ne $qs[$j]) && ($ss[$i+$j] ne $qs_rev_comp[$j])  ) {
					++$num_mismatches;
				}
					
			next OUTER if $num_mismatches > ($max_mismatch );
			next OUTER if $bases_left_in_search_string_to_compare < $min_overlap_at_the_end_of_read ; 
			
		}
		
#		storing all the cases where the query aligns with the search string with mismatch 
		push(@all_match_start_positions,$i);
		push(@all_num_mistmatch_per_start_position,$num_mismatches);
		
	} ## END for loop to go over each character in search string
		
#	print Dumper($all_possible_matches);
	
	return (\@all_match_start_positions, \@all_num_mistmatch_per_start_position);
	
}



sub is_right_pair_based_on_header {
	
	
	my $SUB = 'is_right_pair_based_on_header';
	
	my $header_1 = shift @_;
	my $header_2 = shift @_;
	
	
	$header_1 =~s/\/1//;
	$header_2 =~s/\/2//;
	
	
#	print "$header_1 \t\t $header_2 \n";
	
	if ( $header_1 == $header_2 ){
		
#		print "\n 1 ";
		return 1;
		
	} 
	
	else {
		return 0;
	}
	
}



