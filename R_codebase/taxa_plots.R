
##Plot1
qplot( GC_percent,data=level_0_taxa, facets=taxa ~ ., colour=taxa)


##Plot 2
png("GC_percent_vs_Kmer_cov.png",width=800,height=600)
qplot(GC_percent,kmer_cov, data=level_0_taxa, facets= taxa ~ . , ylim=c(0,100) , colour=taxa)
dev.off()



##Plot 3
png("Contig_len_vs_Kmer_cov.png",width=800,height=600)
qplot(len,kmer_cov, data=level_0_taxa, facets= taxa ~ . , ylim=c(0,100) , colour=taxa, main="Contig_Len v/s Kmer_Coverage")
dev.off()


##Plot 4
png("Contig_Len_vs_logKmer_Cov.png", width=800, height=600, pointsize=14)
qplot(len,log(kmer_cov), data=level_0_taxa,  colour=taxa, main="Contig_Len v/s log(Kmer_Coverage)")
 dev.off()


##Plot 5
png("GC_percent_vs_Contig_len.png", width=800, height=600, pointsize=14)
qplot(len,GC_percent, data=level_0_taxa,  colour=taxa, main="Contig_Len v/s GC_percent",xlim=c(500, max(level_0_taxa$len)) ) 
dev.off()


##Plot 6




