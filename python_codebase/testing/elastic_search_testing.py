#!/usr/bin/env python

import sys
import time
from pyes import  *

conn = ES(['128.55.54.149:9200','128.55.54.149:9201'],timeout=20)

#
#try:
#    conn.delete_index("test-index")
#except:
#    print 'No index by name test-index exists'
#
#
#
#conn.create_index("test-index")
#
#conn.index({"name":"Joe Tester", "parsedtext":"Joe tester nice guy", "uuid":"1111","position":1},"test-index","test-type",1)
#conn.index({"name":"Bill Baloney", "parsedtext":"Joe Testere nice guy", "uuid":"22222", "position":2}, "test-index", "test-type", 2)
#
#
#conn.refresh(["test-index"])
#
#
#q = TermQuery("parsedtext","tester")
#results = conn.search(query=q)
#
#for r in results:
#    print r
#    
    
#print conn.cluster_health()
#print conn.cluster_state()
#print conn.cluster_nodes()
#print conn.collect_info()

print conn.get_indices()

taxa_names = []

with open(sys.argv[1]) as fh:
    taxa_names=[x.strip() for x in fh]
    
print 'Total names in list %d' % (len(taxa_names));

#q1 = TermQuery("tax_name","abhishek")
#results = conn.search({"name":"Abhishek"})
#for r in results:
#    print r

loop_start_time = time.clock()

for num,name in enumerate(taxa_names):
    name=name.lower()
    
    q1 = TermQuery("tax_name",name)
    results  = conn.search(query=q1)
#    print '%s : %d hits' % (name, len(results))
    
    if num % 100000 == 0 and num >0:
        loop_check_point = time.clock()
        print 'processed %d names in %d secs' % (num,loop_check_point - loop_start_time);
    
    
    if len(results) == 0:
        print 'No hits for %s' % name    
        
    
    
            
    
    