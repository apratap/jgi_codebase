#!/usr/bin/env python


import os
import sys


##updating the library look up path
dir = os.path.dirname(__file__)
sys.path.append(os.path.join(dir,'../lib'));
import seqUtils
import fastqUtils



sequence = 'ACG'
print seqUtils.mutate_kmers(sequence, num_errors=1)


for kmer_len in xrange(3,4):
        kmers = seqUtils.get_kmers_dict_with_one_error(sequence, kmer_len)
        print 'Kmer : %d , found %d kmers' % (kmer_len, len(kmers))
        print kmers
