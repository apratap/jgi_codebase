#!/usr/bin/env python

import os
import sys
import time
from whoosh.fields import Schema, TEXT, STORED
from whoosh.index import create_in, open_dir
from whoosh.query import *
from whoosh.qparser import QueryParser



file=sys.argv[1]

def whatisthis(s):
    if isinstance(s, str):
        print "ordinary string"
    elif isinstance(s, unicode):
        print "unicode string"
    else:
        print "not a string"




#creating the index
if not os.path.exists("index"):
    os.mkdir("index")

schema = Schema(tax_id=STORED, name=TEXT(stored=True))
ix =create_in("index",schema)
writer = ix.writer()


prog_start = time.clock()
with open(file) as fh:
    for num,line in enumerate(fh):
        split_line = line.strip().split('|')
        tax_id = split_line[0].strip()
        name = split_line[1].strip()
        if num % 10000 == 0 and num > 0:
            loop_check_time = time.clock()
            sys.stderr.write('inserted %s records in whoosh index in %d seconds\n' % (num,loop_check_time-prog_start))
            
        writer.add_document(tax_id=tax_id,name=unicode(name))
    
    loop_break_time = time.clock()
    sys.stderr.write('inserted %s records in whoosh index in %d seconds\n' % (num,loop_break_time-prog_start))        
    sys.stderr.write('Committing %s records in whoosh index\n' % (num))
    writer.commit()
    commit_finish_time = time.clock()
    sys.stderr.write('Commited %s records in whoosh index in %d seconds\n' % (num,commit_finish_time-prog_start))
    
    
ix.close()
#
#ix=open_dir("index")
#with ix.searcher() as searcher:
#    query = QueryParser("name", ix.schema).parse(u'putrefaciens')
#    results = searcher.search(query)
#    for result in results:
#        print result










#writer.add_document(tax_id="17",name=u"Methyliphilus methylitrophus")
#writer.add_document(tax_id="17",name=u"Methylophilus methylotrophus Jenkins et al. 1987")
#writer.add_document(tax_id="45",name=u"Chondromyces lichenicolus")
#writer.commit()



'''
with ix.searcher() as searcher:
    query = QueryParser("name",ix.schema).parse(u'Jenkins')
    results = searcher.search(query)
    for result in results:
        print result
'''