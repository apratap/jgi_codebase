#!/jgi/tools/bin/python


__author__ = "Abhi (apratap@lbl.gov)"
__version__ = "$Revision: 1.2 $"
__date__ = "$Date: March 10 2011$"
__copyright__ = "Copyright (c) 2011 Abhishek Pratap"
__license__ = "Python"


def buildConnectionString(params):
    """ Build a connection string from dictionary of parameters.
    
    
    Returns string ."""
    return ";".join("%s=%s" % (k,v) for k,v in params.items())

if __name__ == "__main__":
    myParams =  {"server":"mpilgrim", \
                 "database":"master", \
                 "uid":"sa" , \
                 "pwd":"secret" \
                 }
    print buildConnectionString(myParams)