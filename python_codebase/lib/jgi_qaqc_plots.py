#!/usr/bin/env python

import sys
import os
import datetime
import pprint
import matplotlib.pyplot as plt
import decimal
import datetime
import pandas
import numpy as np
import cStringIO
from matplotlib.font_manager import FontProperties
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.table import Table


##updating the library look up path
dir = os.path.dirname(__file__)
sys.path.append(os.path.join(dir,'../lib'));
import db_access




def draw_plate_plot(data,plot_title=None,min_val=None, max_val=None,
                    fmt='{:.2f}', bkg_colors=['#FFFF99','white'],axis=None,
                    row_labels=None,col_labels=None,**kwargs):
    """
    TO BE UPDATED
    given a numpy array generated from create_data_array{function}
    create a plate level plot
    """
    if axis is None:
        fig = plt.figure(figsize=(10,8))
        axis= plt.subplot(1,1,1)
        
    axis.set_axis_off()
    axis.set_title(plot_title,verticalalignment='center')
    tb = Table(axis,bbox=[0,0,1,1])
    
    #get the dimension of the numpy array
    nrows, ncols = data.shape
    
    #width and height of each cell
    width, height = 1.0/ncols, 1.0/nrows
    
    for (i,j), val in np.ndenumerate(data):
        ## if  i is even we want even cols in the row to be colored
        ## if i is odd we want odd cols in the row to be colored
        idx = [ j %2, (j+1) % 2][ i % 2]
        color = bkg_colors[idx]

        if np.isnan(val):
            color = 'grey'
        if max_val and val >= max_val:
            color = 'red'
        if min_val and val <= min_val:
            color = 'red'
        tb.add_cell(i,j,width,height,text=fmt.format(val),loc='center',facecolor=color)
        
    #col labels
    if col_labels is None:
        col_labels = range(1,(ncols+1)) 
    for j,label in enumerate(col_labels):
        #print j, label
        #tb.add_cell(-1,j, width, height/2, text=label,loc='center',edgecolor="none",facecolor="none")
        tb.add_cell(nrows,j, width, height/2, text=label,loc='center',edgecolor="none",facecolor="none")
        
    #row labels
    if row_labels is None:
        row_labels = ['A','B','C','D','E','F','G','H']
    for i,label in enumerate(row_labels):
        tb.add_cell(i,-1, width/2, height, text=label,loc='center',edgecolor="none",facecolor="none")
    axis.add_table(tb)
    

#   Q20 read length plot    
def get_Q20_read_plot(libs_summary_pandas_df,axis=None,plate_view=False,**kwargs):
    
    if axis is None:
        fig = plt.figure()
        axis = fig.add_subplot(111)
    
    
    if plate_view:
        print libs_summary_pandas_df.pivot_table(rows=['row'],cols=['col'],values=['min_q20'],aggfunc=lambda x: float(x.values[0]))
        x=np.array(libs_summary_pandas_df.pivot_table(rows=['row'],cols=['col'],values=['min_q20'],aggfunc=lambda x: float(x.values[0])))
        x=x.astype(float)
        return draw_plate_plot(x,axis=axis,**kwargs)
    else:
        ax1 = libs_summary_pandas_df[['read1_length','read2_length']].plot(kind='line',ax=axis)
        ax2 = libs_summary_pandas_df[['read1_q20','read2_q20']].plot(kind='bar',ax=axis,color=['#377EB8','#4DAF4A'])
        ax2.set_ylabel('read length')
        fontP = FontProperties()
        fontP.set_size('small')
        ax2.legend(loc='upper right',prop=fontP,bbox_to_anchor=(1.13,1))
        ax2.axhline(y=100,color='#E41A1C',linestyle='dashed',linewidth=3)
        ax2.set_xlabel('')
    
  
    if axis is None:
        return fig

def get_read_num_plot(libs_summary_pandas_df,axis=None):
    #reads for each lib
    if axis is None:
        fig = plt.figure()
        axis = fig.add_subplot(111)
    
    ax=libs_summary_pandas_df[['read_count']].plot(kind='bar',ax=axis,color=['#E41A1C'])
    ax.set_ylabel('read counts')
    ax.set_xlabel('')
    ax.yaxis.grid(color='lightgray',linestyle='dashed')
    fontP = FontProperties()
    fontP.set_size('small')
    ax.legend(loc='upper right',prop=fontP,bbox_to_anchor=(1.13,1))


def get_yield_plot(libs_summary_pandas_df,axis=None):
    #reads for each lib
    if axis is None:
        fig = plt.figure()
        axis = fig.add_subplot(111)
    
    #count the bases in each lane
    ax=libs_summary_pandas_df.total_yield.map(lambda x: x/1000000000.0).plot(kind='line',ax=axis)
    ax.set_ylabel('Yield(Gb)')
    ax.set_xlabel('')
    #ax.yaxis.grid(color='lightgray',linestyle='dashed')
    fontP = FontProperties()
    fontP.set_size('small')
    ax.legend(loc='upper right',prop=fontP,bbox_to_anchor=(1.13,1))


def get_readQ20_vs_readGC_plot(libs_summary_pandas_df,axis=None,label=None,color=None,size=None,marker='o'):
    #Q20 v/s GC
    if axis is None:
        fig = plt.figure()
        axis = fig.add_subplot(111)
        
    if color is None:
        color = 'black'
        
    libs_summary_pandas_df['min_q20'] = libs_summary_pandas_df[['read1_q20','read2_q20']].min(axis=1)
    axis.plot( list(libs_summary_pandas_df.read_GC_mean),list(libs_summary_pandas_df.min_q20),'o',label=label,color=color,markersize=size,marker=marker)
    axis.set_xlabel('Read GC mean')
    axis.set_ylabel('Min Q20')
    plt.title('Read min(Q20) v/s mean(GC)')
    fontP = FontProperties()
    fontP.set_size('small')
    axis.legend(loc='upper right',prop=fontP,bbox_to_anchor=(1.13,1))
    
    
def get_readGC_plot(libs_summary_pandas_df,axis=None,label=None,color=None,size=None,marker='o'):
    #GC
    if axis is None:
        fig = plt.figure()   
        axis = fig.add_subplot(111)
        
    if color is None:
        color = 'black'
        
    libs_summary_pandas_df.read_GC_mean.plot(kind='line',ax=axis)
    axis.set_ylabel('Read GC')
    #fontP = FontProperties()
    #fontP.set_size('small')
    #axis.legend(loc='upper right',prop=fontP,bbox_to_anchor=(1.13,1))


def get_contam_plot(libs_summary_pandas_df,axis=None): 
    if axis is None:
        fig = plt.figure()
        axis = fig.add_subplot(111)
    ax=libs_summary_pandas_df[['IL_artifacts','JGI_contam','rRNA',
                               'chloroplast','mito','ecoli',
                               'DNA_spike_in','RNA_spike_in']].plot(kind='bar',ax=axis,stacked=True,color=['#1B9E77','#999999','#7570B3',
                                                                                                           '#E7298A','#66A61E','#E6AB02',
                                                                                                           '#A65628','#F781BF'])
    ax.set_ylabel('percent contamination')
    ax.set_xlabel('')
    fontP = FontProperties()
    fontP.set_size('small')
    ax.legend(loc='upper right',prop=fontP,bbox_to_anchor=(1.13,1))
    ax.yaxis.grid(color='lightgray',linestyle='dashed')
    


def get_libComplexity_plot(libs_summary_pandas_df,axis=None):
    if axis is None:
        fig = plt.figure()
        axis = fig.add_subplot(111)
    
    ax=libs_summary_pandas_df[['startmer_lib_complexity','randmer_lib_complexity']].plot(kind='bar',ax=axis,use_index=False,color=['#FED976','#E31A1C'])
    ax.set_ylabel('random 20mer percent')
    ax.set_xlabel('')
    ax.set_xticklabels(libs_summary_pandas_df.index)
    fontP = FontProperties()
    fontP.set_size('small')
    ax.legend(loc='upper right',prop=fontP,bbox_to_anchor=(1.13,1))
    
    
def get_mappedReads_percent_plot(libs_summary_pandas_df,axis=None):
    
    
    if axis is None:
        fig = plt.figure()
        axis = fig.add_subplot(111)
    
    ax=libs_summary_pandas_df[['genome_map_percent','transcriptome_map_percent']].plot(kind='bar',ax=axis)
    ax.set_ylabel('read mapping percent')
    ax.set_xlabel('')
    fontP = FontProperties()
    fontP.set_size('small')
    ax.legend(loc='upper right',prop=fontP,bbox_to_anchor=(1.13,1))
    


def get_matplotlib_on_canvas(mplot_figure):

    canvas = FigureCanvas(mplot_figure)
    output = cStringIO.StringIO()
    canvas.print_png(output)
    return output



def get_libs_qaqc_plots(library_names=None,libs_summary_pandas_df=None,plate_view=False,out_plot_file=None):
    
    
    if libs_summary_pandas_df is None:
        if not library_names:
            #to be improved
            print 'no library names specified..exiting\n\n '
            sys.exit(1)        
        #get the lib summary as a pandas df
        raw_lib_summary_pandas_df = db_access.get_lib_query_results(library_names=library_names)
        #clean and process the dataframe
        libs_summary_pandas_df = db_access.process_N_clean_pandas_DF(raw_lib_summary_pandas_df) 


    #debug
    #libs_summary_pandas_df.to_csv('cleaned.csv',na_rep='NA')
    #print type(libs_summary_pandas_df)
    #print libs_summary_pandas_df.dtypes
    
    #main plot
    fig = plt.figure(figsize=(20,25))
    fig.subplots_adjust(hspace=.4)
    fig.suptitle('Sequencing Quality Summary',size='large')
    
    #Q20 read length
    ax1 = plt.subplot2grid((6,4),(0,0),colspan=4)
    ax1.set_title('Q20 read length',size='large')
    get_Q20_read_plot(libs_summary_pandas_df,axis=ax1,plate_view=plate_view)
    #get_readGC_plot(libs_summary_pandas_df,axis=ax1.twinx())
    #ax1.tick_params(axis='x',labelsize=8)
    plt.setp(plt.xticks()[1])
    #plt.savefig('q20.png')
    
    ax2 = plt.subplot2grid((6,4),(1,0),colspan=4)
    ax2.set_title('#reads/Library',size='large')
    #get_yield_plot(libs_summary_pandas_df,axis=ax2.twinx())
    get_read_num_plot(libs_summary_pandas_df,axis=ax2)
    plt.setp(plt.xticks()[1])
    
    ax3 = plt.subplot2grid((6,4),(2,0),colspan=4)
    ax3.set_title('Contamination',size='large')
    get_contam_plot(libs_summary_pandas_df,axis=ax3)
    plt.setp(plt.xticks()[1])
    #plt.savefig('contamination.png')
    
    ax4 = plt.subplot2grid((6,4),(3,0),colspan=4)
    ax4.set_title('Lib Complexity',size='large')
    get_libComplexity_plot(libs_summary_pandas_df,axis=ax4)
    plt.setp(plt.xticks()[1])
    #plt.savefig('lib_complexity.png')
    
    
    ax5 = plt.subplot2grid((6,4),(4,0),colspan=4)
    ax5.set_title('Read Mapping Percentage',size='large')
    get_mappedReads_percent_plot(libs_summary_pandas_df,axis=ax5)
    plt.setp(plt.xticks()[1])
    #plt.savefig('read_mapping.png')
    
    #ax6 = plt.subplot2grid((6,4),(5,0),colspan=4)
    #ax6.set_title('GC v/s Q20 compared to other libs',size='large')
    #get_readQ20_vs_readGC_plot(year_lib_summary_HiSeq,axis=ax6,label='HiSeq Libs',color='#FFEDA0',size=8)
    #get_readQ20_vs_readGC_plot(libs_summary_pandas_df,axis=ax6,label='Current Libs',color='#B10026',size=16,marker='*')
    
    
    
    if out_plot_file is None:
        return get_matplotlib_on_canvas(fig)
    else:
        plt.savefig(out_plot_file)












#get the all the uniq read lengths in libs
#    lib_min_read1_lengths = min(libs_summary_pandas_df.read1_length.unique())
#    lib_min_read2_lengths = min(libs_summary_pandas_df.read2_length.unique())


################
#    #CUSTOM Block
#    ###############
##    get the library summary over a one year period as a pandas df
##     to be used as background for comparison
#    now = datetime.datetime.now()
#    end_date=now.date()
#    start_date = now.date() - datetime.timedelta(days=365)
#
#    #start_date = datetime.datetime.strptime('2012-08-01'.replace("'",''),'%Y-%m-%d').date()
#    #end_date = datetime.datetime.strptime('2012-08-10'.replace("'",''),'%Y-%m-%d').date()
#
#    year_libs_pandas_df = get_lib_query_results(start_date=start_date,end_date=end_date)
#    grouped = year_libs_pandas_df.groupby(('file_name','lib_name','instrument_type','read1_length','read2_length'))
#    year_lib_summary = grouped.apply(_summarize_datebased_pandas_df_group)
#    year_lib_summary=year_lib_summary.reset_index()
#    
#    #type coercion 
#    year_lib_summary['read1_length'] = year_lib_summary.read1_length.map(int)
#    year_lib_summary['read2_length'] = year_lib_summary.read2_length.map(int)
#    
#    #HiSeq only and have the read lengths that are present in the libs entered by the user
#    year_lib_summary_HiSeq = year_lib_summary[ (year_lib_summary.instrument_type == 'HiSeq')  & 
#                                               (year_lib_summary.read1_length >= lib_min_read1_lengths ) & 
#                                               (year_lib_summary.read2_length >= lib_min_read2_lengths )]
#
#    ###############
#    ### END Custom Block
#    ###############




