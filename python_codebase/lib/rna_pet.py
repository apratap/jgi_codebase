#!/house/homedirs/a/apratap/playground/software/epd-7.2-2-rh5-x86_64/bin/python

import os
import sys
import bamUtils
import subprocess
import pysam
import numpy as np
import collections





def split_bam_to_sense_antisense(bamFile, force_run=False):
    
    '''given an input bam file only with mate pair direction reads
       and color tag YC:Z:green (5 prime) or YC:Z:orange( 3 prime)
       the function will split it into two bam files
       1. reads capturing cDNA ends on the sense strand
       2. reads capturing cDNA ends on the antisense strand
    '''
    SUB = 'split_bam_to_sense_antisense'
    print '[%s]:opening bam %s for reading ' % (SUB,bamFile)
    
    
    bamFile_bai = bamFile + '.bai'
    if not os.path.exists(bamFile_bai):
        bamUtils.index_bam(bamFile)
    
    
    input_bamFile_handle = pysam.Samfile(bamFile,'rb')
    base_dir = os.path.dirname(bamFile) or '.'
    file_prefix=os.path.basename(bamFile).replace('.bam','')
    
    sense_bamFile     = base_dir+'/'+file_prefix+'_sense.bam'
    antisense_bamFile = base_dir+'/'+file_prefix+'_antisense.bam';
    
    #if ( 1 and 1 ):
    if ( os.path.exists(sense_bamFile) and os.path.exists(antisense_bamFile) and ( force_run == False) ):
        print '[%s]: Expected output  %s  and  %s  already present .....no processing required' % (SUB,os.path.basename(sense_bamFile), os.path.basename(antisense_bamFile))
        return (sense_bamFile, antisense_bamFile)
    
    sense_bamFile_handle = pysam.Samfile(sense_bamFile,'wb',template=input_bamFile_handle)
    antisense_bamFile_handle = pysam.Samfile(antisense_bamFile,'wb',template=input_bamFile_handle)
    
    for read in input_bamFile_handle.fetch():
        color  = read.tags[-1][1]  #last entry in the list of tuples and then extract the second element of tuple
        
        if color not in ('orange','green'):
            print 'Warning : color %s is not orange/green' % color
            continue
        
        if read.is_reverse:  #read maps in the opposite direction
            if color == 'green': ##read is coming from sense strand
                #print 'Found %s color' % color
                sense_bamFile_handle.write(read)
            elif color == 'orange':
                #print 'Found %s color' % color
                antisense_bamFile_handle.write(read)
    
    #read maps in the same direction as reference 5 prime to 3 prime
        else:
            if color == 'orange':  ##read is coming from the sense strand
                sense_bamFile_handle.write(read)
        
            elif color == 'green': ##read is coming from antisense strand
                antisense_bamFile_handle.write(read)
    print '[%s]: Created sense bam %s' % (SUB, os.path.basename(sense_bamFile))
    print '[%s]: Create antisense bam %s' % (SUB,os.path.basename(antisense_bamFile))
    
    
    #print 'Ready to index %s and %s files' % (os.path.basename(sense_bamFile),os.path.basename(antisense_bamFile))
    
    #for some reason this doesnt work inside this method
    # works fine outside this function
    #index_bam(sense_bamFile)
    #index_bam(antisense_bamFile_path)
    
    
    return(sense_bamFile,antisense_bamFile)


def generate_uniq_start_end_ranges_per_chromosome(bamFile,chromosome=None,strand=None):
    
    SUB='generate_uniq_start_end_ranges_per_chromosome'
    
    dir = os.path.dirname(bamFile)    
    
    bamFile_handle = pysam.Samfile(bamFile,'rb')
    references = bamFile_handle.references
    
    bamFile_handle_mate = pysam.Samfile(bamFile,'rb')
    
    ranges = []  #to store the uniq ranges in the chromosome
    counter=0;   # to count the number of pairs processed
    
    if not chromosome:
        print 'No chromosome was specified in the function call to [%s]' % SUB
        sys.exit(3)
    if not strand:
        print 'No strand was specified in the function call to [%s]' % SUB
        sys.exit(3)
    
    
    ##checking if the expected output from this function is already present
    dir = os.path.dirname(bamFile) or '.'
    chr_ranges_file = dir + '/'+ chromosome + '_strand_' + strand + '_uniq_clones.txt'
    if (os.path.exists(chr_ranges_file) ):
        print '[%s]: Expected output  %s  already present .....no processing required' % (SUB,os.path.basename(chr_ranges_file))
        return (chr_ranges_file)
        
    for read in bamFile_handle.fetch(chromosome):
        ''' below is a very customized code block
            should only work on the mate paired reads for RNA-PET
            NOT TO BE GENERALIZED
        '''

        if read.is_read1:
            counter += 1
            
            if (counter % 10000) == 0:
                print '[%s]: Processed %d pairs' % (SUB,counter)
                            
            mate = bamFile_handle.mate(read) 
            start_beg = read.pos  
            start_end = read.pos + read.qlen
            
            end_beg = mate.pos
            end_end = mate.pos + mate.qlen
           
            if read.is_reverse:
                #print "Reverse: %d %s start_beg %s start_end %s end_beg %s end_end %s" % (counter,read.qname, start_beg,start_end,end_beg,end_end)
                ranges.append((start_beg,start_end,end_beg,end_end))
            
            else:
                #else the reverse happens
                # start_beg becomes end_beg and vice-a-versa
                start_beg,end_beg = end_beg,start_beg
                start_end,end_end = end_end,start_end
                #print "Not Reverse :%d %s start_beg %s start_end %s end_beg %s end_end %s" % (counter,read.qname,start_beg,start_end,end_beg,end_end)
                ranges.append((start_beg,start_end,end_beg,end_end))
    
    
    if (  len(ranges)  > 0):
       ranges_array = np.array(ranges)
       print 'Shape of numpy array',ranges_array.shape
       print 'Size of list points is %d' % sys.getsizeof(ranges)
       print 'Size of numpy array is %d' % sys.getsizeof(ranges) ## will just be a reference so not the actual size

    
       ##spit out the range file
       fh = open(chr_ranges_file,'w')
       for (start_beg,start_end,end_beg,end_end) in ranges:
           out = '%s\t%s\t%s\t%s\n' %(start_beg,start_end,end_beg,end_end)
           fh.write(out)
       fh.close()
       print '[%s]: created %s' % (SUB,chr_ranges_file)
       
    return chr_ranges_file




"""


def cluster_points_DBSCAN(data_numpy_array):
    
    SUB = 'cluster_points_DBSCAN'
    
    #eucledian distance calculation
    D = distance.pdist(data_numpy_array)
    S = distance.squareform(D)
    H = 1 - S/np.max(S)
    db = DBSCAN().fit(H, eps=0.95)
    
    core_samples_indices = db.core_sample_indices_
    labels = db.labels_
    components = db.components_
    
    n_clusters_ = len(set(labels)) - ( 1 if -1 in labels else 0)
    
    print 'Num of clusters %d' % n_clusters_

    #f = open('check.bed', 'w')
    
    
    
    for k in set(labels):
        index_class_members = [ index[0] for index in np.argwhere(labels == k ) ]
        data = data_numpy_array[index_class_members]
        mean_start = mean(data[:,0])
        mean_end = mean(data[:,1]) 
        print '%d \t %d : shape %s \n' % (mean_start, mean_end, data.shape)
        print "======================="



def cluster_pet_points_per_chromosome(bamFile):
    
    SUB = 'cluster_pet_points_per_chromosome'
    
    bamFile_handle = pysam.Samfile(bamFile,'rb')
    references = bamFile_handle.references
    
    for chromosome in references:
        points = []
        for read in bamFile_handle.fetch(chromosome):
            ''' below is a very customized code block
                should only work on the mate paired reads for RNA-PET
                NOT TO BE GENERALIZED
            '''
            if read.is_read1:
                if read.is_reverse: #start of transcript is read.pos and end is read.mpos+length of read 2 
                    start = read.pos
                    end = read.mpos
                    points.append((start,end))
                else:
                    start = read.mpos
                    end = read.pos
                    points.append((start,end))
                    
        if ( len(points) > 0):
            
            points_array = np.array(points, dtype='int')
            print 'Shape of array is ', points_array.shape
            cluster_points_DBSCAN(points)
"""
                
