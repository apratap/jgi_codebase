#!/usr/bin/env python


import os
import sys
import time
import fileinput #to join a list of files
from pyes import  *


from whoosh.fields import Schema, TEXT, STORED
from whoosh.index import create_in, open_dir
from whoosh.query import *
from whoosh.qparser import QueryParser


##updating the library look up path
dir = os.path.dirname(__file__)
sys.path.append(os.path.join(dir,'../lib'));
import utils





##create the gi_id to tax_id table in database
def create_gi_to_tax_id_table(sqllite_conn,nucl_gi_to_tax_id,prot_gi_to_tax_id):
    SUB = 'create_gi_to_tax_id_table'
    db_cursor = sqllite_conn.cursor()
    db_cursor.execute('DROP TABLE IF EXISTS gi_to_taxID')
    db_cursor.execute('CREATE TABLE gi_to_taxID (gi_id INTEGER PRIMARY KEY, tax_id INTEGER)')
       
    db_entries = []
    for num,line in enumerate(fileinput.input([nucl_gi_to_tax_id,prot_gi_to_tax_id])):
        (gi,tax_id)=line.strip().split('\t')
        db_entries.append((gi,tax_id))
        #batch insert
        if num % 1000000 == 0 and num > 0:
            insert_start = time.clock()
            db_cursor.executemany('INSERT INTO gi_to_taxID VALUES(?,?)',db_entries)
            db_entries = []
            insert_end = time.clock()
            sys.stderr.write('batch inserted %d million records in the database in %s secs' % (num,insert_end-insert_start))
            sqllite_conn.commit()

    sys.stderr.write('[%s]: Inserted %d lines into taxonomy db in table gi_to_taxID' % (SUB,num))
    



def get_tax_id_to_scientific_name_dict(ncbi_tax_id_to_names_file):
    SUB='get_tax_id_to_scientific_name_dict'
    #create tax_id to names dictionary
    
    prog_start=time.clock()
    tax_id_to_scientific_names = {};
    count_names = 0

    for num,line in enumerate(fileinput.input([ncbi_tax_id_to_names_file])):
        split_line=line.strip().split('|')
        if split_line[3].strip() == 'scientific name':
            tax_id = int(split_line[0].strip())
            name = split_line[1].strip()
            count_names += 1
            tax_id_to_scientific_names[tax_id] = name
    prog_end  = time.clock()
    sys.stderr.write('[%s]:Took %d seconds to read %d lines \n' % (SUB,prog_end-prog_start,num))
    return (tax_id_to_scientific_names)
#    print 'Dict has %d lines ' % len(tax_id_to_scientific_names)



def get_ncbi_taxonomy_child_parent_relation(ncbi_node_file):
    SUB = 'get_ncbi_taxonomy_child_parent_relation'
    prog_start=time.clock()
    tax_id_child_parent_relation = {};
    for num,line in enumerate(fileinput.input([ncbi_node_file])):
        split_line=line.strip().split('|')
        child = int(split_line[0].strip())
        parent = int(split_line[1].strip())
        tax_id_child_parent_relation[child] = parent
    prog_end  = time.clock()
    sys.stderr.write('[%s]: Took %d seconds to read %d lines \n' % (SUB,prog_end-prog_start,num))
    return (tax_id_child_parent_relation)
    




def get_ncbi_taxid_from_ncbi_gi(gi_id_list,db_cursor,chunk_size=30):
    SUB ='get_ncbi_taxid_from_ncbi_gi'
    """
        given a list of GI's get the tax_id for them
        does the database query in chunks, defined by the chunk size
    """ 
    results = {}
    loop_start = time.clock()
    for (num_chunk,gi_chunk) in enumerate(utils.chunks(gi_id_list,chunk_size)):
        #the query below is complicated 
        #reference : http://stackoverflow.com/questions/7447938/sqlite3-operationalerror-near-syntax-error-in-python-using-in-operato
        #basically the IN clause needs to have same amount about (?,?...) as the number of items
        # you are searching for in the IN clause
#        print 'Chunk %d \n %s \n' %(num_chunk,gi_chunk)

        #three things at one go
        results.update( db_cursor.execute('SELECT * FROM gi_to_taxID where gi_id IN (%s)' % ('?, '*len(gi_chunk))[:-2], gi_chunk ).fetchall())
    
    loop_end = time.clock()
    sys.stderr.write('[%s]: Took %d seconds to map gi to tax ids \n' % (SUB,loop_end-loop_start))
    return results



def get_taxonomy_from_taxid(tax_id,tax_id_to_scientific_names,tax_id_child_parent_relation,gi_id=None):
    
    SUB ='get_taxonomy_from_taxid'
    taxonomy = []
    
    try:
        while ( tax_id_to_scientific_names[tax_id] != 'root'):
         taxonomy.append(tax_id_to_scientific_names[tax_id])
         parent_tax_id = tax_id_child_parent_relation[tax_id]
         tax_id = parent_tax_id
    except KeyError:
        #unkown taxonomy for a GI
        if tax_id == 0:
            return 'unknown'
        else:
            sys.stderr.write('[%s]: Cant find scientific name for tax_id : %s with GI: %s \n' % (SUB,tax_id,gi_id))
    #reverse the taxonomy join in a line and print
    return ';'.join(taxonomy[::-1])


###################
####using whoosh index
###################
def create_ncbi_taxonomy_names_index(index_dir,ncbi_names_file):
    
    SUB='create_ncbi_taxonomy_names_index'
    
    if not os.path.exists(index_dir):
        os.mkdir(index_dir)
        
    schema = Schema(tax_id=STORED, name=TEXT(stored=True))
    ix =create_in(index_dir,schema)
    writer = ix.writer()
    
    prog_start = time.clock()
    with open(ncbi_names_file) as fh:
        for num,line in enumerate(fh):
            split_line = line.strip().split('|')
            tax_id = split_line[0].strip()
            name = split_line[1].strip()
            if num % 100000 == 0 and num > 0:
                loop_check_time = time.clock()
                sys.stderr.write('inserted %s records in whoosh index in %d seconds\n' % (num,loop_check_time-prog_start))
            #write to whoosh index
            writer.add_document(tax_id=tax_id,name=unicode(name))
        
    loop_break_time = time.clock()
    sys.stderr.write('inserted %s records in whoosh index in %d seconds\n' % (num,loop_break_time-prog_start))        
    sys.stderr.write('Committing %s records in whoosh index\n' % (num))
    writer.commit()
    commit_finish_time = time.clock()
    sys.stderr.write('Commited %s records in whoosh index in %d seconds\n' % (num,commit_finish_time-prog_start))
    ix.close()




def search_name_in_NCBI_taxa_names_using_Whoosh_index(query,ncbi_taxa_names_index_dir):    
    SUB = 'match_names_to_NCBI_taxa_names'
    ix=open_dir(ncbi_taxa_names_index_dir)
    with ix.searcher() as searcher:
        query = QueryParser("name", ix.schema).parse(unicode(query))
        results = searcher.search(query)
        tax_ids = [result['tax_id'] for result in results]
        tax_ids_uniq = utils.uniqify_list(tax_ids)
#        print tax_ids
#        print tax_ids_uniq
        return tax_ids_uniq
    



def search_name_in_NCBI_taxa_names_using_ES_index(query,conn,top_hits=10):    
    SUB = 'search_name_in_NCBI_taxa_names_using_ES_index'
    q1 = TermQuery("tax_name",query.lower().strip())
    results = conn.search(query=q1)
    results.next()
#    print type(results)
#    a=len(results)
#    return results[0]['tax_id']
#    tax_ids = [result['tax_id'] for result in results[:top_hits]]
    #keeping only uniq ids
#    tax_ids = set(tax_ids);
#    return tax_ids;


    
    
    
    