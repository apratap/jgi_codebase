#!/usr/bin/env python

import sys
import MySQLdb
import datetime
import pprint
import cStringIO
import cx_Oracle
import matplotlib.pyplot as plt
import decimal
import datetime
import pandas
import numpy as np
from matplotlib.font_manager import FontProperties
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas


def connect_DB():        
    db_conn = MySQLdb.connect(host='sequoia.jgi-psf.org',user='rqc_ro',passwd='rqcro',db='rqc')
    return db_conn 


def connect_ITS_db():
    
    db_name = "dwprd1"
    dw_its_username = 'dw_user'
    dw_its_passwd = 'dw_user'
    conn_str = '%s/%s@%s' % (dw_its_username,dw_its_passwd,db_name)
    
    conn = cx_Oracle.connect(conn_str)
    return conn
        
    
def summarize_sequencing_by_runtype(results, instrument_type):
    
    if results.get(instrument_type,None) is None:
        print 'Instrument type : %s not present in the results' % instrument_type
        return (None,None)
    else:
        dates = []
        readcounts = []
        
        if instrument_type == 'HiSeq':
            #separate the control lane
            hiseq_control_lane_dates = []
            hiseq_control_lane_read_counts = []
        
        for (count,fastq) in enumerate(results[instrument_type].keys()):
            physical_run_type = results[instrument_type][fastq]['physical_run_type']
            run_date = results[instrument_type][fastq]['run_date']
            
            if type(run_date) is not datetime.date:
                print ('arg must be a datetime.date, not a %s' % type(run_date))
                print instrument_type,fastq,run_date,results[instrument_type][fastq]['read count'],results[instrument_type][fastq]['library_name']
                print 'Skipping this row of data'
                continue

            try:
                lane_read_count = int(results[instrument_type][fastq]['read count'])
            except:
                print 'error %s ' % results[instrument_type][fastq]['read count']
                print instrument_type,fastq,run_date,results[instrument_type][fastq]['read count'],results[instrument_type][fastq]['library_name']
                print 'Skipping this row of data'
                continue
            
            #double the read count for single read runs for HiSeq for a fairer comparison
            if instrument_type == 'HiSeq' and physical_run_type.find('Single-Read') != -1:
                #print 'doubling read: %s \t %d \t %d ' % (physical_run_type,lane_read_count,lane_read_count*2)
                lane_read_count = lane_read_count * 2
            
            
            if instrument_type == 'HiSeq' and (results[instrument_type][fastq]['library_name'] == 'PhiX' 
                                               or 
                                               results[instrument_type][fastq]['library_name'] == 'GAZP'
                                               or
                                               results[instrument_type][fastq]['library_name'] == 'PhiX_pool' ):
                #print instrument_type,fastq,run_date,results[instrument_type][fastq]['read count'],results[instrument_type][fastq]['library_name']
                hiseq_control_lane_dates.append(run_date)
                hiseq_control_lane_read_counts.append(lane_read_count)
                continue
            
                
            
            dates.append(run_date)
            readcounts.append(lane_read_count)
    print 'Processed %d fastq for instrument %s' % (count+1,instrument_type)
    
    if instrument_type == 'HiSeq':
        return (dates,readcounts,hiseq_control_lane_dates,hiseq_control_lane_read_counts)
    else:
        return (dates,readcounts)
    
    
def get_sequencing_summary(db_conn,startDate,endDate):
    
    
#    fmt_start_date = datetime.datetime.strptime(startDate.replace("'",''),'%Y-%m-%d').date()
#    fmt_end_date = datetime.datetime.strptime(endDate.replace("'",''),'%Y-%m-%d').date()
    
#    print 'formatted_start_date : %s ' % fmt_start_date
#    print 'formatted_end_date : %s ' % fmt_end_date

    fmt_start_date = startDate
    fmt_end_date  = endDate

    sql_query = """
    select DISTINCT 
    sps.physical_run_unit_id,spr.physical_run_unit_id,spr.physical_run_id
    ,sps.dt_begin
    ,srun.physical_run_type,srun.platform_name,srun.instrument_type,srun.instrument_name,srun.read1_length,srun.read2_length
    ,ssu.library_name
    ,ssf.file_name
    ,spr.run_section
    ,ras.stats_name,ras.stats_value
    from 
    sdm.physical_run_unit_status_history sps
    ,sdm.physical_run_unit spr
    ,sdm.physical_run srun
    ,sdm.sdm_seq_unit ssu
    ,sdm.sdm_seq_unit_file ssf
    ,rqc.m2m_analysis_seq_unit rm2m
    ,rqc.rqc_analysis_stats ras
    where sps.dt_begin >= '%s' and sps.dt_begin < '%s'
    and sps.physical_run_unit_status_id = 3
    and sps.physical_run_unit_id = spr.physical_run_unit_id
    and spr.physical_run_id = srun.physical_run_id
    and spr.physical_run_unit_id = ssu.physical_run_unit_id
    and ssu.sdm_seq_unit_id = ssf.sdm_seq_unit_id
    and ssf.file_type = 'FASTQ'
    and ssf.file_name REGEXP '^.+[0-9]\.fastq.gz'
    and REPLACE (ssf.file_name, '.fastq.gz','.srf') = rm2m.seq_unit_name
    and rm2m.analysis_id = ras.analysis_id
    and ras.stats_name = 'read count'
    """ % (fmt_start_date,fmt_end_date+datetime.timedelta(days=1))
    
    
    cur = db_conn.cursor()    
    cur.execute(sql_query)
    results = {}
    
    for (count_rows,row) in enumerate(cur.fetchall()):
        #first 3 cols returned by sql are for debugging purposes only
        debug_part = row[:3]
        data = row[3:]


        #the order of the following is based on sql query
        run_date = data[0]
        physical_run_type = data[1]
        platform = data[2]
        instrument_type = data[3]
        instrument_name = data[4]
        read1_length = data[5]
        read2_length = data[6]
        library_name = data[7]
        seq_unit = data[8]
        seq_unit_lane = data[9]
        fastq_rqc_stats_name = data[10]
        fastq_rqc_stats_value =  data[11]
                
         #print run_date,physical_run_type,platform,instrument_type,instrument_name,library_name,seq_unit
        
        #register new instrument type
        if results.get(instrument_type, None) is None:
            results[instrument_type] = {}
        
        #register new fastq
        if results[instrument_type].get(seq_unit, None) is None:
            results[instrument_type][seq_unit] = {}
            results[instrument_type][seq_unit]['run_date'] = run_date.date()
            results[instrument_type][seq_unit]['physical_run_type'] = physical_run_type
            results[instrument_type][seq_unit]['platform'] = platform
            results[instrument_type][seq_unit]['instrument_type'] = instrument_type
            results[instrument_type][seq_unit]['instrument_name'] = instrument_name
            results[instrument_type][seq_unit]['library_name'] = library_name
            results[instrument_type][seq_unit]['lane'] = seq_unit_lane
            results[instrument_type][seq_unit][fastq_rqc_stats_name] = fastq_rqc_stats_value
        else:
            results[instrument_type][seq_unit][fastq_rqc_stats_name] = fastq_rqc_stats_value
        
    
    count_rows += 1
    print 'processed %d rows from mysql results' % count_rows
    
    return results


def plot_sequencing_summary(startDate,endDate):
    
    db_conn = connect_DB()

    fmt_start_date = datetime.datetime.strptime(startDate.replace("'",''),'%Y-%m-%d').date()
    fmt_end_date = datetime.datetime.strptime(endDate.replace("'",''),'%Y-%m-%d').date()
    
    results = get_sequencing_summary(db_conn,fmt_start_date,fmt_end_date)
    
    (hiseq_dates,hiseq_lanes_read_counts,hiseq_control_lane_dates,hiseq_control_lane_read_counts)=summarize_sequencing_by_runtype(results,'HiSeq')
    (miseq_dates,miseq_lanes_read_counts)=summarize_sequencing_by_runtype(results,'MiSeq')
    
    
    fig = plt.figure(figsize=(14,12))
    fig.suptitle('JGI - Sequencing Summary (read counts) \n Q: %s-%s -- %s-%s' % (fmt_start_date.strftime("%B"),fmt_start_date.year,fmt_end_date.strftime("%B"),fmt_end_date.year), fontsize=20)
    
    
    #hiseq
    ax1 = plt.subplot2grid((3,4),(0,0),colspan=4)
    #ax1.axhline(y=200)
    ax1.plot_date(hiseq_dates,hiseq_lanes_read_counts,color='#33A02C')
    plt.title("HiSeq's Output")
    ax1.set_ylabel('#reads per lane(in 100 millions)')
    ax1.plot_date(hiseq_control_lane_dates,hiseq_control_lane_read_counts,color='red',label="PhIX-lane",)
    
    
    fontP = FontProperties()
    fontP.set_size('small')
    plt.legend(loc='upper left',prop=fontP)
    
    
    ax2 = plt.subplot2grid((3,4),(2,0),colspan=2)
    ax2.hist(hiseq_lanes_read_counts,bins=50,color='#33A02C')
    ax2.hist(hiseq_control_lane_read_counts,bins=50,color='red',label="PhiX")
    ax2.set_xlabel('Hi-Seq read count histogram',fontsize=14)
    plt.legend(loc='upper left')
    
    
    #MiSeq
    ax7 = plt.subplot2grid((3,4),(1,0),colspan=4,sharex=ax1)
    plt.plot_date(x=miseq_dates,y=miseq_lanes_read_counts,color='#A6CEE3')
    plt.title("MiSeq's Output")
    plt.ylabel('#reads per lane(in 10 millions)')
    plt.xlabel('Time Line',fontsize=12)
    ax8 = plt.subplot2grid((3,4),(2,2),colspan=2)
    plt.hist(miseq_lanes_read_counts,bins=50,color='#A6CEE3')
    plt.xlabel('Mi-Seq read count histogram',fontsize=14)
    
    #create a canvas and put the figure on it
    canvas = FigureCanvas(fig)
    #create a IO bufffer
    output = cStringIO.StringIO()
    #write the canvas on a IO buffer
    canvas.print_png(output)
    
    #return the buffer
    return output


#//TODO : needs to spit error messages in a better way








def get_ITS_info_for_libs(lib_names=None,startDate=None,endDate=None):
    
    if lib_names is None and startDate is None and endDate is None:
        print 'library names or start/end Date not provided'
        return None
    if lib_names and startDate and endDate:
        print 'enter either library names or start/end Date provided, NOT both'
        return None
    
    #connect to the ITS dbase
    conn   = connect_ITS_db()
    cursor = conn.cursor()
    
    #form the SQL query
    base_sql = """   
               select DISTINCT air.*, ssus.run_date
               from all_inclusive_report air 
               inner join sdm_seq_unit_statistics ssus 
               on ssus.LIBRARY_NAME = air.LIBRARY_NAME
               """
    
    #select the query for query using lib names
    if lib_names:
        library_names_line = '(%s)' % ','.join(map(repr,lib_names))
        sql = base_sql + """
                               where air.pool_name IN %s OR air.library_name IN  %s
                         """   % (library_names_line,library_names_line)
    #query using start and end dates
    elif startDate and endDate:
        sql = base_sql + """
                             where ssus.RUN_DATE >=  to_timestamp('%s', 'mm-dd-yyyy')
                             AND ssus.RUN_DATE <=  to_timestamp('%s', 'mm-dd-yyyy')
                         """ % (startDate,endDate)
    else:
        print 'cant form sql query'
        return None
    
    #fetch results
    try:
        result=cursor.execute(sql)
    except cx_Oracle.DataError, exc:
        error = exc.args
        print error.code
        print error.messgae
    
    #create a list of data for data frame creation
    data = []
    x = [ data.append(row) for row in result ]
    
    
    #get the names of DB cols to be used for pandas df col names
    db_colnames = [r[0] for r in result.description ]
    
    #ACTUAL names of the columns used to 
    columns_to_pick = ['NCBI_ORGANISM_NAME','PROPOSAL_ID','TARGET_LOGICAL_AMOUNT','PRINCIPAL_INVESTIGATOR_NAME',
                       'ACTUAL_RUN_TYPE','SCIENTIFIC_PROGRAM','SEQ_UNIT_NAME','SAMPLE_ID','MATERIAL_TYPE','RUN_DATE',
                       'LIBRARY_NAME','POOL_NAME','SEQUENCER_MODEL','PHYSICAL_RUN_ID','LANE'
                      ]
    #create a np array
    temp_array = np.array(data)
    
    #if ITS results return None
    # for example incase of MiSeq libs
    if temp_array.size == 0:    
        return pandas.DataFrame([np.zeros(len(columns_to_pick))], columns=columns_to_pick)
        
    
    #create pandas data frame
    df = pandas.DataFrame(temp_array,columns=db_colnames)
    
    #pick only the reqd columns
    df =df[columns_to_pick]
    
    #delete the duplicates
    df = df.drop_duplicates(cols=['ACTUAL_RUN_TYPE','SEQ_UNIT_NAME','LIBRARY_NAME','POOL_NAME','SEQUENCER_MODEL','PHYSICAL_RUN_ID','LANE'])
    
    #clean and process the datafram
    def _temp_func(x):
        if x is None:
            return x
        else:
            return x.replace('.srf','.fastq.gz')
    
    df.SEQ_UNIT_NAME = df.SEQ_UNIT_NAME.map(_temp_func)
    
    #change dtype for PROPOSAL_ID
    df.PROPOSAL_ID  = df.PROPOSAL_ID.astype(str)
    
    conn.close()
    return df


def filter_rqc_stats_value(pandas_df,rqc_stats_name,out_col_name=None,convert_to=None):
    if out_col_name is None:
        out_col_name = rqc_stats_name
    
    #filter the stats needed
    cleaned_data=pandas_df[ (pandas_df.stats_name == rqc_stats_name )]
    
    #remove duplicate rows, index and type convert
    cleaned_data = cleaned_data.drop_duplicates(cols=['fastq','stats_value']).set_index('fastq')['stats_value'].astype(convert_to)
    cleaned_data.name = out_col_name 
        
    return cleaned_data


def get_appropriate_sql_query(library_names=None,start_date=None,end_date=None):
    base_sql_query = """
                        select   ssu.sdm_seq_unit_id,ssu.physical_run_unit_id,spu.physical_run_id,spu.run_section
                            ,ssf.file_name as fastq,ssu.parent_sdm_seq_unit_id
                            ,ssu1.library_name as parent_library_name, ssf1.file_name as parent_lib_file_name
                            ,ssu.library_name as lib_name, ssu.seq_barcode,ssu.is_multiplexed
                            ,srun.physical_run_type,srun.platform_name,srun.instrument_type
                            ,srun.instrument_name
                            ,rm2m.analysis_id
                            ,rstats.stats_name,rstats.stats_value
                            
                        from 
                        sdm.sdm_seq_unit ssu

                        INNER JOIN sdm.sdm_seq_unit_file ssf
                        ON ssf.sdm_seq_unit_id = ssu.sdm_seq_unit_id
                        and ssf.file_name NOT LIKE '%FAIL%'
    
                        INNER JOIN sdm.physical_run_unit spu
                        ON ssu.physical_run_unit_id = spu.physical_run_unit_id

                        INNER JOIN sdm.physical_run srun
                        on spu.physical_run_id = srun.physical_run_id            

                        LEFT JOIN sdm.physical_run_unit_status_history sprs
                        on sprs.physical_run_unit_id = ssu.physical_run_unit_id
                        and sprs.physical_run_unit_status_id = 9 
        
                        LEFT JOIN sdm.sdm_seq_unit ssu1
                        on ssu.parent_sdm_seq_unit_id = ssu1.sdm_seq_unit_id
        
                        LEFT JOIN sdm.sdm_seq_unit_file ssf1
                        on ssu1.sdm_seq_unit_id = ssf1.sdm_seq_unit_id
                        and ssf1.file_name NOT LIKE '%FAIL%'
            
                        LEFT JOIN rqc.m2m_analysis_seq_unit rm2m
                        on REPLACE (ssf.file_name, '.fastq.gz','.srf') = rm2m.seq_unit_name
        
                        LEFT JOIN rqc.rqc_analysis_stats rstats
                        on rm2m.analysis_id = rstats.analysis_id
                        and rstats.stats_value IS NOT NULL
                        """

    base_AND_clause = """    
                            and sprs.physical_run_unit_status_id = 9 
                            and ssf.file_name NOT LIKE '%FAIL%'
                            and ssf1.file_name NOT LIKE '%FAIL%'
                      """
    if library_names is not None:
        #formatting the lib tuple to produce
        #('XXXX','XXXX') style line for SQL
        #this will work for even size one tuple
        library_names = [ lib_name for lib_name in library_names if lib_name is not None]
        library_names_line = '(%s)' % ','.join(map(repr,library_names))
        
        query_by_library_name = """
                                where ssu.parent_sdm_seq_unit_id in ( 
                                select sdm_seq_unit_id from sdm.sdm_seq_unit 
                                where  library_name  IN %s
                                )
                                or ssu.library_name in %s
                            """ % (library_names_line,library_names_line)
        #print base_sql_query + query_by_library_name
        return (base_sql_query + query_by_library_name )
    
        
    elif start_date is not None and end_date is not None:        
        pass




def get_lib_query_results(**kwd):
    sql_query = get_appropriate_sql_query(**kwd)
    db_conn = connect_DB()
    cur =db_conn.cursor()
    cur.execute(sql_query)
        
    colnames = []
    for col in cur.description:
        colnames.append(col[0])
    
    data = []   
    for row in cur.fetchall():
        data.append(tuple(row))
    out_pandas_df = pandas.DataFrame.from_records(data,columns=colnames)
    
    #close the data database connection
    db_conn.close()
    
    return out_pandas_df
    


def process_N_clean_pandas_DF(out_pandas_df):
    
    #set the index 
    out_pandas_df.set_index('lib_name')
    
    #setting all the 'N/A' values to None (python and pandas compatible) 
    out_pandas_df.stats_value[out_pandas_df.stats_value == 'N/A'] = None
    
    #filtering out the UNKNOWN libs
    #out_pandas_df = out_pandas_df[out_pandas_df.lib_name != 'UNKNOWN']
    
    #remove any duplicates row if any
    out_pandas_df = out_pandas_df.drop_duplicates()
    
    
    #creating the cleaned data frame
    final_columns_needed = ['parent_library_name','lib_name','fastq',
                            'physical_run_id','run_section','seq_barcode', 'physical_run_type']
    cleaned_dataframe = out_pandas_df[final_columns_needed].drop_duplicates(['parent_library_name','lib_name','fastq']).set_index('fastq')
    
    
    #
    read1_length = filter_rqc_stats_value(out_pandas_df,'read length 1',out_col_name='read1_length',convert_to='int' )
    read2_length = filter_rqc_stats_value(out_pandas_df,'read length 2',out_col_name='read2_length',convert_to='int' )
    read1_q20 = filter_rqc_stats_value(out_pandas_df,'read q20 read1',out_col_name='read1_q20',convert_to='int' )
    read2_q20 = filter_rqc_stats_value(out_pandas_df,'read q20 read2',out_col_name='read2_q20',convert_to='int' )
    read_GC_mean = filter_rqc_stats_value(out_pandas_df,'read GC mean',out_col_name='read_GC_mean',convert_to='float')
    read_GC_SD = filter_rqc_stats_value(out_pandas_df,'read GC std',out_col_name='read_GC_SD',convert_to='float')
    read_count = filter_rqc_stats_value(out_pandas_df,'read count',out_col_name='read_count',convert_to='int')
    total_yield = filter_rqc_stats_value(out_pandas_df,'read base count',out_col_name='total_yield',convert_to='int')
    randmer_lib_complexity = filter_rqc_stats_value(out_pandas_df,'read 20mer percentage random mers',out_col_name='randmer_lib_complexity',convert_to='float')
    startmer_lib_complexity = filter_rqc_stats_value(out_pandas_df,'read 20mer percentage starting mers',out_col_name='startmer_lib_complexity',convert_to='float')
    transcriptome_map_percent = filter_rqc_stats_value(out_pandas_df,'transcriptome_mapped_percent',out_col_name='transcriptome_map_percent',convert_to='float')    
    transcriptome_mapped_reads = filter_rqc_stats_value(out_pandas_df,'transcriptome_mapped_reads',out_col_name='transcriptome_mapped_reads',convert_to='float')
    genome_map_percent = filter_rqc_stats_value(out_pandas_df,'genome_map_percent',out_col_name='genome_map_percent',convert_to='float')
    genome_map_reads = filter_rqc_stats_value(out_pandas_df,'genome_map_reads',out_col_name='genome_map_reads',convert_to='float')
    
    
    #contam
    IL_artifacts = filter_rqc_stats_value(out_pandas_df,'illumina read percent contamination artifact',out_col_name='IL_artifacts',convert_to='float')
    JGI_contam = filter_rqc_stats_value(out_pandas_df,'illumina read percent contamination contaminants',out_col_name='JGI_contam',convert_to='float')
    rRNA = filter_rqc_stats_value(out_pandas_df,'illumina read percent contamination rrna',out_col_name='rRNA',convert_to='float')
    chloroplast = filter_rqc_stats_value(out_pandas_df,'illumina read percent contamination plastid',out_col_name='chloroplast',convert_to='float')
    mito = filter_rqc_stats_value(out_pandas_df,'illumina read percent contamination mitochondrion',out_col_name='mito',convert_to='float')
    ecoli = filter_rqc_stats_value(out_pandas_df,'illumina read percent contamination ecoli combined',out_col_name='ecoli',convert_to='float')
    DNA_spike_in = filter_rqc_stats_value(out_pandas_df,'illumina read percent contamination DNA spikein',out_col_name='DNA_spike_in',convert_to='float')
    RNA_spike_in = filter_rqc_stats_value(out_pandas_df,'illumina read percent contamination RNA spikein',out_col_name='RNA_spike_in',convert_to='float')
 
    
    #merging the stats value    
    results_to_merge = (read1_length,read2_length,read1_q20,read2_q20,
                        read_count,read_GC_mean,read_GC_SD,
                        total_yield,
                        randmer_lib_complexity,
                        transcriptome_map_percent,startmer_lib_complexity,
                        genome_map_percent,
                        DNA_spike_in,RNA_spike_in,
                        IL_artifacts,JGI_contam,rRNA,chloroplast,
                        mito,ecoli,transcriptome_mapped_reads,genome_map_reads)
    
    #getting the column name
    column_names = [ col.name for col in results_to_merge ]
    
  
    
    #creating one dataframe from all the returned values
    all_results = pandas.concat(results_to_merge,axis=1)
    all_results.columns = column_names
    final_df = cleaned_dataframe.join(all_results)
    
   
    #debug
    #pandas_final_cleaned_df.to_csv('cleaned.tsv',sep="\t")
    
    
    #adding a new column to indicate the total reads coming from known references
    # mostly contamination besides spike-ins
    final_df['total_contam'] = final_df[['IL_artifacts','JGI_contam','rRNA','chloroplast','mito',
                                         'ecoli','DNA_spike_in','RNA_spike_in']].apply(sum,1)
                                                                                                                                                                       
    #adding the min_q20 column
    final_df['min_q20']=final_df[['read1_q20','read2_q20']].min(axis=1)
    
    
    #setting the parent_lib_name == lib_name for parent libs
    #so no parent_lib_name is not equal to None when lib_name itself is a parent library
    parent_lib_names = final_df.ix[pandas.isnull(final_df.parent_library_name)]['lib_name']
    final_df.parent_library_name.update(parent_lib_names)

    
    
    #get the JGI default index map
    jgi_plate_map = get_plate_index_info_df()
    
    #integrate the plate map with
    final_df=final_df.reset_index().merge(jgi_plate_map,left_on='seq_barcode',right_on='index',how='left',sort=False)
    #fastq indexing is lost as a reset_index 
    
    
    #sort the data frame to correctly order the index within each pool
    final_df['_temp_len_fastq_name']=final_df.fastq.map(len) #getting the length of fastq name mainly for odering
    final_df=final_df.sort_index(by=['parent_library_name','_temp_len_fastq_name'],axis=0).set_index('lib_name')
        
    return final_df



def filter_lib_summary_pandas_df(df,min_Q20=20,min_readCount=None,min_yield=None,min_genome_map_percent=None,
                                 min_transcriptome_map_percent=None, min_transcriptome_mapped_reads=None,
                                 max_contam_percent = None,
                                 **kwargs):
    
    SUB ='filter_lib_summary_pandas_df'
    
    filters = []
    filters_metadata = []
    
    #1. base Q20 filter
    filters.append( '(df.min_q20 >= %d) ' % (min_Q20) )
    filters_metadata.append('read1_q20 & read2_q20 >= %d' % (min_Q20) )
 
    #2. add read count filter if needed (by default the input is in millions) so multiply by 1000000
    if min_readCount:
        filters.append( ' & (df.read_count > %d )' % (min_readCount * 1000000)  )
        filters_metadata.append(' & read_count > %d ' % (min_readCount * 1000000))
    
    #3. add min_yield if needed and covert Gb into bp
    if min_yield:
        filters.append( '& (df.total_yield > %d ) ' % (min_yield * 1e9)  )
        filters_metadata.append('& total_yield > %d  ' % (min_yield * 1e9))
    
    #4. add genome mapping percent
    if min_genome_map_percent:
        filters.append( ' & (df.genome_map_percent > %d )' % (min_genome_map_percent) )
        filters_metadata.append( '& genome_map_percent > %d )' % (min_genome_map_percent) )
        
    #5. add read count filter if needed
    if min_transcriptome_map_percent:
        filters.append( ' & (df.transcriptome_map_percent > %d )' % (min_transcriptome_map_percent)  )
        filters_metadata.append('& transcriptome_map_percent > %d ' % (min_transcriptome_map_percent)) 
        
    #6. add transcriptome mapped reads filter if needed (by default the input is in millions) so multiply by 1000000
    if min_transcriptome_mapped_reads:
        filters.append( ' & (df.transcriptome_mapped_reads > %d )' % (min_transcriptome_mapped_reads * 1000000) )
        filters_metadata.append( ' & transcriptome_mapped_reads > %d ' % (min_transcriptome_mapped_reads * 1000000) )
        
    #7. add total contamination filter if needed
    if max_contam_percent:
        filters.append( ' & (df.total_contam < %d )' % (max_contam_percent) )
        filters_metadata.append( ' & total_contam < %d ' % (max_contam_percent) )
        
        
    filter_line = ' '.join(filters)
    filters_metadata_line = '  '.join(filters_metadata)
    
    #log the filters used
    if kwargs.get('logger',None):
        logger = kwargs['logger']
        logger.info('Filters used: %s' % (filters_metadata_line))
    

    #filter the df
    passed_filtering = eval(filter_line)
    failed_filtering = [ not val for val in passed_filtering ]
    

    passed_libs_df = df[passed_filtering]
    failed_libs_df = df[failed_filtering]
    
    passed_libs_df['STATUS'] = 'PASS'
    failed_libs_df['STATUS'] = 'FAIL'
    
    final_df = pandas.concat([passed_libs_df,failed_libs_df])
    
    return (final_df,filters_metadata_line)



def get_plate_index_info_df(refresh = False,plate_index_file=None):
    '''
    Get a plate well map as a pandas data frame indicating
    which well have spikes
    
    If refresh = True; a new well map will be created using the plate_index_file
    expected format for plate index file
    IT001   ATCACG
    IT002   CGATGT
    IT003   TTAGGC
    IT004   TGACCA
    
    Where IT001 indicates well 1 colwise
    and col2 is the expected index in that well
    
    
    If refresh = False
    return the apriori created well map
    '''
    #file to store the last created index map
    last_created_wellmap = '/global/homes/a/apratap/databases/plate_indexes/plate_indexInfo.txt'
    
    if refresh:
        if plate_index_file is None:
            plate_index_file = '/global/homes/a/apratap/databases/plate_indexes/JGI_index_info.txt'
        ##get the standard 96 well plate index map
        df_index_well_map = pandas.read_csv(JGI_indexes,names =['index_num','index'], sep='\t')
        
        ##adding the well location
        df_index_well_map['well'] = df_index_well_map['index_num'].map(index_num_to_wellName)
        df_index_well_map['row']= df_index_well_map['well'].map(lambda x: int(well_rowName_to_rowIndex.get(x[0])) )
        df_index_well_map['col']= df_index_well_map['well'].map(lambda x: int(x[1:3]) )
        df_index_well_map['Spiked']= df_index_well_map['well'].map(is_well_spiked)
        df_index_well_map.to_csv(last_created_wellmap,sep='\t',index=False)
    
    return pandas.read_csv(last_created_wellmap, sep='\t',header=0)






    
    
def _summarize_datebased_pandas_df_group(group):
    
    r1_q20 = list(group[ group.stats_name == 'read q20 read1' ]['stats_value'].map(int))
    r2_q20=  list(group[ group.stats_name == 'read q20 read2' ]['stats_value'].map(int))
    read_GC_mean= list(group[ group.stats_name == 'read GC mean' ]['stats_value'].map(float))
    #read_count= list(group[ group.stats_name == 'read count' ]['stats_value'].map(float))
    
    #the following if else logic could be improved
    if not r1_q20:
        r1_q20 = None
    else:
        r1_q20 = r1_q20[0]
    
    if not r2_q20:
        r2_q20 = None
    else:
        r2_q20 = r2_q20[0]
    
    if not read_GC_mean:
        read_GC_mean = None
    else:
        read_GC_mean = read_GC_mean[0]
    
    return pandas.Series({'read1_q20': r1_q20,
                          'read2_q20' : r2_q20,
                          'read_GC_mean':read_GC_mean })



if __name__ == '__main__':
    pass
    #db_conn = connect_DB()
    
    '''
    startDate = sys.argv[1]
    endDate = sys.argv[2]
    
    plot_sequencing_summary(startDate,endDate)
    '''
    
    #get_lib_query_results('CXYY')
    
    
    
    
    
    
    
    
#select DISTINCT 
#                                            spr.physical_run_id
#                                            ,sps.dt_begin
#                                            ,srun.physical_run_type,srun.platform_name,srun.instrument_type,srun.instrument_name
#                                            ,srun.read1_length,srun.read2_length
#                                            ,ssu.library_name as lib_name
#                                            ,ssf.file_name
#                                            ,spr.run_section
#                                            ,ras.stats_name,ras.stats_value
#                            from 
#                                sdm.physical_run_unit_status_history sps
#                                ,sdm.physical_run_unit spr
#                                ,sdm.physical_run srun
#                                ,sdm.sdm_seq_unit ssu
#                                ,sdm.sdm_seq_unit_file ssf
#                                ,rqc.m2m_analysis_seq_unit rm2m
#                                ,rqc.rqc_analysis_stats ras
#                            
#                            where sps.dt_begin >= '%s' and sps.dt_begin < '%s'
#                            and sps.physical_run_unit_status_id = 3
#                            and sps.physical_run_unit_id = spr.physical_run_unit_id
#                            and spr.physical_run_id = srun.physical_run_id
#                            and spr.physical_run_unit_id = ssu.physical_run_unit_id
#                            and ssu.sdm_seq_unit_id = ssf.sdm_seq_unit_id
#                            and ssf.file_type = 'FASTQ'
#                            and ssf.file_name REGEXP '^.+[0-9]\.fastq.gz'
#                            and REPLACE (ssf.file_name, '.fastq.gz','.srf') = rm2m.seq_unit_name
#                            and rm2m.analysis_id = ras.analysis_id
#                            and ras.stats_name in ('read GC mean','read q20 read1','read count','read q20 read2')



 