#!/jgi/tools/bin/python

import sys
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
import math

file_name=sys.argv[1]

split_by=50

out_file_name=file_name+'.fastq'
out_handle=open(out_file_name,'w')

dummy_quality='fgfggggg`gfgfggfdggggggggeggggggggafffefgggggggggdggg'
dummy_quality=dummy_quality[:split_by]
    
#print dummy_quality

for seq_record in SeqIO.parse(file_name,"fasta"):
    print ("Processing %s fasta record") % seq_record.id
    

    for read_num,i in enumerate(range(0,len(seq_record.seq),split_by)):
        my_seq=seq_record.seq[i:i+split_by]
        my_id = seq_record.id+'_'+str(read_num)

        
        fastq_string = "@%s\n%s\n+\n%s\n" % (my_id,my_seq,dummy_quality[:len(my_seq)])
        out_handle.write(fastq_string)
    print ("..done creating %d reads\n") % read_num
        
