#!/jgi/tools/bin/python

import sys 
import csv
import copy
import pprint



def do_inexact_matching( indexes_exact_matches ):
    """ do inexact matching on indexes stored in dictnary"""
    indexes_inexact_matches = {}
    unique_indexes = indexes_exact_matches.keys()
    
    pp = pprint.PrettyPrinter();
    
    for i,index in enumerate(unique_indexes):
        cumulative_count_per_index = 0
        
        print "%d matching %s to %d left indexes" % (i+1,index,len(indexes_exact_matches.keys()))
        for index_in_dict in indexes_exact_matches.keys():
            num_mismatch = compare_substition_in_two_strings_of_equal_length(index,index_in_dict)
            
            
            if(num_mismatch <= 1):
                cumulative_count_per_index += indexes_exact_matches[index_in_dict]
                del indexes_exact_matches[index_in_dict]
        
        if (cumulative_count_per_index):
            indexes_inexact_matches[index] = cumulative_count_per_index
    return indexes_inexact_matches  
                
                
            
            

def compare_substition_in_two_strings_of_equal_length(s1,s2):
    
    if len(s1)  != len(s2):
        print 'length of two input strings not equal \n will skip'
        pass
    
    count_mismatches = 0 ;
    for i in xrange(len(s1)):
            if s1[i] != s2[i]:
                count_mismatches +=1
                if count_mismatches > 1:
                    break
                    
    return (count_mismatches)
    




input_file = sys.argv[1]
csv.register_dialect('multiplex_info',delimiter=' ', quoting=csv.QUOTE_NONE)

with open(input_file, 'rb') as fh:
    reader= csv.reader(fh,'multiplex_info')
    
    indexes_exact_matches = {}
   
   
    
    
    for row in reader:
        if '#' in row:
            continue

        index = row[1].upper()[:-1]
        count = int(row[2])
        
        ##exact matching
        if indexes_exact_matches.get(index) is None:
            indexes_exact_matches[index] =  count 
        else:
            old_count = indexes_exact_matches[index]
            indexes_exact_matches[index] = old_count + count 

        
    ##making a deep of the dictionary
    copy_of_indexes_exact_matches = copy.deepcopy(indexes_exact_matches)
    
    
    #in-exact matching
    indexes_inexact_matches = do_inexact_matching(copy_of_indexes_exact_matches)
        

print 'Total unique indexes %d' % len(indexes_exact_matches)
print 'Total unique indexes %d with one mismatch allowed ' %  len(indexes_inexact_matches)


print 'Top 15 barcodes based on frequency'


sorted_barcodes_exact_match = sorted(indexes_exact_matches, key=indexes_exact_matches.get, reverse=True)
sorted_barcodes_INexact_match = sorted(indexes_inexact_matches, key=indexes_inexact_matches.get, reverse=True)


#print the report
for i in xrange(20):
    barcode_exact_match = sorted_barcodes_exact_match[i]
    barcode_INexact_match = sorted_barcodes_INexact_match[i]

    print "%d \t %s \t %d \t %s \t %d" %(i+1,barcode_exact_match,indexes_exact_matches[barcode_exact_match],barcode_INexact_match,indexes_inexact_matches[barcode_INexact_match])
