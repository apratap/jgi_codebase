#!/usr/bin/env python



import os
import sys
import time
import sqlite3
import fileinput #to join a list of files
import argparse
import itertools

##updating the library look up path
dir = os.path.dirname(__file__)
sys.path.append(os.path.join(dir,'../lib'));
import utils
import taxonomy




def main():
    
    SUB = 'main'
    #user command line args processing
    parser = argparse.ArgumentParser(description='Local Taxonomy Finder:',add_help=True)
    parser.add_argument('-gil','-gi-list-csv',dest='gi_list_csv',default=None,metavar='',help='A comma separated list of NCBI GI numbers')
    parser.add_argument('-gif' ,'-gi-file',dest='gi_file',default=None,metavar='', help='A file with each line equal to a NCBI GI number')
    parser.add_argument('-n','-name-list-csv',dest='name_list_csv',default=None,metavar='',help='A comma separated list of taxa Names')
    parser.add_argument('-nf','-name-file',dest='name_file',default=None,metavar='',help='A file with each line equal to taxa Name')
    parser.add_argument('-version',action='version',version='%(prog)s 1.0')
    user_args = parser.parse_args()
    
    
    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit()
    
    
    if user_args.gi_list_csv is None and user_args.gi_file is None and user_args.name_list_csv is None and user_args.name_file is None:
        parser.print_help()
        sys.exit()
    
    #list to hold all the gi ids
    gi_ids = []
    if user_args.gi_file and os.path.exists(user_args.gi_file):
        with open(user_args.gi_file) as fh:
            gi_ids=[x.strip() for x in fh]
            
    if user_args.gi_list_csv:
        gi_ids.extend(user_args.gi_list_csv.split(','))
    
    
    #list to hold all the names
    taxa_names = []
    if user_args.name_file and os.path.exists(user_args.name_file):
        with open(user_args.name_file) as fh:
            taxa_names=[x.strip() for x in fh]
    if user_args.name_list_csv:
        taxa_names.extend( user_args.name_list_csv.strip().split(',') )
    
    print 'Total names %d' % len(taxa_names)

    #base dir where the NCBI dump is stored
    tax_data_version='Aug.06.2012'
    tax_data_base_dir='/global/homes/a/apratap/projects/Aug_12_NCBI_Taxonomy_local/downloaded_data/08.06.12_dump'
    db_location= tax_data_base_dir + '/taxonomy.db'
    ncbi_tax_id_to_names_file= tax_data_base_dir + '/' + 'names.dmp'
    ncbi_nucl_gi_to_tax_id_file= tax_data_base_dir + '/' + 'gi_taxid_nucl.dmp'
    ncbi_prot_gi_to_tax_id_file= tax_data_base_dir + '/' + 'gi_taxid_prot.dmp' 
    ncbi_nodes_file= tax_data_base_dir + '/' + 'nodes.dmp'
    ncbi_taxanomic_names_whoosh_index_dir = tax_data_base_dir + '/' + 'ncbi_taxa_names_whoosh_index/'
    update=False
    
    
    ############################
    ##NCBI taxa names whoosh index check
    ############################
    if not os.path.exists(ncbi_taxanomic_names_whoosh_index_dir) or update == True:
        sys.stderr.write('[%s]: Creating NCBI taxa names whoosh index %s ...' % (SUB,ncbi_taxanomic_names_whoosh_index_dir))
        taxonomy.create_ncbi_taxonomy_names_index(ncbi_taxanomic_names_whoosh_index_dir, ncbi_tax_id_to_names_file)
        sys.stderr.write('Done \n')
    
        
    #################################
    ##Data base updation phase
    #################################
    #create the dbase
    if not os.path.exists(db_location) or update == True:
        sqllite_conn = sqlite3.connect(db_location)
        taxonomy.create_gi_to_tax_id_table(sqllite_conn,ncbi_nucl_gi_to_tax_id_file,ncbi_prot_gi_to_tax_id_file)
    else:
        sqllite_conn = sqlite3.connect(db_location)
    
    
    #####Main Logic
    sys.stderr.write('NCBI Taxonomy Data Version: %s\n' % tax_data_version)        
    db_cursor = sqllite_conn.cursor()
    tax_id_to_scientific_names      =   taxonomy.get_tax_id_to_scientific_name_dict(ncbi_tax_id_to_names_file)
    tax_id_child_parent_relation    =   taxonomy.get_ncbi_taxonomy_child_parent_relation(ncbi_nodes_file)
    
    
    

    if gi_ids:    
        #################################
        ##mapping gi to taxanomic lineage
        #################################
        #map the gi's to tax_ids
        gi_to_tax_id_map_results = taxonomy.get_ncbi_taxid_from_ncbi_gi(gi_ids,db_cursor)
        
        taxonomy_start = time.clock()
        loop_start = time.clock()
        num = 0;
        for (gi_id,tax_id) in gi_to_tax_id_map_results.items():
            num += 1
            if num % 100000 == 0:
                loop_end = time.clock()
                sys.stderr.write('[%s]: Processed %d GIs in %d seconds \n' % (SUB,num,loop_end-loop_start))
            
            taxonomy_lineage=taxonomy.get_taxonomy_from_taxid(tax_id,tax_id_to_scientific_names,tax_id_child_parent_relation,gi_id)
            sys.stdout.write( '%d \t %d \t %s \n' % (gi_id,tax_id,taxonomy_lineage) )
        
    
    
    if taxa_names:
        #################################
        ##mapping taxa names to taxanomic lineage
        #################################
        loop_start = time.clock()
        for num,taxa_name in enumerate(taxa_names):
            
            if num % 1000 == 0 and num > 0:
                loop_time_check = time.clock()
                sys.stderr.write('[%s]: Processed %d user specified taxa names in %d seconds \n' % (SUB,num,loop_time_check-loop_start))
            
#            print 'Name: %s' % taxa_name
            matched_uniq_tax_ids = taxonomy.search_name_in_NCBI_taxa_names(taxa_name,ncbi_taxanomic_names_whoosh_index_dir)
            for tax_id in matched_uniq_tax_ids:
                taxonomy_lineage=taxonomy.get_taxonomy_from_taxid(int(tax_id),tax_id_to_scientific_names,tax_id_child_parent_relation)
                sys.stdout.write('%s \n' % taxonomy_lineage)
                 
    
    
        
    
#main program
if __name__ == "__main__":
  
    main()


















