#!/jgi/tools/misc_software/python/2.7/bin/python

'''
Read the cufflinks produced gtf file to produce a set of genes
Extract the gene sequence from the genome, given the genome fasta file(one file)
given a DESeq for genes, select only the genes that are part of the list and produce gene and protein fasta file for them 




'''


import sys
sys.path.append('/house/homedirs/a/apratap/dev/eclipse_workspace/python_scripts/lib');

import HTSeq;
import string;
from Bio.Seq import Seq;
from pysam import Fastafile;
from bedtools import IntervalFile;
import seqUtils;
import csv

#import argparse
#
#
#user_args = argparse.ArgumentParser(description = 'Convert Cufflinks transcripts to gene level features')
#user_args.add_argument('-gtf_file', action='store', help='cufflinks transcript gtf file', dest="cuff_gtf" )
#
#print user_args.parse_args(args, namespace)
#








def parse_cuff_gtf_file_and_produce_genes_and_proteins_fasta(cuff_gtf_file,genome_fasta_file,gene_list=None):
    
    gene_fasta_file = 'cuff_genes.fasta'
    protein_fasta_file = 'cuff_proteins.fasta'
    
    if gene_list:
        print "Found list of genes which only be used to create a gene and protein fasta file"
        gene_fasta_file = 'cuff_genes_subset_significantly_expressed.fasta'
        protein_fasta_file = 'cuff_proteins_subset_significantly_expressed.fasta'
    
    if gene_list is None:
        print "Gene list not found"
        sys.exit()
    
    
    
    stranded = 'True'
    genes_start = {};
    genes_end = {};
    genes_strand = {};
    genes_chrom = {};
    
    gff = HTSeq.GFF_Reader(cuff_gtf_file)
    
    i =0
    
    for f in gff:
        if f.type == "transcript":
            try:
                gene_name = f.attr['gene_id']
                genes_strand[gene_name] = f.iv.strand
                genes_chrom[gene_name] = f.iv.chrom
            except KeyError:
                sys.exit("Feature % doesn't contain %s attribute" % (f.name, 'gene_id'))
            if stranded and f.iv.strand == ".":
                sys.stderr.write("Feature %s at %s does not have the strand information \t" % (f.type, f.iv))
                sys.stderr.write("Will skip this feature\n")
                continue
    #        print type(f.iv)
            
            if genes_start.get(gene_name):
                if genes_start.get(gene_name) > f.iv.start:
                    genes_start[gene_name] = f.iv.start
    #                genes_start[gene_name]['strand'] = f.iv.strand
            else:
                genes_start[gene_name] = f.iv.start
    #            genes_start[gene_name]['strand'] = f.iv.strand
            
            if genes_end.get(gene_name):
                if genes_end.get(gene_name) < f.iv.end:
                    genes_end[gene_name] = f.iv.end
    #                genes_end[gene_name]['strand'] = f.iv.strand
            else:
                genes_end[gene_name] = f.iv.end
    #            genes_end[gene_name]['strand'] = f.iv.strand
                
                
            i += 1
            if i % 1000 == 0:
                sys.stderr.write("%d GFF lines processed \n" % i)
           
    print 'Number of genes present %d' % len ( genes_start.keys());

    fasta_fh = Fastafile(genome_fasta_file)
    genes_out_fh = open(gene_fasta_file, 'w')
    proteins_out_fh = open(protein_fasta_file, 'w')
    
    num_genes = 0
    for gene in genes_start.keys():
        
        if gene_list:
            if gene not in gene_list:
#                print "Gene %s not found in gene_list...skipping" % (gene)
                continue
        
        gene_start  = genes_start.get(gene)
        gene_end    = genes_end.get(gene)
        gene_strand = genes_strand.get(gene)
        gene_chrom  = genes_chrom.get(gene)
        gene_fasta_header = '>gene:%s_chrom:%s_strand:%s_start:%d_end:%d\n' % (gene,gene_chrom,gene_strand,gene_start,gene_end);
       
        # fetching the DNA sequence of the gene
        seq = fasta_fh.fetch(gene_chrom,gene_start,gene_end);
        
        ## extracing the rev compliment to work only on the 5' to 3' strand
        if gene_strand == '-':
            seq = seqUtils.dna_RevCompliment(seq)

        genes_out_fh.write(gene_fasta_header);
        genes_out_fh.write(  '\n'.join( seq[i:i+80] for i in xrange(0,len(seq),80)  )     )
        genes_out_fh.write('\n')
        
        orf_analysis_result = seqUtils.orf_predictor_3frames(seq)
        protein_len     = orf_analysis_result[0];
        protein_start   = orf_analysis_result[1];
        protein_end     = orf_analysis_result[2];
        protein_frame   = orf_analysis_result[3];
        protein_seq     = str(orf_analysis_result[4]);
        
        protein_fasta_header = '>gene:%s_chrom:%s_strand:%s_start:%d_end:%d_frame:%d_len:%d\n' % (gene,gene_chrom,gene_strand,protein_start,protein_end,protein_frame,len(protein_seq));
        proteins_out_fh.write(protein_fasta_header)
        proteins_out_fh.write( '\n'.join( protein_seq[i:i+80] for i in xrange(0,len(protein_seq),80 ) ) )
        proteins_out_fh.write( '\n')
        
        num_genes += 1
        if num_genes % 100 == 0:
            print "%d genes processed \n" % num_genes
        
        
        


def get_list_of_significantly_expressed_genes(gene_expression_file):
    '''
    read in the file generated by DESeg which is supposed to have
    list of significantly expressed gene names in the first column
     
    '''
    significant_expressed_gene_list = [];
    geneReader = csv.reader(open(gene_expression_file,'r'),delimiter='\t')

    for row in geneReader:
        if  'id' in row:
            continue
        significant_expressed_gene_list.append(row[0])
    
    print 'Total genes read %d from file %s ' % (len(significant_expressed_gene_list), gene_expression_file)
    return significant_expressed_gene_list;



def main():

    cuff_gtf_file = sys.argv[1]
    genome_fasta_file = sys.argv[2]
    diff_exp_genes = sys.argv[3]

    print 'cuff file is %s ' % cuff_gtf_file
    gene_list = get_list_of_significantly_expressed_genes(diff_exp_genes)
    
    parse_cuff_gtf_file_and_produce_genes_and_proteins_fasta(cuff_gtf_file,genome_fasta_file,gene_list)
    
    
    
    
main()
    
        