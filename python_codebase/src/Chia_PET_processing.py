#!/usr/bin/env python


import os
import sys
import re
import itertools
import logging
import pprint
import time
import pickle
import matplotlib.pyplot as plt
import pysam
import math
import multiprocessing

##updating the library look up path
dir = os.path.dirname(__file__)
sys.path.append(os.path.join(dir,'../lib'));
import bamUtils
import seqUtils
import fastqUtils
import utils



##################
## program globals
###################

#min length for cleaned reads
#mouse genome approximation
min_clean_read_len = 20
  
#kmer length
kmer_len = 12



    
def debug(read1,read2,message):
    """
    print the debug information
    for a read pair
    """
    SUB='debug'
    print '[%s]: Message: %s' % (SUB,message)
    print '%s \n %s \n type: %s \n' % ( read1, read2, type(read1))
   



def get_linker_position(linker_matches,only_first_match=False):
    
    if linker_matches is not False and only_first_match is False:
        return ([ x[0] for x in linker_matches ])
    
    if linker_matches is not False and only_first_match is True:
        return (linker_matches[0][0])
    


def split_merged_reads_by_linker_cleaning(merged_fastq,**kwargs):
    """
    given a merged fastq(using flash normally) from ChIA-PET run
    clean the reads by taking out linker from both sides,
    and saving the split reads as read1 & read2
    """
    
    SUB = '%s:%s' % (__name__,'split_merged_reads_by_linker_cleaning')
    
    
    if kwargs.get('logger',None) is None:
        logger = utils._get_logger('default.log',logger_name=SUB)
    else:
        logger = kwargs.get('logger',None)
    
     
    #user settings
    logger.info('processing fastq file %s' % merged_fastq )
    logger.info('kmer-len %d' % kmer_len)
    logger.info('min cleaned read len: %d' % min_clean_read_len)
    
    
    #output data file
    clean_read1 = merged_fastq+'_jlinker_read_1.fastq'
    clean_read2 = merged_fastq+'_jlinker_read_2.fastq'
    clean_read1_fh = open(clean_read1,'w')
    clean_read2_fh = open(clean_read2,'w')
    
    
    #partial linker A
    linkerA = 'GTTGGATAAGATATCGCGG'
    #partial linker B
    linkerB = 'GTTGGAATGTATATCGCGG'
    

    #build the kmer dictionary
    kmers_linker_A = seqUtils.get_kmers_dict_with_one_error(linkerA, kmer_len)
    kmers_linker_B = seqUtils.get_kmers_dict_with_one_error(linkerB, kmer_len)
    
    
    #dict to store read and trim lengths
    read_lengths            = []
    read_1_trim_lengths    = []
    read_2_trim_lengths    = []
    

    count_linker_found = 0
    count_multi_linker = 0
    
    count_A = 0
    count_A_gt_min_len = 0
    count_B = 0
    count_B_gt_min_len = 0
    count_AB = 0
    count_no_linker = 0
    
    prog_start = time.clock()
    loop_start = time.clock()
    
    for (count,read) in  itertools.izip(itertools.count(1),fastqUtils.get_genericfastq_iterator(merged_fastq)):
        if (count % 100000 == 0):
            loop_stop = time.clock()
            time_taken = loop_stop - loop_start 
            total_time_taken = loop_stop - prog_start
            logger.info('Processed %d merged in %s seceonds: total time: %s seconds' % (count, time_taken,total_time_taken))
            loop_start = loop_stop
        

        read_lengths.append(len(read.seq))
        
        read_linkerA_matches  = seqUtils.do_kmer_dict_match(read.seq, kmers_linker_A, kmer_len,report_all_matches=True)
        read_linkerB_matches  = seqUtils.do_kmer_dict_match(read.seq, kmers_linker_B, kmer_len,report_all_matches=True)
        
        ##reverse read to find the trimming point from the other end
        rev_read = read.get_revComp_read()
        rev_read_linkerA_matches  = seqUtils.do_kmer_dict_match(rev_read.seq, kmers_linker_A, kmer_len,report_all_matches=True)
        rev_read_linkerB_matches  = seqUtils.do_kmer_dict_match(rev_read.seq, kmers_linker_B, kmer_len,report_all_matches=True)
        
        
        #to debug        
        #debug(read,rev_read,read_linkerA_matches,read_linkerB_matches,rev_read_linkerA_matches,rev_read_linkerB_matches)
        

        #AB pair
        if(read_linkerA_matches and read_linkerB_matches):
            count_AB += 1
            count_linker_found += 1
            continue
        #no linker is found
        elif(read_linkerA_matches is False and read_linkerB_matches is False):
            count_no_linker +=1
        #useful pair either AA or BB
        else:
            count_linker_found += 1
            if read_linkerA_matches:
                if not rev_read_linkerA_matches:
                    logger.critical('Error: the rev comp of the read %s doesnt show linker match' % read.header)
                else:
                    count_A += 1
                    trim_read_1_len = read_linkerA_matches[0]
                    trim_read_2_len = rev_read_linkerA_matches[0]
                    read_1_trim_lengths.append(trim_read_1_len)
                    read_2_trim_lengths.append(trim_read_2_len)
#                    global min_clean_read_len
                    if ( trim_read_1_len >= min_clean_read_len and trim_read_2_len >= min_clean_read_len):
                        count_A_gt_min_len += 1
                        trimmed_read_1 = read.trim_read(trim_read_1_len)
                        trimmed_read_2 = rev_read.trim_read(trim_read_2_len)
                        fastqUtils.writeFastq_read(clean_read1_fh, trimmed_read_1)
                        fastqUtils.writeFastq_read(clean_read2_fh, trimmed_read_2)
#                        print '---------Cleaned fastq--------'
#                        trimmed_read_1.print_read()
#                        trimmed_read_2.print_read()
            
            elif read_linkerB_matches:
                if not rev_read_linkerB_matches:
                    logger.critical('Error: the rev comp of the read %s doesnt show linker match' % read.header)
                else:
                    count_B += 1
                    trim_read_1_len = read_linkerB_matches[0]
                    trim_read_2_len = rev_read_linkerB_matches[0]        
                    read_1_trim_lengths.append(trim_read_1_len)
                    read_2_trim_lengths.append(trim_read_2_len)
                    if ( trim_read_1_len >= min_clean_read_len and trim_read_2_len >= min_clean_read_len):
                        count_B_gt_min_len += 1 
                        trimmed_read_1 = read.trim_read(trim_read_1_len)
                        trimmed_read_2 = rev_read.trim_read(trim_read_2_len)
                        fastqUtils.writeFastq_read(clean_read1_fh, trimmed_read_1)
                        fastqUtils.writeFastq_read(clean_read2_fh, trimmed_read_2)
             
    logger.info('#########################################')
    logger.info('total reads processed: %d' % (count))
    logger.info('found linker in %d reads ' % (count_linker_found))
    logger.info('no linker in %d reads'  % (count_no_linker))
#    print 'found multi-linker in %d reads ' % (count_multi_linker)
    logger.info('found A in %d | %d reads (each read > %d bp) ' % (count_A, count_A_gt_min_len,min_clean_read_len))
    logger.info('found B in %d | %d  reads(each read > %d bp) ' % (count_B, count_B_gt_min_len,min_clean_read_len))
    logger.info('found AB in %d reads ' % (count_AB))
    clean_read1_fh.close()
    clean_read2_fh.close()
    
    #pickle.dump(read_length,open('merged_read_length.pickle','wb'))
    #pickle.dump(,open('linker_first_match_position.pickle','wb'))
    
    #matplotlib.rcParams['figure.figsize'] = (12,8)
    plt.hist(read_1_trim_lengths,bins=50,label='read_1_trim_lengths')
    plt.hist(read_2_trim_lengths,bins=50, label='read_2_trim_lengths')
    plt.grid(True)
    plt.xlabel('Read position')
    plt.ylabel('frequency')
    plt.legend()
    plt.savefig('linker_first_match_position.png',dpi=175)
    

 

def convert_to_proper_intER_chrom_pair(read1,read2,counters):
        
    #both reads map in reverse
    if (read1.is_reverse and read2.is_reverse):
        counters.count_chimeric_opp_strand += 1
        read1.flag =  1 + 16 + 32 + 64         # ( read is paired, rev_comp, mate_is_rev_comp, first in pair); NOT properly aligned so no sam flag 2
        read2.flag =  1 + 16 + 32 + 128        # ( read is paired, rev_comp, mate_is_rev_comp, second in pair); NOT properly aligned so no sam flag 2
        
    
    #both reads map in forward direction
    elif (not read1.is_reverse and not read2.is_reverse):
        counters.count_chimeric_same_strand += 1 
        read1.flag =  1 + 64         # ( read is paired, first in pair); NOT properly aligned so no sam flag 2
        read2.flag =  1 + 128        # ( read is paired, second in pair); NOT properly aligned so no sam flag 2
    
    #read 1 maps on the same strand and read 2 on the opposite
    elif ( not read1.is_reverse and read2.is_reverse):
        counters.count_chimeric_diff_strand += 1
        read1.flag =  1 + 64 + 32       # ( read is paired, first in pair,mate_is_rev_comp); NOT properly aligned so no sam flag 2
        read2.flag =  1 + 128 + 16        # ( read is paired, second in pair, rev_comp); NOT properly aligned so no sam flag 2
        
    
    #read 1 maps on the opp strand and read 1 on the same
    elif ( read1.is_reverse and not read2.is_reverse):
        counters.count_chimeric_diff_strand += 1
        read1.flag =  1 + 64 + 16       # ( read is paired, first in pair,rev_comp); NOT properly aligned so no sam flag 2
        read2.flag =  1 + 128 + 32        # ( read is paired, second in pair, mate_is_rev_comp); NOT properly aligned so no sam flag 2    
        
    else:
        print "can't determine direction of chimeric pair"
        print read1.qname
        return False
    
    
    ### update the PNEXT tag
    read1.pnext = read2.pos
    read2.pnext = read1.pos
        
    ##add the RNEXT tag
    read1.rnext = read2.tid
    read2.rnext  = read1.tid
    
    #fixing the insert size
    insert_size = 0 #in the case of chimeric pairs
    read1.tlen = insert_size
    read2.tlen = insert_size

    return True



def convert_to_proper_intRA_chrom_pair(read1,read2,counters):
    
    
    #finding the absolute insert size of the fragment
    abs_insert_size = 0
    if (read1.pos < read2.pos):
        abs_insert_size = read2.pos + read2.qlen - read1.pos
    elif (read2.pos < read1.pos):
        abs_insert_size = read1.pos + read1.qlen - read2.pos
    elif (read1.pos == read2.pos):
        abs_insert_size = 0
    else:
        debug_messsage= "Can't determine the abs insert size"
        debug(read1,read2,debug_messsage)
        
    
    counters.intraChrom_pairs_insert_size.append(math.log10(abs_insert_size+1))
        
            
    #case 1 : read 1 maps on the opp strand 
        #two sub cases possible
    if (read1.is_reverse and not read2.is_reverse):
        read1.flag =  1 + 2 + 16 + 64         # ( read is paired, properly aligned, rev_comp, first in pair);
        read2.flag =  1 + 2 + 32 + 128        # ( read is paired, properly aligned, mate_is_rev_comp, second in pair);
        
        # <------(r1) ----->(r2)
        if (read1.pos <= read2.pos):
            read1.tlen = abs_insert_size
            read2.tlen = -abs_insert_size
            counters.count_intraChrom_outward_facing += 1
            
            
        # ----->(r2)  <------(r1) 
        elif (read2.pos <= read1.pos):
            read1.tlen = -abs_insert_size
            read2.tlen = abs_insert_size
            counters.count_intraChrom_inward_facing += 1
            
        
    
    #case 2 : read 1 is on the same strand
            #two sub cases possible
    elif (not read1.is_reverse and read2.is_reverse):
        read1.flag =  1 + 2 + 32 + 64         # ( read is paired, properly aligned, mate_is_rev_comp, first in pair);
        read2.flag =  1 + 2 + 16 + 128        # ( read is paired, properly aligned, rev_comp , second in pair);
        # <---------(r2)  -------->(r1)
        if (read2.pos <= read1.pos):
            read1.tlen = -abs_insert_size
            read2.tlen = abs_insert_size
            counters.count_intraChrom_outward_facing += 1
        # ------>(r1)    <-------(r2)
        elif (read1.pos <= read2.pos):
            read1.tlen = abs_insert_size
            read2.tlen = -abs_insert_size
            counters.count_intraChrom_inward_facing += 1
        
    
    #case 3 : read 1 and 2 on the same strand
        #two sub cases possible
    elif (not read1.is_reverse and not read2.is_reverse):         
        read1.flag =  1 + 2 + 32 + 64         # ( read is paired, properly aligned, first in pair);
        read2.flag =  1 + 2 + 16 + 128        # ( read is paired, properly aligned, second in pair);
        
        counters.count_intraChrom_same_strand += 1
        
        #fixing the insert size
        # ------>(r2) ------>(r1)
        if (read2.pos <= read1.pos):
            read1.tlen = abs_insert_size
            read2.tlen = -abs_insert_size
            
        # ------>(r1) ------>(r2)
        elif (read1.pos <= read2.pos):
            read1.tlen = abs_insert_size
            read2.tlen = -abs_insert_size
        
        
        
    #case 4 : read 1 and 2 map on the opp strand
        #two sub cases possible
    elif ( read1.is_reverse and read2.is_reverse):
        read1.flag =  1 + 2 + 16 + 32 + 64         # ( read is paired, properly aligned, rev_comp, mate_is_rev_comp, first in pair);
        read2.flag =  1 + 2 + 16 + 32 + 128        # ( read is paired, properly aligned, rev_comp, mate_is_rev_comp, second in pair);
        
        counters.count_intraChrom_opp_strand += 1
        
        #fixing the insert size
        # <------(r1) <------(r2)
        if (read2.pos <= read1.pos):
            read1.tlen = abs_insert_size
            read2.tlen = -abs_insert_size
        # <------(r2) <------(r1))
        elif (read1.pos <= read2.pos):
            read1.tlen = abs_insert_size
            read2.tlen = -abs_insert_size
    
    else:
        message="Can't determine the intra-Chrom read pair bin"
        debug(read1,read2,message)
        return (False)
    
        
    ### update the PNEXT tag
    read1.pnext = read2.pos
    read2.pnext = read1.pos
        
    ##add the RNEXT tag
    read1.rnext = read2.tid
    read2.rnext  = read1.tid
    
    return True

 
 
def ChIA_PET_manual_pairing(read1,read2,counters,interChrom_bam_fh,intraChrom_bam_fh):
    '''
    given two reads of a pair/fragment
    the function tries to manually pair them 
    '''
    
    
    
    #have to be a sequencing pair : header same
    if bamUtils.is_a_sequencing_readPair(read1,read2) is False:
        print "Read header's are not name : most like input read1 and read2 are not name sorted"
        print '%s \t %s' % (read1.qname,read2.qname)
        sys.exit(1)
    
    #unmapped pair
    elif read1.is_unmapped and read2.is_unmapped:
        counters.count_unmapped_pairs += 1
        return 
    
    #singleton pair
    elif read1.is_unmapped or read2.is_unmapped:
        counters.count_singleton_pairs += 1
        return 
    
        
    #chimeric pair (interChrom pair)
    elif bamUtils.is_chimeric_pair(read1, read2) is True:
        counters.count_chimeric_pairs += 1
        
        if convert_to_proper_intER_chrom_pair(read1,read2,counters) is not False:
            #write  the pair to intra_chrom bam
            interChrom_bam_fh.write(read1)
            interChrom_bam_fh.write(read2)
        return 
    
    #intraChrom pair
    elif bamUtils.is_intraChrom_pair(read1,read2):
        counters.count_intraChrom_pairs += 1
        
        if convert_to_proper_intRA_chrom_pair(read1,read2,counters) is not False:
            intraChrom_bam_fh.write(read1)
            intraChrom_bam_fh.write(read2)
        return 
        
    ##undetermined cases
    else:
        counters.count_undetermined_pairs += 1
        


def analyze_split_cleaned_individual_mapped_reads(read1_bamFile,read2_bamFile):
    '''
    given a bam(read 1 and read 2) file the method will split it into two bins:
        1. read pairs that are mapping to two different chromosomes : chimeras
        2. read pairs that map on the same chromosome
        PS: Not sure what needs to be done about the singletons and unmapped pairs : for now they will be removed
        
        Besides giving out the summary of #chimeric pairs, #proper reads, #singletons, #unmapped
        
    '''
    
    print ' reached: test_analyze_split_cleaned_individual_mapped_reads'
    
    #sorting both the bam files at the same time
    jobs = []
    out_q = multiprocessing.Queue()
    
    for bamFile in [read1_bamFile,read2_bamFile]:
        p=multiprocessing.Process(target=bamUtils.name_sort_bam, args=(bamFile,out_q,))
        jobs.append(p)
        p.start()
        p.join()
    
    ##possible bug : not checked##
    ## assuming that the order of return in the queue is same as the order in which the worker function were called
    name_sorted_read_1_bamFile = out_q.get() 
    name_sorted_read_2_bamFile = out_q.get() 
    
    
    print 'read 1 name sorted %s ' % name_sorted_read_1_bamFile
    print 'read 2 name sorted %s ' % name_sorted_read_2_bamFile
    
    
    read_1_fh = pysam.Samfile(name_sorted_read_1_bamFile,'rb')
    read_2_fh = pysam.Samfile(name_sorted_read_2_bamFile,'rb')
    
    
    #out files
    interChrom_bam_fh=pysam.Samfile('inter_chrom.bam','wb',template=read_1_fh)
    intraChrom_bam_fh=pysam.Samfile('intra_chrom.bam','wb',template=read_1_fh)
    
    
    class counters(object):
        #counters
        count_singleton_pairs       = 0
        count_chimeric_pairs        = 0
        count_chimeric_same_strand = 0
        count_chimeric_opp_strand = 0
        count_chimeric_diff_strand = 0
        count_unmapped_pairs        = 0
        count_intraChrom_pairs      = 0
        count_intraChrom_inward_facing = 0
        count_intraChrom_outward_facing = 0
        count_intraChrom_same_strand = 0
        count_intraChrom_opp_strand = 0
        count_undetermined_pairs    = 0
        
        
        #intra-chrom insert size bin
        intraChrom_pairs_insert_size = [];
        
    
    for (pair_num,read1,read2) in itertools.izip(itertools.count(1),read_1_fh,read_2_fh):
        #main function to check for suitable pair
        #print (pair_num,read1,read2)
        
        ChIA_PET_manual_pairing(read1,read2,counters,interChrom_bam_fh,intraChrom_bam_fh)
        
        if (pair_num % 100000 == 0):
            print 'Processed %d pairs' % pair_num

#        if (pair_num % 1000000 == 0):
#            break
        
    #summary stats
    print '[Summary]:'
    print '#pairs processed: %d ' % (pair_num)
    print '#Singleton pairs: %d' % (counters.count_singleton_pairs)
    print '#Unmapped pairs: %d' % (counters.count_unmapped_pairs)
    print '#Undetermined pairs: %d' % (counters.count_undetermined_pairs)
    print '#Intra-Chrom pairs: %d' % (counters.count_intraChrom_pairs)
    print '\t\t inwards : %d' % (counters.count_intraChrom_inward_facing)
    print '\t\t outwards : %d' % (counters.count_intraChrom_outward_facing)
    print '\t\t same strand : %d' % (counters.count_intraChrom_same_strand)
    print '\t\t opp strand : %d' % (counters.count_intraChrom_opp_strand)
    print '#Chimeric(Inter-Chrom) pairs: %d' % (counters.count_chimeric_pairs)
    print '\t\t same strand : %d' % (counters.count_chimeric_same_strand)
    print '\t\t opp strand : %d' % (counters.count_chimeric_opp_strand)
    print '\t\t diff strand : %d' % (counters.count_chimeric_diff_strand)
    
    
    #creating the insert size for intra-chromosomal mapped reads
    plt.hist(counters.intraChrom_pairs_insert_size,bins=50,label='intra-chromosomal-insert-size')
    plt.grid(True)
    plt.xlabel('log10(Insert size+1)')
    plt.ylabel('frequency')
    plt.legend()
    plt.savefig('hist_intra_chromosomal_insertSize.png',dpi=175)



def find_ChIA_pet_interactions(merged_bamFile,read2_bamFile):
    '''
     
     brief summary:
     Files needed:
     1. sorted bam file of merged inter N intra chrom reads
     2. TSS regions
     3. Uniq regions : formed by extending reads by 1.5Kb on both sides and doing the merge
     
     
     
     '''
    
    
    
    
    




#######
#MAIN
#######    
def main():
    
    #input fastq to be cleaned
    merged_fastq_file = sys.argv[1]
    split_merged_reads_by_linker_cleaning(sys.argv[1])
    
    #bamFile =  sys.argv[1]
    #analyze_split_cleaned_individual_mapped_reads(sys.argv[1],sys.argv[2])
    
    
   
    
################################
#Program start
################################
if __name__ == "__main__":
    main()
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

    
    '''
    
    
    #intra chromosomal reads
    nameSorted_intra_chrom_bamFile = nameSorted_bamFile.replace('.bam','_intra_Chrom.bam')
    nameSorted_intra_chrom_bamFile_handle = pysam.Samfile(nameSorted_intra_chrom_bamFile,'wb',template=nameSorted_bamFile_handle)
    
    #inter chromosomal reads
    nameSorted_inter_chrom_bamFile = nameSorted_bamFile.replace('.bam','_inter_Chrom.bam')
    nameSorted_inter_chrom_bamFile_handle = pysam.Samfile(nameSorted_inter_chrom_bamFile,'wb',template=nameSorted_bamFile_handle)
    
    #debug
    debug_bamFile_handle = pysam.Samfile('debug.bam','wb',template=nameSorted_bamFile_handle)
    
    
    #counters
    chimeric_pairs = 0
    proper_mapped_pairs = 0
    proper_mapped_pairs_insert_sizes = []
    unmapped_pairs = 0
    singleton_pairs = 0
    
    

    for count,read1,read2 in itertools.izip(itertools.count(1),nameSorted_bamFile_handle,nameSorted_bamFile_handle):
        if count % 1000000 == 0:
            print 'Processed %d pairs ' % count
#        if count >= 10000000:
#            break
        
        if bamUtils.is_a_sequencing_readPair(read1,read2) is False:
            print 'Read pair %s \n %s are not paired \n' % (read1,read2)
            sys.exit(3) 

        if bamUtils.is_chimeric_pair(read1, read2) is True:
            chimeric_pairs += 1
            
            ##add the color for chimeric reads
            #read1.tags = read1.tags + [("YC", 'red' )]
            #read2.tags = read2.tags + [("YC", 'red' )]
            
            #write the pair of reads to inter_chrom bam
            nameSorted_inter_chrom_bamFile_handle.write(read1)
            nameSorted_inter_chrom_bamFile_handle.write(read2)
            
            #read1_map_chr = bamFile_handle.getrname(read1.tid)
            #read2_map_chr = bamFile_handle.getrname(read2.tid)
            #read1_map_start = read1.pos
            #read1_len = read1.qlen
            #print max([ pileup.n for pileup in bamFile_handle.pileup(read1_map_chr,read1_map_start,read1_map_start+read1_len ,max_depth=20000 )])
            #print read1.qname,read1_map_chr,read2_map_chr,read1_map_start,read1_len
            continue
        
        elif read1.is_unmapped and read2.is_unmapped:
            unmapped_pairs +=1
            continue
        
        elif read1.is_unmapped or read2.is_unmapped:
            singleton_pairs += 1
            continue
        
        else:
            proper_mapped_pairs += 1
            fragment_insert_size = math.fabs(read1.tlen)
            proper_mapped_pairs_insert_sizes.append(math.log(fragment_insert_size+1,10)  )
            nameSorted_intra_chrom_bamFile_handle.write(read1)
            nameSorted_intra_chrom_bamFile_handle.write(read2)
            
            #debugging
            if fragment_insert_size == 0:
                debug_bamFile_handle.write(read1)
                debug_bamFile_handle.write(read2)
            
        
    

    #creating the insert size for intra-chromosomal mapped reads
    plt.hist(proper_mapped_pairs_insert_sizes,bins=50,label='intra-chromosomal-insert-size')
    plt.grid(True)
    plt.xlabel('log10(Insert size+1)')
    plt.ylabel('frequency')
    plt.legend()
    plt.savefig('hist_intra_chromosomal_insertSize.png',dpi=175)
    

    print '#pairs %d' % count
    print '#unmapped %d' % unmapped_pairs
    print '#singletons %d' % singleton_pairs
    print '#proper %d' % proper_mapped_pairs
    print '#chimeric %d' % chimeric_pairs
    print '#proper %d' % len(proper_mapped_pairs_insert_sizes)

    '''
    
    

    
    ####
    ##checking for uniqueness between the two linkers to determine the appropriate 
    ####
        
    '''
    for kmer_len in xrange(8,35):
        kmers_linker_A = seqUtils.get_kmers_dict_with_one_error(linkerA, kmer_len)
#        kmers_linker_AB = seqUtils.get_kmers_dict_with_one_error(linkerAB, kmer_len)
        linker_B_matches_A = seqUtils.do_kmer_dict_match(linkerB, kmers_linker_A, kmer_len,report_all_matches=True)
#        linker_B_matches_AB = seqUtils.do_kmer_dict_match(linkerB, kmers_linker_AB, kmer_len,report_all_matches=True)
        
        print 'Overlap of linkerB At kmer_len %d with linkerA %s' % ( kmer_len, linker_B_matches_A)
        print '#kmers Linker A : %d ' % (len(kmers_linker_A))
       
    
    
    print 'Len Linker A : %d \nLen Linker B: %d' % (len(linkerA),len(linkerB))
    print '%s  linker A' % linkerA
    print '%s  linker B' % linkerB
    #print '%s  linker AB' % linkerAB
    
    
    #looks like the minimum kmer-length at which this is no overlap between linkerA,B is 12 based on the above test
    '''
    
    
    
    
    
    
    
    #fulllinker A
#    linkerA = 'GTTGGATAAGATATCGCGGCCGCGATATCTTATCCAAC'
    #fulllinker B
#    linkerB = 'GTTGGAATGTATATCGCGGCCGCGATATACATTCCAAC'
    #full chimeric linker
#    linkerAB = 'GTTGGATAAGATATCGCGGCCGCGATATACATTCCAAC'
   
    
    
    
    
    
    
    
    
    '''
    
    print '#################'
    print 'Summary:'
    print '#################'
    print 'Length LinkerA: %d LinkerB: %d'  % (len(linkerA), len(linkerB))
    print '#pairs:  %d'                     % count
    print '#AA_pairs: %d  | %d '                   % (pairs_AA, pairs_AA_gt_minlen)
    print '#BB_pairs: %d  | %d '                   % (pairs_BB, pairs_BB_gt_minlen)
    print '#pairs_singeltons: %d | %d '           % (pairs_singeltons,pairs_singeltons_gt_min)
    
    print '#AB_pairs: %d'                   % pairs_AB
    print '#pairs_ambiguous: %d'            % pairs_ambiguous
    
    
    print '#pairs_no_linker: %d'            % pairs_no_linker
    print '#pairs_cant_determine:%d'        % pairs_cant_determine
    
    print '----------------------'
    print '###For debugging:'
    print '#pairs with multi matches %d '   % pairs_with_multi_matches
    print '#pairs with one match/read %d '   % pairs_with_single_match_per_read
    print '#total linker match positions %d'    % len(linker_all_positions)
    print '#total first linker match %d'        % len(linker_first_match_positions)
    
    
    pickle.dump(linker_all_positions,open('linker_all_positions.pickle','wb'))
    pickle.dump(linker_first_match_positions,open('linker_first_match_position.pickle','wb'))
    

    #matplotlib.rcParams['figure.figsize'] = (12,8)
    plt.hist(linker_all_positions,bins=50,label='All linker positions')
    plt.hist(linker_first_match_positions,bins=50, label='First linker match position')
    plt.grid(True)
    plt.xlabel('Read position')
    plt.ylabel('frequency')
    plt.legend()
    plt.savefig('linker_match_positions.png',dpi=175)
    
    
    prog_stop = time.clock()
    print 'Total Time taken : %s' % (prog_stop-prog_start) 
    '''
    
    
"""
#updating the dictionary to store the linker match position in a read
        if read1_linkerA_matches:
            linker_all_positions.extend(read1_linkerA_matches)
            linker_first_match_positions.append(read1_linkerA_matches[0])
        if read1_linkerB_matches:
            linker_all_positions.extend(read1_linkerB_matches)
            linker_first_match_poget_ncbi_taxid_from_ncbi_gisitions.append(read1_linkerB_matches[0])
        if read2_linkerA_matches:
            linker_all_positions.extend(read2_linkerA_matches)
            linker_first_match_positions.append(read2_linkerA_matches[0])
        if read2_linkerB_matches:
            linker_all_positions.extend(read2_linkerB_matches)
            linker_first_match_positions.append(read2_linkerB_matches[0])
    
        
        #######################################
        ##AA pair    
        ## read 1 and read 2 both have linker A
        #######################################
        if (  read1_linkerA_matches is not False and read1_linkerB_matches is False
              and
              read2_linkerA_matches is not False and read2_linkerB_matches is False
           ):
            result='AA Pair'
            pairs_AA += 1 
            #check if the pair is usable
            if read1_linkerA_matches[0] >= min_clean_read_len and read2_linkerA_matches[0] >= min_clean_read_len:
                pairs_AA_gt_minlen += 1 
                result = 'Clean AA Pair'
                fastqUtils.write(clean_read1_fh,read1,trim_length=read1_linkerA_matches[0])
                fastqUtils.write(clean_read2_fh,read2,trim_length=read2_linkerA_matches[0])
                #check if multiple linkers found
                if len(read1_linkerA_matches) > 1 or len(read2_linkerA_matches) > 1:
                    pairs_with_multi_matches += 1
                else:
                    pairs_with_single_match_per_read += 1
                 
    
        #########################################
        ##BB pair
        ## read 1 and read 2 both have linker B
        #########################################
        elif (  
                read1_linkerA_matches is  False  and read1_linkerB_matches is not False
                and 
                read2_linkerA_matches is  False  and read2_linkerB_matches is not False
            ):
            result='BB Pair'
            pairs_BB += 1
            #check if the pair is usable
            if read1_linkerB_matches[0] >= min_clean_read_len and read2_linkerB_matches[0]>=min_clean_read_len:
                pairs_BB_gt_minlen += 1 
                result = 'Clean BB Pair'
                fastqUtils.write(clean_read1_fh,read1,trim_length=read1_linkerB_matches[0])
                fastqUtils.write(clean_read2_fh,read2,trim_length=read2_linkerB_matches[0])
                #check if multiple linkers found
                if len(read1_linkerB_matches) > 1 or len(read2_linkerB_matches) > 1:
                    pairs_with_multi_matches += 1
                else:
                    pairs_with_single_match_per_read +=1
        
        
        ########################################
        ##AB or BA pair
        ## read 1 and read 2 both have linker A
        ########################################
        elif (  
                ( read1_linkerA_matches is not False or read1_linkerB_matches is not False )
                and 
                ( read2_linkerA_matches is not False or read2_linkerB_matches is not False )
            ):
            
            #sub case : if linker A and B found on both the reads 1 and 2
            if  ( 
                 ( read1_linkerA_matches is not False and read1_linkerB_matches is not False )
                 and
                 ( read2_linkerA_matches is not False and read2_linkerB_matches is not False )
                ):
                result='Ambiguous pair : linker A/B found on read 1 and 2' 
                pairs_ambiguous += 1
                
            else:
                result='AB / BA pair'
                pairs_AB += 1
                
                
                
        #################################### 
        ## singleton pair : A or B
        ## linker found on only one read
        #####################################
        elif (  
                ( read1_linkerA_matches is not False or read1_linkerB_matches is not False )
                or 
                ( read2_linkerA_matches is not False or read2_linkerB_matches is not False )
           ):
            
            
        
            #sub case : if both linker A and B are found on one read (either read 1 or 2)
            if(
                  ( read1_linkerA_matches is not False and read1_linkerB_matches is not False )
                  or
                  (read2_linkerA_matches is not False and read2_linkerB_matches is not False )
                ):
                result='Ambiguous pair : linker A/B found on read 1 and 2' 
                pairs_ambiguous += 1
                
            else:
                result='singleton A or B Pair'
                pairs_singeltons += 1
                
                #checking if this singleton pair could satisfy the condition of a clean read pair
                if read1_linkerA_matches:
                    if read1_linkerA_matches[0] > min_clean_read_len:
                        pairs_singeltons_gt_min +=1
                        result='Clean singleton A or B Pair'
                        fastqUtils.write(clean_read1_fh,read1,trim_length=read1_linkerA_matches[0])
                        fastqUtils.write(clean_read2_fh,read2,trim_length=None)
                        #check if multiple linkers found
                        if len(read1_linkerA_matches) > 1:
                            pairs_with_multi_matches += 1
                        else:
                            pairs_with_single_match_per_read += 1
                        
                if read1_linkerB_matches:
                    if read1_linkerB_matches[0] > min_clean_read_len:
                        pairs_singeltons_gt_min +=1
                        result='Clean singleton A or B Pair'
                        fastqUtils.write(clean_read1_fh,read1,trim_length=read1_linkerB_matches[0])
                        fastqUtils.write(clean_read2_fh,read2,trim_length=None)
                        #check if multiple linkers found
                        if len(read1_linkerB_matches) > 1:
                            pairs_with_multi_matches += 1
                        else:
                            pairs_with_single_match_per_read += 1
                        
                        
                if read2_linkerA_matches:
                    if read2_linkerA_matches[0] > min_clean_read_len:
                        pairs_singeltons_gt_min +=1
                        result='Clean singleton A or B Pair'
                        fastqUtils.write(clean_read1_fh,read1,trim_length=None)
                        fastqUtils.write(clean_read2_fh,read2,trim_length=read2_linkerA_matches[0])
                        #check if multiple linkers found
                        if len(read2_linkerA_matches) > 1:
                            pairs_with_multi_matches += 1 
                        else:
                            pairs_with_single_match_per_read += 1                       
                        
                if read2_linkerB_matches:
                    if read2_linkerB_matches[0] > min_clean_read_len:
                        pairs_singeltons_gt_min +=1
                        result='Clean singleton A or B Pair'
                        fastqUtils.write(clean_read1_fh,read1,trim_length=None)
                        fastqUtils.write(clean_read2_fh,read2,trim_length=read2_linkerB_matches[0])
                        #check if multiple linkers found
                        if len(read2_linkerB_matches) > 1:
                            pairs_with_multi_matches += 1  
                        else:
                            pairs_with_single_match_per_read += 1
                        
                
        #############################
        ##pair with no linker at all
        #############################
        elif (  
                ( read1_linkerA_matches is False and read1_linkerB_matches is False )
                and
                ( read2_linkerA_matches is False or read2_linkerB_matches is False )
           ):
            result='No linker found'
            pairs_no_linker += 1
    
        
        ################################
        ##can't determine the kind of pair in the code
        #################################
        else:
            result='Cant determine the type of Pair'
            pairs_cant_determine += 1
            debug(read1,read2,read1_linkerA_matches,read1_linkerB_matches,read2_linkerA_matches,read2_linkerB_matches,result)
            
            
            
            

def analyze_split_cleaned_mapped_reads(bamFile):
    '''
    given a bam file the method will split it into two bins:
        1. read pairs that are mapping to two different chromosomes : chimeras
        2. read pairs that map on the same chromosome
        PS: Not sure what needs to be done about the singletons and unmapped pairs : for now they will be removed
        
        Besides giving out the summary of #chimeric pairs, #proper reads, #singletons, #unmapped
        
    '''

    #name sort the file(done just once)
    nameSorted_bamFile = bamUtils.name_sort_bam(bamFile)
    nameSorted_bamFile_handle = pysam.Samfile(nameSorted_bamFile,'rb')
    
    #intra chromosomal reads
    nameSorted_intra_chrom_bamFile = nameSorted_bamFile.replace('.bam','_intra_Chrom.bam')
    nameSorted_intra_chrom_bamFile_handle = pysam.Samfile(nameSorted_intra_chrom_bamFile,'wb',template=nameSorted_bamFile_handle)
    
    #inter chromosomal reads
    nameSorted_inter_chrom_bamFile = nameSorted_bamFile.replace('.bam','_inter_Chrom.bam')
    nameSorted_inter_chrom_bamFile_handle = pysam.Samfile(nameSorted_inter_chrom_bamFile,'wb',template=nameSorted_bamFile_handle)
    
    #debug
    debug_bamFile_handle = pysam.Samfile('debug.bam','wb',template=nameSorted_bamFile_handle)
    
    
    #counters
    chimeric_pairs = 0
    proper_mapped_pairs = 0
    proper_mapped_pairs_insert_sizes = []
    unmapped_pairs = 0
    singleton_pairs = 0
    
    

    for count,read1,read2 in itertools.izip(itertools.count(1),nameSorted_bamFile_handle,nameSorted_bamFile_handle):
        if count % 1000000 == 0:
            print 'Processed %d pairs ' % count
        
        
        if bamUtils.is_a_sequencing_readPair(read1,read2) is False:
            print 'Read pair %s \n %s are not paired \n' % (read1,read2)
            sys.exit(3) 

        if bamUtils.is_chimeric_pair(read1, read2) is True:
            chimeric_pairs += 1
            
            ##add the color for chimeric reads
            #read1.tags = read1.tags + [("YC", 'red' )]
            #read2.tags = read2.tags + [("YC", 'red' )]
            
            #write the pair of reads to inter_chrom bam
            nameSorted_inter_chrom_bamFile_handle.write(read1)
            nameSorted_inter_chrom_bamFile_handle.write(read2)
            
            #read1_map_chr = bamFile_handle.getrname(read1.tid)
            #read2_map_chr = bamFile_handle.getrname(read2.tid)
            #read1_map_start = read1.pos
            #read1_len = read1.qlen
            #print max([ pileup.n for pileup in bamFile_handle.pileup(read1_map_chr,read1_map_start,read1_map_start+read1_len ,max_depth=20000 )])
            #print read1.qname,read1_map_chr,read2_map_chr,read1_map_start,read1_len
            continue
        
        elif read1.is_unmapped and read2.is_unmapped:
            unmapped_pairs +=1
            continue
        
        elif read1.is_unmapped or read2.is_unmapped:
            singleton_pairs += 1
            continue
        
        else:
            proper_mapped_pairs += 1
            fragment_insert_size = math.fabs(read1.tlen)
            proper_mapped_pairs_insert_sizes.append(math.log(fragment_insert_size+1,10)  )
            nameSorted_intra_chrom_bamFile_handle.write(read1)
            nameSorted_intra_chrom_bamFile_handle.write(read2)
            
            #debugging
            if fragment_insert_size == 0:
                debug_bamFile_handle.write(read1)
                debug_bamFile_handle.write(read2)
            
        
    #creating the insert size for intra-chromosomal mapped reads
    plt.hist(proper_mapped_pairs_insert_sizes,bins=50,label='intra-chromosomal-insert-size')
    plt.grid(True)
    plt.xlabel('log10(Insert size+1)')
    plt.ylabel('frequency')
    plt.legend()
    plt.savefig('hist_intra_chromosomal_insertSize.png',dpi=175)
    

    print '#pairs %d' % count
    print '#unmapped %d' % unmapped_pairs
    print '#singletons %d' % singleton_pairs
    print '#proper %d' % proper_mapped_pairs
    print '#chimeric %d' % chimeric_pairs
    print '#proper %d' % len(proper_mapped_pairs_insert_sizes)

"""

