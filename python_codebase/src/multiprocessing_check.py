#!/jgi/tools/bin/python

#basic forking example in python
#ref: http://www.ibm.com/developerworks/aix/library/au-multiprocessing/

import os
from os import environ
import multiprocessing
import time
import sys

def my_fork1():
    child_pid = os.fork()
    if child_pid == 0:
        print "fork1: Child Process: PID %s" % os.getpid()
    else:
        print "fork1: Parent process: PID %s" % os.getpid()
        



print "\n\n\n"



##copying the environment varaible

def my_fork2():
    environ['FOO']="baz"
    print "fork2: FOO environmental variable set to: %s" % environ['FOO']
    environ['FOO']="bar"
    print "fork2: FOO environmental variable changed to: %s" % environ['FOO']
    child_pid = os.fork()
    
    if child_pid == 0:
        print "fork2: Child Process: PID %s" % os.getpid()
        print "fork2: Child FOO env var == %s" % os.environ['FOO']
    else:
        print "fork2: Parent process: PID %s" % os.getpid()
        print "fork2: Parent FOO env var == %s" % os.environ['FOO']
        

#my_fork2()
#my_fork1()



def worker():
    """worker function"""
    name = multiprocessing.current_process().name
    print name, 'Starting'
    time.sleep(2)
    print name, 'Exiting'
    
def my_service():
    name = multiprocessing.current_process().name
    print name, 'Starting'
    [i for i in xrange(40000000)]
    print name, 'Exiting'


def daemon():
    name = multiprocessing.current_process().name
    print name, 'Starting'
    time.sleep(2)
    print name, 'Exiting'

def non_daemon():
    print 'Starting:', multiprocessing.current_process().name
    print 'Exiting :', multiprocessing.current_process().name


def exit_error():
    sys.exit(1)
    
def exit_ok():
    return

def return_value():
    return 1

def raises():
    raise RuntimeError('There was an error!')

def terminated():
    time.sleep(3)
    

if __name__ == "__main__":

    """
    service = multiprocessing.Process(name='my_service', target=my_service)
    worker_1 = multiprocessing.Process(name='worker 1', target=worker)
    worker_2 = multiprocessing.Process(target=worker) # use default name

    worker_1.start()
    worker_2.start()
    
    for i in range(10):
        service = multiprocessing.Process(name='my_service', target=my_service)
        service.start()
    
    d = multiprocessing.Process(name='daemon',target=daemon)
    d.daemon = True
    
    e = multiprocessing.Process(name='non-daemon',target=non_daemon)
    e.daemon = False
    
    d.start()
    time.sleep(1)
    e.start()
    
    d.join()
    e.join()
    """
    
    
    
    """Catching the job's exit code"""
    """
    jobs  = []
    
    for f in [exit_error, exit_ok, return_value,raises, terminated]:
        print 'Starting process for ', f.func_name
        j = multiprocessing.Process(target=f, name=f.func_name)
        jobs.append(j)
        j.start()
        
    jobs[-1].terminate()
    
    for j in jobs:
        j.join()
        print '%s.exitcode = %s' % (j.name,j.exitcode)
    
    """
    
    

