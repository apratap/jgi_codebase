#!/house/homedirs/a/apratap/playground/software/epd-7.2-2-rh5-x86_64/bin/python


import os
import sys
import pysam
import re
import subprocess
import multiprocessing
sys.path.append('/house/homedirs/a/apratap/dev/eclipse_workspace/python_scripts/lib');

import bamUtils
import rna_pet



def create_bed_file_from_clusters(in_file,out_file,min_uniq_clones_per_cluster):
    
    fh_in = open(in_file,'r')
    fh_out = open(out_file,'w') 
    
    for (line_num,line) in enumerate(fh_in):
        
        ##skip the header line
        if line_num == 0:
            continue
    
        line_list = line.strip().split('\t')
        transcript_name = line_list[0] + '_pt_' + str(line_num)
    
        #only print the clusters which have >     
        if int(line_list[6]) >= min_uniq_clones_per_cluster:
            bed_line = line_list[0]+'\t'+line_list[2]+'\t'+line_list[5]+'\t'+transcript_name+'\t'+line_list[6]+'\t'+line_list[1]+'\n'
            fh_out.write(bed_line)
        
        
    
    fh_out.close()
    fh_in.close()
    
    
    
    

#input file is duplicates removed 
bamFile = sys.argv[1]

##min cov
min_uniq_clones_per_cluster = 3

print "Total number of processors available %d " % multiprocessing.cpu_count()

(list_of_bam_files_split_by_chr, list_of_chromosomes) = bamUtils.split_bam_by_chromosomes(bamFile)


for bam,chr in zip(list_of_bam_files_split_by_chr,list_of_chromosomes):
    
    dir = os.path.dirname(bam)
    #spliting the bam file into sense and antisense bam ..
    
    if chr == "chromosome_4":
        
        print '[Main]: Creating sense & antisense bam for %s on chr %s ' % (bam, chr)
        
        
        sense_bamFile, antisense_bamFile = rna_pet.split_bam_to_sense_antisense(bam);
        
        #indexing the bams
        bamUtils.index_bam(sense_bamFile)
        bamUtils.index_bam(antisense_bamFile)
        
        """
        ##removing the low coverage reads to reduce the clustering complexity
        sense_bamFile_mincov_removed     = bamUtils.remove_low_coverage_reads(sense_bamFile, min_coverage=2)
        antisense_bamFile_mincov_removed  = bamUtils.remove_low_coverage_reads(antisense_bamFile, min_coverage=2)
        
        #indexing the bams
        bamUtils.index_bam(sense_bamFile_mincov_removed)
        bamUtils.index_bam(antisense_bamFile_mincov_removed)
        """
        
        chr_sense_range_file = rna_pet.generate_uniq_start_end_ranges_per_chromosome(sense_bamFile,chromosome=chr,strand='+')
        chr_rna_pet_cluster_sense = dir + '/' + chr +'_sense_rna_pet_clusters.txt'
        chr_rna_pet_cluster_sense_bed   = dir + '/' + chr +'_sense_rna_pet_clusters_min_clone_cov_'+str(min_uniq_clones_per_cluster)+'.bed'
        
        args = ['/house/homedirs/a/apratap/dev/eclipse_workspace/R_Scripts/rna_pet_clustering.R',chr_sense_range_file,chr,'+',chr_rna_pet_cluster_sense]
        #print args
#        return_code = subprocess.check_call(args) 
#        if return_code == 0:
#            print 'Completed RNA-PET clustering for %s' % (chr_sense_range_file) 
        
        chr_antisense_range_file = rna_pet.generate_uniq_start_end_ranges_per_chromosome(antisense_bamFile,chromosome=chr,strand='-')
        chr_rna_pet_cluster_antisense       = dir + '/' + chr +'_antisense_rna_pet_clusters.txt'
        chr_rna_pet_cluster_antisense_bed   = dir + '/' + chr +'_antisense_rna_pet_clusters_min_clone_cov_'+str(min_uniq_clones_per_cluster)+'.bed'
        
        args = ['/house/homedirs/a/apratap/dev/eclipse_workspace/R_Scripts/rna_pet_clustering.R',chr_antisense_range_file,chr,'-',chr_rna_pet_cluster_antisense]
        #print args
#        return_code = subprocess.check_call(args) 
#        if return_code == 0:
#            print 'Completed RNA-PET clustering for %s' % (chr_antisense_range_file)
        
        
        ##creating the bed file for each chr/strand at min cluster count 2
        create_bed_file_from_clusters(chr_rna_pet_cluster_sense,chr_rna_pet_cluster_sense_bed,min_uniq_clones_per_cluster)
        create_bed_file_from_clusters(chr_rna_pet_cluster_antisense,chr_rna_pet_cluster_antisense_bed,min_uniq_clones_per_cluster)
        
        
        
            
            
            
            
            
            