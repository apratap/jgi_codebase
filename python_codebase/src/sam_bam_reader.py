#!/jgi/tools/bin/python


import sys
import pysam

bamfile = pysam.Samfile('/house/homedirs/a/apratap/playground/GIS_RNA_SEQ/pet_RNA/combined_analysis/mapping_BWA/read_1_sorted.bam',"rb");


# for read in samfile.fetch():
#    print read,"\n";


for pileupcolumn in bamfile.pileup():
    print
    print 'cov at base %s = %s' % (pileupcolumn.pos, pileupcolumn.n)


