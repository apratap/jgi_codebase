#!/usr/bin/env python

import sys
import os
import argparse



##updating the library look up path
dir = os.path.dirname(__file__)
sys.path.append(os.path.join(dir,'../lib'));
import bamUtils





def main():
    
    #user command line args processing
    parser = argparse.ArgumentParser(description="create plots to evaluate read/mapping quality: reads shoudl be of equal length",add_help=True)
    parser.add_argument('--bam','-bam',dest='bam',default=None,metavar='',help='bam-file')
    parser.add_argument('-mrl','--max_read_len',dest='max_read_len',default=None,metavar='',help='max read length',type=int)
    parser.add_argument('-q','--qual_offset',dest='qual_offset',default=33,metavar='',help='33:Sanger(default) 64:older illumina',type=int)
    
    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit()
    
    
    
    #parse the args
    user_args = parser.parse_args()
    
    
    if user_args.bam is None or user_args.max_read_len is None:
        print 'ERROR: BAM or max_read_length not given\n'
        parser.print_help()
        sys.exit()
    
    
    
    
    bamSummary = bamUtils.mappingSummary(user_args.bam,
                                         max_readlen=user_args.max_read_len,
                                         quality_offset=user_args.qual_offset,
                                         minQual=30)
    df = bamSummary.eval_alignQ()
    bamSummary.plot_alignment_quality()
    
    
    
    
if __name__ == "__main__":
    main()