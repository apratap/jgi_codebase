#!/jgi/tools/bin/python


import sys
from Bio import SeqIO
from Bio.Seq import Seq
import MOODS


def linker_info():
    
    linker = Seq('CTGCTGTACCGTACATCCGCCTTGGCCGTACAGCAG')
    
    print 'Linker : %s' % (linker)
    print 'Length of linker is %d' % len(linker)
    return primer2pwm(linker)
    


def primer2pwm(primer):
    """
    Write a primer sequence as a position weight matrix.
    """
    # Create 4 lists of length equal to primer's length.
    matrix = [[0] * len(primer) for i in range(4)]
    
    
    # List of correspondance IUPAC.
    IUPAC = {
        "A" : ["A"], 
        "C" : ["C"], 
        "G" : ["G"],        
        "T" : ["T"],        
        "U" : ["U"],        
        "R" : ["G", "A"], 
        "Y" : ["T", "C"], 
        "K" : ["G", "T"], 
        "M" : ["A", "C"], 
        "S" : ["G", "C"], 
        "W" : ["A", "T"], 
        "B" : ["C", "G", "T"], 
        "D" : ["A", "G", "T"], 
        "H" : ["A", "C", "T"], 
        "V" : ["A", "C", "G"], 
        "N" : ["A", "C", "G", "T"]
    }
    # Position of nucleotides in the PWM.
    dico = {"A" : 0,  "C" : 1, "G" : 2, "T" : 3}
    # Read each IUPAC letter in the primer.
    for index, letter in enumerate(primer):
        for nuc in IUPAC.get(letter):
            i = dico.get(nuc)
            matrix[i][index] = 1
    return matrix


#GCTAGCTGCGCTGGGACTTGTCAACGTTTGTAAATTTTGAGAGAAGCTGCTGTACGGCCAAGGCGGATGTACGGTAAAGGAGGTGGGGGATTTCAAACGA
#                                              CTGCTGTACCGTACATCCGCCTTGGCCGTACAGCAG



def clean_reads(fastq_file_name):
    
    file_handle = open(fastq_file_name, 'rU')
    
    matrix = linker_info()
    count_reads = 0
    count_linker_found = 0
    count_linker_not_found = 0 
    
#    check_for_linker('GCTAGCTGCGCTGGGACTTGTCAACGTTTGTAAATTTTGAGAGAAGCTGCTGTACGGCCAAGGCGGATGTACGGTAAAGGAGGTGGGGGATTTCAAACGA',matrix)
    
    for record in SeqIO.parse(file_handle,'fastq-illumina'):
        count_reads +=1
#        print record.id
#        print record.seq
#        print len(record)
#        
        (results_fwd_strand,results_rev_strand) = check_for_linker(record.seq,matrix)
        
        if results_fwd_strand:
            count_linker_found +=1
#           print 'linker found on positive strand\n'
        elif results_rev_strand:
            count_linker_found +=1 
#            print 'linker found on negative strand\n'
        else:
            count_linker_not_found += 1
#            print 'linker not found'
        
        
        if (count_reads % 100 == 0 ):
            print 'procssed %d reads ' % (count_reads)
            
        


                                            




def check_for_linker(sequence,matrix):
    results_fwd_strand = MOODS.search(sequence,[matrix],0.005)
    results_rev_strand = MOODS.search(sequence,[MOODS.reverse_complement(matrix)],0.005)

    return(results_fwd_strand,results_rev_strand)
    








fastq_file_name=sys.argv[1]
clean_reads(fastq_file_name)

