#!/jgi/tools/bin/python

import os
import time
import sys
import multiprocessing


def read_file(fh,num,chunk_size):
    
    fh.seek( num*chunk_size )
    print 'Starting:', multiprocessing.current_process().name, 'at ',fh.tell()
    count = 0
    test_file='test_'+str(num)+'.fastq'
    print 'Process', multiprocessing.current_process().name, 'opening file', test_file
    fh1 = open(test_file,'w')
    while ( fh.tell() < (chunk_size*(num+1) ) ):
            count +=1
#            fh1.write(line)
            if count % 10000 == 0:
                print 'Process %s : counted %d reads' % (multiprocessing.current_process().name, count )
            fh.next()
            
#    fh1.close()
    print count
    return count
        
        
    


if __name__ == "__main__":
    
    file=sys.argv[1]
    splits = int(sys.argv[2])
    
    if not os.path.exists(file):
        print 'File %s doesn\'t exists' % file
        sys.exit()
    
    if not os.path.isfile(file):
        print '%s is not a file' %file
        sys.exit()
    
    
    file_size_bytes = os.path.getsize(file)
    print "The file size is %d" % file_size_bytes
    
    chunk_size = file_size_bytes / splits    
    fh = open(file,"r")

    
    jobs = []
    for i in range(splits):
        job = multiprocessing.Process(target=read_file, args=(fh,i,chunk_size) )
        jobs.append(job)
        job.start()
        #job.join()
    
    
    
    
    

    

