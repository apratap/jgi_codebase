#!/usr/bin/env python



import pysam
import os
import sys
import re
import pickle
import pandas

sys.path.append('/global/homes/a/apratap/dev/eclipse_workspace/python_scripts/lib/');
#print sys.path
import bamUtils
import utils



def create_data_array_from_reference_count(file_list):
    
    SUB = 'create_data_array_from_reference_count'
    
    data = []
    
    for file_name in file_list:
        with open(file_name, 'r') as fh:
            file_basename = os.path.basename(file_name)
            pattern='^.+?experiment\/(N_.+?)_([AB]).*$'
            matches = re.search(pattern,file_name)
            print '[%s]: Reading data from file file %s' % (SUB,file_basename),
            if matches:
#                print matches.group(1)
#                print matches.group(2)
#                print '----------------------'
                condition=matches.group(1)
                replicate=matches.group(2)
            else:
                print '[%s]:Warn: Could not find condition and replicate info'
                condition = 'NA'
                replicate = 'NA' 
                
            for (line_num,line) in enumerate(fh):
                
                #skipping the header
                if line_num == 0:
                    continue
                
                each_list = line.split('\t')
                spike_ref_seq = each_list[0].strip()
                spike_ref_seq_len = each_list[1].strip()
                read_count_sense_strand = each_list[2].strip()
                read_count_antisense_strand = each_list[3].strip()
                if spike_ref_seq == '*':
                    continue
                temp_tuple=(condition,replicate,spike_ref_seq,spike_ref_seq_len,read_count_sense_strand,read_count_antisense_strand)
                data.append(temp_tuple)
            print '...Done'
    print 'Read %d files' % (len(file_list))
    return data
    


pattern = '*_all_reads.bam'
bam_files_root_path = '/global/projectb/projectdirs/PI/Chlamy_Chia_Lin/Chlamy_Nitro_deprivation_Project/Chlamydomonas_reinhardtii_4A+_404151/experiment'


bam_files = utils.get_FilesList(bam_files_root_path,pattern=pattern) 
bamUtils.generate_read_count_per_chr_per_bam(bam_files)


##getting the all spike-in data
pattern='*.bam.refCounts.txt'
ref_count_files = utils.get_FilesList(bam_files_root_path,pattern=pattern)
data = create_data_array_from_reference_count(ref_count_files)
print 'Data array has, %d rows' % len(data)




##creating the pandas dataframe
df = pandas.DataFrame(data,columns=['condition','replicate','ref_name','ref_len','count_pos','count_neg'])



#saving the data as a pickled pandas dataframe
pickle.dump(df,open('data_ERCC_counts_per_sample.pickle', 'wb'))


path='/global/projectb/projectdirs/PI/Chlamy_Chia_Lin/Chlamy_Nitro_deprivation_Project/Chlamydomonas_reinhardtii_4A+_404151/ERCC_mapping/'

#loading the ERCC subgroups info
ercc_subgroups_file = path + '/ERCC_RNA_Seq_spike_in_groups.csv'
ercc_subgroups = pandas.read_csv(ercc_subgroups_file,sep='\t')



##loading the raw reads per sample
read_counts_file = path + '/cleaned_reads_per_sample.txt'
read_counts_per_sample = pandas.read_csv(read_counts_file,sep='\t')


##merging the data frames
fdf= pandas.merge(df,ercc_subgroups)
fdf = pandas.merge(fdf,read_counts_per_sample)

#saving the data as a pickled pandas dataframe
pickle.dump(fdf,open('data_ERCC_all_samples.pickle', 'wb'))



#write the data for ggplot2 graphs
out_data_file = path + '/final_data.tsv'
fdf.to_csv(out_data_file,sep='\t',index=False)