#!/usr/bin/env python


import os
import sys
import pysam
import re
import itertools
import subprocess




def index_bam(bamFile):
    """
    Taking a bamFile as input
    produce a index file for the bam (bamFile.fai)
    """
    SUB = 'index_bam'
    
    bamFile_bai = bamFile + '.bai'
    if os.path.exists(bamFile_bai):
        print '[%s]: Bam file %s already indexed ' % (SUB,bamFile)
    else:
        print '[%s]: indexing %s' % (SUB,bamFile)
        args = ['samtools','index',bamFile]
        return_code = subprocess.check_call(args) 
        if return_code == 0:
            print '[%s]: Created index for %s' % (SUB,os.path.basename(bamFile))




def name_sort_bam(bamFile):
    """
    Taking a bamFile as input
    produce a sorted bam file for the bam (bamFile)
    """
    SUB = 'name_sort_bam'
    
    bamFile_name_sorted_preifx = bamFile.replace('.bam','_name_sorted')
    bamFile_name_sorted        = bamFile_name_sorted_preifx + '.bam'
    if os.path.exists(bamFile_name_sorted):
        print '[%s]: Bam file %s already name sorted ' % (SUB,bamFile)
        return bamFile_name_sorted
    else:
        print '[%s]: Creating a name sorted bam' % (SUB)
        args = ['samtools','sort','-n',bamFile,bamFile_name_sorted_preifx]
        return_code = subprocess.check_call(args) 
        if return_code == 0:
            print '[%s]: Created name sorted bam for %s' % (SUB,os.path.basename(bamFile))
            return bamFile_name_sorted







def coordinate_sort_bam(bamFile):
    """
    Taking a bamFile as input
    produce a coordinate sorted bam file
    """
    SUB = 'coordinate_sort_bam'
    
    bamFile_coordinate_sorted_preifx = bamFile.replace('.bam','_cord_sorted')
    bamFile_coordinate_sorted        = bamFile_coordinate_sorted_preifx + '.bam'
    if os.path.exists(bamFile_coordinate_sorted):
        print '[%s]: Bam file %s already coordinate sorted ' % (SUB,bamFile)
        return bamFile_coordinate_sorted
    else:
        args = ['samtools','sort',bamFile,bamFile_coordinate_sorted_preifx]
        return_code = subprocess.check_call(args) 
        if return_code == 0:
            print '[%s]: Created coordinate sorted bam for %s' % (SUB,os.path.basename(bamFile))
            return bamFile_coordinate_sorted



def split_bam_to_sense_antisense(bamFile, force_run=False):
    
    '''given an input bam file only with mate pair direction reads
       and color tag YC:Z:green (5 prime) or YC:Z:orange( 3 prime)
       the function will split it into two bam files
       1. reads capturing cDNA ends on the sense strand
       2. reads capturing cDNA ends on the antisense strand
    '''
    SUB = 'split_bam_to_sense_antisense'
    print '[%s]:opening bam %s for reading ' % (SUB,bamFile)
    
    
    bamFile_bai = bamFile + '.bai'
    if not os.path.exists(bamFile_bai):
        index_bam(bamFile)
    
    
    input_bamFile_handle = pysam.Samfile(bamFile,'rb')
    base_dir = os.path.dirname(bamFile) or '.'
    file_prefix=os.path.basename(bamFile).replace('.bam','')
    
    sense_bamFile     = base_dir+'/'+file_prefix+'_sense.bam'
    antisense_bamFile = base_dir+'/'+file_prefix+'_antisense.bam';
    
    #if ( 1 and 1 ):
    if ( os.path.exists(sense_bamFile) and os.path.exists(antisense_bamFile) and ( force_run == False) ):
        print '[%s]: Expected output  %s  and  %s  already present .....no processing required' % (SUB,os.path.basename(sense_bamFile), os.path.basename(antisense_bamFile))
        return (sense_bamFile, antisense_bamFile)
    
    sense_bamFile_handle = pysam.Samfile(sense_bamFile,'wb',template=input_bamFile_handle)
    antisense_bamFile_handle = pysam.Samfile(antisense_bamFile,'wb',template=input_bamFile_handle)
    
    for read in input_bamFile_handle.fetch():
        color  = read.tags[-1][1]  #last entry in the list of tuples and then extract the second element of tuple
        
        if color not in ('orange','green'):
            print 'Warning : color %s is not orange/green' % color
            continue
        
        if read.is_reverse:  #read maps in the opposite direction
            if color == 'green': ##read is coming from sense strand
                #print 'Found %s color' % color
                sense_bamFile_handle.write(read)
            elif color == 'orange':
                #print 'Found %s color' % color
                antisense_bamFile_handle.write(read)
    
    #read maps in the same direction as reference 5 prime to 3 prime
        else:
            if color == 'orange':  ##read is coming from the sense strand
                sense_bamFile_handle.write(read)
        
            elif color == 'green': ##read is coming from antisense strand
                antisense_bamFile_handle.write(read)
    print '[%s]: Created sense bam %s' % (SUB, os.path.basename(sense_bamFile))
    print '[%s]: Create antisense bam %s' % (SUB,os.path.basename(antisense_bamFile))
    
    return(sense_bamFile,antisense_bamFile)


def debug(read1,read2,message):
    """
    print the debug information
    for a read pair
    """
    SUB='debug'
    
    print '[%s]: Message: %s' % (SUB,message)
    print '%s \n %s \n type: %s \n' % (read1, read2, type(read1))
   
     
def get_read_color(read_dir):
    
    colors = {
              5:'green',
              3:'orange'}
    
    return colors.get(int(read_dir),'black')



def is_proper_rna_pet_pair(read1,read2,read_1_dir,read_2_dir):
    
    #access the file handle
    global rna_pet_out_fh
    
    #each read should come from different direction
    if (read_1_dir == read_2_dir):
        print 'read_1_dir : %d, read_2_dir: %d ' % (read_1_dir, read_2_dir)
        return False
    

    if (read1.is_reverse and not read2.is_reverse) and (read1.pos <= read2.pos):
        
        #fixing the sam flag
        read1.flag =  1 + 2 + 16 + 64         # ( read is paired, properly aligned, rev_comp, first in pair);
        read2.flag =  1 + 2 + 32 + 128        # ( read is paired, properly aligned, mate_is_rev_comp, second in pair);
        
        #fixing the insert size
        insert_size = read2.pos + read2.qlen - read1.pos
        #print 'Insert size is %d' % insert_size
        read1.tlen = insert_size
        read2.tlen = -insert_size
        
        
    elif (not read1.is_reverse and read2.is_reverse) and (read2.pos <= read1.pos):
        read1.flag =  1 + 2 + 32 + 64         # ( read is paired, properly aligned, mate_is_rev_comp, first in pair);
        read2.flag =  1 + 2 + 16 + 128        # ( read is paired, properly aligned, rev_comp , second in pair);
        
        
        #fixing the insert size
        insert_size = read1.pos + read1.qlen - read2.pos
        #print 'Insert size is %d' % insert_size
        
        read1.tlen = -insert_size
        read2.tlen = insert_size
        
    else:
        #message="Can't determine the read pair as RNA-PET"
        #debug(read1,read2,message)
        return False
    
        
    ##add the direction of the read
    read1.tags = read1.tags + [("YC", get_read_color(read_1_dir) )]
    read2.tags = read2.tags + [("YC", get_read_color(read_2_dir) )]
    
    ### update the PNEXT tag
    read1.pnext = read2.pos
    read2.pnext = read1.pos
        
    ##add the RNEXT tag
    read1.rnext = read2.tid
    read2.rnext  = read1.tid

    rna_pet_out_fh.write(read1)
    rna_pet_out_fh.write(read2)
    return True



def is_chimeric_pair(read1,read2):
    
    ##both reads should map to same chromosome
    if read1.tid != read2.tid:
        global count_chimeric_pairs
        count_chimeric_pairs  += 1
        print "Possible Chimera"
        return True
    else:
        return False


def pair_read(read1,read2):
    
    (read_1_header,read_1_dir) =  get_header_N_direction(read1)
    (read_2_header,read_2_dir) =  get_header_N_direction(read2)
    
    #fix the headers remove the direction information
    read1.qname = read_1_header
    read2.qname = read_2_header
    
    
    ##both reads have to be mapped
    if read1.is_unmapped or read2.is_unmapped:
        global count_singleton_pairs
        count_singleton_pairs += 1
        return 
    
    #unmapped pair
    elif read1.is_unmapped and read2.is_unmapped:
        global count_unmapped_pairs
        count_unmapped_pairs += 1
        return 
    
    #header has to be same for both the reads in the pair
    elif read_1_header != read_2_header:
        print "Read header's are not name : most like input read1 and read2 are not name sorted"
        print '%s \t %s' % (read_1_header,read_2_header) 
    
    
    # if it is chimeric pair
    elif is_chimeric_pair(read1,read2):
        global count_chimeric_pairs
        count_chimeric_pairs += 1
    
    
    ##if the pair is RNA_PET type
    elif is_proper_rna_pet_pair(read1,read2,read_1_dir,read_2_dir):
        global count_proper_rna_pet_pairs 
        count_proper_rna_pet_pairs += 1
    
    
    ##undetermined cases
    else:
        global count_undetermined_pairs
        count_undetermined_pairs += 1
        
        
        
def get_header_N_direction(read):
    """
    Custom function for this script only
    Take a pysam's csamtools.AlignedRead ojbect and split
    the custom read header into
    pure read header and read direction
    and return them
    """ 
    
    ##pattern based on which regular expression search will be done
    pattern='^(.+?)_dir=(.+?)_prime\/?.*$'
    regex = re.compile(pattern)
    match = regex.search(read.qname)
    
    if match is None:
        return None
    else:
        read_header = match.group(1)
        read_dir    = int(match.group(2))
        return (read_header, read_dir)
        
 
 
###################
###################
#MAIN
################### 
 
 
 
##user input
read_1 = sys.argv[1]
read_2 = sys.argv[2]
#fai_file = sys.argv[3]
#original_read_length = 100


#counters
count_singleton_pairs       = 0
count_chimeric_pairs        = 0
count_unmapped_pairs        = 0
count_proper_rna_pet_pairs  = 0
count_undetermined_pairs    = 0

#name sort bam
read_1_name_sorted = name_sort_bam(read_1)
read_2_name_sorted = name_sort_bam(read_2)

read_1_fh = pysam.Samfile(read_1_name_sorted,'rb')
read_2_fh = pysam.Samfile(read_2_name_sorted,'rb')

#out file handles
rna_pet_out_file = 'rna_pet.bam'
rna_pet_out_fh = pysam.Samfile(rna_pet_out_file,'wb',template=read_1_fh)


for (num_pairs,read1,read2) in itertools.izip(itertools.count(1),read_1_fh,read_2_fh):
    #main function to check for suitable pair
    pair_read(read1,read2)
    if (num_pairs % 100000 == 0):
        print 'Processed %d pairs' % num_pairs



#close the file handle
rna_pet_out_fh.close()

#converting to sorted and index bam
rna_pet_sorted_bam = coordinate_sort_bam(rna_pet_out_file)
index_bam(rna_pet_sorted_bam)


##split the RNA_PET coordinate sorted bam into sense and antisense bam
(rna_pet_sense_bamFile,rna_pet_antisense_bamFile) = rna_pet.split_bam_to_sense_antisense(rna_pet_sorted_bam)


##index bams
index_bam(rna_pet_sense_bamFile)
index_bam(rna_pet_antisense_bamFile)

   
#summary stats
print '[Summary]:'
print '#pairs processed: %d ' % (num_pairs)
print '#RNA-PET pairs: %d' % (count_proper_rna_pet_pairs)
print '#Singleton pairs: %d' % (count_singleton_pairs)
print '#Chimeric pairs: %d' % (count_chimeric_pairs)
print '#Undetermined pairs: %d' % (count_undetermined_pairs)



