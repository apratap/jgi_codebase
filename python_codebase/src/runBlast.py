#!/jgi/tools/bin/python


import sys
from Bio.Blast import NCBIXML


blast_out_file = sys.argv[1]

print "Input file is %s"  % blast_out_file 


result_handle = open(blast_out_file)
blast_records = NCBIXML.parse(result_handle)

total_alignments = 0

for blast_record in blast_records:
    for count,description in enumerate(blast_record.descriptions):
        print 'sequence:', description.title
        print 'Num Alignments', description.num_alignments
        total_alignments += description.num_alignments
    print "***********END OF BLAST RECORD****************"

print "Total Alignments %d " % total_alignments
    
        