#!/usr/bin/env python


import os
import sys
import pysam
import re
import itertools

sys.path.append('/house/homedirs/a/apratap/dev/eclipse_workspace/python_scripts/lib');

import bamUtils
import rna_pet


def debug(read1,read2,message):
    """
    print the debug information
    for a read pair
    """
    SUB='debug'
    
    print '[%s]: Message: %s' % (SUB,message)
    print '%s \n %s \n type: %s \n' % (read1, read2, type(read1))
   
     
def get_read_color(read_dir):
    
    colors = {
              5:'green',
              3:'orange'}
    
    return colors.get(int(read_dir),'black')



def is_proper_rna_pet_pair(read1,read2,read_1_dir,read_2_dir):
    
    #access the file handle
    global rna_pet_out_fh
    
    #each read should come from different direction
    if (read_1_dir == read_2_dir):
        print 'read_1_dir : %d, read_2_dir: %d ' % (read_1_dir, read_2_dir)
        return False
    

    if (read1.is_reverse and not read2.is_reverse) and (read1.pos <= read2.pos):
        
        #fixing the sam flag
        read1.flag =  1 + 2 + 16 + 64         # ( read is paired, properly aligned, rev_comp, first in pair);
        read2.flag =  1 + 2 + 32 + 128        # ( read is paired, properly aligned, mate_is_rev_comp, second in pair);
        
        #fixing the insert size
        insert_size = read2.pos + read2.qlen - read1.pos
        #print 'Insert size is %d' % insert_size
        read1.tlen = insert_size
        read2.tlen = -insert_size
        
        
    elif (not read1.is_reverse and read2.is_reverse) and (read2.pos <= read1.pos):
        read1.flag =  1 + 2 + 32 + 64         # ( read is paired, properly aligned, mate_is_rev_comp, first in pair);
        read2.flag =  1 + 2 + 16 + 128        # ( read is paired, properly aligned, rev_comp , second in pair);
        
        
        #fixing the insert size
        insert_size = read1.pos + read1.qlen - read2.pos
        #print 'Insert size is %d' % insert_size
        
        read1.tlen = -insert_size
        read2.tlen = insert_size
        
    else:
        #message="Can't determine the read pair as RNA-PET"
        #debug(read1,read2,message)
        return False
    
        
    ##add the direction of the read
    read1.tags = read1.tags + [("YC", get_read_color(read_1_dir) )]
    read2.tags = read2.tags + [("YC", get_read_color(read_2_dir) )]
    
    ### update the PNEXT tag
    read1.pnext = read2.pos
    read2.pnext = read1.pos
        
    ##add the RNEXT tag
    read1.rnext = read2.tid
    read2.rnext  = read1.tid

    rna_pet_out_fh.write(read1)
    rna_pet_out_fh.write(read2)
    return True



def is_chimeric_pair(read1,read2):
    
    ##both reads should map to same chromosome
    if read1.tid != read2.tid:
        global count_chimeric_pairs
        count_chimeric_pairs  += 1
        print "Possible Chimera"
        return True
    else:
        return False


def pair_read(read1,read2):
    
    (read_1_header,read_1_dir) =  get_header_N_direction(read1)
    (read_2_header,read_2_dir) =  get_header_N_direction(read2)
    
    #fix the headers remove the direction information
    read1.qname = read_1_header
    read2.qname = read_2_header
    
    
    ##both reads have to be mapped
    if read1.is_unmapped or read2.is_unmapped:
        global count_singleton_pairs
        count_singleton_pairs += 1
        return 
    
    #unmapped pair
    elif read1.is_unmapped and read2.is_unmapped:
        global count_unmapped_pairs
        count_unmapped_pairs += 1
        return 
    
    #header has to be same for both the reads in the pair
    elif read_1_header != read_2_header:
        print "Read header's are not name : most like input read1 and read2 are not name sorted"
        print '%s \t %s' % (read_1_header,read_2_header) 
    
    
    # if it is chimeric pair
    elif is_chimeric_pair(read1,read2):
        global count_chimeric_pairs
        count_chimeric_pairs += 1
    
    
    ##if the pair is RNA_PET type
    elif is_proper_rna_pet_pair(read1,read2,read_1_dir,read_2_dir):
        global count_proper_rna_pet_pairs 
        count_proper_rna_pet_pairs += 1
    
    
    ##undetermined cases
    else:
        global count_undetermined_pairs
        count_undetermined_pairs += 1
        
        
        
def get_header_N_direction(read):
    """
    Custom function for this script only
    Take a pysam's csamtools.AlignedRead ojbect and split
    the custom read header into
    pure read header and read direction
    and return them
    """ 
    
    ##pattern based on which regular expression search will be done
    pattern='^(.+?)_dir=(.+?)_prime\/?.*$'
    regex = re.compile(pattern)
    match = regex.search(read.qname)
    
    if match is None:
        return None
    else:
        read_header = match.group(1)
        read_dir    = int(match.group(2))
        return (read_header, read_dir)
        
    

##user input
read_1 = sys.argv[1]
read_2 = sys.argv[2]
fai_file = sys.argv[3]
original_read_length = 100

#counters
count_singleton_pairs       = 0
count_chimeric_pairs        = 0
count_unmapped_pairs        = 0
count_proper_rna_pet_pairs  = 0
count_undetermined_pairs    = 0


#check if input is sam file
if read_1.endswith('.sam'):
    read_1_sam = read_1
    read_1 = bamUtils.sam_to_bam(read_1,fai_file)
    

if read_2.endswith('.sam'):
    read_2_sam = read_2
    read_2 = bamUtils.sam_to_bam(read_2,fai_file)


#name sort bam
read_1_name_sorted = bamUtils.name_sort_bam(read_1)
read_2_name_sorted = bamUtils.name_sort_bam(read_2)

#index name sorted bam : not possible to index name sorted bam
#bamUtils.index_bam(read_1_name_sorted)
#bamUtils.index_bam(read_2_name_sorted)


read_1_fh = pysam.Samfile(read_1_name_sorted,'rb')
read_2_fh = pysam.Samfile(read_2_name_sorted,'rb')


#out file handles
rna_pet_out_file = 'rna_pet.bam'
rna_pet_out_fh = pysam.Samfile(rna_pet_out_file,'wb',template=read_1_fh)


for (num_pairs,read1,read2) in itertools.izip(itertools.count(1),read_1_fh,read_2_fh):
    
    #main function to check for suitable pair
    pair_read(read1,read2)
    
    
    if (num_pairs % 100000 == 0):
        print 'Processed %d pairs' % num_pairs

#close the file handle
rna_pet_out_fh.close()


#converting to sorted and index bam
rna_pet_sorted_bam = bamUtils.coordinate_sort_bam(rna_pet_out_file)
bamUtils.index_bam(rna_pet_sorted_bam)


##split the RNA_PET coordinate sorted bam into sense and antisense bam
(rna_pet_sense_bamFile,rna_pet_antisense_bamFile) = rna_pet.split_bam_to_sense_antisense(rna_pet_sorted_bam)


##index bams
bamUtils.index_bam(rna_pet_sense_bamFile)
bamUtils.index_bam(rna_pet_antisense_bamFile)

#debug info
#print dir(read1)


#summary stats
print '[Summary]:'
print '#pairs processed: %d ' % (num_pairs)
print '#RNA-PET pairs: %d' % (count_proper_rna_pet_pairs)
print '#Singleton pairs: %d' % (count_singleton_pairs)
print '#Chimeric pairs: %d' % (count_chimeric_pairs)
print '#Undetermined pairs: %d' % (count_undetermined_pairs)



