#!/jgi/tools/bin/python


import sys
import pprint
sys.path.append('/house/homedirs/a/apratap/lib/python');
import argparse

    

def read_file(file_handle):
    """Read the first sam file through the given file 
    handle and create a dictionary
    """
    count_lines_read = 0

    for line in file_handle:
        
        line.strip()
        
        if ( line.startswith('@') ):
            continue
        
        count_lines_read +=1
        if ( count_lines_read % 1000000 == 0 ):
            print 'Read %s reads ' % count_lines_read
        
        temp = line.split("\t")
#        print 'header : %s \t flag : %s' % (temp[0],temp[1])
        
        read_header = temp[0]
        sam_flag    = int(temp[1])
        
        assert isinstance(sam_flag,int), 'must be int!'
        
        if( sam_flag & int('0x80',16) ): 
            if(read_header in read_header_count ):
                read_header_count[read_header]+=1
            else:
                read_header_count[read_header] = 1
        else:
            next
            
    return read_header_count


def count_common_reads():
    """Count the read headers that have count == 2 
    """
    count_overlap_reads = 0
    
    for (header,read_count) in read_header_count.items():
        if ( read_count == 2  ):
            count_overlap_reads += 1

    return count_overlap_reads




parser = argparse.ArgumentParser(description='Process some integers')

parser.add_argument('--sam_file_1', type=file, required=True,  help='Name of sam file 1')
parser.add_argument('--sam_file_2', type=file, required=True,  help='Name of sam file 2')

args=parser.parse_args()
#pp = pprint.PrettyPrinter(indent=4)
#pp.pprint(args)
   
sam_file_1 = args.sam_file_1
sam_file_2 = args.sam_file_2

#print 'sam _file_1 has the value %s\n' % sam_file_1
#print 'type of var %s\n' % type(sam_file_1)

read_header_count = {}

read_header_count = read_file(sam_file_1)
read_header_count = read_file(sam_file_2)

print 'Found %s common pairs between two sam files' % count_common_reads();

