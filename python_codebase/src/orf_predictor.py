#!/jgi/tools/bin/python

import sys
from Bio import SeqIO
from Bio.Seq import import Seq
from Bio.SeqUtils import GC
import pprint



def orf_predictor(sequence):
    
    
    min_protein_length = 100
    answer = []
    
    if( len(sequence) < min_protein_length*3):
        return (0,0,0,0,'NA')
    
    for strand,nuc in [ (+1, sequence), (-1, sequence.reverse_complement()) ]:
        seq_len = len(nuc)
        for frame in range(3):
            proc = nuc[frame:].translate()
            full_proc_len = len(proc)
            aa_start = 0
            aa_end  = 0
            
#            print 'Initial aa_start and aa_end %s, %s ' % (aa_start,aa_end)
            
            
            while aa_start < full_proc_len:
                aa_end = proc.find('*',aa_start)
#                print aa_start,aa_end,aa_end-aa_start
                
                if aa_end == -1:
                    aa_end = full_proc_len
#                    print 'aa_start and aa_end %s, %s ' % (aa_start,aa_end)
                if aa_end-aa_start >= min_protein_length:
                    if strand == 1:
                        start = frame + aa_start*3
                        end = min(seq_len, frame+aa_end*3+3 )
#                        print 'aa_start and aa_end %s, %s ' % (aa_start,aa_end)
                    else:
                        start = seq_len -(frame-aa_end*3-3)
                        end   = seq_len -(frame-aa_start*3)
#                        print 'aa_start and aa_end %s, %s ' % (aa_start,aa_end)
                    proc_length = aa_end-aa_start
#                    print (proc_length,start,end,strand,proc[aa_start:aa_end])
                    answer.append((proc_length,start,end,strand,frame,proc[aa_start:aa_end]))
               
                aa_start = aa_end +1
                answer.sort(reverse=True)
    return answer[0] if(answer) else (0,0,0,0,'NA')

        






fasta_file = sys.argv[1]


for seq_record in SeqIO.parse(fasta_file, "fasta"):
    
    sequence_5_to_3_dir = seq_record.seq if (seq_record.name.count('(+)')) else seq_record.seq.reverse_complement()
    actual_strand = '+' if (seq_record.name.count('(+)')) else '-'
    predicted_orf = orf_predictor(sequence_5_to_3_dir)
    
    output = seq_record.id + "\t" 
    output +='\t'.join(str(i) for i in predicted_orf)
    
        
#    print '-------------------------------------------'
#    print seq_record.id
#    print "Actual Sequence %s...%s" % (seq_record.seq[:30],seq_record.seq[-3:])
#    print "Actual Strand %s"  % (actual_strand)
#    print "Length "
#    print predicted_orf
#    print "\n"
    print output
    

#orf_predictor(sequence_5_to_3_dir,actual_strand)



                    