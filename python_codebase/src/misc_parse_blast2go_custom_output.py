#!/jgi/tools/misc_software/python/2.7/bin/python


import sys
import csv
sys.path.append('/house/homedirs/a/apratap/dev/eclipse_workspace/python_scripts/lib');





def create_EC_codeFile(fh_blast2go_custom_annotation_output,fh_DESeq_diff_exp_genes_output):

    gene_with_EC_codes = {}
    num_gene_with_EC_codes = 0
    num_down_reg_genes = 0
    num_up_ref_genes   = 0
    

    
    for blast2go_line in fh_blast2go_custom_annotation_output:
    # skip the header
      if 'Sequence Name' in blast2go_line:
          continue
      
    
    #if the annotation has the enzyme code EC  
      if blast2go_line[1]:
          num_gene_with_EC_codes += 1
          header = blast2go_line[0].split('_')
          gene_name=(header[0].split(':'))[1]
          EC_codes = blast2go_line[1]
          blast_hit_protein_uniprot_name = blast2go_line[2] 
          
#          print gene_name, EC_codes,blast_hit_protein_uniprot_name,"\n"
          
          gene_with_EC_codes[gene_name] = EC_codes
          


    fh_ec_codes_up_genes = open('ec_codes_up_regulated_genes.txt','w')
    fh_ec_codes_down_genes = open('ec_codes_down_regulated_genes.txt','w')


    for DEGSEq_gene_line in fh_DESeq_diff_exp_genes_output:
#        print DEGSEq_gene_line,"\n"
        gene_name = DEGSEq_gene_line[0]
    
        if 'id' in DEGSEq_gene_line:
            continue 
        
        log2_fold_change = float(DEGSEq_gene_line[5])
        
        ''' multiplying log2_fold_change by -1 to reverse the ratio
            in the current DESeq file the fold change is indicated with respect to treated
            we want to reflect it in terms of contol 
            so the ratio needs to be reveresed
        '''
        log2_fold_change =  -1.00 * log2_fold_change


        if log2_fold_change < 0.0:
            num_down_reg_genes += 1
            if gene_with_EC_codes.get(gene_name):
               for ec_code in gene_with_EC_codes.get(gene_name).split(';'):
                   fh_ec_codes_down_genes.write( '%s\n' % (ec_code.strip().replace(':','')))
        
        elif log2_fold_change > 0:
            num_up_ref_genes += 1
            if gene_with_EC_codes.get(gene_name):
                 for ec_code in gene_with_EC_codes.get(gene_name).split(';'):
                    fh_ec_codes_up_genes.write( '%s\n' % ( ec_code.strip().replace(':','') ) )

    print 'Total number of genes with EC numbers %d' % num_gene_with_EC_codes
    print 'Total numnber of up regulated genes with EC numbers %d'  % num_up_ref_genes
    print 'Total numnber of down regulated genes with EC numbers %d'  % num_down_reg_genes
    








def main():

    blast2go_custom_annotation_output = sys.argv[1]
    DESeq_diff_exp_genes_output = sys.argv[2]
    
    fh_blast2go_custom_annotation_output    = csv.reader( open(blast2go_custom_annotation_output,'r'), delimiter='\t')
    fh_DESeq_diff_exp_genes_output          = csv.reader( open(DESeq_diff_exp_genes_output, 'r'), delimiter='\t')
    
    create_EC_codeFile(fh_blast2go_custom_annotation_output,fh_DESeq_diff_exp_genes_output)
    
    
    





# run the code
main()


