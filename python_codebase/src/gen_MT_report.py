#!/house/homedirs/a/apratap/playground/software/epd-7.2-2-rh5-x86_64/bin/python

import sys
import os, fnmatch


#get the assembly base dir
asm_base_dir = sys.argv[1]


def get_FilesList(path,pattern=None):
    """
    traverse a dir recursively and find files with full paths
    that match the given pattern
    """
    
    SUB='get_FilesList'
    
    if pattern is None:
    	"[%s] : pattern not suppiled to the function call \n" % (SUB)
    
    found_files = []
    for root,dirs,files in os.walk(path):
        for basename in files:
            if fnmatch.fnmatch(basename,pattern):
                filename = str(os.path.join(root,basename))
                filename.strip()
                found_files.append(filename.strip())
    print '[%s]: Found %d files at %s' % ( SUB,len(found_files),path)
    return found_files
    
    






if __name__ == "__main__":	
	rRNA_mapping_result_files = get_FilesList(asm_base_dir,'*_qtrim_map_rRNA.bam.alignstats')
	