#!/jgi/tools/bin/python

import drmaa
import os
import time



def main():
    
    print drmaa.__version__
    
    """Create a drmaa session and exit"""
    s = drmaa.Session()
    s.initialize()
    
    """
    print 'A session was started successfully'
    response = s.contact
    
    print 'session contact returns' + response
    
    s.exit()
    
    print 'Exited from the session'
    
    s.initialize(response)
    print 'Session was restarted successfully'
    response = s.contact
    print 'session re-contact returns' + response
    s.exit()
    
    """
    
    """
    print 'Creating a job template'
    jt = s.createJobTemplate()
    
    jt.remoteCommand = os.getcwd() + '/sleeper.sh'
    jt.args = ['42','Abhi syas']
    jt.nativeSpecification = '-l normal.c'
    jt.joinFiles = True
    
    jobid = s.runJob(jt)
    print 'Your job has been submitted with id ' + jobid
    
    print 'Cleaning up'
    s.deleteJobTemplate(jt)
    s.exit()
    """
    
    """
    ##bulk jobs
    print 'Creating a job template'
    jt = s.createJobTemplate()
    
    jt.remoteCommand = os.getcwd() + '/sleeper.sh'
    jt.args = ['42','Abhi syas']
    jt.nativeSpecification = '-l normal.c'
    jt.joinFiles = True
    
    jobid = s.runBulkJobs(jt,1,30,2)
    print 'Your job has been submitted with id ' + str(jobid)
    
    print 'Cleaning up'
    s.deleteJobTemplate(jt)
    s.exit()
    """
    
    
    """
     Submit a job, and check its progress.
        Note, need file called sleeper.sh in home directory.
    """


    print 'Creating job template'
    jt = s.createJobTemplate()
    jt.remoteCommand = os.getcwd() + '/sleeper.sh'
    jt.args = ['42','Simon says:']
    jt.joinFiles=True
    
    jobid = s.runJob(jt)
    print 'Your job has been submitted with id ' + jobid

    # Who needs a case statement when you have dictionaries?
    decodestatus = {
        drmaa.JobState.UNDETERMINED: 'process status cannot be determined',
        drmaa.JobState.QUEUED_ACTIVE: 'job is queued and active',
        drmaa.JobState.SYSTEM_ON_HOLD: 'job is queued and in system hold',
        drmaa.JobState.USER_ON_HOLD: 'job is queued and in user hold',
        drmaa.JobState.USER_SYSTEM_ON_HOLD: 'job is queued and in user and system hold',
        drmaa.JobState.RUNNING: 'job is running',
        drmaa.JobState.SYSTEM_SUSPENDED: 'job is system suspended',
        drmaa.JobState.USER_SUSPENDED: 'job is user suspended',
        drmaa.JobState.DONE: 'job finished normally',
        drmaa.JobState.FAILED: 'job finished, but failed',
        }
    
    for ix in range(10):
        print 'Checking ' + str(ix) + ' of 10 times'
        print decodestatus[s.jobStatus(jobid)]
        time.sleep(5)
    
    print 'Cleaning up'
    s.deleteJobTemplate(jt)
    s.exit()
    
    
    """
    #!/usr/bin/env python

    print 'A DRMAA object was created'
    print 'Supported contact strings: ' + s.contact
    print 'Supported DRM systems: ' + str(s.drmsInfo)
    print 'Supported DRMAA implementations: ' + str(s.drmaaImplementation)
    print 'Version ' + str(s.version)

    print 'Exiting'
    s.exit()    
    """
if __name__ == '__main__':
    main()