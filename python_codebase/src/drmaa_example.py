#!/jgi/tools/bin/python

import drmaa
import os




NBULKS=1
JOB_CHUNK=3




def get_status():
    """Query the system """
    s = drmaa.Session()
    s.initialize()
    
    print 'A DRMAA object was created '
    print 'Support contact strings :' + s.contact
    print 'Supported DRM systems:' + str(s.drmsInfo)
    print 'Supported DRMAA implementations:' + str(s.drmaaImplementation)
    print 'Version '+ str(s.version)
    
    print 'Exiting'
    s.exit()
    
    

def main():
    s=drmaa.Session()
    s.initialize()
    print 'A session was started successfully'
    response = s.contact
    print 'session contact returns:' + response
    
    
    print 'Creating a job template'
    jt = s.createJobTemplate()
    jt.remoteCommand = os.getcwd() + '/sleeper.sh'
    print 'Command is ' + jt.remoteCommand
    jt.args = ['42','Simon says:']
    jt.joinFiles=True
    
    jobid = s.runJob(jt)
    print 'Your job has been submitted with id' + jobid
    
    
    retval = s.wait(jobid,s.TIMEOUT_WAIT_FOREVER)
    print 'Job: ' + str(retval.jobId) + '  Finished with status '+ str(retval.hasExited)

    
    print 'Cleaning up'
    s.deleteJobTemplate(jt)
    s.exit()
    
    
if __name__=='__main__':
    get_status()
    main()
    


