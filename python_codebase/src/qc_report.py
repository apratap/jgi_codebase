#!/usr/bin/env python

import sys
import os
import argparse
import pprint
import matplotlib
import re
import collections
matplotlib.use('Agg')
import pandas
import numpy as np
import matplotlib.pyplot as plt


##updating the library look up path
dir = os.path.dirname(__file__)
sys.path.append(os.path.join(dir,'../lib'));
import db_access
import utils
import jgi_qaqc_plots





def main():
    
    #user command line args processing
    parser = argparse.ArgumentParser(description="TODO",add_help=True)
    parser.add_argument('-l','--libs','-libs',dest='input_libs',default=None,metavar='',help='A comma/space separated list of libs ##Note: for space separated libs quote the input string')
    parser.add_argument('-f','--filter',dest='filter',default=False,action='store_true',help='Filter based on QC metrics <required for any filtering to take place>')
    parser.add_argument('-q20','--min_q20',dest='min_q20',default=100,metavar='',help='filter based on Q20',type=int)
    parser.add_argument('-tla',dest='tla',default=None,metavar='',help='force set the TLA for all the libs',type=float)
    parser.add_argument('-mc','--max_contam',dest='max_contam_percent',default=None,metavar='',help='filter based on total contamination percent',type=float)
    parser.add_argument('-rc','--min_readCounts',dest='min_readCounts',default=None,metavar='',help='filter based on readCounts(in millions)',type=int)
    parser.add_argument('-yd', '--yield',dest='min_yield',default=None,metavar='',help='filter based on yield',type=float)
    parser.add_argument('-gmp', '--genome_map_percent',dest='genome_map_percent',default=None,metavar='',help='filter based on percent reads map/genome',type=float)
    parser.add_argument('-tmp', '--transcriptome_map_percent',dest='transcriptome_map_percent',default=None,metavar='',help='filter based on percent reads map/transcriptome',type=float)
    parser.add_argument('-tmr', '--transcriptome_map_reads',dest='transcriptome_map_reads',default=None,metavar='',help='filter based on #reads map/transcriptome(in millions)',type=int)
    parser.add_argument('-mppp', '--min_pool_pass_percentage',dest='min_ppp',default=100,metavar='',help='percent of libs passed/pool for it to be considered DONE',type=float)
    parser.add_argument('-cla',dest='cla',default=None,metavar='',help='set the column name to used as CLA for all the libs \n Options: total_yield (or) transcriptome_mapped_reads or any integer/float',type=str)
    parser.add_argument('-ec',dest='comments',default='',metavar='',help='Additional comments for all libs',type=str)
    
    
    user_args = parser.parse_args()
    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit()
    
    logfile = 'qc_report.log'
    logger = utils._get_logger(logfile)
    
    library_names = user_args.input_libs
    lib_tuple = tuple(re.split(r'[,;\s]+',library_names))
    
    
    #log the user arguments
    [ logger.info('%s=%s' %(arg_name,value)) for arg_name,value in user_args._get_kwargs()]
    
    #get the lib summary as a pandas df
    raw_lib_summary_pandas_df = db_access.get_lib_query_results(library_names=(lib_tuple))
    
    
    #clean and process the dataframe
    libs_summary_pandas_df = db_access.process_N_clean_pandas_DF(raw_lib_summary_pandas_df)
    
    
    #libs_summary_pandas_df.to_csv('lib.info')
    
    #get the additional ITS info
    ITS_lib_info_df = db_access.get_ITS_info_for_libs(lib_tuple)
    
    
    #merge the additional info into main data frame
    final_df = pandas.merge(libs_summary_pandas_df.reset_index(),ITS_lib_info_df,how='outer',left_on='fastq',right_on='SEQ_UNIT_NAME')
    
   
    #filter the df based on
    if user_args.filter:
        (libs_df_QCd,filters_metadata) = db_access.filter_lib_summary_pandas_df(final_df,
                                                                                min_Q20=user_args.min_q20,
                                                                                min_readCount=user_args.min_readCounts,
                                                                                min_yield=user_args.min_yield,
                                                                                min_genome_map_percent=user_args.genome_map_percent,
                                                                                min_transcriptome_map_percent=user_args.transcriptome_map_percent,
                                                                                min_transcriptome_mapped_reads=user_args.transcriptome_map_reads,
                                                                                max_contam_percent = user_args.max_contam_percent,
                                                                                logger=logger)
    
    # no filtering applied, all libs are passed
    else:
        libs_df_QCd = final_df
        libs_df_QCd['STATUS'] = 'NO_QC_DONE'  #as no filtering logic has been applied
        libs_df_QCd['QC_comments'] = 'NA'
    
    #remove the UNKNOWN Library
    libs_df_QCd = libs_df_QCd[ libs_df_QCd.lib_name != 'UNKNOWN']
    
    #create a group by object based on parent library name
    #taking run_id and lane  is imp 
    #if a pool is run twice then we need to separte them out and not count all the re-run libs in one pool
    grouped = libs_df_QCd.groupby(['parent_library_name','physical_run_id','run_section'])
    
   
    def __temp_group_func(group,mppp):
        subset_group =group[group.lib_name != 'UNKNOWN']; 
        pass_percent = float( np.sum(subset_group.STATUS == "PASS") * 100/( np.sum(subset_group.STATUS == "PASS") + np.sum(subset_group.STATUS == "FAIL") ) )
                                                          
        if pass_percent >= mppp:
            status='Done'
        else:
            status = 'Manual QC needed'
         
        return pandas.Series( {'#Pass'  : np.sum(subset_group.STATUS == "PASS"), 
                               '#Fail'  : np.sum(subset_group.STATUS == "FAIL"),
                               '#Total' : np.sum(subset_group.STATUS == "PASS") + np.sum(subset_group.STATUS == "FAIL") ,
                               'pass%'  : pass_percent,
                               'Status' : status
                             })

    if user_args.filter:
        QC_summary_df = grouped.apply(__temp_group_func,user_args.min_ppp)
        logger.info('#####################')   
        logger.info('QC Summary \n %s' % (QC_summary_df.sort(['Status'])))
        logger.info('#####################')
        
        if user_args.min_ppp:
            #rather tricky and convoluted logic
            #all pools which have > min pass percentage libs should be passed as a whole
            #however the individual libs which have lower than accepted quality values
            #should be flagged as FORCE PASS
            
            def form_QC_comment_line(x,pass_percent,mppp=None):
                q20 = float(min(x.read1_q20,x.read2_q20))
                QC_comment_line = repr('%s \n Q20:%0.0f \n GC: %0.2f+/-%0.2f%% \n Lib Complexity: %0.2f %% random 20mers were unique after measuring %0.0f reads \n Total Contamination: %0.2f %% \n Illumina Artifacts: %0.2f \n DNA Spike in: %0.2f \n RNA Spike in: %0.2f  \n rRNA: %0.2f \n JGI Contaminants: %0.2f \n QC Filters Used: %s' 
                                       % (user_args.comments,q20,x.read_GC_mean,x.read_GC_SD,x.randmer_lib_complexity, x.read_count,x.total_contam,
                                          x.IL_artifacts,x.DNA_spike_in,x.RNA_spike_in,x.rRNA,x.JGI_contam,filters_metadata))
                
                #force passing the libs for which overall pool pass% >> min pass percetage per pool
                if pass_percent >= mppp:
                    if x.STATUS == 'FAIL':
                        x.STATUS = 'FORCE_PASS'
                        additional_comments = repr('Note: While this library did not pass QC, the project is considered done as majority of the libs in the pool passed \n')
                        QC_comment_line = '%s %s' % (additional_comments,QC_comment_line)
                else:
                    x.STATUS = 'Manual QC Needed'
                QC_comment_line=QC_comment_line.replace("'", "")
                x['QC_comments']= QC_comment_line
                return x
                            
            def apply_min_pass_percentage_per_pool(group,min_pass_percentage):
                pass_percent = float(np.sum(group.STATUS == "PASS") * 100/( np.sum(group.STATUS == "PASS") + np.sum(group.STATUS == "FAIL") ) )
                grouped = group.groupby('lib_name')
                #apply the QC comment
                group = grouped.apply(form_QC_comment_line,pass_percent,mppp=min_pass_percentage)
                return group
            
            ##apply the minimum number of pass libs per pool logic to each pool
            libs_df_QCd=grouped.apply(apply_min_pass_percentage_per_pool,user_args.min_ppp)
    
    
   
    qc_result_file = 'libs_QC_metrics.txt'
    batch_qc_submission_file = 'batch_qc_submit.txt'
    #remove null libs
    libs_df_QCd = libs_df_QCd[pandas.notnull(libs_df_QCd.lib_name)]
    
    #create another column for yield in GB
    libs_df_QCd['total_yield_GB'] = libs_df_QCd['total_yield']/ float(10**9)
    
    #updating Target Logical Amount if requested by user
    if user_args.tla:
        libs_df_QCd['TARGET_LOGICAL_AMOUNT'] = user_args.tla

    
    #creating the COMPLETED logical amount column
    if user_args.cla:
        if user_args.cla == "total_yield":
            #convert the total yield to Gb from default bp just for neat output
            libs_df_QCd['COMPLETED_LOGICAL_AMOUNT'] = libs_df_QCd[user_args.cla]/ float(10**9)
        elif user_args.cla == "transcriptome_mapped_reads":
            #convert the total number of reads to millions
            libs_df_QCd['COMPLETED_LOGICAL_AMOUNT'] = libs_df_QCd[user_args.cla]/ float(10**6)
        else:
            libs_df_QCd['COMPLETED_LOGICAL_AMOUNT'] = user_args.cla
    else:
        logger.warn('Column to use for TLA not specified..using NA')
        libs_df_QCd['COMPLETED_LOGICAL_AMOUNT'] = 'NA'
    
    
    qc_report_columns_to_write = ['parent_library_name','lib_name','SAMPLE_ID','read1_length','read2_length','read1_q20','read2_q20','read_count',
                                  'total_yield_GB', 'randmer_lib_complexity','startmer_lib_complexity','transcriptome_map_percent', 'genome_map_percent',
                                  'DNA_spike_in', 'RNA_spike_in', 'IL_artifacts', 'JGI_contam', 'rRNA', 'chloroplast', 'mito', 'ecoli','total_contam',   
                                  'transcriptome_mapped_reads', 'genome_map_reads', 'read_GC_mean', 
                                  'SCIENTIFIC_PROGRAM', 'fastq','STATUS']
    
    
    libs_df_QCd[qc_report_columns_to_write].to_csv(qc_result_file,sep="\t",na_rep='NA',header=True,float_format='%0.2f',index=False)
    logger.info('Results: QC metrics stored in file %s ' % (qc_result_file))
    
   
    
    #generate the file for    
    fh=open(batch_qc_submission_file,'w')
    fh.write('#File used to submit bulk QCs to database\n')
    fh.write('#Data must be in the following format \n')
    fh.write('# sequnit(*.fastq.gz format)\tSTATUS\tTLA\tCLA\tANALYST_QC_COMMENTS\n\n')
    fh.write('#Analyst Name:\n')
    fh.write('Analyst=%s\n\n\n' % utils.get_full_username())
    columns_for_batch_qc = ['fastq','STATUS','TARGET_LOGICAL_AMOUNT','COMPLETED_LOGICAL_AMOUNT','QC_comments']
    libs_df_QCd[columns_for_batch_qc].to_csv(fh,sep="\t",na_rep='NA',float_format='%0.2f',index=False)
    logger.info('Results: For Batch QC data stored in %s ' % (batch_qc_submission_file))
    fh.close()
    
    
    #generate the qc plots
    #plot_file = 'qc_summary_plots.png'
    #jgi_qaqc_plots.get_libs_qaqc_plots(libs_summary_pandas_df=libs_df_QCd,plot_file=plot_file)
    
    

if __name__ == "__main__":
    main()

